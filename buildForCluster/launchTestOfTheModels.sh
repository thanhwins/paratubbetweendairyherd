#!/bin/bash

bin/PrtbBtwnDrHrd_SingleThread_testInference_sigma_pSlopeGlobal_betaDist -i ../ -o ../Results/ --trajSeqNumber 1 --p-propInf 0.8 --p-prevMu 1.50 --p-prevSigma 10.0 --p-probPI 0.10 --p-betaG 3.1623e-07 --p-diagTestSe 0.30 -r 1 -f 1 -j 1