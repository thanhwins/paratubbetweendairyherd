## ParatubBetweenDairyHerd - MultiThreaded

### First: for BIOEPAR users only, select the branch "improved-for-user"
### Linh: stay on the master branch (it contains the last updates of the code)

### Requirements:
*  Parameters:
    *  at the moment, multiple files used to define the different class of parameters -> currently refactoring this in order to use only one file.
*  Data:
    *  set of initial within-herd prevalence distribution
    *  initial headcount (per herd)
    *  birth events (weekly, per herd)
    *  demography parameters (annually, per herd)
    *  animal movements (weekly)


### Dependencies:
*  STL (c++11)
*  GSL (2.3 or earlier, only headers)
*  BOOST (1.60.0 or earlier, only headers):
    *  boost/accumulators
    *  boost/array.hpp
*  TCLAP (1.2.1, only headers)


### How to compile:
*  go to `build`
*  update the makefile in order to check and set the right path for the libraries
*  `make clean`
*  `make` or `make -j10` (to compile in parallel, -jX, with X the number of threads to use)
*  the executable binary is located in `build/bin`


### How to launch:
To get help:
`./PrtbBtwnDrHrd_MultiThreaded -h` 

To compute 10 rep. with default parameters:
`./PrtbBtwnDrHrd_MultiThreaded -i ../../../  -o ../../../Results/ -r 10` 

See `launchTestOfTheModels.sh` for an other example.


### Time and memory consumption:
For 12857 herds on 9 years (2005-2013) ~ 125 seconds (1 rep.) & 3.1 Go ram

