//
//  ABCsmcParticle.h
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunée on 27/05/2015.
//  Copyright (c) 2015 Gaël Beaunée. All rights reserved.
//

#ifndef __ParatubBetweenDairyHerd__ABCsmcParticle__
#define __ParatubBetweenDairyHerd__ABCsmcParticle__

#include <stdio.h>
#include <map>
#include <string>

#include "ABCsmcParticleAttributes.h"

class ABCsmcParticle {
    
public:
    // Constructor =========================================================
    ABCsmcParticle();
    
    // Variables ===========================================================
    std::map<std::string, ABCsmcParticleAttributes> parameters;
    
    double distance;
    
    // Member functions ====================================================
    
};

#endif /* defined(__ParatubBetweenDairyHerd__ABCsmcParticle__) */
