//
//  Parameters.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 24/06/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_Parameters_h
#define ParatubBetweenDairyHerd_Parameters_h

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <unordered_map>
#include <cmath>
#include <algorithm>
#include <gsl/gsl_rng.h>
#include <ctime>
#include <chrono>
#include <string>
#include <stdexcept>
#include <random>
#include <tclap/CmdLine.h>

#include "UtilitiesFunctions.h"
#include "HerdCharacteristics.h"
#include "ObservedSampleInformations.hpp"
#include "Chronometer.h"
#include "VectorPlus.h"

class Parameters {
private:
    static Parameters *instance;
    
    Parameters(int argc, const char * argv[]);
    
public:
    // Constructor =========================================================
    static Parameters& get_instance(int argc, const char * argv[]);
    
    static void kill();
    
    // Variables ===========================================================
    static std::string folderPath;
    static std::string ABCfolderPath;
    static std::string ResultsFolderPath;
    
    static int nbThreads;
    // Seed and random number generator
    static gsl_rng * randomGenerator;
    static long long Seed;
    
    // Parameters ==========================================================
    // General information about simulations -------------------------------
    static int nbRuns;
    static int firstRunID;
    static int nbRunsWithNan;
    
    static double nbHerds;
    static double propHerdsInfected;
    static int nbHerdsInfected;
    static int nbInfectedAnimalsIntroduced;
    
    static int nbYears; //(doit être un multiple du nombre d'années de données de mouvements utilisé)
    static const int timestepInOneYear = 52;
    static int simutime;// = nbYears*timestepInOneYear+1;
//    static int weeknumber;
    
//    static std::vector<std::string> vListOfHerdsId;

    
    // Parameters related to age -------------------------------------------
    //
    static const int ageWeaning = 10;
    static const int ageCalf = 52;
    static const int ageYheifer = 91;
    static const int ageTwoYear = 102;
    static const int ageFirstCalving = 130;
    static const int ageAdults = 130;
    static const int ageGrazing = 26;
    static const int ageSign = ageYheifer + 1;
    //
    static const int nbStagesInAdultGroup = 5;
    static const int nbStage = ageFirstCalving + nbStagesInAdultGroup;
    
    
    
    // Temporal parametres -------------------------------------------------
    // Duration phase
    static const int durSusceptible = 52;
    static const int durShedding = 25;
    static const int durL = 52;
    static const int durIs = 2*52;
    // Calving-to-calving interval
    constexpr static double dLacta = 56.3;
    constexpr static double dLactaI = dLacta;
    // Grazing
    static const int startGrazing = 14;
    static const int endGrazing = 46;
    // Aging
    constexpr static double durOneYear = 56.3; // 52.0;
    
    
    
    // Herd size and its composition ---------------------------------------
    // Herd size
    constexpr static double homogeneousHerdSize = 0;
    constexpr static double popCorrectionRateA = 2.386;
    constexpr static double popCorrectionRateB = 2.888;
    // Proportion of super shedder among subclinical and clinical animals. (same settings for the Is and Ic, except for exrections)
//    constexpr static double propSS = 0.; // Can take the following values: 0.001, 0.005, 0.01, 0.02, 0.05, 0.1, 0.2
    //
    constexpr static double adultsTogether = 1.;
    
//    constexpr static double sexRatioFemale = 0.5;
    
    // Output parameters ---------------------------------------------------
    // Sales
    
    // Death-rate
    constexpr static double deathRateIs = 0.;
    constexpr static double deathRateIc = 0.;
    // Culling-rate
    constexpr static double cullingRateIs = 0.;
    static double cullingRateIc;// = 1. - std::exp( - (1./26.) );
    // Bacteria death-rate
    static double deathRateBactInside;// = 1. - std::exp( - (0.4) );
    static double deathRateBactOutside;// = 1. - std::exp( - (1./14.) );
    // Cleaning
    static double cleaningCP;// = 1. - std::exp( - (1./6.) ); // cleaning of collective pens
    
    
    
    // Transmission parameters ---------------------------------------------
    // Transmission routes
    static const bool colostrumTR = true;
    static const bool milkTR = true;
    static const bool localTR = true;
    // Calves susceptibility
    constexpr static const double susceptibilityCoeff = 0.1; // Susceptibility follows an exponential decrease : exp( -h*(age-1) )
    // Map infectious dose
    static double infectiousDose;
    // Infection rate
    static double infGlobalEnv;
    static double infGrazing;
    static double infLocalEnv;
    static double infIngestion;
    static double probaToPurchaseInfectedAnimalOutsideTheMetapopulation;
    static double probaToPurchaseInfectedAnimalOutsideTheMetapopulation_initialValue;
    static double probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope_global;
    static double probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope;
    static double typeOfTheRiskToPurchaseInfectedAnimalOutsideTheMetapopulation;
    // Vertical transmission (in utero or immediately after birth by contact between dam and daughter)
    constexpr static double infinuteroT = 0.149;
    constexpr static double infinuteroL = 0.149;
    constexpr static double infinuteroIs = 0.149;
    constexpr static double infinuteroIc = 0.65;
    
    
    
    // Quantity of bacteria shed in the different environments -------------
    // Quantity of faeces produced
    constexpr static double qtyFaecesCnw = 0.5*7.; // Unweaned calf
    constexpr static double qtyFaecesCw = 5.5*7.; // Weaned calf
    constexpr static double qtyFaecesHf = 10*7.; // Young heifer
    constexpr static double qtyFaecesAd = 30*7.; // Cow
    // Quantity of milk and colostrum drunk and produced
    constexpr static double qtyMilkDrunk = 7.*7.;
    constexpr static double qtyColoDrunk = 5.*3.;
    constexpr static double qtyMilkProd = 25.*7.;
    constexpr static double qtyColoProd = 25.*7.;
    constexpr static double propLactatingCows = 0.85;
    constexpr static double qtyMilkProdL = qtyMilkProd * (1.-0.08);
    constexpr static double qtyMilkProdIs = qtyMilkProd * (1.-0.11);
    constexpr static double qtyMilkProdIc = qtyMilkProd * (1.-0.25);
    // Map shedding in faeces
    // - for calves
    constexpr static double alphaFaecesT = 8.8;
    constexpr static double betaFaecesT = 19.;
    // - for Adults
    static double alphaFaecesIs;
    static double betaFaecesIs;
    static double alphaFaecesIc;
    static double betaFaecesIc;
    static double alphaFaecesIss;
    static double betaFaecesIss;
    static double alphaFaecesIcs;
    static double betaFaecesIcs;
    // Map shedding in milk
    // - direct shedding
    constexpr static double alphaMilkDirIs = 8.;
    constexpr static double betaMilkDirIs = 8.;
    constexpr static double alphaMilkDirIc = 8.;
    constexpr static double betaMilkDirIc = 8.;
    // - indirect shedding (faecal contamination)
    constexpr static double alphaMilkIndirIs = 1.;
    constexpr static double betaMilkIndirIs = 25.;
    constexpr static double alphaMilkIndirIc = 50.;
    constexpr static double betaMilkIndirIc = 200.;
    // Map shedding in colostrum
    // - direct shedding
    constexpr static double alphaColoDirIs = alphaMilkDirIs;
    constexpr static double betaColoDirIs = betaMilkDirIs;
    constexpr static double alphaColoDirIc = alphaMilkDirIc;
    constexpr static double betaColoDirIc = betaMilkDirIc;
    // - indirect shedding(faecal contamination)
    constexpr static double alphaColoIndirIs = alphaMilkIndirIs;
    constexpr static double betaColoIndirIs = betaMilkIndirIs;
    constexpr static double alphaColoIndirIc = alphaMilkIndirIc;
    constexpr static double betaColoIndirIc = betaMilkIndirIc;
    // Map shedding in milk and colostrum for Is and Ic
    constexpr static double propExcreMilkIs = 0.40;
    constexpr static double propExcreMilkIc = 0.90;
    constexpr static double propExcreColoIs = 0.40;
    constexpr static double propExcreColoIc = 0.90;
    

    // Parameters related initial states -----------------------------------
    static std::map<std::string, HerdCharacteristics> mFarmsCharacteristics;
    static std::vector<std::string> vFarmID;

    static std::map<std::string, std::vector<short int>> mHerdHeadcounts;
    static std::map<std::string, std::vector<double>> mHerdCullingRates;
    static std::map<std::string, std::vector<double>> mHerdBirthsRatio;
    static std::map<std::string, std::vector<short int>> mHerdBirthEvents;
    
    static bool SelectingInfectedHerdInAPreferentialWay;
    static int TypeOfPreferentialWay;
    static std::vector<double> vHerdDataForSelectingTheInfectedHerdsUniform;
    static std::vector<double> vHerdDataForSelectingTheInfectedHerdsPreferentialOutDegree;
    static std::vector<double> vHerdDataForSelectingTheInfectedHerdsPreferentialNumberOfOutMovements;

    static std::vector<std::vector<std::vector<std::vector<double>>>> vvvProbaInitInfection;
    
    static bool initializeHerdsWithEndemicState;
    static int initializeSimutime;
    static int endemicPeriodMin;
    static int endemicPeriodMax;
    
    static double muDistributionPrevIntra;
    static double sigmaDistributionPrevIntra;

    static double alphaBetaDistributionPrevIntra;
    static double betaBetaDistributionPrevIntra;

    static std::string typeOfWithinHerdPrevalenceDistribution;
    
    static int maxPrevLevel;
    static std::map<int, std::vector<std::vector<std::vector<double>>>> vvvPrevalenceLevelDistribution;
    static std::map<int, std::vector<std::vector<double>>> vvPrevalenceLevelAverageDistribution;
    
    
    // Test at purchase/sale -----------------------------------------------
    static int startTest;
    static double testSp;
    static double testSeT;
    static double testSeL;
    static double testSeIs;
    static int typeOfTestStrategy;
    static const int strategyHerdRandomlyChosenMovIn = 0;
    static const int strategyHerdRandomlyChosenMovOut = 1;
    static const int strategyHerdRandomlyChosenMovInOut = 2;
    static const int strategyHerdWithIc = 3;
    static const int strategyHerdWithMoreThanTwoIc = 4;
    static const int strategyBasedOnNumberOfIncomingMovements = 5;
    static double minimumOfIncomingMovements;
    static double numberOfHerdsAboveTheMinimumOfIncomingMovements;
    static const int strategyBasedOnNumberOfOutgoingMovements = 6;
    static double minimumOfOutgoingMovements;
    static double numberOfHerdsAboveTheMinimumOfOutgoingMovements;
    static double proportionOfFarmsWithTestAtPurchase;
    
    
    // biosecurity measures ------------------------------------------------
    static bool withinHerdBiosecurity;
    
    static double proportionOfFarmsWithCullingImprovement;
    static double cullingRateIcIfCullingImprovement;
    
    static double proportionOfFarmsWithHygieneImprovement;
    static double deathRateBactInsideIfHygieneImprovement;
    
    static double proportionOfFarmsWithCalfManagementImprovement;
    static double decreaseOfAdultToCalfContact;
    static double infGlobalEnvIfCalfManagementImprovement;
    
    static double proportionOfFarmsWithTestAndCull;
    static const int typeOfTestAndCullStrategy = 1;
    static const int testAndCullStrategyEachYearAfterOnsetOfIc = 1;
    static const int testAndCullStrategyIfOnsetOfNewIc = 2;
    static double testAndCullSp;
    static double testAndCullSeT;
    static double testAndCullSeL;
    static double testAndCullSeIs;
    static double testAndCullSeIc;
    static int testAndCullMinimumAge;
    static int testAndCullMaximumAge;
    static double proportionOfAnimalsTested;
    static double proportionOfAnimalsRemoved;
    static bool replacementAfterTestAndCull;
    static int delayBetweenTheOnsetOfAnIcAndTheImplementationOfTheTestAndCull;
    
    
    // ABC -----------------------------------------------------------------
    static int ABCsmcRunNumber;
    static double ABCsmcThreshold;
    
    static bool ABCsmcCurrentRunIsCompleted;
    
    static bool ABCrejection;
    static bool ABCsmc;
    static bool ABCcompleteTrajectories;

    static bool computeSummaryStatistics;
    
    static int ABCsmcTotalNumberOfRuns;
    static int numberOfAcceptedParticlesRequiredForTheFirstRun;
    static int numberOfAcceptedParticlesRequiredToGoToTheNextRun;
    static double perturbationKernelparameter;
    static int ABCsmcSequenceNumber;
    
    static int ABCcompleteTrajectoriesSequenceNumber;
    
    static double abcTestSensitivity;
    static double abcTestSensitivity_min;
    static double abcTestSensitivity_max;
    static double abcTestSpecificity;
    
    static double probaToPurchaseInfectedAnimal_initialValue_min;
    static double probaToPurchaseInfectedAnimal_initialValue_max;
    
    static double probaToPurchaseInfectedAnimal_slope_min_global;
    static double probaToPurchaseInfectedAnimal_slope_min;
    static double probaToPurchaseInfectedAnimal_slope_max_global;
    static double probaToPurchaseInfectedAnimal_slope_max;
    
    static double abcInfGlobalEnv_min;
    static double abcInfGlobalEnv_max;
    
    static double abcMuDistributionPrevIntra_min;
    static double abcMuDistributionPrevIntra_max;
    static double abcSigmaDistributionPrevIntra_min;
    static double abcSigmaDistributionPrevIntra_max;
    
    static std::vector<std::string> vHoldingsForResume;
    
    static const bool managementOfPositiveTestedTakeIntoAccount = true;
    static double cullingRatePositiveTestedAnimals;// = 1. - std::exp( - (1./24.) ); // TODO : check if param set like this or with a proba?

    static std::map<std::string, std::map<int, ObservedSampleInformations>> mFarmsDateSampleCount;
    
    // Member functions ====================================================
    static void addDataToMatrixOfProbaInitInfection(std::string fileName, int timeInYear);
    
    static void loadMatrixInfectionInitPrevalenceLevelDistribution();
    
    static void updateTheABCsmcRunNumber();
    
    static void updateABCsmcCurrentRunIsCompleted();
    
    static void updateProbaToPurchaseInfectedAnimalOutsideTheMetapopulation(const int& timeStep);
    
};

#endif
