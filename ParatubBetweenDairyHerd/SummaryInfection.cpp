//
//  SummaryInfection.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 02/08/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#include "SummaryInfection.h"

// Constructor =========================================================
SummaryInfection::SummaryInfection(){
    // Variables ===========================================================
    ageAtInfection.resize(Parameters::durSusceptible,0);
    ageAtInfection_inutero.resize(Parameters::durSusceptible,0);
    ageAtInfection_colostrum.resize(Parameters::durSusceptible,0);
    ageAtInfection_milk.resize(Parameters::durSusceptible,0);
    ageAtInfection_local.resize(Parameters::durSusceptible,0);
    ageAtInfection_global.resize(Parameters::durSusceptible,0);
    
    IncidenceT.resize(Parameters::simutime,0);
    transmissionRoutes_inutero.resize(Parameters::simutime,0);
    transmissionRoutes_colostrum.resize(Parameters::simutime,0);
    transmissionRoutes_milk.resize(Parameters::simutime,0);
    transmissionRoutes_local.resize(Parameters::simutime,0);
    transmissionRoutes_global.resize(Parameters::simutime,0);
    
    IncidenceIs.resize(Parameters::simutime,0);
    IncidenceIc.resize(Parameters::simutime,0);
    
    persistence.resize(Parameters::simutime,0);
}


// Member functions ====================================================
void SummaryInfection::persistenceUpdate(const int& t){
    persistence[t] = 1;
}



void SummaryInfection::persistencePastUpdate(const int& t){
    persistence[t-1] = 1;
}



