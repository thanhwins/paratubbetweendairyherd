//
//  MovementMatrix.h
//  ParatubBetweenDairyHerd
//
//  Created by Gaël Beaunée on 03/01/2014.
//  Copyright (c) 2014 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_MovementMatrix_h
#define ParatubBetweenDairyHerd_MovementMatrix_h

#include <vector>
#include <map>
#include <unordered_map>
#include <random>
#include <algorithm>
#include <iostream>
//#include <iomanip>
#include <fstream>
#include <sstream>

#include "Parameters.h"
#include "MovementUnit.h"

typedef std::vector<int> vInt;
typedef std::map<int, vInt> mInt2vInt;
typedef std::vector<mInt2vInt> vMapInt2vInt;
typedef std::vector<vInt> vIntVectors;

class MovementMatrix {
private:
    static MovementMatrix *instance;
    
    MovementMatrix();
    
    
    int k;
    int step;
    
public:
    // Constructor =========================================================
    static MovementMatrix& get_instance();
    
    static void kill();
    
    // Variables ===========================================================
    static std::map<std::string, std::map<int, std::vector<MovementUnit>> > mmOutMoves;
    
    // Member functions ====================================================
    void loadTheNetworkData();
    
};

#endif
