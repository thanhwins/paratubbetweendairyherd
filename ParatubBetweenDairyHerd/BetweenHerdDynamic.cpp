//
//  BetweenHerdDynamic.cpp
//  ParatubBetweenDairyHerd
//
//  Created by Gaël Beaunée on 03/01/2014.
//  Copyright (c) 2014 Gaël Beaunée. All rights reserved.
//

#include "BetweenHerdDynamic.h"


// Constructor =========================================================
BetweenHerdDynamic::BetweenHerdDynamic(){
    vAgeMov.resize(Parameters::nbStage, 0);
    vAgeMovAfterCorrection.resize(Parameters::nbStage, 0);
    vAgeFailure.resize(Parameters::nbStage, 0);
    
    nbPersistentSimu = 0;
    numberOfSimulationDone = 0;
    
    for (int herd=0; herd<Parameters::nbHerds; herd++) {
        std::string currentId = Parameters::vFarmID[herd];
        if (currentId == "outside") {
            std::cout << "*** outside ***" << std::endl;
        } else if (currentId == "") {
            std::cout << "*** na ***" << std::endl;
        }
        std::vector<double> vect;
        vvIntraHerdPrevalenceAtTheEnd[currentId] = vect;
    }
}


// Member functions ====================================================
void BetweenHerdDynamic::Initialize(ParametersSpecificToEachRepetition& ParamRep, std::map<std::string, DairyHerd>& mFarms, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary, BufferGlobalResults& iBufferGlobalResults, std::map<std::string, std::map<int, std::vector<MovementUnit>> >& mmOutMoves){
    // initialisation des différents troupeaux
    for (int herd=0; herd<Parameters::nbHerds; herd++) {
        std::string currentId = Parameters::vFarmID[herd];
        if (currentId == "outside") {
            std::cout << "*** outside ***" << std::endl;
        } else if (currentId == "") {
            std::cout << "*** na ***" << std::endl;
        }
        mFarms[currentId] = DairyHerd(ParamRep, currentId);
        mFarms[currentId].Initialize();
    }
    
    
    // choix des troupeaux infectés initiaux
    if (Parameters::nbHerdsInfected > 0) {
        if (Parameters::ABCrejection == true | Parameters::ABCsmc == true) {
            choiceOfHerdsInfectedInABC(ParamRep, mFarms, iBufferPersistentResults, iMetapopSummary);
        } else if (Parameters::ABCcompleteTrajectories) {
            choiceOfHerdsInfectedRandomAndPrevIntraRandom(ParamRep, mFarms, iBufferPersistentResults, iMetapopSummary);
        } else if (Parameters::withinHerdBiosecurity) {
            choiceOfHerdsInfectedRandom(ParamRep, mFarms, iBufferPersistentResults, iMetapopSummary);
        } else if (Parameters::SelectingInfectedHerdInAPreferentialWay) {
            choiceOfHerdsInfectedPreferential(ParamRep, mFarms, iBufferPersistentResults, iMetapopSummary);
        } else {
//            choiceOfHerdsInfectedUniform(ParamRep, mFarms, iBufferPersistentResults, iMetapopSummary);
            choiceOfHerdsInfectedRandomAndPrevIntraRandom(ParamRep, mFarms, iBufferPersistentResults, iMetapopSummary);
        }
    }
    
    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::typeOfTheRiskToPurchaseInfectedAnimalOutsideTheMetapopulation == 0) {
        iMetapopSummary.Clear(ParamRep);
    }
    
    int bufferNbHerdsInfected = 0;
    int bufferNbHerdsAffected = 0;
    
    for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
#ifndef ABCreduce
        if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
            iBufferPersistentResults.updateBuffer(0, it->second.typeInfectedHerd, it->second.iSummaryDynamic);
        }
#endif
        it->second.initMetapopSummary(0, iMetapopSummary);
        
        if (it->second.iSummaryDynamic.infected[0] > 0) {
            bufferNbHerdsInfected ++;
        }
        
        if (it->second.iSummaryDynamic.affected[0] > 0) {
            bufferNbHerdsAffected ++;
        }
    }
    
//    iMetapopSummary.Resume(0); // Need to be done after SendOutMovesExt (also when time = 0)
    // In place of resume a copy of present variables in past variables is done in order to be able to get informations needed when movement are done (because at time 0 present variables are set to zero, when swapping the past and present vaiables, without copy past variables will be set to zero and cause problem when use by some functions)
    iMetapopSummary.initialCopy(0);

    iBufferPersistentResults.bInfectedHerds[0] = bufferNbHerdsInfected;
#ifndef ABCreduce
    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
        iBufferPersistentResults.bAffectedHerds[0] = bufferNbHerdsAffected;
        iBufferPersistentResults.bNeverInfectedHerds[0] = Parameters::nbHerds - bufferNbHerdsInfected;
        iBufferPersistentResults.bAlreadyInfectedHerds[0] = bufferNbHerdsInfected;
    }
#endif
    
}











void BetweenHerdDynamic::Run(){
    chronoRun.start();
    
#ifdef ACTIVATE_PARALLEL
    omp_set_num_threads(Parameters::nbThreads);
#endif
#pragma omp parallel  // parallel region
    {
#pragma omp master  // only the master thread will execute this code
        {
#ifdef ACTIVATE_PARALLEL
            int nthreads = omp_get_num_threads();  // computation of the total number of threads
            std::cout << std::endl << nthreads << " thread(s) available for computation" << std::endl;
#endif
        }
        
#pragma omp barrier  // barrier to display nthreads before threadid
        
#pragma omp critical (ready)  // in order to "protect" the common output
        {
#ifdef ACTIVATE_PARALLEL
            std::cout << "Thread " << omp_get_thread_num() << " is ready for computation" << std::endl;
#endif
        }
    }
    
    std::cout << std::endl << "STARTING COMPUTATION" << std::endl << std::endl;
    // Begin runs loop
#pragma omp parallel for schedule(dynamic) //dynamic or guided
    for (int run=1; run<=Parameters::nbRuns; run++) {

        
        // to check if there is a problem or not with the movements
        long double countAgeFailure = 0;
        long double countMov = 0;
        
        
        ParametersSpecificToEachRepetition ParamRep = ParametersSpecificToEachRepetition(run);
        
        // ABC - parameter values setting
        if (Parameters::ABCrejection == true | Parameters::ABCsmc == true | Parameters::ABCcompleteTrajectories == true | Parameters::computeSummaryStatistics == true) {
            ParamRep.setParameterValuesForABC(Parameters::ABCsmcRunNumber, iABCInfosAndResults.previousParticles);
        }
        
#pragma omp critical (progression)
        {
            if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                //std::cout << std::defaultfloat << std::setprecision(3) << "R" << run << " - " << numberOfSimulationDone << " | Seed: " << ParamRep.Seed << std::endl;
                std::cout << std::setprecision(3) << "R" << run << " - " << numberOfSimulationDone << " | Seed: " << ParamRep.Seed << std::endl;
            }
            
            if (Parameters::ABCrejection == true) {
                //std::cout << "abcInfGlobalEnv_log: " << ParamRep.abcInfGlobalEnv_log << " - " << "abcInfGlobalEnv: " << ParamRep.abcInfGlobalEnv << " - " << "abcInfGlobalEnvIfCalfManagementImprovement: " << ParamRep.abcInfGlobalEnvIfCalfManagementImprovement << std::endl;
                std::cout << std::setprecision(3) << "R" << run << " | Seed: " << ParamRep.Seed << " | " << "Parameter values for the particle | " << "probaToPurchaseInfectedAnimal_initialValue: " << ParamRep.probaToPurchaseInfectedAnimal_initialValue << " - " << "probaToPurchaseInfectedAnimal_slope: " << ParamRep.probaToPurchaseInfectedAnimal_slope << " - " << "abcInfGlobalEnv: " << ParamRep.abcInfGlobalEnv << " - " << "abcTestSensitivity: " << ParamRep.abcTestSensitivity << std::endl << std::endl;
            }
            
            if (Parameters::ABCsmc == true) {
                //std::cout << "abcInfGlobalEnv_log: " << ParamRep.abcInfGlobalEnv_log << " - " << "abcInfGlobalEnv: " << ParamRep.abcInfGlobalEnv << " - " << "abcInfGlobalEnvIfCalfManagementImprovement: " << ParamRep.abcInfGlobalEnvIfCalfManagementImprovement << std::endl;
                std::cout << std::setprecision(3) << "ABCsmc run " << Parameters::ABCsmcRunNumber << " | Seed: " << ParamRep.Seed << " | " << "Parameter values for the particle | " << "probaToPurchaseInfectedAnimal_initialValue: " << ParamRep.probaToPurchaseInfectedAnimal_initialValue << " - " << "probaToPurchaseInfectedAnimal_slope: " << ParamRep.probaToPurchaseInfectedAnimal_slope << " - " << "abcInfGlobalEnv: " << ParamRep.abcInfGlobalEnv << " - " << "abcTestSensitivity: " << ParamRep.abcTestSensitivity << std::endl << std::endl;
            }
            
            if (Parameters::ABCcompleteTrajectories == true) {
                //std::cout << "abcInfGlobalEnv_log: " << ParamRep.abcInfGlobalEnv_log << " - " << "abcInfGlobalEnv: " << ParamRep.abcInfGlobalEnv << " - " << "abcInfGlobalEnvIfCalfManagementImprovement: " << ParamRep.abcInfGlobalEnvIfCalfManagementImprovement << std::endl;
                std::cout << std::setprecision(3) << "R" << run << " | Seed: " << ParamRep.Seed << " | " << "Parameter values for the particle | " << "propHerdsInfected: " << Parameters::propHerdsInfected << " - " << "muDistributionPrevIntra: " << Parameters::muDistributionPrevIntra << " - " << "sigmaDistributionPrevIntra: " << Parameters::sigmaDistributionPrevIntra << " - " << "probaToPurchaseInfectedAnimal_initialValue: " << Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation_initialValue << " - " << "probaToPurchaseInfectedAnimal_slope: " << Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope << " - " << "InfGlobalEnv: " << Parameters::infGlobalEnv << " - " << "abcTestSensitivity: " << ParamRep.abcTestSensitivity << std::endl << std::endl;
            }
        }
        
        
        std::map<std::string, DairyHerd> mFarms;
        MetapopSummary iMetapopSummary;
//        Parameters::updateProbaToPurchaseInfectedAnimalOutsideTheMetapopulation(0);
//        if (Parameters::ABCrejection == true | Parameters::ABCsmc == true | Parameters::ABCcompleteTrajectories == true) {
//            ParamRep.updateValueForProbaToPurchaseInfectedAnimal(0);
//        }

        BufferGlobalResults iBufferGlobalResults;
        BufferPersistentResults iBufferPersistentResults;
        
        int nbHerdExtinct = 0;
        std::vector<std::string> vIdHerdExtinct;
        
        // Initialize herds
#pragma omp critical (initializeHerds)
        {
            Initialize(ParamRep, mFarms, iBufferPersistentResults, iMetapopSummary, iBufferGlobalResults, MovementMatrix::mmOutMoves);
        }
        
        for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
            it->second.simulateSamplingAndMoveAnimalsInPositiveTestedCompartmentForFirstTimeStep(ParamRep, 0);
            if (MovementMatrix::mmOutMoves[it->first].count(0)){ //vOutMoves[herd].find(timeStep) != vOutMoves[herd].end()
                it->second.SendOutMoves(ParamRep, 0, MovementMatrix::mmOutMoves, mFarms, vAgeMov, countMov, vAgeFailure, countAgeFailure, vAgeMovAfterCorrection, iBufferPersistentResults, iMetapopSummary);
            }
        }
        SendOutMovesExt(ParamRep, 0, mFarms, iMetapopSummary, iBufferPersistentResults, MovementMatrix::mmOutMoves, countMov, countAgeFailure);

        // Update of MetapopSummary: swap past and present variables, re-initialize present variables
        iMetapopSummary.Resume(0);
        
        // Initialize the weekNumber
        int weeknumber = 0;
        
        // Time loop
//        std::cout << "Time loop" << std::endl;
        for (int timeStep=1; timeStep<Parameters::simutime; timeStep++) {
            //#ifndef ACTIVATE_PARALLEL
            //            std::cout << std::endl << "R" << run << " - T" << timeStep << std::endl;
            //#endif
            // Update of the weekNumber
            if (weeknumber<52) {
                weeknumber +=1;
            } else {
                weeknumber = 1;
            }
            
            // Update parameters link to the time
            Parameters::updateProbaToPurchaseInfectedAnimalOutsideTheMetapopulation(timeStep);
            if (Parameters::ABCrejection == true | Parameters::ABCsmc == true | Parameters::ABCcompleteTrajectories == true) {
                ParamRep.updateValueForProbaToPurchaseInfectedAnimal(timeStep);
            }
            
            // Herds loop
            for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
                // Dynamic intra-troupeau (dont mise à jours des paramètres du troupeau) et mouvements
                if (timeStep >= it->second.iParametersSpecificToEachHerd.beginDate) {
                    // receive "In" movements
                    ReceiveInMoves(ParamRep, timeStep, it->first, mFarms, iBufferPersistentResults, iMetapopSummary, run);
                    // update summary of headcounts and check if infection persists (in Past)
                    it->second.UpdatePastSummary(timeStep);
                    // within herd dynamic
                    it->second.Compute(timeStep, ParamRep, weeknumber);
                    // simulate Sampling AndMoveAnimalsInPositiveTestedCompartment
                    it->second.simulateSamplingAndMoveAnimalsInPositiveTestedCompartment(ParamRep, timeStep);
                    // simulate last sampling
                    it->second.simulateLastSampling(ParamRep, timeStep);
                    // test and cull measure
                    it->second.isTheTestAndCullMeasureShouldBeApplied(ParamRep, timeStep, Parameters::testAndCullMinimumAge, Parameters::testAndCullMaximumAge, Parameters::proportionOfAnimalsTested, iMetapopSummary);
                }
                
                // send "Out" movements
                if (MovementMatrix::mmOutMoves[it->first].count(timeStep)){ //vOutMoves[herd].find(timeStep) != vOutMoves[herd].end()
                    it->second.SendOutMoves(ParamRep, timeStep, MovementMatrix::mmOutMoves, mFarms, vAgeMov, countMov, vAgeFailure, countAgeFailure, vAgeMovAfterCorrection, iBufferPersistentResults, iMetapopSummary);
                }
                
                if (timeStep >= it->second.iParametersSpecificToEachHerd.beginDate) {
                    // update summary of headcounts and check if infection persists (in Present)
                    it->second.UpdateSummary(timeStep, iMetapopSummary);
                    
#ifndef ABCreduce
                    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                        it->second.updateBufferPersistentResults(ParamRep, timeStep, iBufferPersistentResults, run);
                        iBufferPersistentResults.updateBuffer(timeStep, it->second.typeInfectedHerd, it->second.iSummaryDynamic);
                    }
#endif
                    
                    if (it->second.iSummaryDynamic.headcount[timeStep]==0 & it->second.iSummaryDynamic.extinct==false) {
                        nbHerdExtinct ++;
                        it->second.iSummaryDynamic.extinct = true;
                        //std::cout << "Extinction: " << herd << std::endl;
                        vIdHerdExtinct.emplace_back(it->first);
                    }
                }
                
            } // End herds loop
            
            SendOutMovesExt(ParamRep, timeStep, mFarms, iMetapopSummary, iBufferPersistentResults, MovementMatrix::mmOutMoves, countMov, countAgeFailure);
            
            // Update of MetapopSummary: swap past and present variables, re-initialize present variables
            iMetapopSummary.Resume(timeStep);
            
#ifndef ABCreduce
            if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                if (iBufferPersistentResults.bInfectedHerds[timeStep] > 0) {
                    iBufferGlobalResults.vMetapopPersistence[timeStep] = 1;
                }
            }
#endif
            
            //#ifndef ACTIVATE_PARALLEL
            //            if (weeknumber == 52) {
            //                int currentYear = ((timeStep-1) / Parameters::timestepInOneYear) + 1;
            //                std::cout << "year - " << currentYear << std::endl;
            //            }
            //#endif
            
            if (Parameters::ABCsmc == true & Parameters::ABCsmcRunNumber > 1) {
                if (ParamRep.distanceBetweenSimulationAndObservationForThisRun > iABCInfosAndResults.threshold) {
                    std::cout << "distanceBetweenSimulationAndObservationForThisRun > iABCInfosAndResults.threshold!" << " - " << "timeStep: " << timeStep << "/" << Parameters::simutime << std::endl;
                    break;
                }
            }
            
        } // End time loop
//        std::cout << "End time loop" << std::endl;


        bool nanWereFound = false;


#pragma omp critical (ABCresults)
        {
            if (Parameters::ABCrejection == true | Parameters::ABCsmc == true) {
                // function to compute and store Particles and associated distances
//                iABCInfosAndResults.storeSimulatedSummaryStatistics(mFarms);  // A remplacer par la sauvegarde des distances par troupeaux
                iABCInfosAndResults.computeAndStoreDistanceForTheRun(ParamRep, mFarms);
                if (Parameters::ABCsmc == true) {
                    if (Parameters::ABCsmcRunNumber == 1) {
                        std::cout << Parameters::ABCsmcRunNumber << " - " << iABCInfosAndResults.numberOfGeneratedParticles << "/" << Parameters::numberOfAcceptedParticlesRequiredForTheFirstRun << " | " << "distance between simulation and observation for this run: " << ParamRep.distanceBetweenSimulationAndObservationForThisRun << std::endl << std::endl;
                    } else {
                        std::cout << Parameters::ABCsmcRunNumber << " - " << iABCInfosAndResults.numberOfSelectedParticles << "/" << Parameters::numberOfAcceptedParticlesRequiredToGoToTheNextRun << " | " << "distance between simulation and observation for this run: " << ParamRep.distanceBetweenSimulationAndObservationForThisRun << " (thrshld: " << Parameters::ABCsmcThreshold << ")" << std::endl << std::endl;
                    }
                }
                if (Parameters::ABCsmc) {
                    run = 0; // programm can't stop if ABCsmcRunNumber is not reach (this avoid to update the for loop of repetition)
                    // iABCInfosAndResults.particlesProcessingForNextRunOfABCsmc();
                    // if (Parameters::ABCsmcRunNumber > Parameters::ABCsmcTotalNumberOfRuns) {
                    //     break;
                    //     // run = Parameters::nbRuns + 1;
                    // }
                    iABCInfosAndResults.saveInformationsOnTheCurrentRun();
//                    iABCInfosAndResults.saveSimulatedSummaryStatistics(); // A remplacer par la sauvegarde des distances par troupeaux
                    if (Parameters::ABCsmcCurrentRunIsCompleted == true) {
                        //break;
                        run = Parameters::nbRuns + 1;
                    }
                }
            }


            if (Parameters::ABCcompleteTrajectories == true | Parameters::computeSummaryStatistics == true) {
                nanWereFound = ResultsWriting::saveABCcompleteTrajectoriesResults(mFarms, run);
                if (nanWereFound) {
#ifndef ACTIVATE_PARALLEL
//                    run--;
                    Parameters::nbRuns++;
                    Parameters::nbRunsWithNan++;
#endif
                    std::cout << "nan were found in the results > run need to be re-compute (will be done if not parallelized)" << std::endl;
                }
            }

            if (Parameters::ABCcompleteTrajectories == true) {
                if (vIdHerdExtinct.size()>0) {
                    std::cout << "R" << run << " - " << "Id of Herd Extinct:";
                    std::sort(vIdHerdExtinct.begin(), vIdHerdExtinct.end());
                    for (int i = 0; i < vIdHerdExtinct.size(); i++) {
                        std::cout << " " << vIdHerdExtinct[i];
                    }
                }
                std::cout << std::endl; // << std::endl;
//                std::cout << "Number of movements : " << std::fixed << std::setprecision(0) << countMov << "  |  Number of age failure : " << std::fixed << std::setprecision(0) << countAgeFailure << " (" << std::setprecision(2) << countAgeFailure/countMov*100 << "%)" << std::endl << std::endl;
                std::cout << "Number of movements : " << static_cast<long int>(countMov) << "  |  Number of age failure : " << static_cast<long int>(countAgeFailure) << " (" << std::setprecision(2) << countAgeFailure/countMov*100 << "%)" << std::endl << std::endl;
            }
        }


        
#ifndef ABCreduce
        if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
            // Compute results
            if (nanWereFound == false) {
#pragma omp critical (results)
                {
                    // RESULTS ARE NOT UPDATED IN THE ACCUMULATORS
                    //acc_globalResults.updateAcc(iBufferGlobalResults, mFarms);
                    //acc_persistentResults.updateAcc(iBufferGlobalResults.vMetapopPersistence[Parameters::simutime-1], iBufferPersistentResults, iMetapopSummary);
                    if (iBufferGlobalResults.vMetapopPersistence[Parameters::simutime-1] == 1) {
                        nbPersistentSimu++;
                    }

                    //updateAccSecondPrev(vHerd);

                    ////saveInfectionDynamic(run, vHerd);

                    //saveWithinHerdPrevalenceOverTime(mFarms, run);

                    //
                    // RESULTS SAVE FOR EACH RUN HAVE TO BE ADD HERE
                    ResultsWriting::openFileAndWrite("vInfectedHerdsOverTime", iBufferPersistentResults.bInfectedHerds, run);
                    //

                    updateIntraHerdPrevalenceAtTheEnd(mFarms);
                }
            }
            
            int birthFakeBecauseNotEnoughCows = 0;
            int birthTotal = 0;
            for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
                birthFakeBecauseNotEnoughCows += it->second.birthFake;
                birthTotal += it->second.birthTot;
            }
            double birthWithPbmProportion = static_cast<double>(birthFakeBecauseNotEnoughCows)/static_cast<double>(birthTotal)*100.0;
            
#pragma omp critical (runResume)
            {
                numberOfSimulationDone ++;
                std::cout << "R" << run << " - " << numberOfSimulationDone << " | " << "BirthsWithProblem: " << birthFakeBecauseNotEnoughCows << " (" << std::setprecision(2) << birthWithPbmProportion << "%)" << " | Extinct " << nbHerdExtinct;
                std::cout << std::setprecision(6);
                
                if (vIdHerdExtinct.size()>0) {
                    std::cout << " - Id Herd Extinct:";
                    std::sort(vIdHerdExtinct.begin(), vIdHerdExtinct.end());
                    for (int i = 0; i < vIdHerdExtinct.size(); i++) {
                        std::cout << " " << vIdHerdExtinct[i];
                    }
                }
                std::cout << std::endl; // << std::endl;
            }
        }
#endif
        
        
        
//#ifndef ACTIVATE_PARALLEL
//#ifndef ABCreduce
//        if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
//            int farmPos = static_cast<int>( std::find(Parameters::vFarmID.begin(), Parameters::vFarmID.end(), "35078074") - Parameters::vFarmID.begin() );
//            for (int i = 0; i<mean(acc_globalResults.vAccAnnualHeadcounts[farmPos]).size(); i++) {
//                std::cout << mean(acc_globalResults.vAccAnnualHeadcounts[farmPos])[i] << " ";
//            }
//            std::cout << std::endl << std::endl;
//        }
//#endif
//#endif

        
        
        gsl_rng_free (ParamRep.randomGenerator);
    } // End runs loop
    std::cout << std::endl << "END OF COMPUTATION" << std::endl;
    
    
    chronoRun.stop();
    
    
    std::cout << std::endl;
//    #ifndef ACTIVATE_PARALLEL
//        std::cout << "Number of movements : " << std::fixed << std::setprecision(0) << countMov << std::endl;
//        std::cout << "Number of age failure : " << std::fixed << std::setprecision(0) << countAgeFailure << " (" << std::setprecision(2) << countAgeFailure/countMov*100 << "%)" << std::endl;
//    #endif
    
#ifndef ABCreduce
    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
        std::cout << "Number of run with infection at the end of simulation : " << nbPersistentSimu << std::endl;
        
//#ifndef ACTIVATE_PARALLEL
//        if (Parameters::withinHerdBiosecurity == false) {
//            std::cout << "Number of infected movements from First: " << acc_persistentResults.vSourceAndDestinationOfInfectedMovements_SourceIsFirst.size() << std::endl;
//            std::cout << "Number of infected movements from Second: " << acc_persistentResults.vSourceAndDestinationOfInfectedMovements_SourceIsSecond.size() << std::endl;
//            std::cout << "Number of infected movements from Ext: " << acc_persistentResults.vSourceAndDestinationOfInfectedMovements_SourceIsExt.size() << std::endl;
//        }
//#endif
    }
#endif
    
}











// SELECTION DES TROUPEAUX A REVOIR ICI !!
void BetweenHerdDynamic::choiceOfHerdsInfectedUniform(ParametersSpecificToEachRepetition& ParamRep, std::map<std::string, DairyHerd>& mFarms, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary){
    std::vector<double> vHerdProbs(Parameters::vHerdDataForSelectingTheInfectedHerdsUniform.begin(), Parameters::vHerdDataForSelectingTheInfectedHerdsUniform.end());
//    std::random_device rd;
//    std::mt19937 gen(rd());
    std::mt19937 gen(static_cast<int>(ParamRep.Seed));
    std::discrete_distribution<> d(vHerdProbs.begin(), vHerdProbs.end());
    
    std::vector<std::string> vHerdInfected;
    
    int n = 0;
    while (n < Parameters::nbHerdsInfected) {
        int farmIndex = d(gen);
//        gsl_ran_discrete_t *sample_struct = gsl_ran_discrete_preproc (vHerdProbs.size(), &vHerdProbs[0]);
//        int farmIndex = static_cast<int>(gsl_ran_discrete (ParamRep.randomGenerator, sample_struct));
        std::string theFarmId = Parameters::vFarmID[farmIndex];
//        std::cout << "farmIndex: " << farmIndex << std::endl;
//        vHerdProbs[farmIndex] = 0.0;
        if(std::find(vHerdInfected.begin(), vHerdInfected.end(), theFarmId) == vHerdInfected.end()) {
            if (Parameters::initializeHerdsWithEndemicState) {
                mFarms[theFarmId].firstCaseDate = 0;
                mFarms[theFarmId].hasAlreadyBeenInfected = true;
                mFarms[theFarmId].firstInfectedHerd = true;
                mFarms[theFarmId].typeInfectedHerd = 1;
                mFarms[theFarmId].vInfectionDynamic[0] = 2;
                mFarms[theFarmId].InitializeInfectionInHerd(ParamRep, Parameters::initializeSimutime);
                mFarms[theFarmId].UpdatePastSummary(1);
                
                iBufferPersistentResults.updateBufferInit(theFarmId, mFarms[theFarmId].iParametersSpecificToEachHerd.farmIndex, mFarms[theFarmId].typeInfectedHerd, mFarms[theFarmId].iSummaryDynamic);
                iBufferPersistentResults.updateBuffer(0, mFarms[theFarmId].typeInfectedHerd, mFarms[theFarmId].iSummaryDynamic);
                
                
                vHerdInfected.emplace_back(theFarmId);
                
            } else {
                mFarms[theFarmId].AddingInfectedAnimals(0, Parameters::nbInfectedAnimalsIntroduced);
                mFarms[theFarmId].firstCaseDate = 0;
                mFarms[theFarmId].hasAlreadyBeenInfected = true;
                mFarms[theFarmId].firstInfectedHerd = true;
                mFarms[theFarmId].typeInfectedHerd = 1;
                mFarms[theFarmId].vInfectionDynamic[0] = 2;
                
                mFarms[theFarmId].UpdatePastSummary(1);
                
                iBufferPersistentResults.updateBufferInit(theFarmId, mFarms[theFarmId].iParametersSpecificToEachHerd.farmIndex, mFarms[theFarmId].typeInfectedHerd, mFarms[theFarmId].iSummaryDynamic);
                //                iBufferPersistentResults.updateBuffer(0, mFarms[theFarmId].typeInfectedHerd, mFarms[theFarmId].iSummaryDynamic);
                
                
                vHerdInfected.emplace_back(theFarmId);
            }
            n++;
        }
    }
}



void BetweenHerdDynamic::choiceOfHerdsInfectedPreferential(ParametersSpecificToEachRepetition& ParamRep, std::map<std::string, DairyHerd>& mFarms, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary){
    std::vector<double> vHerdProbs;
    
    switch (Parameters::TypeOfPreferentialWay) {
        case 1:
            //            vHerdProbs = Parameters::vHerdDataForSelectingTheInfectedHerdsPreferentialOutDegree;
            vHerdProbs.insert(vHerdProbs.begin(), Parameters::vHerdDataForSelectingTheInfectedHerdsPreferentialOutDegree.begin(), Parameters::vHerdDataForSelectingTheInfectedHerdsPreferentialOutDegree.end());
            break;
            
        case 2:
            //            vHerdProbs = Parameters::vHerdDataForSelectingTheInfectedHerdsPreferentialNumberOfOutMovements;
            vHerdProbs.insert(vHerdProbs.begin(), Parameters::vHerdDataForSelectingTheInfectedHerdsPreferentialNumberOfOutMovements.begin(), Parameters::vHerdDataForSelectingTheInfectedHerdsPreferentialNumberOfOutMovements.end());
            break;
            
        default:
            break;
    }
    
//    std::random_device rd;
//    std::mt19937 gen(rd());
    std::mt19937 gen(static_cast<int>(ParamRep.Seed));
    std::discrete_distribution<> d(vHerdProbs.begin(), vHerdProbs.end());
    
    std::vector<std::string> vHerdInfected;
    
    int n = 0;
    while (n < Parameters::nbHerdsInfected) {
        int farmIndex = d(gen);
//        int farmIndex = static_cast<int>(gsl_ran_discrete (ParamRep.randomGenerator, gsl_ran_discrete_preproc (vHerdProbs.size(), &vHerdProbs[0])));
        std::string theFarmId = Parameters::vFarmID[farmIndex];
//        vHerdProbs[farmIndex] = 0.0;
        if(std::find(vHerdInfected.begin(), vHerdInfected.end(), theFarmId) == vHerdInfected.end()) {
            if (Parameters::initializeHerdsWithEndemicState) {
                mFarms[theFarmId].firstCaseDate = 0;
                mFarms[theFarmId].hasAlreadyBeenInfected = true;
                mFarms[theFarmId].firstInfectedHerd = true;
                mFarms[theFarmId].typeInfectedHerd = 1;
                mFarms[theFarmId].vInfectionDynamic[0] = 2;
                
                mFarms[theFarmId].InitializeInfectionInHerd(ParamRep, Parameters::initializeSimutime);
                mFarms[theFarmId].UpdatePastSummary(1);
                
                iBufferPersistentResults.updateBufferInit(theFarmId, mFarms[theFarmId].iParametersSpecificToEachHerd.farmIndex, mFarms[theFarmId].typeInfectedHerd, mFarms[theFarmId].iSummaryDynamic);
                //                iBufferPersistentResults.updateBuffer(0, mFarms[theFarmId].typeInfectedHerd, mFarms[theFarmId].iSummaryDynamic);
                
                
                vHerdInfected.emplace_back(theFarmId);
                
            } else {
                mFarms[theFarmId].AddingInfectedAnimals(0, Parameters::nbInfectedAnimalsIntroduced);
                mFarms[theFarmId].firstCaseDate = 0;
                mFarms[theFarmId].hasAlreadyBeenInfected = true;
                mFarms[theFarmId].firstInfectedHerd = true;
                mFarms[theFarmId].typeInfectedHerd = 1;
                mFarms[theFarmId].vInfectionDynamic[0] = 2;
                
                mFarms[theFarmId].UpdatePastSummary(1);
                
                iBufferPersistentResults.updateBufferInit(theFarmId, mFarms[theFarmId].iParametersSpecificToEachHerd.farmIndex, mFarms[theFarmId].typeInfectedHerd, mFarms[theFarmId].iSummaryDynamic);
                //                iBufferPersistentResults.updateBuffer(0, mFarms[theFarmId].typeInfectedHerd, mFarms[theFarmId].iSummaryDynamic);
                
                
                vHerdInfected.emplace_back(theFarmId);
            }
            n++;
        }
    }
}



void BetweenHerdDynamic::choiceOfHerdsInfectedRandom(ParametersSpecificToEachRepetition& ParamRep, std::map<std::string, DairyHerd>& mFarms, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary){
    
    std::vector<int> vDataHerdId;
    
    for (int i=0; i<Parameters::nbHerds; i++) {
        vDataHerdId.emplace_back(i);
    }
    
//    std::vector<int> vHerdInfected = U_functions::RandomSamplingWithoutReplacement(vDataHerdId, Parameters::nbHerdsInfected);
    std::vector<int> vHerdInfected = U_functions::RandomSamplingWithoutReplacement(ParamRep.randomGenerator, vDataHerdId, Parameters::nbHerdsInfected);
    
    for (int i = 0; i < vHerdInfected.size(); i++) {
        int farmIndex = vHerdInfected[i];
        std::string theFarmId = Parameters::vFarmID[farmIndex];
        
//        std::random_device rd;
//        std::mt19937 gen(rd());
        std::mt19937 gen(static_cast<int>(ParamRep.Seed*i));
        std::uniform_int_distribution<> distUnif(Parameters::endemicPeriodMin, Parameters::endemicPeriodMax);
        int endemicPeriod = distUnif(gen);
        
        if (Parameters::initializeHerdsWithEndemicState) {
            mFarms[theFarmId].firstCaseDate = 0;
            mFarms[theFarmId].hasAlreadyBeenInfected = true;
            mFarms[theFarmId].firstInfectedHerd = true;
            mFarms[theFarmId].typeInfectedHerd = 1;
            mFarms[theFarmId].vInfectionDynamic[0] = 2;
            
            mFarms[theFarmId].InitializeInfectionInHerd(ParamRep, endemicPeriod);
            mFarms[theFarmId].UpdatePastSummary(1);
            
            iBufferPersistentResults.updateBufferInit(theFarmId, mFarms[theFarmId].iParametersSpecificToEachHerd.farmIndex, mFarms[theFarmId].typeInfectedHerd, mFarms[theFarmId].iSummaryDynamic);
//            iBufferPersistentResults.updateBuffer(0, mFarms[theFarmId].typeInfectedHerd, mFarms[theFarmId].iSummaryDynamic);

        } else {
            mFarms[theFarmId].AddingInfectedAnimals(0, Parameters::nbInfectedAnimalsIntroduced);
            mFarms[theFarmId].firstCaseDate = 0;
            mFarms[theFarmId].hasAlreadyBeenInfected = true;
            mFarms[theFarmId].firstInfectedHerd = true;
            mFarms[theFarmId].typeInfectedHerd = 1;
            mFarms[theFarmId].vInfectionDynamic[0] = 2;
            
            mFarms[theFarmId].UpdatePastSummary(1);
            
            iBufferPersistentResults.updateBufferInit(theFarmId, mFarms[theFarmId].iParametersSpecificToEachHerd.farmIndex, mFarms[theFarmId].typeInfectedHerd, mFarms[theFarmId].iSummaryDynamic);
//            iBufferPersistentResults.updateBuffer(0, mFarms[theFarmId].typeInfectedHerd, mFarms[theFarmId].iSummaryDynamic);

        }
    }
}



void BetweenHerdDynamic::choiceOfHerdsInfectedInABC(ParametersSpecificToEachRepetition& ParamRep, std::map<std::string, DairyHerd>& mFarms, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary){
    
    for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
        std::string theFarmId = it->first;
        if (Parameters::mFarmsCharacteristics[theFarmId].prevInit > 0.0) {
            // mFarms[theFarmId].AddingInfectedAnimals(0, Parameters::nbInfectedAnimalsIntroduced);
            mFarms[theFarmId].firstCaseDate = 0;
            mFarms[theFarmId].hasAlreadyBeenInfected = true;
            mFarms[theFarmId].firstInfectedHerd = true;
            mFarms[theFarmId].typeInfectedHerd = 1;
            mFarms[theFarmId].vInfectionDynamic[0] = 2;
            
            mFarms[theFarmId].InitializeInfectionInHerdForABC(ParamRep);
            mFarms[theFarmId].UpdatePastSummary(1);
            
//             iBufferPersistentResults.updateBufferInit(theFarmId, mFarms[theFarmId].iParametersSpecificToEachHerd.farmIndex, mFarms[theFarmId].typeInfectedHerd, mFarms[theFarmId].iSummaryDynamic);

//             iBufferPersistentResults.updateBuffer(0, mFarms[theFarmId].typeInfectedHerd, mFarms[theFarmId].iSummaryDynamic);
        }
    }
    
}



void BetweenHerdDynamic::choiceOfHerdsInfectedRandomAndPrevIntraRandom(ParametersSpecificToEachRepetition& ParamRep, std::map<std::string, DairyHerd>& mFarms, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary){
    
    std::vector<int> vDataHerdId;
    
    for (int i=0; i<Parameters::nbHerds; i++) {
        vDataHerdId.emplace_back(i);
    }
    
    //    std::vector<int> vHerdInfected = U_functions::RandomSamplingWithoutReplacement(vDataHerdId, Parameters::nbHerdsInfected);
    std::vector<int> vHerdInfected = U_functions::RandomSamplingWithoutReplacement(ParamRep.randomGenerator, vDataHerdId, Parameters::nbHerdsInfected);
    
    for (int i = 0; i < vHerdInfected.size(); i++) {

//        std::cout << i << " : "; // DEBUG20170727

        int farmIndex = vHerdInfected[i];
        std::string theFarmId = Parameters::vFarmID[farmIndex];

        bool prevWasSetSupToZero = mFarms[theFarmId].InitializeInfectionInHerdForABC(ParamRep);

        if (prevWasSetSupToZero) {
            mFarms[theFarmId].firstCaseDate = 0;
            mFarms[theFarmId].hasAlreadyBeenInfected = true;
            mFarms[theFarmId].firstInfectedHerd = true;
            mFarms[theFarmId].typeInfectedHerd = 1;
            mFarms[theFarmId].vInfectionDynamic[0] = 2;
            mFarms[theFarmId].UpdatePastSummary(1);

            iBufferPersistentResults.updateBufferInit(theFarmId, mFarms[theFarmId].iParametersSpecificToEachHerd.farmIndex, mFarms[theFarmId].typeInfectedHerd, mFarms[theFarmId].iSummaryDynamic);
        }

    }
}



void BetweenHerdDynamic::ReceiveInMoves(ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, const std::string& theFarmId, std::map<std::string, DairyHerd>& mFarms, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary, const int& run){
    
    vAnimalMovingVectors::iterator it = mFarms[theFarmId].vPendingMoveVectors.begin();
    int gardeFou = 0;
    while (it != mFarms[theFarmId].vPendingMoveVectors.end()){
        gardeFou ++;
        if (gardeFou > 10000) {
            std::cout << "garde-fou - ReceiveInMoves" << std::endl;
            std::exit(EXIT_FAILURE);
        }
        if ( it->date == timeStep - 1 ){ //
            
#ifndef ABCreduce
            if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                iBufferPersistentResults.bNbIncomingMov[timeStep]++;
            }
#endif
            
            bool numberOfIncomingMovsAboveTheMinimum = mFarms[theFarmId].iParametersSpecificToEachHerd.numberOfIncomingMovementsAboveTheMinimum;
            bool numberOfOutgoingMovsAboveTheMinimum = mFarms[theFarmId].iParametersSpecificToEachHerd.numberOfOutgoingMovementsAboveTheMinimum;
            
            int animalAge = it->age;
            switch (it->healthState) {      //TODO: enum ?
                case 0 : // SR
                    receiveAnimal("SR", animalAge, ParamRep, timeStep, theFarmId, mFarms, iBufferPersistentResults, it, numberOfIncomingMovsAboveTheMinimum, numberOfOutgoingMovsAboveTheMinimum, iMetapopSummary, run);
                    break;
                case 1 : // T
                    receiveAnimal("T", animalAge, ParamRep, timeStep, theFarmId, mFarms, iBufferPersistentResults, it, numberOfIncomingMovsAboveTheMinimum, numberOfOutgoingMovsAboveTheMinimum, iMetapopSummary, run);
                    break;
                case 2 : // L
                    receiveAnimal("L", animalAge, ParamRep, timeStep, theFarmId, mFarms, iBufferPersistentResults, it, numberOfIncomingMovsAboveTheMinimum, numberOfOutgoingMovsAboveTheMinimum, iMetapopSummary, run);
                    break;
                case 3 : // Is
                    receiveAnimal("Is", animalAge, ParamRep, timeStep, theFarmId, mFarms, iBufferPersistentResults, it, numberOfIncomingMovsAboveTheMinimum, numberOfOutgoingMovsAboveTheMinimum, iMetapopSummary, run);
                    break;
                    
                default : throw std::runtime_error("Wrong format in 'pending moves vector'.");
            }
            
            it = mFarms[theFarmId].vPendingMoveVectors.erase(it);
            
        } else
            ++it;
    }
}



void BetweenHerdDynamic::receiveAnimal(std::string healthState, const int& animalAge, ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, const std::string& theFarmId, std::map<std::string, DairyHerd>& mFarms, BufferPersistentResults& iBufferPersistentResults, const vAnimalMovingVectors::iterator& it, bool& numberOfIncomingMovsAboveTheMinimum, bool& numberOfOutgoingMovsAboveTheMinimum, MetapopSummary& iMetapopSummary, const int& run){
    
    bool aTestMustBeDone = false;
    
#ifndef ABCreduce
    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
        if (healthState != "SR") {
            iBufferPersistentResults.bNbIncomingInfectedMov[timeStep]++;
        }
    }
#endif
    
    if (timeStep >= Parameters::startTest) {
        if (it->herdSourceID != "outside") {
            if (Parameters::typeOfTestStrategy == Parameters::strategyHerdRandomlyChosenMovIn) {
                if (mFarms[theFarmId].iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm) {
                    aTestMustBeDone = true;
#ifndef ABCreduce
                    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                        iBufferPersistentResults.bNbIncomingMovTested[timeStep]++;
                        if (healthState != "SR") {
                            iBufferPersistentResults.bNbIncomingInfectedMovTested[timeStep]++;
                        }
                    }
#endif
                }
            } else
                if (Parameters::typeOfTestStrategy == Parameters::strategyHerdRandomlyChosenMovOut) {
                    if (mFarms[it->herdSourceID].iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm) {
                        aTestMustBeDone = true;
#ifndef ABCreduce
                        if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                            iBufferPersistentResults.bNbIncomingMovTested[timeStep]++;
                            if (healthState != "SR") {
                                iBufferPersistentResults.bNbIncomingInfectedMovTested[timeStep]++;
                            }
                        }
#endif
                    }
                } else
                    if (Parameters::typeOfTestStrategy == Parameters::strategyHerdRandomlyChosenMovInOut) {
                        if (mFarms[theFarmId].iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm | mFarms[it->herdSourceID].iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm) {
                            aTestMustBeDone = true;
#ifndef ABCreduce
                            if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                                iBufferPersistentResults.bNbIncomingMovTested[timeStep]++;
                                if (healthState != "SR") {
                                    iBufferPersistentResults.bNbIncomingInfectedMovTested[timeStep]++;
                                }
                            }
#endif
                        }
                    } else
                        if (Parameters::typeOfTestStrategy == Parameters::strategyHerdWithIc) {
                            if (it->presenceOfIcAtThisTime == 1) {
                                if (mFarms[it->herdSourceID].iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm) {
                                    aTestMustBeDone = true;
#ifndef ABCreduce
                                    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                                        iBufferPersistentResults.bNbIncomingMovTested[timeStep]++;
                                        if (healthState != "SR") {
                                            iBufferPersistentResults.bNbIncomingInfectedMovTested[timeStep]++;
                                        }
                                    }
#endif
                                }
                            }
                        } else if (Parameters::typeOfTestStrategy == Parameters::strategyHerdWithMoreThanTwoIc) {
                            if (it->presenceOfAtLeastTwoIcAtThisTime == 1) {
                                if (mFarms[it->herdSourceID].iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm) {
                                    aTestMustBeDone = true;
#ifndef ABCreduce
                                    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                                        iBufferPersistentResults.bNbIncomingMovTested[timeStep]++;
                                        if (healthState != "SR") {
                                            iBufferPersistentResults.bNbIncomingInfectedMovTested[timeStep]++;
                                        }
                                    }
#endif
                                }
                            }
                        } else if (Parameters::typeOfTestStrategy == Parameters::strategyBasedOnNumberOfIncomingMovements) {
                            if (mFarms[theFarmId].iParametersSpecificToEachHerd.numberOfIncomingMovementsAboveTheMinimum) {
                                if (mFarms[theFarmId].iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm) {
                                    aTestMustBeDone = true;
#ifndef ABCreduce
                                    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                                        iBufferPersistentResults.bNbIncomingMovTested[timeStep]++;
                                        if (healthState != "SR") {
                                            iBufferPersistentResults.bNbIncomingInfectedMovTested[timeStep]++;
                                        }
                                    }
#endif
                                }
                            }
                        } else if (Parameters::typeOfTestStrategy == Parameters::strategyBasedOnNumberOfOutgoingMovements) {
                            if (it->numberOfOutgoingMovementsAboveTheMinimum == 1) {
                                if (mFarms[it->herdSourceID].iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm) {
                                    aTestMustBeDone = true;
#ifndef ABCreduce
                                    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                                        iBufferPersistentResults.bNbIncomingMovTested[timeStep]++;
                                        if (healthState != "SR") {
                                            iBufferPersistentResults.bNbIncomingInfectedMovTested[timeStep]++;
                                        }
                                    }
#endif
                                }
                            }
                        } else {
                            throw std::runtime_error("Wrong strategies in SendOutMoves function");
                        }
        } else {
            if (Parameters::typeOfTestStrategy == Parameters::strategyHerdRandomlyChosenMovIn) {
                if (mFarms[theFarmId].iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm) {
                    aTestMustBeDone = true;
#ifndef ABCreduce
                    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                        iBufferPersistentResults.bNbIncomingMovTested[timeStep]++;
                        if (healthState != "SR") {
                            iBufferPersistentResults.bNbIncomingInfectedMovTested[timeStep]++;
                        }
                    }
#endif
                }
            } else
                if (Parameters::typeOfTestStrategy == Parameters::strategyHerdRandomlyChosenMovOut) {
                    if (gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::proportionOfFarmsWithTestAtPurchase)) {
                        aTestMustBeDone = true;
#ifndef ABCreduce
                        if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                            iBufferPersistentResults.bNbIncomingMovTested[timeStep]++;
                            if (healthState != "SR") {
                                iBufferPersistentResults.bNbIncomingInfectedMovTested[timeStep]++;
                            }
                        }
#endif
                    }
                } else
                    if (Parameters::typeOfTestStrategy == Parameters::strategyHerdRandomlyChosenMovInOut) {
                        if (mFarms[theFarmId].iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm | gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::proportionOfFarmsWithTestAtPurchase)) {
                            aTestMustBeDone = true;
#ifndef ABCreduce
                            if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                                iBufferPersistentResults.bNbIncomingMovTested[timeStep]++;
                                if (healthState != "SR") {
                                    iBufferPersistentResults.bNbIncomingInfectedMovTested[timeStep]++;
                                }
                            }
#endif
                        }
                    } else
                        if (Parameters::typeOfTestStrategy == Parameters::strategyHerdWithIc) {
                            if (it->presenceOfIcAtThisTime == 1) {
                                if (gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::proportionOfFarmsWithTestAtPurchase)) {
                                    aTestMustBeDone = true;
#ifndef ABCreduce
                                    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                                        iBufferPersistentResults.bNbIncomingMovTested[timeStep]++;
                                        if (healthState != "SR") {
                                            iBufferPersistentResults.bNbIncomingInfectedMovTested[timeStep]++;
                                        }
                                    }
#endif
                                }
                            }
                        } else
                            if (Parameters::typeOfTestStrategy == Parameters::strategyHerdWithMoreThanTwoIc) {
                                if (it->presenceOfAtLeastTwoIcAtThisTime == 1) {
                                    if (gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::proportionOfFarmsWithTestAtPurchase)) {
                                        aTestMustBeDone = true;
#ifndef ABCreduce
                                        if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                                            iBufferPersistentResults.bNbIncomingMovTested[timeStep]++;
                                            if (healthState != "SR") {
                                                iBufferPersistentResults.bNbIncomingInfectedMovTested[timeStep]++;
                                            }
                                        }
#endif
                                    }
                                }
                            } else
                                if (Parameters::typeOfTestStrategy == Parameters::strategyBasedOnNumberOfIncomingMovements) {
                                    if (mFarms[theFarmId].iParametersSpecificToEachHerd.numberOfIncomingMovementsAboveTheMinimum) {
                                        if (mFarms[theFarmId].iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm) {
                                            aTestMustBeDone = true;
#ifndef ABCreduce
                                            if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                                                iBufferPersistentResults.bNbIncomingMovTested[timeStep]++;
                                                if (healthState != "SR") {
                                                    iBufferPersistentResults.bNbIncomingInfectedMovTested[timeStep]++;
                                                }
                                            }
#endif
                                        }
                                    }
                                } else
                                    if (Parameters::typeOfTestStrategy == Parameters::strategyBasedOnNumberOfOutgoingMovements) {
                                        if (it->numberOfOutgoingMovementsAboveTheMinimum == 1) {
                                            if (gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::proportionOfFarmsWithTestAtPurchase)) {
                                                aTestMustBeDone = true;
#ifndef ABCreduce
                                                if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                                                    iBufferPersistentResults.bNbIncomingMovTested[timeStep]++;
                                                    if (healthState != "SR") {
                                                        iBufferPersistentResults.bNbIncomingInfectedMovTested[timeStep]++;
                                                    }
                                                }
#endif
                                            }
                                        }
                                    } else {
                                        throw std::runtime_error("Wrong strategies in SendOutMoves function");
                                    }
        }
    }
#ifndef ABCreduce
    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
        if (healthState != "SR") {
            iBufferPersistentResults.bIntroducionOfInfectiousAnimalsIfNoTest++;
        }
    }
#endif
    
    if (timeStep >= Parameters::startTest) {
        if (Test(ParamRep, it->healthState, aTestMustBeDone)) {
            if (healthState == "SR") {
                StatusChoice(ParamRep, it->age, theFarmId, it->typeOfInfectedHerd, mFarms, iMetapopSummary, iBufferPersistentResults);
            } else {
                StatusChoiceBis(ParamRep, timeStep, it->healthState, it->age, theFarmId, it->typeOfInfectedHerd, mFarms, it->herdSourceID, it->typeOfInfectedHerd, iMetapopSummary, iBufferPersistentResults, run);
            }
        } else {
            if (healthState == "SR") {
                ++mFarms[theFarmId].iHealthStateCompartmentsContainer.iCompartmentSR.past[animalAge];
            } else if (healthState == "T") {
                ++mFarms[theFarmId].iHealthStateCompartmentsContainer.iCompartmentT.past[animalAge];
                mFarms[theFarmId].numberOfPurchasesOfInfectedAnimalsT++;
            } else if (healthState == "L") {
                ++mFarms[theFarmId].iHealthStateCompartmentsContainer.iCompartmentL.past[animalAge];
                mFarms[theFarmId].numberOfPurchasesOfInfectedAnimalsL++;
            } else if (healthState == "Is") {
                ++mFarms[theFarmId].iHealthStateCompartmentsContainer.iCompartmentIs.past[animalAge];
                mFarms[theFarmId].numberOfPurchasesOfInfectedAnimalsIs++;
            }
            
            if (healthState != "SR") {
                mFarms[theFarmId].numberOfInfectedAnimalsPurchased++;
                
                updateSourceAndDestinationResults(timeStep, theFarmId, it->healthState, it->age, mFarms, it->herdSourceID, it->typeOfInfectedHerd, iBufferPersistentResults, iMetapopSummary, run);
                if (mFarms[theFarmId].typeOfFirstAnimalPurchase == 0) {
                    mFarms[theFarmId].typeOfFirstAnimalPurchase = it->healthState;
                }
            }
            
#ifndef ABCreduce
            if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                if (healthState != "SR") {
                    if (mFarms[theFarmId].persistenceIndicator == false) {
                        switch (it->typeOfInfectedHerd) {
                            case 0: iBufferPersistentResults.bInfectedByExt++; break;
                            case 1: iBufferPersistentResults.bInfectedByFirst++; break;
                            case 2: iBufferPersistentResults.bInfectedBySecond++; break;
                        }
                        
                        iBufferPersistentResults.bNbNewInfectedHerdsSinceSimulationStart ++;
                        iBufferPersistentResults.bNbNewInfectedHerdsSinceTestsStart ++;
                    }
                }
            }
#endif
        }
    } else {
        if (healthState == "SR") {
            ++mFarms[theFarmId].iHealthStateCompartmentsContainer.iCompartmentSR.past[animalAge];
        } else if (healthState == "T") {
            ++mFarms[theFarmId].iHealthStateCompartmentsContainer.iCompartmentT.past[animalAge];
            mFarms[theFarmId].numberOfPurchasesOfInfectedAnimalsT++;
        } else if (healthState == "L") {
            ++mFarms[theFarmId].iHealthStateCompartmentsContainer.iCompartmentL.past[animalAge];
            mFarms[theFarmId].numberOfPurchasesOfInfectedAnimalsL++;
        } else if (healthState == "Is") {
            ++mFarms[theFarmId].iHealthStateCompartmentsContainer.iCompartmentIs.past[animalAge];
            mFarms[theFarmId].numberOfPurchasesOfInfectedAnimalsIs++;
        }
        
        if (healthState != "SR") {
            mFarms[theFarmId].numberOfInfectedAnimalsPurchased++;
            
            updateSourceAndDestinationResults(timeStep, theFarmId, it->healthState, it->age, mFarms, it->herdSourceID, it->typeOfInfectedHerd, iBufferPersistentResults, iMetapopSummary, run);
            if (mFarms[theFarmId].typeOfFirstAnimalPurchase == 0) {
                mFarms[theFarmId].typeOfFirstAnimalPurchase = it->healthState;
            }
        }
        
#ifndef ABCreduce
        if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
            if (healthState != "SR") {
                if (mFarms[theFarmId].persistenceIndicator == false) {
                    switch (it->typeOfInfectedHerd) {
                        case 0: iBufferPersistentResults.bInfectedByExt++; break;
                        case 1: iBufferPersistentResults.bInfectedByFirst++; break;
                        case 2: iBufferPersistentResults.bInfectedBySecond++; break;
                    }
                    
                    iBufferPersistentResults.bNbNewInfectedHerdsSinceSimulationStart ++;
                }
            }
        }
#endif
    }
}



void BetweenHerdDynamic::updateSourceAndDestinationResults(const int& timeStep, const std::string& theFarmId, const int& type, const int& age, std::map<std::string, DairyHerd>& mFarms, const std::string& herdSource, const int& typeOfInfectedHerdSource, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary, const int& run){
#ifndef ABCreduce
    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
        if (Parameters::withinHerdBiosecurity == false) {
            std::vector<std::string> vMovInf(17);
            
            vMovInf[0] = herdSource; //vHerd[herdSource].iParametersSpecificToEachHerd.herdID;
            
            if (herdSource != "outside") {
                vMovInf[1] = std::to_string(mFarms[herdSource].typeInfectedHerd);
                
                double prevalenceInfected = static_cast<double>(mFarms[herdSource].iSummaryDynamic.infected[timeStep-2])/static_cast<double>(mFarms[herdSource].iSummaryDynamic.headcount[timeStep-2]);
                double prevalenceInfectious = static_cast<double>(mFarms[herdSource].iSummaryDynamic.infectious[timeStep-2])/static_cast<double>(mFarms[herdSource].iSummaryDynamic.headcount[timeStep-2]);
                double prevalenceAffected = static_cast<double>(mFarms[herdSource].iSummaryDynamic.affected[timeStep-2])/static_cast<double>(mFarms[herdSource].iSummaryDynamic.headcount[timeStep-2]);
                
                vMovInf[2] = std::to_string(prevalenceInfected);
                vMovInf[3] = std::to_string(prevalenceInfectious);
                vMovInf[4] = std::to_string(prevalenceAffected);
                
                vMovInf[5] = std::to_string(mFarms[herdSource].presenceOfIcAtThisTime);
                vMovInf[6] = std::to_string(mFarms[herdSource].cumulativeNumberOfIcForTheLastPeriodOfInfection);
                vMovInf[7] = std::to_string(mFarms[herdSource].numberOfWeeksSinceInfected);
                
            } else {
                vMovInf[1] = std::to_string(0);
                
                double prevalenceInfected = iMetapopSummary.pastInfected/iMetapopSummary.pastHeadcount;
                double prevalenceInfectious = iMetapopSummary.pastInfectious/iMetapopSummary.pastHeadcount;
                double prevalenceAffected = iMetapopSummary.pastAffected/iMetapopSummary.pastHeadcount;
                
                vMovInf[2] = std::to_string(prevalenceInfected);
                vMovInf[3] = std::to_string(prevalenceInfectious);
                vMovInf[4] = std::to_string(prevalenceAffected);
            }
            
            vMovInf[8] = theFarmId; //vHerd[herdNum].iParametersSpecificToEachHerd.herdID;
            vMovInf[9] = std::to_string(mFarms[theFarmId].typeInfectedHerd);
            
            if (type == 1) {vMovInf[10] = std::to_string(1);} else {vMovInf[10] = std::to_string(0);} // T
            if (type == 2) {vMovInf[11] = std::to_string(1);} else {vMovInf[11] = std::to_string(0);} // L
            if (type == 3) {vMovInf[12] = std::to_string(1);} else {vMovInf[12] = std::to_string(0);} // Is
            
            if (mFarms[theFarmId].persistenceIndicator == false) {vMovInf[13] = std::to_string(1);} else {vMovInf[13] = std::to_string(0);}
            
            vMovInf[14] = std::to_string(age);
            
            vMovInf[15] = std::to_string(run);
            
            vMovInf[16] = std::to_string(timeStep);
            if (typeOfInfectedHerdSource == 1) {
                iBufferPersistentResults.vBufferSourceAndDestinationOfInfectedMovements_SourceIsFirst.emplace_back(vMovInf);
            } else if (typeOfInfectedHerdSource == 2) {
                iBufferPersistentResults.vBufferSourceAndDestinationOfInfectedMovements_SourceIsSecond.emplace_back(vMovInf);
            } else if (typeOfInfectedHerdSource == 0) {
                iBufferPersistentResults.vBufferSourceAndDestinationOfInfectedMovements_SourceIsExt.emplace_back(vMovInf);
            }
        }
    }
#endif
}



void BetweenHerdDynamic::SendOutMovesExt(ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, std::map<std::string, DairyHerd>& mFarms, MetapopSummary& iMetapopSummary, BufferPersistentResults& iBufferPersistentResults, std::map<std::string, std::map<int, std::vector<MovementUnit>> >& mmOutMoves, long double& countMov, long double& countAgeFailure){
    
    for (int i = 0; i<static_cast<int>(mmOutMoves["outside"][timeStep].size()); i++) {
        
        
        int age = mmOutMoves["outside"][timeStep][i].age;
#ifndef ACTIVATE_PARALLEL
        vAgeMov[age]++;
#endif
        countMov++;
        
        std::vector<double> vProbs(4);

        // TODO: le calcul pourrait être modifier afin de prendre en compte les proportions d'animaux présent dans la classe d'age considérée à la place de l'age lui même.
        
        double SR = static_cast<double>(iMetapopSummary.presentSR[age]);
        double T = static_cast<double>(iMetapopSummary.presentT[age]);
        double L = static_cast<double>(iMetapopSummary.presentL[age]);
        double Is = static_cast<double>(iMetapopSummary.presentIs[age]);
        double proportionOfHerdsWithIc = iMetapopSummary.presentNbHerdsWithIc / Parameters::nbHerds;
        double proportionOfHerdsWithMoreThanTwoIc = iMetapopSummary.presentNbHerdsWithMoreThanTwoIc / Parameters::nbHerds;
        double proportionOfHerdsWithOutgoingMovementsAboveTheMinimum = Parameters::numberOfHerdsAboveTheMinimumOfOutgoingMovements/Parameters::nbHerds;

        if (Parameters::typeOfTheRiskToPurchaseInfectedAnimalOutsideTheMetapopulation == 1) {
            if ((T + L + Is) == 0) {
                if (age < Parameters::ageYheifer) {
                    T = 1.;
                    L = 1.;
                    Is = 0.;
                } else if (age >= Parameters::ageYheifer & age < Parameters::ageAdults) {
                    T = 1.;
                    L = 1.;
                    Is = 1.;
                } else { // age >= Parameters::ageAdults
                    T = 0.;
                    L = 1.;
                    Is = 1.;
                }
            }

//            if ((T + L + Is) > 0) {
                if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
                    SR = ((1-Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation) * (T + L + Is)) / (Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation);
                } else {
                    SR = ((1-ParamRep.probaToPurchaseInfectedAnimal_currentValue) * (T + L + Is)) / (ParamRep.probaToPurchaseInfectedAnimal_currentValue);
                }
//            }
        }

        double N = SR + T + L + Is;
        
        vProbs[0] = (SR / N);
        vProbs[1] = (T / N);
        vProbs[2] = (L / N);
        vProbs[3] = (Is / N);
        
//        std::cout << SR << " | " << T << " | " << L << " | " << Is << " | " << std::endl;
//        std::cout << "vProbs: " << vProbs[0] << " | " << vProbs[1] << " | " << vProbs[2] << " | " << vProbs[3] << " | " << std::endl;

        std::vector<int> vIndivNbForEachCompart = U_functions::multinomialDistribution(ParamRep.randomGenerator, 1, vProbs);
        
        std::vector<int> v(7);
        v[0] = timeStep;
        
        if (vIndivNbForEachCompart[0] > 0){ // SR
            v[1] = 0;
        } else if (vIndivNbForEachCompart[1] > 0){ // T
            v[1] = 1;
        } else if (vIndivNbForEachCompart[2] > 0){ // L
            v[1] = 2;
        } else if (vIndivNbForEachCompart[3] > 0){ // Is
            v[1] = 3;
        } else {
#ifndef ACTIVATE_PARALLEL
            vAgeFailure[age]++;
#endif
            countAgeFailure++;
        }
        
        v[2] = age;
        v[3] = 0;
        v[4] = gsl_ran_bernoulli(ParamRep.randomGenerator, proportionOfHerdsWithIc); // presenceOfIcAtThisTime
        v[5] = gsl_ran_bernoulli(ParamRep.randomGenerator, proportionOfHerdsWithMoreThanTwoIc); // cumulativeNumberOfIc
        v[6] = gsl_ran_bernoulli(ParamRep.randomGenerator, proportionOfHerdsWithOutgoingMovementsAboveTheMinimum);
        
        AnimalMoving pendingMov = AnimalMoving(v, "outside");
        
        if (mmOutMoves["outside"][timeStep][i].destination != "outside") {
//            mFarms[mmOutMoves["outside"][timeStep][i].destination].vPendingMoveVectors.emplace_back(pendingMov);
            if (mFarms.count(mmOutMoves["outside"][timeStep][i].destination) != 0) {
                mFarms[mmOutMoves["outside"][timeStep][i].destination].addPendingMov(timeStep, pendingMov);
            }
        }
        
    }
    
    //    // Update of MetapopSummary: swap past and present variables, re-initialize present variables
    //    iMetapopSummary.Resume(); // DO JUST AFTER IN "Run"
}


bool BetweenHerdDynamic::Test(ParametersSpecificToEachRepetition& ParamRep, const int& status, const bool& aTestMustBeDone){
    bool results = false;
    
    if (aTestMustBeDone) {
        switch (status) {      //TODO: enum ?
            case 0 : // SR
                if (Parameters::ABCrejection | Parameters::ABCsmc | Parameters::ABCcompleteTrajectories) {
                    results = gsl_ran_bernoulli(ParamRep.randomGenerator, 1-Parameters::abcTestSpecificity);
                } else {
                    results = gsl_ran_bernoulli(ParamRep.randomGenerator, 1-Parameters::testSp);
                }
                break;
            case 1 : // T
                if (Parameters::ABCrejection | Parameters::ABCsmc | Parameters::ABCcompleteTrajectories) {
                    results = gsl_ran_bernoulli(ParamRep.randomGenerator, ParamRep.abcTestSensitivity);
                } else {
                    results = gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::testSeT);
                }
                break;
            case 2 : // L
                if (Parameters::ABCrejection | Parameters::ABCsmc | Parameters::ABCcompleteTrajectories) {
                    results = gsl_ran_bernoulli(ParamRep.randomGenerator, ParamRep.abcTestSensitivity);
                } else {
                    results = gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::testSeL);
                }
                break;
            case 3 : // Is
                if (Parameters::ABCrejection | Parameters::ABCsmc | Parameters::ABCcompleteTrajectories) {
                    results = gsl_ran_bernoulli(ParamRep.randomGenerator, ParamRep.abcTestSensitivity);
                } else {
                    results = gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::testSeIs);
                }
                break;
            default : throw std::runtime_error("Wrong status in 'pending moves vector'.");
        }
    }
    
    return results;
}


void BetweenHerdDynamic::StatusChoice(ParametersSpecificToEachRepetition& ParamRep, const int& age, const std::string& theFarmId, const int& typeHerdSource, std::map<std::string, DairyHerd>& mFarms, MetapopSummary& iMetapopSummary, BufferPersistentResults& iBufferPersistentResults){
    std::vector<double> vProbs(4);
    
    double SR = static_cast<double>(iMetapopSummary.pastSR[age]);
    double T = static_cast<double>(iMetapopSummary.pastT[age]);
    double L = static_cast<double>(iMetapopSummary.pastL[age]);
    double Is = static_cast<double>(iMetapopSummary.pastIs[age]);
    
    double N = SR + T + L + Is;
    
    
    if (Parameters::ABCrejection | Parameters::ABCsmc | Parameters::ABCcompleteTrajectories) {
        vProbs[0] = (SR / N) * Parameters::abcTestSpecificity;
        vProbs[1] = (T / N) * (1-ParamRep.abcTestSensitivity);
        vProbs[2] = (L / N) * (1-ParamRep.abcTestSensitivity);
        vProbs[3] = (Is / N) * (1-ParamRep.abcTestSensitivity);
    } else {
        vProbs[0] = (SR / N) * Parameters::testSp;
        vProbs[1] = (T / N) * (1-Parameters::testSeT);
        vProbs[2] = (L / N) * (1-Parameters::testSeL);
        vProbs[3] = (Is / N) * (1-Parameters::testSeIs);
    }
    
    std::vector<int> vIndivNbForEachCompart = U_functions::multinomialDistribution(ParamRep.randomGenerator, 1, vProbs);
    
    //SR
    if (vIndivNbForEachCompart[0]>0) {
        ++mFarms[theFarmId].iHealthStateCompartmentsContainer.iCompartmentSR.past[age];
    } else if (vIndivNbForEachCompart[1]>0) {
        ++mFarms[theFarmId].iHealthStateCompartmentsContainer.iCompartmentT.past[age];
        mFarms[theFarmId].numberOfPurchasesOfInfectedAnimalsT++;
        mFarms[theFarmId].numberOfInfectedAnimalsPurchased++;
#ifndef ABCreduce
        if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
            if (mFarms[theFarmId].persistenceIndicator == false) {
                iBufferPersistentResults.bInfectedByTestedMov ++;
                iBufferPersistentResults.bNbNewInfectedHerdsSinceSimulationStart ++;
                iBufferPersistentResults.bNbNewInfectedHerdsSinceTestsStart ++;
            }
        }
#endif
        
    } else if (vIndivNbForEachCompart[2]>0) {
        ++mFarms[theFarmId].iHealthStateCompartmentsContainer.iCompartmentL.past[age];
        mFarms[theFarmId].numberOfPurchasesOfInfectedAnimalsL++;
        mFarms[theFarmId].numberOfInfectedAnimalsPurchased++;
#ifndef ABCreduce
        if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
            if (mFarms[theFarmId].persistenceIndicator == false) {
                iBufferPersistentResults.bInfectedByTestedMov ++;
                iBufferPersistentResults.bNbNewInfectedHerdsSinceSimulationStart ++;
                iBufferPersistentResults.bNbNewInfectedHerdsSinceTestsStart ++;
            }
        }
#endif
        
    } else if (vIndivNbForEachCompart[3]>0) {
        ++mFarms[theFarmId].iHealthStateCompartmentsContainer.iCompartmentIs.past[age];
        mFarms[theFarmId].numberOfPurchasesOfInfectedAnimalsIs++;
        mFarms[theFarmId].numberOfInfectedAnimalsPurchased++;
#ifndef ABCreduce
        if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
            if (mFarms[theFarmId].persistenceIndicator == false) {
                iBufferPersistentResults.bInfectedByTestedMov ++;
                iBufferPersistentResults.bNbNewInfectedHerdsSinceSimulationStart ++;
                iBufferPersistentResults.bNbNewInfectedHerdsSinceTestsStart ++;
            }
        }
#endif
        
    }
}


void BetweenHerdDynamic::StatusChoiceBis(ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, const int& type, const int& age, const std::string& theFarmId, const int& typeHerdSource, std::map<std::string, DairyHerd>& mFarms, const std::string& herdSource, const int& typeOfInfectedHerdSource, MetapopSummary& iMetapopSummary, BufferPersistentResults& iBufferPersistentResults, const int& run){
    std::vector<double> vProbs(4);
    
    double SR = static_cast<double>(iMetapopSummary.pastSR[age]);
    double T = static_cast<double>(iMetapopSummary.pastT[age]);
    double L = static_cast<double>(iMetapopSummary.pastL[age]);
    double Is = static_cast<double>(iMetapopSummary.pastIs[age]);
    
    double N = SR + T + L + Is;
    
    if (Parameters::ABCrejection | Parameters::ABCsmc | Parameters::ABCcompleteTrajectories) {
        vProbs[0] = (SR / N) * Parameters::abcTestSpecificity;
        vProbs[1] = (T / N) * (1-ParamRep.abcTestSensitivity);
        vProbs[2] = (L / N) * (1-ParamRep.abcTestSensitivity);
        vProbs[3] = (Is / N) * (1-ParamRep.abcTestSensitivity);
    } else {
        vProbs[0] = (SR / N) * Parameters::testSp;
        vProbs[1] = (T / N) * (1-Parameters::testSeT);
        vProbs[2] = (L / N) * (1-Parameters::testSeL);
        vProbs[3] = (Is / N) * (1-Parameters::testSeIs);
    }
    
    std::vector<int> vIndivNbForEachCompart = U_functions::multinomialDistribution(ParamRep.randomGenerator, 1, vProbs);
    
    //SR
    if (vIndivNbForEachCompart[0]>0) {
        ++mFarms[theFarmId].iHealthStateCompartmentsContainer.iCompartmentSR.past[age];
#ifndef ABCreduce
        if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
            iBufferPersistentResults.bIntroducionOfInfectiousAnimalsAvoided++;
        }
#endif
        
    } else if (vIndivNbForEachCompart[1]>0) {
        ++mFarms[theFarmId].iHealthStateCompartmentsContainer.iCompartmentT.past[age];
        mFarms[theFarmId].numberOfPurchasesOfInfectedAnimalsT++;
        mFarms[theFarmId].numberOfInfectedAnimalsPurchased++;
#ifndef ABCreduce
        if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
            if (mFarms[theFarmId].persistenceIndicator == false) {
                iBufferPersistentResults.bInfectedByTestedMov ++;
                iBufferPersistentResults.bNbNewInfectedHerdsSinceSimulationStart ++;
                iBufferPersistentResults.bNbNewInfectedHerdsSinceTestsStart ++;
            }
        }
#endif
        
        updateSourceAndDestinationResults(timeStep, theFarmId, 1, age, mFarms, herdSource, typeOfInfectedHerdSource, iBufferPersistentResults, iMetapopSummary, run);
        if (mFarms[theFarmId].typeOfFirstAnimalPurchase == 0) {
            mFarms[theFarmId].typeOfFirstAnimalPurchase = 1;
        }
        
    } else if (vIndivNbForEachCompart[2]>0) {
        ++mFarms[theFarmId].iHealthStateCompartmentsContainer.iCompartmentL.past[age];
        mFarms[theFarmId].numberOfPurchasesOfInfectedAnimalsL++;
        mFarms[theFarmId].numberOfInfectedAnimalsPurchased++;
#ifndef ABCreduce
        if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
            if (mFarms[theFarmId].persistenceIndicator == false) {
                iBufferPersistentResults.bInfectedByTestedMov ++;
                iBufferPersistentResults.bNbNewInfectedHerdsSinceSimulationStart ++;
                iBufferPersistentResults.bNbNewInfectedHerdsSinceTestsStart ++;
            }
        }
#endif
        
        updateSourceAndDestinationResults(timeStep, theFarmId, 2, age, mFarms, herdSource, typeOfInfectedHerdSource, iBufferPersistentResults, iMetapopSummary, run);
        if (mFarms[theFarmId].typeOfFirstAnimalPurchase == 0) {
            mFarms[theFarmId].typeOfFirstAnimalPurchase = 2;
        }
        
    } else if (vIndivNbForEachCompart[3]>0) {
        ++mFarms[theFarmId].iHealthStateCompartmentsContainer.iCompartmentIs.past[age];
        mFarms[theFarmId].numberOfPurchasesOfInfectedAnimalsIs++;
        mFarms[theFarmId].numberOfInfectedAnimalsPurchased++;
#ifndef ABCreduce
        if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
            if (mFarms[theFarmId].persistenceIndicator == false) {
                iBufferPersistentResults.bInfectedByTestedMov ++;
                iBufferPersistentResults.bNbNewInfectedHerdsSinceSimulationStart ++;
                iBufferPersistentResults.bNbNewInfectedHerdsSinceTestsStart ++;
            }
        }
#endif
        
        updateSourceAndDestinationResults(timeStep, theFarmId, 3, age, mFarms, herdSource, typeOfInfectedHerdSource, iBufferPersistentResults, iMetapopSummary, run);
        if (mFarms[theFarmId].typeOfFirstAnimalPurchase == 0) {
            mFarms[theFarmId].typeOfFirstAnimalPurchase = 3;
        }
        
    }
}







void BetweenHerdDynamic::ExecutionTimeDisplay(){
    // Display execution time
    std::cout << std::endl;
    printf("Time :\n");
    printf("Execution ________________ %.3f seconds\n", chronoRun.display());
    //    std::cout << "Execution ________________ " << std::setprecision(4) << chronoRun.display() << " seconds\n";
    //    printf("Init _____________________ %.5f seconds\n", chronoInit.display());
    //    printf("Dynamic __________________ %.5f seconds\n", chronoDynamic.display());
    //    printf("Movements ________________ %.5f seconds\n", chronoMovements.display());
    //    printf("Compute Results __________ %.5f seconds\n", chronoComputeResults.display());
    //    printf("Clear ____________________ %.5f seconds\n", chronoClearHerds.display());
}



void BetweenHerdDynamic::SaveResults(){
#ifndef ABCreduce
    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
        ResultsWriting::openFileAndWrite("vFarmID", Parameters::vFarmID);
        
        ResultsWriting::openFileAndWrite("accMetapopPersistence", acc_globalResults.accMetapopPersistence);
        
        ResultsWriting::openFileAndWrite("accNbHerdsWithIc", acc_persistentResults.accNbHerdsWithIc);
        ResultsWriting::openFileAndWrite("accNbHerdsWithMoreThanTwoIc", acc_persistentResults.accNbHerdsWithMoreThanTwoIc);
        
        ResultsWriting::openFileAndWrite("vInfectedHerdsVariation", acc_persistentResults.vInfectedHerdsVariation);
        ResultsWriting::openFileAndWrite("vAlreadyInfectedHerdsVariation", acc_persistentResults.vAlreadyInfectedHerdsVariation);
        
        
        ResultsWriting::openFileAndWrite("vOutDegreeOfSelectedNode", acc_persistentResults.vOutDegreeOfSelectedNode);
        ResultsWriting::openFileAndWrite("vNbOutMovsOfSelectedNode", acc_persistentResults.vNbOutMovsOfSelectedNode);
        ResultsWriting::openFileAndWrite("vInDegreeOfSelectedNode", acc_persistentResults.vInDegreeOfSelectedNode);
        ResultsWriting::openFileAndWrite("vNbInMovsOfSelectedNode", acc_persistentResults.vNbInMovsOfSelectedNode);
        ResultsWriting::openFileAndWrite("vSizeOfSelectedNode", acc_persistentResults.vSizeOfSelectedNode);
        ResultsWriting::openFileAndWrite("vPrevalenceInSelectedNodes", acc_persistentResults.vPrevalenceInSelectedNodes);
        
        ResultsWriting::openFileAndWrite("vNbNewInfectedHerdsSinceSimulationStart", acc_persistentResults.vNbNewInfectedHerdsSinceSimulationStart);
        ResultsWriting::openFileAndWrite("vNbNewInfectedHerdsSinceTestsStart", acc_persistentResults.vNbNewInfectedHerdsSinceTestsStart);
        
        ResultsWriting::openFileAndWrite("accInfectedHerds", acc_persistentResults.accInfectedHerds);
        ResultsWriting::openFileAndWrite("accAffectedHerds", acc_persistentResults.accAffectedHerds);
        ResultsWriting::openFileAndWrite("accNeverInfectedHerds", acc_persistentResults.accNeverInfectedHerds);
        ResultsWriting::openFileAndWrite("accAlreadyInfectedHerds", acc_persistentResults.accAlreadyInfectedHerds);
        
        ResultsWriting::openFileAndWrite("accNbInitiallyInfectedHerds", acc_persistentResults.accNbInitiallyInfectedHerds);
        ResultsWriting::openFileAndWrite("accNbOtherInfectedHerds", acc_persistentResults.accNbOtherInfectedHerds);
        
        ResultsWriting::openFileAndWrite("accInfectedProportionMeta", acc_persistentResults.accInfectedProportionMeta);
        ResultsWriting::openFileAndWrite("accInfectiousProportionMeta", acc_persistentResults.accInfectiousProportionMeta);
        ResultsWriting::openFileAndWrite("accAffectedProportionMeta", acc_persistentResults.accAffectedProportionMeta);
        
        ResultsWriting::openFileAndWrite("accInfectedProportionFirst", acc_persistentResults.accInfectedProportionFirst);
        ResultsWriting::openFileAndWrite("accInfectiousProportionFirst", acc_persistentResults.accInfectiousProportionFirst);
        ResultsWriting::openFileAndWrite("accAffectedProportionFirst", acc_persistentResults.accAffectedProportionFirst);
        
//        if (Parameters::withinHerdBiosecurity == false) {
//            ResultsWriting::openFileAndWrite("accInfectedProportionFirstInit", acc_persistentResults.accInfectedProportionFirstInit);
//            ResultsWriting::openFileAndWrite("accInfectiousProportionFirstInit", acc_persistentResults.accInfectiousProportionFirstInit);
//            ResultsWriting::openFileAndWrite("accAffectedProportionFirstInit", acc_persistentResults.accAffectedProportionFirstInit);
//        }

        ResultsWriting::openFileAndWrite("accInfectedProportionSecond", acc_persistentResults.accInfectedProportionSecond);
        ResultsWriting::openFileAndWrite("accInfectiousProportionSecond", acc_persistentResults.accInfectiousProportionSecond);
        ResultsWriting::openFileAndWrite("accAffectedProportionSecond", acc_persistentResults.accAffectedProportionSecond);
        
        
        ResultsWriting::openFileAndWrite("accInfectedByFirst", acc_persistentResults.accInfectedByFirst);
        ResultsWriting::openFileAndWrite("accInfectedBySecond", acc_persistentResults.accInfectedBySecond);
        ResultsWriting::openFileAndWrite("accInfectedByExt", acc_persistentResults.accInfectedByExt);
        
        ResultsWriting::openFileAndWrite("accInfectedByTestedMov", acc_persistentResults.accInfectedByTestedMov);
        
        ResultsWriting::openFileAndWrite("AgeFailure", vAgeFailure);
        ResultsWriting::openFileAndWrite("AgeMov", vAgeMov);
        ResultsWriting::openFileAndWrite("AgeMovAfterCorrection", vAgeMovAfterCorrection);
//        ResultsWriting::openFileAndWrite("DateOfFirstCase", acc_globalResults.vDateOfFirstCase);

        
        
        ResultsWriting::openFileAndWrite("accProportionOfOutgoingMovTested", acc_persistentResults.accProportionOfOutgoingMovTested);
        ResultsWriting::openFileAndWrite("accProportionOfOutgoingInfectedMovTested", acc_persistentResults.accProportionOfOutgoingInfectedMovTested);
        
        ResultsWriting::openFileAndWrite("accProportionOfIncomingMovTested", acc_persistentResults.accProportionOfIncomingMovTested);
        ResultsWriting::openFileAndWrite("accProportionOfIncomingInfectedMovTested", acc_persistentResults.accProportionOfIncomingInfectedMovTested);
        
        
        ResultsWriting::openFileAndWrite("vNumberOfOutgoingMovTested", acc_persistentResults.vNumberOfOutgoingMovTested);
        ResultsWriting::openFileAndWrite("vNumberOfOutgoingMov", acc_persistentResults.vNumberOfOutgoingMov);
        
        ResultsWriting::openFileAndWrite("vNumberOfIncomingMovTested", acc_persistentResults.vNumberOfIncomingMovTested);
        ResultsWriting::openFileAndWrite("vNumberOfIncomingMov", acc_persistentResults.vNumberOfIncomingMov);
        
        
        ResultsWriting::openFileAndWrite("vIntroducionOfInfectiousAnimalsIfNoTest", acc_persistentResults.vIntroducionOfInfectiousAnimalsIfNoTest);
        ResultsWriting::openFileAndWrite("vIntroducionOfInfectiousAnimalsAvoided", acc_persistentResults.vIntroducionOfInfectiousAnimalsAvoided);
        
        ResultsWriting::openFileAndWrite("vProportionOfIntroducionOfInfectiousAnimalsAvoided", acc_persistentResults.vProportionOfIntroducionOfInfectiousAnimalsAvoided);
        
        
        ResultsWriting::openFileAndWrite("accInfectedHerdsQuantiles", acc_persistentResults.accInfectedHerdsQuantiles);
        ResultsWriting::openFileAndWrite("accAlreadyInfectedHerdsQuantiles", acc_persistentResults.accAlreadyInfectedHerdsQuantiles);
        ResultsWriting::openFileAndWrite("accAffectedHerdsQuantiles", acc_persistentResults.accAffectedHerdsQuantiles);
        
        
        ResultsWriting::openFileAndWrite("accInfectedProportionMetaQuantiles", acc_persistentResults.accInfectedProportionMetaQuantiles);
        ResultsWriting::openFileAndWrite("accInfectiousProportionMetaQuantiles", acc_persistentResults.accInfectiousProportionMetaQuantiles);
        ResultsWriting::openFileAndWrite("accAffectedProportionMetaQuantiles", acc_persistentResults.accAffectedProportionMetaQuantiles);
        
        
        ResultsWriting::openFileAndWrite("vPrevalenceInInfectedHerdsAtTheEnd", acc_persistentResults.vPrevalenceInInfectedHerdsAtTheEnd);
        ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vvPrevalenceInInfectedHerdsAtTheEnd", acc_persistentResults.vvPrevalenceInInfectedHerdsAtTheEnd);
        
        
        if (Parameters::withinHerdBiosecurity == false) {
            ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vInfectionDurationWhenExtinction", acc_persistentResults.vInfectionDurationWhenExtinction);
            ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vInfectionDurationIfNoExtinction", acc_persistentResults.vInfectionDurationIfNoExtinction);
            ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vExtinctionBefore1Years", acc_persistentResults.vExtinctionBefore1Years);
            ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vExtinctionBefore2Years", acc_persistentResults.vExtinctionBefore2Years);
            ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vExtinctionBefore3Years", acc_persistentResults.vExtinctionBefore3Years);
            ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vExtinctionBefore5Years", acc_persistentResults.vExtinctionBefore5Years);
            ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vNoExtinctionBefore5Years", acc_persistentResults.vNoExtinctionBefore5Years);
        }
        
        
        
//        if (Parameters::withinHerdBiosecurity == false) {
//            ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vvDynamicHerdPrevalenceInfectedAfter5Years", acc_persistentResults.vvDynamicHerdPrevalenceInfectedAfter5Years);
//            ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vvDynamicHerdPrevalenceInfectiousAfter5Years", acc_persistentResults.vvDynamicHerdPrevalenceInfectiousAfter5Years);
//            ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vvDynamicHerdPrevalenceAffectedAfter5Years", acc_persistentResults.vvDynamicHerdPrevalenceAffectedAfter5Years);
//
//            ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vvPersistenceDynamicOver5Years", acc_persistentResults.vvPersistenceDynamicOver5Years);
//        }

        ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vvInfosHerdPrevalenceAfter5Years", acc_persistentResults.vvInfosHerdPrevalenceAfter5Years);
        
        ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vvPersistenceOver5Years", acc_persistentResults.vvPersistenceOver5Years);
        
        if (Parameters::withinHerdBiosecurity == false) {
            ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vSourceAndDestinationOfInfectedMovements_SourceIsFirst", acc_persistentResults.vSourceAndDestinationOfInfectedMovements_SourceIsFirst);
            ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vSourceAndDestinationOfInfectedMovements_SourceIsSecond", acc_persistentResults.vSourceAndDestinationOfInfectedMovements_SourceIsSecond);
            ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vSourceAndDestinationOfInfectedMovements_SourceIsExt", acc_persistentResults.vSourceAndDestinationOfInfectedMovements_SourceIsExt);
        }
        
        ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vNbInfectedMovementsPerHerd", acc_persistentResults.vNbInfectedMovementsPerHerd);
        
        
        ResultsWriting::openFileAndWrite("vNumberOfTSaleByPrimary", acc_persistentResults.vNumberOfTSaleByPrimary);
        ResultsWriting::openFileAndWrite("vNumberOfLSaleByPrimary", acc_persistentResults.vNumberOfLSaleByPrimary);
        ResultsWriting::openFileAndWrite("vNumberOfIsSaleByPrimary", acc_persistentResults.vNumberOfIsSaleByPrimary);
        ResultsWriting::openFileAndWrite("vNumberOfNewInfectionFromPrimary", acc_persistentResults.vNumberOfNewInfectionFromPrimary);
        
        if (Parameters::withinHerdBiosecurity == false) {
            //ResultsWriting::openFileAndWrite("vAccHeadcounts", acc_globalResults.vAccHeadcounts);
            ResultsWriting::openFileAndWrite("vAccAnnualHeadcounts", acc_globalResults.vAccAnnualHeadcounts);
            ResultsWriting::openFileAndWrite("vAccAnnualHeadcountsLact", acc_globalResults.vAccAnnualHeadcountsLact);
        }
        ResultsWriting::openFileAndWrite("vAccNbPres", acc_globalResults.vAccNbPres);
        ResultsWriting::openFileAndWrite("vAccNbPresLact", acc_globalResults.vAccNbPresLact);
//        if (Parameters::withinHerdBiosecurity == false) {
//            ResultsWriting::openFileAndWrite("vAccAnnualBirths", acc_globalResults.vAccAnnualBirths);
//            ResultsWriting::openFileAndWrite("vAccAnnualBirthsPerAgeGroup", acc_globalResults.vAccAnnualBirthsPerAgeGroup);
//            ResultsWriting::openFileAndWrite("vAccBirthsPerAgeGroup", acc_globalResults.vAccBirthsPerAgeGroup);
//        }

        if (nbPersistentSimu != 0) {
            ResultsWriting::openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfInfectedHerdsAfterHalfTheSimulationTime", acc_persistentResults.vvMatrixOfInfectedHerdsAfterHalfTheSimulationTime);
            ResultsWriting::openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfInfectedHerdsAtTheEndOfTheSimulationTime", acc_persistentResults.vvMatrixOfInfectedHerdsAtTheEndOfTheSimulationTime);
            ResultsWriting::openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfAlreadyInfectedHerdsAfterHalfTheSimulationTime", acc_persistentResults.vvMatrixOfAlreadyInfectedHerdsAfterHalfTheSimulationTime);
            ResultsWriting::openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfAlreadyInfectedHerdsAtTheEndOfTheSimulationTime", acc_persistentResults.vvMatrixOfAlreadyInfectedHerdsAtTheEndOfTheSimulationTime);
            
            ResultsWriting::openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfNumberOfInfection", acc_persistentResults.vvMatrixOfNumberOfInfection);
            ResultsWriting::openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfNumberOfInfectedAnimalsPurchased", acc_persistentResults.vvMatrixOfNumberOfInfectedAnimalsPurchased);
            ResultsWriting::openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfNumberOfWeeksWithInfection", acc_persistentResults.vvMatrixOfNumberOfWeeksWithInfection);
            
            ResultsWriting::openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvPrimaryCaseMatrix", acc_persistentResults.vvPrimaryCaseMatrix);
                        
            
//            ResultsWriting::openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfNumberOfAnimalsTestedMovIn", acc_persistentResults.vvMatrixOfNumberOfAnimalsTestedMovIn);
//            ResultsWriting::openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfNumberOfAnimalsTestedMovOut", acc_persistentResults.vvMatrixOfNumberOfAnimalsTestedMovOut);
            ResultsWriting::openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfNumberOfIcAnimalsCulled", acc_persistentResults.vvMatrixOfNumberOfIcAnimalsCulled);
            ResultsWriting::openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfNumberOfAnimalsCulledDuringTestAndCull", acc_persistentResults.vvMatrixOfNumberOfAnimalsCulledDuringTestAndCull);
            ResultsWriting::openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfNumberOfAnimalsTestedDuringTestAndCull", acc_persistentResults.vvMatrixOfNumberOfAnimalsTestedDuringTestAndCull);
        }
        

        ResultsWriting::openFileAndWrite("vMedianInfectedHerds", acc_persistentResults.vMedianInfectedHerds);
        ResultsWriting::openFileAndWrite("vInfectedHerds", acc_persistentResults.vInfectedHerds);
        ResultsWriting::openFileAndWrite("vAlreadyInfectedHerds", acc_persistentResults.vAlreadyInfectedHerds);
        ResultsWriting::openFileAndWrite("vAffectedHerds", acc_persistentResults.vAffectedHerds);
        
        ResultsWriting::openFileAndWrite("vInfectedProportionMeta", acc_persistentResults.vInfectedProportionMeta);
        ResultsWriting::openFileAndWrite("vInfectiousProportionMeta", acc_persistentResults.vInfectiousProportionMeta);
        ResultsWriting::openFileAndWrite("vAffectedProportionMeta", acc_persistentResults.vAffectedProportionMeta);
        
        ResultsWriting::openFileAndWrite("accNbHerdWithTestOfMovements", acc_persistentResults.accNbHerdWithTestOfMovements);
        ResultsWriting::openFileAndWrite("accNbHerdWithCullingImprovement", acc_persistentResults.accNbHerdWithCullingImprovement);
        ResultsWriting::openFileAndWrite("accNbHerdWithHygieneImprovement", acc_persistentResults.accNbHerdWithHygieneImprovement);
        ResultsWriting::openFileAndWrite("accNbHerdWithCalfManagementImprovement", acc_persistentResults.accNbHerdWithCalfManagementImprovement);
        ResultsWriting::openFileAndWrite("accNbHerdWithTestAndCull", acc_persistentResults.accNbHerdWithTestAndCull);
        
        saveIntraHerdPrevalenceAtTheEnd();



        ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex("vvMatrixOfWithinHerdPrevalenceOverTime_mean", acc_persistentResults.vvMatrixOfWithinHerdPrevalenceOverTime);

//        ResultsWriting::openFileAndWrite_mAcc("mFarms_AccVectWithinHerdPrevalence", acc_persistentResults.mFarms_AccVectWithinHerdPrevalence);
        std::string fileName = "mFarms_AccVectWithinHerdPrevalence";
        // MEAN
        std::string completeFileName_mean = fileName + "_mean.csv";
        std::ofstream ost_mean(Parameters::ResultsFolderPath + completeFileName_mean, std::fstream::out);
        if(!ost_mean){std::cerr << "[ERROR] can't open output file : " << completeFileName_mean << std::endl;}
        // VARIANCE
        std::string completeFileName_variance = fileName + "_variance.csv";
        std::ofstream ost_variance(Parameters::ResultsFolderPath + completeFileName_variance, std::fstream::out);
        if(!ost_variance){std::cerr << "[ERROR] can't open output file : " << completeFileName_variance << std::endl;}
        for (auto farmID : Parameters::vFarmID){
            ost_mean << farmID << ",";
            ost_variance << farmID << ",";
            // MEAN
            std::vector<double> vMean = mean(acc_persistentResults.mFarms_AccVectWithinHerdPrevalence[farmID]);
            for (int i=0; i<vMean.size(); i++) {
                if (i < vMean.size()-1) {
                    ost_mean << vMean[i] << ",";
                } else {
                    ost_mean << vMean[i];
                }
            }
            ost_mean << "\n";
            // VARIANCE
            std::vector<double> vVariance = variance(acc_persistentResults.mFarms_AccVectWithinHerdPrevalence[farmID]);
            for (int i=0; i<vVariance.size(); i++) {
                if (i < vVariance.size()-1) {
                    ost_variance << vVariance[i] << ",";
                } else {
                    ost_variance << vVariance[i];
                }
            }
            ost_variance << "\n";
        }


    }
#endif
    
    if (Parameters::ABCrejection == true) {
        iABCInfosAndResults.saveABCrejectionResults();
    }
    
    if (Parameters::ABCsmc == true) {
        std::string fileName = "ABCsmcSequenceCompleted_" + std::to_string(Parameters::ABCsmcSequenceNumber);
        std::ofstream ost_ABCsmcCompleted(Parameters::ABCfolderPath + "ABCsmcSequencesCompleted/" + fileName + ".csv", std::fstream::out);
        if(!ost_ABCsmcCompleted){std::cerr << "[ERROR] can't open output file : " + fileName + ".csv" << std::endl;} // Check if the file is open!
        // Writing ...
        ost_ABCsmcCompleted << "ABCsmc sequence " + std::to_string(Parameters::ABCsmcSequenceNumber) + " is completed!" << "\n";
    }
}



void BetweenHerdDynamic::saveInfectionDynamic(int run, std::map<std::string, DairyHerd>& mFarms){
    if (run < 5) {
        std::ofstream ost(Parameters::ResultsFolderPath + "infectionDynamic/dynamic_" + std::to_string(run) +".csv", std::fstream::out);
        if(!ost){std::cerr << "[ERROR] can't open output file : infectionDynamic" << std::endl;}
        
        for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
            for (int i = 0; i < it->second.vInfectionDynamic.size(); i++) {
                ost << it->second.vInfectionDynamic[i] << "\t";
            }
            ost << "\n";
        }
    }
    
}



void BetweenHerdDynamic::updateAccSecondPrev(std::map<std::string, DairyHerd>& mFarms){
    for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
        if (it->second.typeInfectedHerd == 2) {
            if (it->second.persistenceIndicator) {
                if (it->second.numberOfWeeksSinceInfected>(Parameters::simutime-Parameters::timestepInOneYear)) {
                    for (int i = 0; i<it->second.numberOfWeeksSinceInfected; i++) {
                        double bInfectedSecond = it->second.iSummaryDynamic.infected[Parameters::simutime-1-it->second.numberOfWeeksSinceInfected+i];
                        double bInfectiousSecond = it->second.iSummaryDynamic.infectious[Parameters::simutime-1-it->second.numberOfWeeksSinceInfected+i];
                        double bAffectedSecond = it->second.iSummaryDynamic.affected[Parameters::simutime-1-it->second.numberOfWeeksSinceInfected+i];
                        double bHeadcountSecond = it->second.iSummaryDynamic.headcount[Parameters::simutime-1-it->second.numberOfWeeksSinceInfected+i];
                        
                        acc_persistentResults.accInfectedProportionSecond[i](bInfectedSecond/bHeadcountSecond);
                        acc_persistentResults.accInfectiousProportionSecond[i](bInfectiousSecond/bHeadcountSecond);
                        acc_persistentResults.accAffectedProportionSecond[i](bAffectedSecond/bHeadcountSecond);
                    }
                }
            }
        }
    }
}



void BetweenHerdDynamic::updateIntraHerdPrevalenceAtTheEnd(std::map<std::string, DairyHerd>& mFarms){
    for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
        double prev = static_cast<double>(it->second.iSummaryDynamic.infected[Parameters::simutime-1])/static_cast<double>(it->second.iSummaryDynamic.headcount[Parameters::simutime-1]);
        vvIntraHerdPrevalenceAtTheEnd[it->first].emplace_back(prev);
    }
}

void BetweenHerdDynamic::saveIntraHerdPrevalenceAtTheEnd(){
    std::ofstream ost(Parameters::ResultsFolderPath + "vvIntraHerdPrevalenceAtTheEnd.csv", std::fstream::out);
    if(!ost){std::cerr << "[ERROR] can't open output file : vvIntraHerdPrevalenceAtTheEnd.csv" << std::endl;}

    ost << "herd";
    for (int i = Parameters::firstRunID; i<=(Parameters::nbRuns+(Parameters::firstRunID-1)-Parameters::nbRunsWithNan); i++) {
        ost << "\t" << "prev_" << i;
    }
    ost << "\n";
    
    for (auto it = vvIntraHerdPrevalenceAtTheEnd.begin(); it != vvIntraHerdPrevalenceAtTheEnd.end(); ++it) {
        ost << it->first << "\t";
        for (int i = 0; i < it->second.size(); i++) {
            if (i < it->second.size()-1) {
                ost << it->second[i] << "\t";
            } else {
                ost << it->second[i];
            }
        }
        ost << "\n";
    }
}



void BetweenHerdDynamic::saveWithinHerdPrevalenceOverTime(std::map<std::string, DairyHerd>& mFarms, const int& theRun){
    std::map<std::string ,std::vector<double>> matrixOfWithinHerdPrevalence;
    for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
        std::vector<double> prev = U_functions::VectorDivision(it->second.iSummaryDynamic.infected, it->second.iSummaryDynamic.headcount);
        matrixOfWithinHerdPrevalence[it->first] = prev;
    }
    ResultsWriting::openFileAndWriteMatrix_farmsInLine_timeInColumn_forTheCurrentRun("matrixOfWithinHerdPrevalence", matrixOfWithinHerdPrevalence, theRun);
}



