//
//  SummaryDynamic.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 01/08/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#include "SummaryDynamic.h"

// Constructor =========================================================
SummaryDynamic::SummaryDynamic(){
    // Variables ===========================================================
    headcount.resize(Parameters::simutime,0);
    infected.resize(Parameters::simutime,0);
    infectious.resize(Parameters::simutime,0);
    affected.resize(Parameters::simutime,0);
    
    headcountLact.resize(Parameters::simutime,0);
    
    extinct = false;
}


// Member functions ====================================================
void SummaryDynamic::update(const int& t, const HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer){
    if (Parameters::managementOfPositiveTestedTakeIntoAccount) {
        headcount[t] = iHealthStateCompartmentsContainer.iCompartmentSR.total[t] + iHealthStateCompartmentsContainer.iCompartmentT.total[t] + iHealthStateCompartmentsContainer.iCompartmentL.total[t] + iHealthStateCompartmentsContainer.iCompartmentIs.total[t] + iHealthStateCompartmentsContainer.iCompartmentIc.total[t] + iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.total[t] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.total[t] + iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.total[t] + iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.total[t] + iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.total[t];
        infected[t] = iHealthStateCompartmentsContainer.iCompartmentT.total[t] + iHealthStateCompartmentsContainer.iCompartmentL.total[t] + iHealthStateCompartmentsContainer.iCompartmentIs.total[t] + iHealthStateCompartmentsContainer.iCompartmentIc.total[t] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.total[t] + iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.total[t] + iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.total[t] + iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.total[t];
        infectious[t] = iHealthStateCompartmentsContainer.iCompartmentT.total[t] + iHealthStateCompartmentsContainer.iCompartmentIs.total[t] + iHealthStateCompartmentsContainer.iCompartmentIc.total[t] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.total[t] + iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.total[t] + iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.total[t];
        affected[t] = iHealthStateCompartmentsContainer.iCompartmentIc.total[t] + iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.total[t];
        headcountLact[t] = iHealthStateCompartmentsContainer.iCompartmentSR.lactAll[t] + iHealthStateCompartmentsContainer.iCompartmentT.lactAll[t] + iHealthStateCompartmentsContainer.iCompartmentL.lactAll[t] + iHealthStateCompartmentsContainer.iCompartmentIs.lactAll[t] + iHealthStateCompartmentsContainer.iCompartmentIc.lactAll[t] + iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.lactAll[t] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.lactAll[t] + iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.lactAll[t] + iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.lactAll[t] + iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.lactAll[t];
    } else {
        headcount[t] = iHealthStateCompartmentsContainer.iCompartmentSR.total[t] + iHealthStateCompartmentsContainer.iCompartmentT.total[t] + iHealthStateCompartmentsContainer.iCompartmentL.total[t] + iHealthStateCompartmentsContainer.iCompartmentIs.total[t] + iHealthStateCompartmentsContainer.iCompartmentIc.total[t];
        infected[t] = iHealthStateCompartmentsContainer.iCompartmentT.total[t] + iHealthStateCompartmentsContainer.iCompartmentL.total[t] + iHealthStateCompartmentsContainer.iCompartmentIs.total[t] + iHealthStateCompartmentsContainer.iCompartmentIc.total[t];
        infectious[t] = iHealthStateCompartmentsContainer.iCompartmentT.total[t] + iHealthStateCompartmentsContainer.iCompartmentIs.total[t] + iHealthStateCompartmentsContainer.iCompartmentIc.total[t];
        affected[t] = iHealthStateCompartmentsContainer.iCompartmentIc.total[t];
        headcountLact[t] = iHealthStateCompartmentsContainer.iCompartmentSR.lactAll[t] + iHealthStateCompartmentsContainer.iCompartmentT.lactAll[t] + iHealthStateCompartmentsContainer.iCompartmentL.lactAll[t] + iHealthStateCompartmentsContainer.iCompartmentIs.lactAll[t] + iHealthStateCompartmentsContainer.iCompartmentIc.lactAll[t];
    }
    
//    assert(headcount[t] >= 0 & headcount[t] < 5000);
    if (headcount[t] < 0 | headcount[t] > 1000) {
        std::cout << "Size pbm - " << t << " | " << headcount[t-1] << " " << headcount[t] << " " << infected[t] << std::endl;
//        for (int age = 0; age <= Parameters::nbStage; age++) {
//            std::cout << iHealthStateCompartmentsContainer.iCompartmentSR.past[age] << " ";
//        }
//        std::cout << std::endl;
//        for (int age = 0; age <= Parameters::nbStage; age++) {
//            std::cout << iHealthStateCompartmentsContainer.iCompartmentT.past[age] << " ";
//        }
//        std::cout << std::endl;
//        for (int age = 0; age <= Parameters::nbStage; age++) {
//            std::cout << iHealthStateCompartmentsContainer.iCompartmentL.past[age] << " ";
//        }
//        std::cout << std::endl;
//        for (int age = 0; age <= Parameters::nbStage; age++) {
//            std::cout << iHealthStateCompartmentsContainer.iCompartmentIs.past[age] << " ";
//        }
//        std::cout << std::endl;
//        for (int age = 0; age <= Parameters::nbStage; age++) {
//            std::cout << iHealthStateCompartmentsContainer.iCompartmentIc.past[age] << " ";
//        }
//        std::cout << std::endl << std::endl;
    }
}



void SummaryDynamic::PastUpdate(const int& t, const HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer){
    if (Parameters::managementOfPositiveTestedTakeIntoAccount) {
        headcount[t-1] = iHealthStateCompartmentsContainer.iCompartmentSR.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentT.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentL.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIs.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIc.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.total[t-1];
        infected[t-1] = iHealthStateCompartmentsContainer.iCompartmentT.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentL.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIs.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIc.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.total[t-1];
        infectious[t-1] = iHealthStateCompartmentsContainer.iCompartmentT.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIs.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIc.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.total[t-1];
        affected[t-1] = iHealthStateCompartmentsContainer.iCompartmentIc.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.total[t-1];
        headcountLact[t-1] = iHealthStateCompartmentsContainer.iCompartmentSR.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentT.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentL.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentIs.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentIc.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.lactAll[t-1];
    } else {
        headcount[t-1] = iHealthStateCompartmentsContainer.iCompartmentSR.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentT.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentL.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIs.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIc.total[t-1];
        infected[t-1] = iHealthStateCompartmentsContainer.iCompartmentT.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentL.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIs.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIc.total[t-1];
        infectious[t-1] = iHealthStateCompartmentsContainer.iCompartmentT.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIs.total[t-1] + iHealthStateCompartmentsContainer.iCompartmentIc.total[t-1];
        affected[t-1] = iHealthStateCompartmentsContainer.iCompartmentIc.total[t-1];
        headcountLact[t-1] = iHealthStateCompartmentsContainer.iCompartmentSR.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentT.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentL.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentIs.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentIc.lactAll[t-1];
    }
    
//    assert(headcount[t-1] >= 0 & headcount[t-1] < 5000);
//    if (headcount[t-1] < 0 | headcount[t-1] > 1000) {
//        std::cout << "Size pbm - " << t << " | " << headcount[t-1] << std::endl;
//    }
}




