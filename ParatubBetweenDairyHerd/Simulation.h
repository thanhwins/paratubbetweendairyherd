//
//  Simulation.h
//  ParatubBetweenDairyHerd
//
//  Created by Gaël Beaunée on 07/01/2014.
//  Copyright (c) 2014 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_Simulation_h
#define ParatubBetweenDairyHerd_Simulation_h

#include <iostream>
#include <vector>
#include <map>
#include <unordered_map>

#include "BetweenHerdDynamic.h"
#include "MovementMatrix.h"

class Simulation {
    // Variables ===========================================================
    MovementMatrix& iMovementMatrix = MovementMatrix::get_instance();
    
    BetweenHerdDynamic iBetweenHerdDynamic = BetweenHerdDynamic();
    
public:
    // Constructor =========================================================
    Simulation();
    
    // Variables ===========================================================
    
    // Member functions ====================================================
    void Simulate();
};

#endif
