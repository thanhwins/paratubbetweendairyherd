//
//  ParametersSpecificToEachRepetition.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 24/06/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_ParametersSpecificToEachRepetition_h
#define ParatubBetweenDairyHerd_ParametersSpecificToEachRepetition_h

#include <iostream>
#include <vector>
//#include <cmath>
#include <gsl/gsl_rng.h>
#include <string>
#include <map>
#include <random>
#include <ctime>
#include <cmath>

#include "ABCsmcSetOfPreviousParticles.h"
#include "ABCsmcParticle.h"
#include "Parameters.h"
#include "UtilitiesFunctions.h"

class ParametersSpecificToEachRepetition {
public:
    // Constructor =========================================================
    ParametersSpecificToEachRepetition(int rep);
    
    // Variables ===========================================================
    // Seed and random number generator
    gsl_rng * randomGenerator;
    long long Seed;
    
    // Parameters specific to the repetition
    ABCsmcParticle theABCsmcParticle;
    double distanceBetweenSimulationAndObservationForThisRun = 0.;
    
    double probaToPurchaseInfectedAnimal_initialValue;
    double probaToPurchaseInfectedAnimal_slope;
    
    double probaToPurchaseInfectedAnimal_currentValue;
    
    double abcTestSensitivity;
    
    double abcInfGlobalEnv_log;
    double abcInfGlobalEnv;
    double abcInfGlobalEnvIfCalfManagementImprovement;
    
    double abcMuDistributionPrevIntra;
    double abcSigmaDistributionPrevIntra;
    
    // Member functions ====================================================
    void setParameterValuesForABC(const int& runNumber, std::map<std::string, ABCsmcSetOfPreviousParticles>& previousParticles);
    void getValueForProbaToPurchaseInfectedAnimal();
    void getPerturbedValueForProbaToPurchaseInfectedAnimal(std::map<std::string, ABCsmcSetOfPreviousParticles>& previousParticles);
    void getValueAbcTestSensitivity();
    void getPerturbedValueAbcTestSensitivity(std::map<std::string, ABCsmcSetOfPreviousParticles>& previousParticles);
    void getValueAbcInfGlobalEnv();
    void getPerturbedValueAbcInfGlobalEnv(std::map<std::string, ABCsmcSetOfPreviousParticles>& previousParticles);
    
    void updateValueForProbaToPurchaseInfectedAnimal(const int& timeStep);
    
};

#endif
