//
//  VectorMinus.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 03/08/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef VectorMinus_h
#define VectorMinus_h

#include <vector>
#include <algorithm>
#include <functional>
#include <assert.h>

template <typename T>
std::vector<T> operator-(const std::vector<T>& a, const std::vector<T>& b)
{
    assert(a.size() == b.size());
    
    std::vector<T> result;
    result.reserve(a.size());
    
    std::transform(a.begin(), a.end(), b.begin(),
                   std::back_inserter(result), std::minus<T>());
    return result;
}

#endif
