//
//  HerdExit.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 28/06/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#include "HerdExit.h"

//std::vector<int> HerdExit::salesOverTimeCalfSR;
//std::vector<int> HerdExit::salesOverTimeCalfTLIsIc;
//std::vector<int> HerdExit::salesOverTimeHeiferSR;
//std::vector<int> HerdExit::salesOverTimeHeiferTLIsIc;

// Constructor =========================================================
HerdExit::HerdExit(){
//    salesOverTimeCalfSR.resize(Parameters::simutime,0);
//    salesOverTimeCalfTLIsIc.resize(Parameters::simutime,0);
//    salesOverTimeHeiferSR.resize(Parameters::simutime,0);
//    salesOverTimeHeiferTLIsIc.resize(Parameters::simutime,0);
}


// Member functions ====================================================

int HerdExit::Update(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& t, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer){
    
    int IcCulledThisTimeStep = 0;
    
    
    PopulationDynamicFromAgeOneToAgeFirstCalvingMinus0ne(randomGenerator, iParametersSpecificToEachHerd, t, iHealthStateCompartmentsContainer);
    

    int adultsEnteringTheAgeClass_SR = 0;
    int adultsLeavingTheAgeClass_SR = 0;
    
    int adultsEnteringTheAgeClass_L = 0;
    int adultsLeavingTheAgeClass_L = 0;
    
    int adultsEnteringTheAgeClass_Is = 0;
    int adultsLeavingTheAgeClass_Is = 0;
    
    int adultsEnteringTheAgeClass_Ic = 0;
    int adultsLeavingTheAgeClass_Ic = 0;
    
    adultsLeavingTheAgeClass_SR = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentSR.past[Parameters::ageFirstCalving]);
    adultsLeavingTheAgeClass_L = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentL.past[Parameters::ageFirstCalving]);
    adultsLeavingTheAgeClass_Is = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentIs.past[Parameters::ageFirstCalving]);
    adultsLeavingTheAgeClass_Ic = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentIc.past[Parameters::ageFirstCalving]);
    
    
//    int adultsLeavingTheAgeClass_T = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentT.past[Parameters::ageFirstCalving]);
//    if (adultsLeavingTheAgeClass_T > 0) {
//        std::cout << "adultsLeavingTheAgeClass_T (a): " << adultsLeavingTheAgeClass_T << std::endl;
//    }
    
    
    iHealthStateCompartmentsContainer.iCompartmentSR.present[Parameters::ageFirstCalving] = iHealthStateCompartmentsContainer.iCompartmentSR.present[Parameters::ageFirstCalving] + gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving-Parameters::ageFirstCalving], iHealthStateCompartmentsContainer.iCompartmentSR.past[Parameters::ageFirstCalving]-adultsLeavingTheAgeClass_SR);
    
    iHealthStateCompartmentsContainer.iCompartmentL.present[Parameters::ageFirstCalving] = iHealthStateCompartmentsContainer.iCompartmentL.present[Parameters::ageFirstCalving] + gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving-Parameters::ageFirstCalving], iHealthStateCompartmentsContainer.iCompartmentL.past[Parameters::ageFirstCalving]-adultsLeavingTheAgeClass_L);
    
    iHealthStateCompartmentsContainer.iCompartmentIs.present[Parameters::ageFirstCalving] = iHealthStateCompartmentsContainer.iCompartmentIs.present[Parameters::ageFirstCalving] + gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving-Parameters::ageFirstCalving])*(1-Parameters::cullingRateIs), iHealthStateCompartmentsContainer.iCompartmentIs.past[Parameters::ageFirstCalving]-adultsLeavingTheAgeClass_Is);
    
    int buff1 = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.cullingRateIcInThisFarm, iHealthStateCompartmentsContainer.iCompartmentIc.past[Parameters::ageFirstCalving]-adultsLeavingTheAgeClass_Ic);
    iHealthStateCompartmentsContainer.iCompartmentIc.present[Parameters::ageFirstCalving] = iHealthStateCompartmentsContainer.iCompartmentIc.present[Parameters::ageFirstCalving] + buff1;
    IcCulledThisTimeStep += (iHealthStateCompartmentsContainer.iCompartmentIc.past[Parameters::ageFirstCalving] - adultsLeavingTheAgeClass_Ic - buff1);


    for (int age = Parameters::ageFirstCalving+1; age <= Parameters::ageFirstCalving+3; age++) {
        adultsEnteringTheAgeClass_SR = adultsLeavingTheAgeClass_SR;
        adultsLeavingTheAgeClass_SR = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentSR.past[age]);
        int adultsEnteringTheAgeClass_SR_alive = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving-1], adultsEnteringTheAgeClass_SR);
        int adultsNotLeavingTheAgeClass_SR_alive = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving], iHealthStateCompartmentsContainer.iCompartmentSR.past[age]-adultsLeavingTheAgeClass_SR);
        iHealthStateCompartmentsContainer.iCompartmentSR.present[age] = adultsEnteringTheAgeClass_SR_alive + adultsNotLeavingTheAgeClass_SR_alive;

        adultsEnteringTheAgeClass_L = adultsLeavingTheAgeClass_L;
        adultsLeavingTheAgeClass_L = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentL.past[age]);
        int adultsEnteringTheAgeClass_L_alive = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving-1], adultsEnteringTheAgeClass_L);
        int adultsNotLeavingTheAgeClass_L_alive = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving], iHealthStateCompartmentsContainer.iCompartmentL.past[age]-adultsLeavingTheAgeClass_L);
        iHealthStateCompartmentsContainer.iCompartmentL.present[age] = adultsEnteringTheAgeClass_L_alive + adultsNotLeavingTheAgeClass_L_alive;
        
        adultsEnteringTheAgeClass_Is = adultsLeavingTheAgeClass_Is;
        adultsLeavingTheAgeClass_Is = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentIs.past[age]);
        int adultsEnteringTheAgeClass_Is_alive = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving-1])*(1-Parameters::cullingRateIs), adultsEnteringTheAgeClass_Is);
        int adultsNotLeavingTheAgeClass_Is_alive = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving])*(1-Parameters::cullingRateIs), iHealthStateCompartmentsContainer.iCompartmentIs.past[age]-adultsLeavingTheAgeClass_Is);
        iHealthStateCompartmentsContainer.iCompartmentIs.present[age] = adultsEnteringTheAgeClass_Is_alive + adultsNotLeavingTheAgeClass_Is_alive;

        adultsEnteringTheAgeClass_Ic = adultsLeavingTheAgeClass_Ic;
        adultsLeavingTheAgeClass_Ic = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentIc.past[age]);
        int adultsEnteringTheAgeClass_Ic_alive = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.cullingRateIcInThisFarm, adultsEnteringTheAgeClass_Ic);
        IcCulledThisTimeStep += (adultsEnteringTheAgeClass_Ic-adultsEnteringTheAgeClass_Ic_alive);
        int adultsNotLeavingTheAgeClass_Ic_alive = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.cullingRateIcInThisFarm, iHealthStateCompartmentsContainer.iCompartmentIc.past[age]-adultsLeavingTheAgeClass_Ic);
        iHealthStateCompartmentsContainer.iCompartmentIc.present[age] = adultsEnteringTheAgeClass_Ic_alive + adultsNotLeavingTheAgeClass_Ic_alive;
        IcCulledThisTimeStep += (iHealthStateCompartmentsContainer.iCompartmentIc.past[age]-adultsLeavingTheAgeClass_Ic - adultsNotLeavingTheAgeClass_Ic_alive);
    }

    
    adultsEnteringTheAgeClass_SR = adultsLeavingTheAgeClass_SR;
    int adultsEnteringTheAgeClass_SR_alive = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vCullingRate[4-1], adultsEnteringTheAgeClass_SR);
    int adultsNotLeavingTheAgeClass_SR_alive = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving+4-Parameters::ageFirstCalving], iHealthStateCompartmentsContainer.iCompartmentSR.past[Parameters::ageFirstCalving+4]);
    iHealthStateCompartmentsContainer.iCompartmentSR.present[Parameters::ageFirstCalving+4] = adultsEnteringTheAgeClass_SR_alive + adultsNotLeavingTheAgeClass_SR_alive;
    
    adultsEnteringTheAgeClass_L = adultsLeavingTheAgeClass_L;
    int adultsEnteringTheAgeClass_L_alive = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vCullingRate[4-1], adultsEnteringTheAgeClass_L);
    int adultsNotLeavingTheAgeClass_L_alive = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving+4-Parameters::ageFirstCalving], iHealthStateCompartmentsContainer.iCompartmentL.past[Parameters::ageFirstCalving+4]);
    iHealthStateCompartmentsContainer.iCompartmentL.present[Parameters::ageFirstCalving+4] = adultsEnteringTheAgeClass_L_alive + adultsNotLeavingTheAgeClass_L_alive;
    
    adultsEnteringTheAgeClass_Is = adultsLeavingTheAgeClass_Is;
    int adultsEnteringTheAgeClass_Is_alive = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[4-1])*(1-Parameters::cullingRateIs), adultsEnteringTheAgeClass_Is);
    int adultsNotLeavingTheAgeClass_Is_alive = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving+4-Parameters::ageFirstCalving])*(1-Parameters::cullingRateIs), iHealthStateCompartmentsContainer.iCompartmentIs.past[Parameters::ageFirstCalving+4]);
    iHealthStateCompartmentsContainer.iCompartmentIs.present[Parameters::ageFirstCalving+4] = adultsEnteringTheAgeClass_Is_alive + adultsNotLeavingTheAgeClass_Is_alive;
    
    adultsEnteringTheAgeClass_Ic = adultsLeavingTheAgeClass_Ic;
    int adultsEnteringTheAgeClass_Ic_alive = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.cullingRateIcInThisFarm, adultsEnteringTheAgeClass_Ic);
    IcCulledThisTimeStep += (adultsEnteringTheAgeClass_Ic-adultsEnteringTheAgeClass_Ic_alive);
    int adultsNotLeavingTheAgeClass_Ic_alive = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.cullingRateIcInThisFarm, iHealthStateCompartmentsContainer.iCompartmentIc.past[Parameters::ageFirstCalving+4]);
    iHealthStateCompartmentsContainer.iCompartmentIc.present[Parameters::ageFirstCalving+4] = adultsEnteringTheAgeClass_Ic_alive + adultsNotLeavingTheAgeClass_Ic_alive;
    IcCulledThisTimeStep += (iHealthStateCompartmentsContainer.iCompartmentIc.past[Parameters::ageFirstCalving+4] - adultsNotLeavingTheAgeClass_Ic_alive);
    
    
    IcCulledThisTimeStep += PopulationDynamicConcerningPositiveTestedAnimals(randomGenerator, iParametersSpecificToEachHerd, t, iHealthStateCompartmentsContainer);
    
    
    return IcCulledThisTimeStep;
}



void HerdExit::PopulationDynamicFromAgeOneToAgeFirstCalvingMinus0ne(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& t, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer){
    for (int age = 1; age <= Parameters::ageYheifer; age++) {
        iHealthStateCompartmentsContainer.iCompartmentSR.present[age] = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vDeathRate[age-1], iHealthStateCompartmentsContainer.iCompartmentSR.past[age-1]);
        iHealthStateCompartmentsContainer.iCompartmentT.present[age] = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vDeathRate[age-1], iHealthStateCompartmentsContainer.iCompartmentT.past[age-1]);
        iHealthStateCompartmentsContainer.iCompartmentL.present[age] = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vDeathRate[age-1], iHealthStateCompartmentsContainer.iCompartmentL.past[age-1]);
    }
    
    for (int age = (Parameters::ageYheifer+1); age <= (Parameters::ageFirstCalving-1); age++) {
        iHealthStateCompartmentsContainer.iCompartmentSR.present[age] = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vDeathRate[age-1], iHealthStateCompartmentsContainer.iCompartmentSR.past[age-1]);
        iHealthStateCompartmentsContainer.iCompartmentT.present[age] = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vDeathRate[age-1], iHealthStateCompartmentsContainer.iCompartmentT.past[age-1]);
        iHealthStateCompartmentsContainer.iCompartmentL.present[age] = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vDeathRate[age-1], iHealthStateCompartmentsContainer.iCompartmentL.past[age-1]);
        iHealthStateCompartmentsContainer.iCompartmentIs.present[age] = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vDeathRate[age-1])*(1-Parameters::deathRateIs), iHealthStateCompartmentsContainer.iCompartmentIs.past[age-1]);
        iHealthStateCompartmentsContainer.iCompartmentIc.present[age] = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.cullingRateIcInThisFarm, iHealthStateCompartmentsContainer.iCompartmentIc.past[age-1]);
    }
    
    iHealthStateCompartmentsContainer.iCompartmentSR.present[Parameters::ageFirstCalving] = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vDeathRate[Parameters::ageFirstCalving-1], iHealthStateCompartmentsContainer.iCompartmentSR.past[Parameters::ageFirstCalving-1]);
    iHealthStateCompartmentsContainer.iCompartmentL.present[Parameters::ageFirstCalving] = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vDeathRate[Parameters::ageFirstCalving-1], iHealthStateCompartmentsContainer.iCompartmentL.past[Parameters::ageFirstCalving-1]);
    iHealthStateCompartmentsContainer.iCompartmentIs.present[Parameters::ageFirstCalving] = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vDeathRate[Parameters::ageFirstCalving-1])*(1-Parameters::deathRateIs), iHealthStateCompartmentsContainer.iCompartmentIs.past[Parameters::ageFirstCalving-1]);
    iHealthStateCompartmentsContainer.iCompartmentIc.present[Parameters::ageFirstCalving] = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.cullingRateIcInThisFarm, iHealthStateCompartmentsContainer.iCompartmentIc.past[Parameters::ageFirstCalving-1]);
    
    iHealthStateCompartmentsContainer.iCompartmentL.present[Parameters::ageFirstCalving] = iHealthStateCompartmentsContainer.iCompartmentL.present[Parameters::ageFirstCalving] + gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.vDeathRate[Parameters::ageFirstCalving-1], iHealthStateCompartmentsContainer.iCompartmentT.past[Parameters::ageFirstCalving-1]);
}



int HerdExit::Update_bis(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& t, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer){
    int IcCulledThisTimeStep = 0;

    int adultsEnteringTheAgeClass_SR = 0;
    int adultsLeavingTheAgeClass_SR = 0;
    int adultsEnteringTheAgeClass_SR_positiveTested = 0;
    int adultsLeavingTheAgeClass_SR_positiveTested = 0;

    int adultsEnteringTheAgeClass_T = 0;
    int adultsLeavingTheAgeClass_T = 0;
    int adultsEnteringTheAgeClass_T_positiveTested = 0;
    int adultsLeavingTheAgeClass_T_positiveTested = 0;

    int adultsEnteringTheAgeClass_L = 0;
    int adultsLeavingTheAgeClass_L = 0;
    int adultsEnteringTheAgeClass_L_positiveTested = 0;
    int adultsLeavingTheAgeClass_L_positiveTested = 0;

    int adultsEnteringTheAgeClass_Is = 0;
    int adultsLeavingTheAgeClass_Is = 0;
    int adultsEnteringTheAgeClass_Is_positiveTested = 0;
    int adultsLeavingTheAgeClass_Is_positiveTested = 0;

    int adultsEnteringTheAgeClass_Ic = 0;
    int adultsLeavingTheAgeClass_Ic = 0;
    int adultsEnteringTheAgeClass_Ic_positiveTested = 0;
    int adultsLeavingTheAgeClass_Ic_positiveTested = 0;

    for (int age = 1; age <= (Parameters::ageFirstCalving); age++) {
        // get the number of animals in this age group
        //
        int numberOfAnimalsInPreviousAgeInPastVector_Ic = iHealthStateCompartmentsContainer.iCompartmentIc.past[age-1];
        //
        int numberOfAnimalsInPreviousAgeInPastVector_positiveTested = iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[age-1] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.past[age-1] + iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past[age-1] + iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past[age-1] + iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[age-1];
        //
        int numberOfAnimalsInPreviousAgeInPastVector_positiveTestedIc = iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[age-1];
        //
        int numberOfAnimalsInPreviousAgeInPastVectorWithoutIc = iHealthStateCompartmentsContainer.iCompartmentSR.past[age-1] + iHealthStateCompartmentsContainer.iCompartmentT.past[age-1] + iHealthStateCompartmentsContainer.iCompartmentL.past[age-1] + iHealthStateCompartmentsContainer.iCompartmentIs.past[age-1];
        // all animals
        int numberOfAnimalsInPreviousAgeInPastVector_all = numberOfAnimalsInPreviousAgeInPastVectorWithoutIc + numberOfAnimalsInPreviousAgeInPastVector_Ic + numberOfAnimalsInPreviousAgeInPastVector_positiveTested;
        // set variables containing the number of death to dispatch in the compartments
        //
        int numberOfAnimalsInPreviousAgeInPastVector_Ic_dead = gsl_ran_binomial(randomGenerator, iParametersSpecificToEachHerd.cullingRateIcInThisFarm, numberOfAnimalsInPreviousAgeInPastVector_Ic);
        //
        int numberOfAnimalsInPreviousAgeInPastVector_positiveTestedIc_dead = gsl_ran_binomial(randomGenerator, iParametersSpecificToEachHerd.cullingRateIcInThisFarm, numberOfAnimalsInPreviousAgeInPastVector_positiveTestedIc);
        //
        int numberOfAnimalsInPreviousAgeInPastVector_positiveTested_dead = gsl_ran_binomial(randomGenerator, Parameters::cullingRatePositiveTestedAnimals, numberOfAnimalsInPreviousAgeInPastVector_positiveTested-numberOfAnimalsInPreviousAgeInPastVector_positiveTestedIc_dead);
        //
        int numberOfAnimalsInPreviousAgeInPastVector_all_dead = gsl_ran_binomial(randomGenerator, iParametersSpecificToEachHerd.vDeathRate[age-1], numberOfAnimalsInPreviousAgeInPastVector_all);
        //
        int remainingDeadAnimalsToDispatch = 0;
        int death_Ic_and_Pos = numberOfAnimalsInPreviousAgeInPastVector_Ic_dead + numberOfAnimalsInPreviousAgeInPastVector_positiveTestedIc_dead + numberOfAnimalsInPreviousAgeInPastVector_positiveTested_dead;
        if (numberOfAnimalsInPreviousAgeInPastVector_all_dead < death_Ic_and_Pos) {
            iHealthStateCompartmentsContainer.extraDeath[age-1] += (death_Ic_and_Pos - numberOfAnimalsInPreviousAgeInPastVector_all_dead);
        } else { // if (numberOfAnimalsInPreviousAgeInPastVector_all_dead > death_Ic_and_Pos)
            remainingDeadAnimalsToDispatch = numberOfAnimalsInPreviousAgeInPastVector_all_dead - death_Ic_and_Pos;
        }
        //
        if (iHealthStateCompartmentsContainer.extraDeath[age-1] > 0) {
            if (iHealthStateCompartmentsContainer.extraDeath[age-1] <= remainingDeadAnimalsToDispatch) {
                remainingDeadAnimalsToDispatch -= iHealthStateCompartmentsContainer.extraDeath[age-1];
                iHealthStateCompartmentsContainer.extraDeath[age-1] = 0;
            } else {
                iHealthStateCompartmentsContainer.extraDeath[age-1] -= remainingDeadAnimalsToDispatch;
                remainingDeadAnimalsToDispatch = 0;
            }
        }
        //
        iHealthStateCompartmentsContainer.iCompartmentSR.present[age] = iHealthStateCompartmentsContainer.iCompartmentSR.past[age-1];
        iHealthStateCompartmentsContainer.iCompartmentT.present[age] = iHealthStateCompartmentsContainer.iCompartmentT.past[age-1];
        iHealthStateCompartmentsContainer.iCompartmentL.present[age] = iHealthStateCompartmentsContainer.iCompartmentL.past[age-1];
        iHealthStateCompartmentsContainer.iCompartmentIs.present[age] = iHealthStateCompartmentsContainer.iCompartmentIs.past[age-1];
        iHealthStateCompartmentsContainer.iCompartmentIc.present[age] = iHealthStateCompartmentsContainer.iCompartmentIc.past[age-1];
        //
        iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[age] = iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[age-1];
        iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age] = iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.past[age-1];
        iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age] = iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past[age-1];
        iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age] = iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past[age-1];
        iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age] = iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[age-1];
        //
        //
        // update the Ic
        iHealthStateCompartmentsContainer.iCompartmentIc.present[age] -= numberOfAnimalsInPreviousAgeInPastVector_Ic_dead;
        iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age] -= numberOfAnimalsInPreviousAgeInPastVector_positiveTestedIc_dead;
        // dispatch the Pos taking into account updated Ic
        if (numberOfAnimalsInPreviousAgeInPastVector_positiveTested_dead > 0) {
            double nbAnimals_positiveTested = iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[age] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age] + iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age] + iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age] + iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age]; // = numberOfAnimalsInPreviousAgeInPastVector_positiveTested - numberOfAnimalsInPreviousAgeInPastVector_positiveTestedIc_dead;
            //
            if (nbAnimals_positiveTested == numberOfAnimalsInPreviousAgeInPastVector_positiveTested_dead) {
                iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[age] = 0;
                iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age] = 0;
                iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age] = 0;
                iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age] = 0;
                iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age] = 0;
            } else {
                // determine where to apply the death
                std::vector<int> vHealthStates(5);
                vHealthStates[0] = iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[age];
                vHealthStates[1] = iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age];
                vHealthStates[2] = iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age];
                vHealthStates[3] = iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age];
                vHealthStates[4] = iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age];
//TEST                //ONLY FOR TEST
//TEST                if (std::accumulate(vHealthStates.begin(), vHealthStates.end(), 0) == 0 & numberOfAnimalsInPreviousAgeInPastVector_positiveTested_dead > 0) {
//TEST                    std::cout << std::endl << "A | ";
//TEST                    for (auto i : vHealthStates) {
//TEST                        std::cout << " " << i;
//TEST                    }
//TEST                    std::cout << " - " << numberOfAnimalsInPreviousAgeInPastVector_positiveTested_dead;
//TEST                }
//TEST                //ONLY FOR TEST
                std::vector<int> vNbDeathForEachCompart = U_functions::RandomSamplingHealthStatesWithoutReplacement(randomGenerator, vHealthStates, numberOfAnimalsInPreviousAgeInPastVector_positiveTested_dead);
                // remove the death
                //TEST if (iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[age] < vNbDeathForEachCompart[0]) {std::cout << "SR1" << std::endl;}
                //TEST if (iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age] < vNbDeathForEachCompart[1]) {std::cout << "T1" << std::endl;}
                //TEST if (iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age] < vNbDeathForEachCompart[2]) {std::cout << "L1" << std::endl;}
                //TEST if (iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age] < vNbDeathForEachCompart[3]) {std::cout << "Is1" << std::endl;}
                //TEST if (iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age] < vNbDeathForEachCompart[4]) {std::cout << "Ic1" << std::endl;}
                //
                iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[age] -= vNbDeathForEachCompart[0];
                iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age] -= vNbDeathForEachCompart[1];
                iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age] -= vNbDeathForEachCompart[2];
                iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age] -= vNbDeathForEachCompart[3];
                iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age] -= vNbDeathForEachCompart[4];
            }
        }
        // dispatch the remaining death taking into account updated Ic and Pos
        if (remainingDeadAnimalsToDispatch > 0) {
            double nbAnimals_all = iHealthStateCompartmentsContainer.iCompartmentSR.present[age] + iHealthStateCompartmentsContainer.iCompartmentT.present[age] + iHealthStateCompartmentsContainer.iCompartmentL.present[age] + iHealthStateCompartmentsContainer.iCompartmentIs.present[age] + iHealthStateCompartmentsContainer.iCompartmentIc.present[age] + iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[age] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age] + iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age] + iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age] + iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age];
            //
            if (nbAnimals_all == remainingDeadAnimalsToDispatch) {
                iHealthStateCompartmentsContainer.iCompartmentSR.present[age] = 0;
                iHealthStateCompartmentsContainer.iCompartmentT.present[age] = 0;
                iHealthStateCompartmentsContainer.iCompartmentL.present[age] = 0;
                iHealthStateCompartmentsContainer.iCompartmentIs.present[age] = 0;
                iHealthStateCompartmentsContainer.iCompartmentIc.present[age] = 0;
                //
                iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[age] = 0;
                iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age] = 0;
                iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age] = 0;
                iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age] = 0;
                iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age] = 0;
            } else {
                // determine where to apply the death
                std::vector<int> vHealthStates(10);
                vHealthStates[0] = iHealthStateCompartmentsContainer.iCompartmentSR.present[age];
                vHealthStates[1] = iHealthStateCompartmentsContainer.iCompartmentT.present[age];
                vHealthStates[2] = iHealthStateCompartmentsContainer.iCompartmentL.present[age];
                vHealthStates[3] = iHealthStateCompartmentsContainer.iCompartmentIs.present[age];
                vHealthStates[4] = iHealthStateCompartmentsContainer.iCompartmentIc.present[age];
                //
                vHealthStates[5] = iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[age];
                vHealthStates[6] = iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age];
                vHealthStates[7] = iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age];
                vHealthStates[8] = iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age];
                vHealthStates[9] = iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age];
//TEST                //ONLY FOR TEST
//TEST                if (std::accumulate(vHealthStates.begin(), vHealthStates.end(), 0) == 0 & remainingDeadAnimalsToDispatch > 0) {
//TEST                    std::cout << std::endl << "B | ";
//TEST                    for (auto i : vHealthStates) {
//TEST                        std::cout << " " << i;
//TEST                    }
//TEST                    std::cout << " - " << remainingDeadAnimalsToDispatch;
//TEST                }
//TEST                //ONLY FOR TEST
                std::vector<int> vNbDeathForEachCompart = U_functions::RandomSamplingHealthStatesWithoutReplacement(randomGenerator, vHealthStates, remainingDeadAnimalsToDispatch);
                // remove the death
                //TEST if (iHealthStateCompartmentsContainer.iCompartmentSR.present[age] < vNbDeathForEachCompart[0]) {std::cout << "SR2 : " << iHealthStateCompartmentsContainer.iCompartmentSR.present[age] << " - " << vNbDeathForEachCompart[0] << std::endl;}
                //TEST if (iHealthStateCompartmentsContainer.iCompartmentT.present[age] < vNbDeathForEachCompart[1]) {std::cout << "T2 : " << iHealthStateCompartmentsContainer.iCompartmentT.present[age] << " - " << vNbDeathForEachCompart[1] << std::endl;}
                //TEST if (iHealthStateCompartmentsContainer.iCompartmentL.present[age] < vNbDeathForEachCompart[2]) {std::cout << "L2 : " << iHealthStateCompartmentsContainer.iCompartmentL.present[age] << " - " << vNbDeathForEachCompart[2] << std::endl;}
                //TEST if (iHealthStateCompartmentsContainer.iCompartmentIs.present[age] < vNbDeathForEachCompart[3]) {std::cout << "Is2 : " << iHealthStateCompartmentsContainer.iCompartmentIs.present[age] << " - " << vNbDeathForEachCompart[3] << std::endl;}
                //TEST if (iHealthStateCompartmentsContainer.iCompartmentIc.present[age] < vNbDeathForEachCompart[4]) {std::cout << "Ic2 : " << iHealthStateCompartmentsContainer.iCompartmentIc.present[age] << " - " << vNbDeathForEachCompart[4] << std::endl;}
                //
                iHealthStateCompartmentsContainer.iCompartmentSR.present[age] -= vNbDeathForEachCompart[0];
                iHealthStateCompartmentsContainer.iCompartmentT.present[age] -= vNbDeathForEachCompart[1];
                iHealthStateCompartmentsContainer.iCompartmentL.present[age] -= vNbDeathForEachCompart[2];
                iHealthStateCompartmentsContainer.iCompartmentIs.present[age] -= vNbDeathForEachCompart[3];
                iHealthStateCompartmentsContainer.iCompartmentIc.present[age] -= vNbDeathForEachCompart[4];
                //
                //
                //TEST if (iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[age] < vNbDeathForEachCompart[5]) {std::cout << "SR3" << std::endl;}
                //TEST if (iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age] < vNbDeathForEachCompart[6]) {std::cout << "T3" << std::endl;}
                //TEST if (iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age] < vNbDeathForEachCompart[7]) {std::cout << "L3" << std::endl;}
                //TEST if (iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age] < vNbDeathForEachCompart[8]) {std::cout << "Is3" << std::endl;}
                //TEST if (iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age] < vNbDeathForEachCompart[9]) {std::cout << "Ic3" << std::endl;}
                //
                iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[age] -= vNbDeathForEachCompart[5];
                iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age] -= vNbDeathForEachCompart[6];
                iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age] -= vNbDeathForEachCompart[7];
                iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age] -= vNbDeathForEachCompart[8];
                iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age] -= vNbDeathForEachCompart[9];
            }
        }

        if (age == Parameters::ageFirstCalving) {
            // Update the L and T at age == ageFirstCalving
            iHealthStateCompartmentsContainer.iCompartmentL.present[age] += iHealthStateCompartmentsContainer.iCompartmentT.present[age];
            iHealthStateCompartmentsContainer.iCompartmentT.present[age] = 0;
            iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age] += iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age];
            iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age] = 0;
            //
            adultsEnteringTheAgeClass_SR = iHealthStateCompartmentsContainer.iCompartmentSR.present[age];
            adultsEnteringTheAgeClass_SR_positiveTested = iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[age];
            adultsEnteringTheAgeClass_T = iHealthStateCompartmentsContainer.iCompartmentT.present[age];
            adultsEnteringTheAgeClass_T_positiveTested = iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age];
            adultsEnteringTheAgeClass_L = iHealthStateCompartmentsContainer.iCompartmentL.present[age];
            adultsEnteringTheAgeClass_L_positiveTested = iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age];
            adultsEnteringTheAgeClass_Is = iHealthStateCompartmentsContainer.iCompartmentIs.present[age];
            adultsEnteringTheAgeClass_Is_positiveTested = iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age];
            adultsEnteringTheAgeClass_Ic = iHealthStateCompartmentsContainer.iCompartmentIc.present[age];
            adultsEnteringTheAgeClass_Ic_positiveTested = iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age];
        }
    }


    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////


    for (int age = Parameters::ageFirstCalving; age <= Parameters::ageFirstCalving+4; age++) {
        //
        int buffer_SR_present = iHealthStateCompartmentsContainer.iCompartmentSR.past[age];
        int buffer_T_present = iHealthStateCompartmentsContainer.iCompartmentT.past[age];
        int buffer_L_present = iHealthStateCompartmentsContainer.iCompartmentL.past[age];
        int buffer_Is_present = iHealthStateCompartmentsContainer.iCompartmentIs.past[age];
        int buffer_Ic_present = iHealthStateCompartmentsContainer.iCompartmentIc.past[age];
        //
        int buffer_SR_positiveTested_present = iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[age];
        int buffer_T_positiveTested_present = iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.past[age];
        int buffer_L_positiveTested_present = iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past[age];
        int buffer_Is_positiveTested_present = iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past[age];
        int buffer_Ic_positiveTested_present = iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[age];
        //
        // get the number of animals in this age group
        //
        int numberOfAnimalsInPreviousAgeInPastVector_Ic = buffer_Ic_present;
        //
        int numberOfAnimalsInPreviousAgeInPastVector_positiveTested = buffer_SR_positiveTested_present + buffer_T_positiveTested_present + buffer_L_positiveTested_present + buffer_Is_positiveTested_present + buffer_Ic_positiveTested_present;
        //
        int numberOfAnimalsInPreviousAgeInPastVector_positiveTestedIc = buffer_Ic_positiveTested_present;
        //
        int numberOfAnimalsInPreviousAgeInPastVectorWithoutIc = buffer_SR_present + buffer_T_present + buffer_L_present + buffer_Is_present;
        // all animals
        int numberOfAnimalsInPreviousAgeInPastVector_all = numberOfAnimalsInPreviousAgeInPastVectorWithoutIc + numberOfAnimalsInPreviousAgeInPastVector_Ic + numberOfAnimalsInPreviousAgeInPastVector_positiveTested;
        // set variables containing the number of death to dispatch in the compartments
        //
        int numberOfAnimalsInPreviousAgeInPastVector_Ic_dead = gsl_ran_binomial(randomGenerator, iParametersSpecificToEachHerd.cullingRateIcInThisFarm, numberOfAnimalsInPreviousAgeInPastVector_Ic);
        //
        int numberOfAnimalsInPreviousAgeInPastVector_positiveTestedIc_dead = gsl_ran_binomial(randomGenerator, iParametersSpecificToEachHerd.cullingRateIcInThisFarm, numberOfAnimalsInPreviousAgeInPastVector_positiveTestedIc);
        //
        int numberOfAnimalsInPreviousAgeInPastVector_positiveTested_dead = gsl_ran_binomial(randomGenerator, Parameters::cullingRatePositiveTestedAnimals, numberOfAnimalsInPreviousAgeInPastVector_positiveTested-numberOfAnimalsInPreviousAgeInPastVector_positiveTestedIc_dead);
        //
        int numberOfAnimalsInPreviousAgeInPastVector_all_dead = gsl_ran_binomial(randomGenerator, iParametersSpecificToEachHerd.vDeathAndCullingRate[age], numberOfAnimalsInPreviousAgeInPastVector_all);
        //
        int remainingDeadAnimalsToDispatch = 0;
        int death_Ic_and_Pos = numberOfAnimalsInPreviousAgeInPastVector_Ic_dead + numberOfAnimalsInPreviousAgeInPastVector_positiveTestedIc_dead + numberOfAnimalsInPreviousAgeInPastVector_positiveTested_dead;
        if (numberOfAnimalsInPreviousAgeInPastVector_all_dead < death_Ic_and_Pos) {
            iHealthStateCompartmentsContainer.extraDeath[age] += (death_Ic_and_Pos - numberOfAnimalsInPreviousAgeInPastVector_all_dead);
        } else { // if (numberOfAnimalsInPreviousAgeInPastVector_all_dead > death_Ic_and_Pos)
            remainingDeadAnimalsToDispatch = numberOfAnimalsInPreviousAgeInPastVector_all_dead - death_Ic_and_Pos;
        }
        //
        if (iHealthStateCompartmentsContainer.extraDeath[age] > 0) {
            if (iHealthStateCompartmentsContainer.extraDeath[age] <= remainingDeadAnimalsToDispatch) {
                remainingDeadAnimalsToDispatch -= iHealthStateCompartmentsContainer.extraDeath[age];
                iHealthStateCompartmentsContainer.extraDeath[age] = 0;
            } else {
                iHealthStateCompartmentsContainer.extraDeath[age] -= remainingDeadAnimalsToDispatch;
                remainingDeadAnimalsToDispatch = 0;
            }
        }
        //
        //
        // update the Ic
        buffer_Ic_present -= numberOfAnimalsInPreviousAgeInPastVector_Ic_dead;
        buffer_Ic_positiveTested_present -= numberOfAnimalsInPreviousAgeInPastVector_positiveTestedIc_dead;
        // dispatch the Pos taking into account updated Ic
        if (numberOfAnimalsInPreviousAgeInPastVector_positiveTested_dead > 0) {
            double nbAnimals_positiveTested = buffer_SR_positiveTested_present + buffer_T_positiveTested_present + buffer_L_positiveTested_present + buffer_Is_positiveTested_present + buffer_Ic_positiveTested_present; // = numberOfAnimalsInPreviousAgeInPastVector_positiveTested - numberOfAnimalsInPreviousAgeInPastVector_positiveTestedIc_dead;
            //
            if (nbAnimals_positiveTested == numberOfAnimalsInPreviousAgeInPastVector_positiveTested_dead) {
                buffer_SR_positiveTested_present = 0;
                buffer_T_positiveTested_present = 0;
                buffer_L_positiveTested_present = 0;
                buffer_Is_positiveTested_present = 0;
                buffer_Ic_positiveTested_present = 0;
            } else {
                // determine where to apply the death
                std::vector<int> vHealthStates(5);
                vHealthStates[0] = buffer_SR_positiveTested_present;
                vHealthStates[1] = buffer_T_positiveTested_present;
                vHealthStates[2] = buffer_L_positiveTested_present;
                vHealthStates[3] = buffer_Is_positiveTested_present;
                vHealthStates[4] = buffer_Ic_positiveTested_present;
//TEST                //ONLY FOR TEST
//TEST                if (std::accumulate(vHealthStates.begin(), vHealthStates.end(), 0) == 0 & numberOfAnimalsInPreviousAgeInPastVector_positiveTested_dead > 0) {
//TEST                    std::cout << std::endl << "C | ";
//TEST                    for (auto i : vHealthStates) {
//TEST                        std::cout << " " << i;
//TEST                    }
//TEST                    std::cout << " - " << numberOfAnimalsInPreviousAgeInPastVector_positiveTested_dead;
//TEST                }
//TEST                //ONLY FOR TEST
                std::vector<int> vNbDeathForEachCompart = U_functions::RandomSamplingHealthStatesWithoutReplacement(randomGenerator, vHealthStates, numberOfAnimalsInPreviousAgeInPastVector_positiveTested_dead);
                // remove the death
                buffer_SR_positiveTested_present -= vNbDeathForEachCompart[0];
                buffer_T_positiveTested_present -= vNbDeathForEachCompart[1];
                buffer_L_positiveTested_present -= vNbDeathForEachCompart[2];
                buffer_Is_positiveTested_present -= vNbDeathForEachCompart[3];
                buffer_Ic_positiveTested_present -= vNbDeathForEachCompart[4];
            }
        }
        // dispatch the remaining death taking into account updated Ic and Pos
        if (remainingDeadAnimalsToDispatch > 0) {
            double nbAnimals_all = buffer_SR_present + buffer_T_present + buffer_L_present + buffer_Is_present + buffer_Ic_present + buffer_SR_positiveTested_present + buffer_T_positiveTested_present + buffer_L_positiveTested_present + buffer_Is_positiveTested_present + buffer_Ic_positiveTested_present;
            //
            if (nbAnimals_all == remainingDeadAnimalsToDispatch) {
                buffer_SR_present = 0;
                buffer_T_present = 0;
                buffer_L_present = 0;
                buffer_Is_present = 0;
                buffer_Ic_present = 0;
                //
                buffer_SR_positiveTested_present = 0;
                buffer_T_positiveTested_present = 0;
                buffer_L_positiveTested_present = 0;
                buffer_Is_positiveTested_present = 0;
                buffer_Ic_positiveTested_present = 0;
            } else {
                // determine where to apply the death
                std::vector<int> vHealthStates(10);
                vHealthStates[0] = buffer_SR_present;
                vHealthStates[1] = buffer_T_present;
                vHealthStates[2] = buffer_L_present;
                vHealthStates[3] = buffer_Is_present;
                vHealthStates[4] = buffer_Ic_present;
                //
                vHealthStates[5] = buffer_SR_positiveTested_present;
                vHealthStates[6] = buffer_T_positiveTested_present;
                vHealthStates[7] = buffer_L_positiveTested_present;
                vHealthStates[8] = buffer_Is_positiveTested_present;
                vHealthStates[9] = buffer_Ic_positiveTested_present;
//TEST                //ONLY FOR TEST
//TEST                if (std::accumulate(vHealthStates.begin(), vHealthStates.end(), 0) == 0 & remainingDeadAnimalsToDispatch > 0) {
//TEST                    std::cout << std::endl << "D | ";
//TEST                    for (auto i : vHealthStates) {
//TEST                        std::cout << " " << i;
//TEST                    }
//TEST                    std::cout << std::endl;
//TEST                    std::cout << "remainingDeadAnimalsToDispatch : " << remainingDeadAnimalsToDispatch << std::endl;
//TEST                    std::cout << std::endl;
//TEST                    std::cout << "extraDeath : " << iHealthStateCompartmentsContainer.extraDeath[age] << std::endl;
//TEST                    std::cout << "all_dead : " << numberOfAnimalsInPreviousAgeInPastVector_all_dead << std::endl;
//TEST                    std::cout << "death_Ic_and_Pos : " << death_Ic_and_Pos << " = " << numberOfAnimalsInPreviousAgeInPastVector_Ic_dead << " + " << numberOfAnimalsInPreviousAgeInPastVector_positiveTestedIc_dead << " + " << numberOfAnimalsInPreviousAgeInPastVector_positiveTested_dead;
//TEST                    std::cout << std::endl;
//TEST                    std::cout << iHealthStateCompartmentsContainer.iCompartmentSR.past[age] << std::endl;
//TEST                    std::cout << iHealthStateCompartmentsContainer.iCompartmentT.past[age] << std::endl;
//TEST                    std::cout << iHealthStateCompartmentsContainer.iCompartmentL.past[age] << std::endl;
//TEST                    std::cout << iHealthStateCompartmentsContainer.iCompartmentIs.past[age] << std::endl;
//TEST                    std::cout << iHealthStateCompartmentsContainer.iCompartmentIc.past[age] << std::endl;
//TEST                    //
//TEST                    std::cout << iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[age] << std::endl;
//TEST                    std::cout << iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.past[age] << std::endl;
//TEST                    std::cout << iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past[age] << std::endl;
//TEST                    std::cout << iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past[age] << std::endl;
//TEST                    std::cout << iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[age] << std::endl;
//TEST                }
//TEST                //ONLY FOR TEST
                std::vector<int> vNbDeathForEachCompart = U_functions::RandomSamplingHealthStatesWithoutReplacement(randomGenerator, vHealthStates, remainingDeadAnimalsToDispatch);
                // remove the death
                buffer_SR_present -= vNbDeathForEachCompart[0];
                buffer_T_present -= vNbDeathForEachCompart[1];
                buffer_L_present -= vNbDeathForEachCompart[2];
                buffer_Is_present -= vNbDeathForEachCompart[3];
                buffer_Ic_present -= vNbDeathForEachCompart[4];
                //
                buffer_SR_positiveTested_present -= vNbDeathForEachCompart[5];
                buffer_T_positiveTested_present -= vNbDeathForEachCompart[6];
                buffer_L_positiveTested_present -= vNbDeathForEachCompart[7];
                buffer_Is_positiveTested_present -= vNbDeathForEachCompart[8];
                buffer_Ic_positiveTested_present -= vNbDeathForEachCompart[9];
            }
        }
        //
        //
        // compute how many animals will go in the next age group and how many will remain in the same age group
        double nbAnimals_all = buffer_SR_present + buffer_T_present + buffer_L_present + buffer_Is_present + buffer_Ic_present + buffer_SR_positiveTested_present + buffer_T_positiveTested_present + buffer_L_positiveTested_present + buffer_Is_positiveTested_present + buffer_Ic_positiveTested_present;
        int nbAnimalsLeavingtheAgeGroup = gsl_ran_binomial(randomGenerator, (1. - std::exp( - (1./Parameters::durOneYear))), nbAnimals_all);
        if (nbAnimalsLeavingtheAgeGroup > 0) {
            std::vector<int> vHealthStates(10);
            vHealthStates[0] = buffer_SR_present;
            vHealthStates[1] = buffer_T_present;
            vHealthStates[2] = buffer_L_present;
            vHealthStates[3] = buffer_Is_present;
            vHealthStates[4] = buffer_Ic_present;
            //
            vHealthStates[5] = buffer_SR_positiveTested_present;
            vHealthStates[6] = buffer_T_positiveTested_present;
            vHealthStates[7] = buffer_L_positiveTested_present;
            vHealthStates[8] = buffer_Is_positiveTested_present;
            vHealthStates[9] = buffer_Ic_positiveTested_present;
            //
//TEST            //ONLY FOR TEST
//TEST            if (std::accumulate(vHealthStates.begin(), vHealthStates.end(), 0) == 0 & nbAnimalsLeavingtheAgeGroup > 0) {
//TEST                std::cout << std::endl << "C | ";
//TEST                for (auto i : vHealthStates) {
//TEST                    std::cout << " " << i;
//TEST                }
//TEST                std::cout << " - " << nbAnimalsLeavingtheAgeGroup;
//TEST            }
//TEST            //ONLY FOR TEST
            std::vector<int> vNbAnimalsLeavingTheAgeGroupForEachCompart = U_functions::RandomSamplingHealthStatesWithoutReplacement(randomGenerator, vHealthStates, nbAnimalsLeavingtheAgeGroup);
            //
            if (age < Parameters::ageFirstCalving+4) {
                adultsLeavingTheAgeClass_SR = vNbAnimalsLeavingTheAgeGroupForEachCompart[0];
                adultsLeavingTheAgeClass_T = vNbAnimalsLeavingTheAgeGroupForEachCompart[1];
                adultsLeavingTheAgeClass_L = vNbAnimalsLeavingTheAgeGroupForEachCompart[2];
                adultsLeavingTheAgeClass_Is = vNbAnimalsLeavingTheAgeGroupForEachCompart[3];
                adultsLeavingTheAgeClass_Ic = vNbAnimalsLeavingTheAgeGroupForEachCompart[4];
                //
                adultsLeavingTheAgeClass_SR_positiveTested = vNbAnimalsLeavingTheAgeGroupForEachCompart[5];
                adultsLeavingTheAgeClass_T_positiveTested = vNbAnimalsLeavingTheAgeGroupForEachCompart[6];
                adultsLeavingTheAgeClass_L_positiveTested = vNbAnimalsLeavingTheAgeGroupForEachCompart[7];
                adultsLeavingTheAgeClass_Is_positiveTested = vNbAnimalsLeavingTheAgeGroupForEachCompart[8];
                adultsLeavingTheAgeClass_Ic_positiveTested = vNbAnimalsLeavingTheAgeGroupForEachCompart[9];
            } else {
                adultsLeavingTheAgeClass_SR = 0;
                adultsLeavingTheAgeClass_T = 0;
                adultsLeavingTheAgeClass_L = 0;
                adultsLeavingTheAgeClass_Is = 0;
                adultsLeavingTheAgeClass_Ic = 0;
                //
                adultsLeavingTheAgeClass_SR_positiveTested = 0;
                adultsLeavingTheAgeClass_T_positiveTested = 0;
                adultsLeavingTheAgeClass_L_positiveTested = 0;
                adultsLeavingTheAgeClass_Is_positiveTested = 0;
                adultsLeavingTheAgeClass_Ic_positiveTested = 0;
            }
        } else {
            adultsLeavingTheAgeClass_SR = 0;
            adultsLeavingTheAgeClass_T = 0;
            adultsLeavingTheAgeClass_L = 0;
            adultsLeavingTheAgeClass_Is = 0;
            adultsLeavingTheAgeClass_Ic = 0;
            //
            adultsLeavingTheAgeClass_SR_positiveTested = 0;
            adultsLeavingTheAgeClass_T_positiveTested = 0;
            adultsLeavingTheAgeClass_L_positiveTested = 0;
            adultsLeavingTheAgeClass_Is_positiveTested = 0;
            adultsLeavingTheAgeClass_Ic_positiveTested = 0;
        }

        //
        //TEST if (buffer_SR_present < 0) {std::cout << "buffer_SR_present < 0" << std::endl;}
        //TEST if (buffer_T_present < 0) {std::cout << "buffer_T_present < 0" << std::endl;}
        //TEST if (buffer_L_present < 0) {std::cout << "buffer_L_present < 0" << std::endl;}
        //TEST if (buffer_Is_present < 0) {std::cout << "buffer_Is_present < 0" << std::endl;}
        //TEST if (buffer_Ic_present < 0) {std::cout << "buffer_Ic_present < 0" << std::endl;}
        //TEST if (buffer_SR_positiveTested_present < 0) {std::cout << "buffer_SR_positiveTested_present < 0" << std::endl;}
        //TEST if (buffer_T_positiveTested_present < 0) {std::cout << "buffer_T_positiveTested_present < 0" << std::endl;}
        //TEST if (buffer_L_positiveTested_present < 0) {std::cout << "buffer_L_positiveTested_present < 0" << std::endl;}
        //TEST if (buffer_Is_positiveTested_present < 0) {std::cout << "buffer_Is_positiveTested_present < 0" << std::endl;}
        //TEST if (buffer_Ic_positiveTested_present < 0) {std::cout << "buffer_Ic_positiveTested_present < 0" << std::endl;}
        //
        //TEST if ((buffer_SR_present - adultsLeavingTheAgeClass_SR + adultsEnteringTheAgeClass_SR) < 0) {std::cout << "buffer_SR_present - adultsLeavingTheAgeClass_SR + adultsEnteringTheAgeClass_SR < 0" << std::endl;}
        //TEST if ((buffer_T_present - adultsLeavingTheAgeClass_T + adultsEnteringTheAgeClass_T) < 0) {std::cout << "buffer_T_present - adultsLeavingTheAgeClass_T + adultsEnteringTheAgeClass_T < 0" << std::endl;}
        //TEST if ((buffer_L_present - adultsLeavingTheAgeClass_L + adultsEnteringTheAgeClass_L) < 0) {std::cout << "buffer_L_present - adultsLeavingTheAgeClass_L + adultsEnteringTheAgeClass_L < 0" << std::endl;}
        //TEST if ((buffer_Is_present - adultsLeavingTheAgeClass_Is + adultsEnteringTheAgeClass_Is) < 0) {std::cout << "buffer_Is_present - adultsLeavingTheAgeClass_Is + adultsEnteringTheAgeClass_Is < 0" << std::endl;}
        //TEST if ((buffer_Ic_present - adultsLeavingTheAgeClass_Ic + adultsEnteringTheAgeClass_Ic) < 0) {std::cout << "buffer_Ic_present - adultsLeavingTheAgeClass_Ic + adultsEnteringTheAgeClass_Ic < 0" << std::endl;}
        //TEST if ((buffer_SR_positiveTested_present - adultsLeavingTheAgeClass_SR_positiveTested + adultsEnteringTheAgeClass_SR_positiveTested) < 0) {std::cout << "buffer_SR_positiveTested_present - adultsLeavingTheAgeClass_SR_positiveTested + adultsEnteringTheAgeClass_SR_positiveTested < 0" << std::endl;}
        //TEST if ((buffer_T_positiveTested_present - adultsLeavingTheAgeClass_T_positiveTested + adultsEnteringTheAgeClass_T_positiveTested) < 0) {std::cout << "buffer_T_positiveTested_present - adultsLeavingTheAgeClass_T_positiveTested + adultsEnteringTheAgeClass_T_positiveTested < 0" << std::endl;}
        //TEST if ((buffer_L_positiveTested_present - adultsLeavingTheAgeClass_L_positiveTested + adultsEnteringTheAgeClass_L_positiveTested) < 0) {std::cout << "buffer_L_positiveTested_present - adultsLeavingTheAgeClass_L_positiveTested + adultsEnteringTheAgeClass_L_positiveTested < 0" << std::endl;}
        //TEST if ((buffer_Is_positiveTested_present - adultsLeavingTheAgeClass_Is_positiveTested + adultsEnteringTheAgeClass_Is_positiveTested) < 0) {std::cout << "buffer_Is_positiveTested_present - adultsLeavingTheAgeClass_Is_positiveTested + adultsEnteringTheAgeClass_Is_positiveTested < 0" << std::endl;}
        //TEST if ((buffer_Ic_positiveTested_present - adultsLeavingTheAgeClass_Ic_positiveTested + adultsEnteringTheAgeClass_Ic_positiveTested) < 0) {std::cout << "buffer_Ic_positiveTested_present - adultsLeavingTheAgeClass_Ic_positiveTested + adultsEnteringTheAgeClass_Ic_positiveTested < 0" << std::endl;}
        //
        iHealthStateCompartmentsContainer.iCompartmentSR.present[age] = buffer_SR_present - adultsLeavingTheAgeClass_SR + adultsEnteringTheAgeClass_SR;
        iHealthStateCompartmentsContainer.iCompartmentT.present[age] = buffer_T_present - adultsLeavingTheAgeClass_T + adultsEnteringTheAgeClass_T;
        iHealthStateCompartmentsContainer.iCompartmentL.present[age] = buffer_L_present - adultsLeavingTheAgeClass_L + adultsEnteringTheAgeClass_L;
        iHealthStateCompartmentsContainer.iCompartmentIs.present[age] = buffer_Is_present - adultsLeavingTheAgeClass_Is + adultsEnteringTheAgeClass_Is;
        iHealthStateCompartmentsContainer.iCompartmentIc.present[age] = buffer_Ic_present - adultsLeavingTheAgeClass_Ic + adultsEnteringTheAgeClass_Ic;
        //
        iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[age] = buffer_SR_positiveTested_present - adultsLeavingTheAgeClass_SR_positiveTested + adultsEnteringTheAgeClass_SR_positiveTested;
        iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age] = buffer_T_positiveTested_present - adultsLeavingTheAgeClass_T_positiveTested + adultsEnteringTheAgeClass_T_positiveTested;
        iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age] = buffer_L_positiveTested_present - adultsLeavingTheAgeClass_L_positiveTested + adultsEnteringTheAgeClass_L_positiveTested;
        iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age] = buffer_Is_positiveTested_present - adultsLeavingTheAgeClass_Is_positiveTested + adultsEnteringTheAgeClass_Is_positiveTested;
        iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age] = buffer_Ic_positiveTested_present - adultsLeavingTheAgeClass_Ic_positiveTested + adultsEnteringTheAgeClass_Ic_positiveTested;
        //
        //
        adultsEnteringTheAgeClass_SR = adultsLeavingTheAgeClass_SR;
        adultsEnteringTheAgeClass_SR_positiveTested = adultsLeavingTheAgeClass_SR_positiveTested;
        adultsEnteringTheAgeClass_T = adultsLeavingTheAgeClass_T;
        adultsEnteringTheAgeClass_T_positiveTested = adultsLeavingTheAgeClass_T_positiveTested;
        adultsEnteringTheAgeClass_L = adultsLeavingTheAgeClass_L;
        adultsEnteringTheAgeClass_L_positiveTested = adultsLeavingTheAgeClass_L_positiveTested;
        adultsEnteringTheAgeClass_Is = adultsLeavingTheAgeClass_Is;
        adultsEnteringTheAgeClass_Is_positiveTested = adultsLeavingTheAgeClass_Is_positiveTested;
        adultsEnteringTheAgeClass_Ic = adultsLeavingTheAgeClass_Ic;
        adultsEnteringTheAgeClass_Ic_positiveTested = adultsLeavingTheAgeClass_Ic_positiveTested;
    }

    return IcCulledThisTimeStep;
}



int HerdExit::PopulationDynamicConcerningPositiveTestedAnimals(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& t, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer){
    
    int IcCulledThisTimeStep = 0;
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Population dynamic concerning positive tested animals:
    
    // SR
    int numberOfAnimalsInSRPositiveTested = std::accumulate(iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past.begin(), iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past.end(), 0);
    if (numberOfAnimalsInSRPositiveTested > 0) {
        
        int firstAgeNotNull = 1;
        for (int i = 1; i < iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past.size(); i++) {
            if (iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[i] > 0) {
                firstAgeNotNull = i;
//                std::cout << "index of firstAgeNotNull for SR: " << firstAgeNotNull << std::endl;
                break;
            }
        }
        
        for (int age = firstAgeNotNull; age <= Parameters::ageFirstCalving; age++) {
            iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[age] = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vDeathRate[age-1])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[age-1]);
        }
        
        int adultsEnteringTheAgeClass = 0;
        int adultsLeavingTheAgeClass = 0;
        
        adultsLeavingTheAgeClass = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[Parameters::ageFirstCalving]);
        iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[Parameters::ageFirstCalving] = iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[Parameters::ageFirstCalving] + gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving-Parameters::ageFirstCalving])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[Parameters::ageFirstCalving]-adultsLeavingTheAgeClass);
        
        for (int age = Parameters::ageFirstCalving+1; age <= Parameters::ageFirstCalving+3; age++) {
            adultsEnteringTheAgeClass = adultsLeavingTheAgeClass;
            adultsLeavingTheAgeClass = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[age]);
            int adultsEnteringTheAgeClass_alive = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving-1])*(1-Parameters::cullingRatePositiveTestedAnimals), adultsEnteringTheAgeClass);
            iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[age] = adultsEnteringTheAgeClass_alive + gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[age]-adultsLeavingTheAgeClass);
        }
        
        adultsEnteringTheAgeClass = adultsLeavingTheAgeClass;
        int adultsEnteringTheAgeClass_alive = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[4-1])*(1-Parameters::cullingRatePositiveTestedAnimals), adultsEnteringTheAgeClass);
        iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[Parameters::ageFirstCalving+4] = adultsEnteringTheAgeClass_alive + gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving+4-Parameters::ageFirstCalving])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[Parameters::ageFirstCalving+4]);
    }
    
    // T
    int numberOfAnimalsInTPositiveTested = std::accumulate(iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.past.begin(), iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.past.end(), 0);
    if (numberOfAnimalsInTPositiveTested > 0) {
        
        int firstAgeNotNull = 1;
        for (int i = 1; i < iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.past.size(); i++) {
            if (iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.past[i] > 0) {
                firstAgeNotNull = i;
//                std::cout << "index of firstAgeNotNull for T: " << firstAgeNotNull << std::endl;
                break;
            }
        }
        
        for (int age = firstAgeNotNull; age <= (Parameters::ageFirstCalving-1); age++) {
            iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age] = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vDeathRate[age-1])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.past[age-1]);
        }
    }
    
    // L
    int numberOfAnimalsInLPositiveTested = std::accumulate(iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past.begin(), iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past.end(), 0) + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.past[Parameters::ageFirstCalving-1];
    if (numberOfAnimalsInLPositiveTested > 0) {
        
        int firstAgeNotNull = 1;
        for (int i = 1; i < iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past.size(); i++) {
            if (iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past[i] > 0) {
                firstAgeNotNull = i;
//                std::cout << "index of firstAgeNotNull for L: " << firstAgeNotNull << std::endl;
                break;
            }
        }
        
        for (int age = firstAgeNotNull; age <= (Parameters::ageFirstCalving-1); age++) {
            iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age] = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vDeathRate[age-1])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past[age-1]);
        }
        
        iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[Parameters::ageFirstCalving] = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vDeathRate[Parameters::ageFirstCalving-1])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past[Parameters::ageFirstCalving-1]);
        iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[Parameters::ageFirstCalving] = iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[Parameters::ageFirstCalving] + gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vDeathRate[Parameters::ageFirstCalving-1])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.past[Parameters::ageFirstCalving-1]);
        
        int adultsEnteringTheAgeClass_L = 0;
        int adultsLeavingTheAgeClass_L = 0;
        
        adultsLeavingTheAgeClass_L = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past[Parameters::ageFirstCalving]);
        
        int adultsLeavingTheAgeClass_T = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.past[Parameters::ageFirstCalving]);
        if (adultsLeavingTheAgeClass_T > 0) {
            std::cout << "adultsLeavingTheAgeClass_T (b): " << adultsLeavingTheAgeClass_T << std::endl;
        }
        
        iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[Parameters::ageFirstCalving] = iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[Parameters::ageFirstCalving] + gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving-Parameters::ageFirstCalving])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past[Parameters::ageFirstCalving]-adultsLeavingTheAgeClass_L);
        
        for (int age = Parameters::ageFirstCalving+1; age <= Parameters::ageFirstCalving+3; age++) {
            adultsEnteringTheAgeClass_L = adultsLeavingTheAgeClass_L;
            adultsLeavingTheAgeClass_L = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past[age]);
            int adultsEnteringTheAgeClass_L_alive = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving-1])*(1-Parameters::cullingRatePositiveTestedAnimals), adultsEnteringTheAgeClass_L);
            iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age] = adultsEnteringTheAgeClass_L_alive + gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past[age]-adultsLeavingTheAgeClass_L);
        }
        
        adultsEnteringTheAgeClass_L = adultsLeavingTheAgeClass_L;
        int adultsEnteringTheAgeClass_L_alive = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[4-1])*(1-Parameters::cullingRatePositiveTestedAnimals), adultsEnteringTheAgeClass_L);
        iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[Parameters::ageFirstCalving+4] = adultsEnteringTheAgeClass_L_alive + gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving+4-Parameters::ageFirstCalving])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past[Parameters::ageFirstCalving+4]);
    }

    //Is
    int numberOfAnimalsInIsPositiveTested = std::accumulate(iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past.begin(), iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past.end(), 0);
    if (numberOfAnimalsInIsPositiveTested > 0) {
        
        int firstAgeNotNull = 1;
        for (int i = 1; i < iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past.size(); i++) {
            if (iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past[i] > 0) {
                firstAgeNotNull = i;
//                std::cout << "index of firstAgeNotNull for Is: " << firstAgeNotNull << std::endl;
                break;
            }
        }
        
        for (int age = firstAgeNotNull; age <= (Parameters::ageFirstCalving-1); age++) {
            iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age] = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vDeathRate[age-1])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past[age-1]);
        }
        
        iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[Parameters::ageFirstCalving] = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vDeathRate[Parameters::ageFirstCalving-1])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past[Parameters::ageFirstCalving-1]);
        
        int adultsEnteringTheAgeClass_Is = 0;
        int adultsLeavingTheAgeClass_Is = 0;
        
        adultsLeavingTheAgeClass_Is = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past[Parameters::ageFirstCalving]);
        
        iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[Parameters::ageFirstCalving] = iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[Parameters::ageFirstCalving] + gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving-Parameters::ageFirstCalving])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past[Parameters::ageFirstCalving]-adultsLeavingTheAgeClass_Is);
        
        for (int age = Parameters::ageFirstCalving+1; age <= Parameters::ageFirstCalving+3; age++) {
            adultsEnteringTheAgeClass_Is = adultsLeavingTheAgeClass_Is;
            adultsLeavingTheAgeClass_Is = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past[age]);
            int adultsEnteringTheAgeClass_Is_alive = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving-1])*(1-Parameters::cullingRatePositiveTestedAnimals), adultsEnteringTheAgeClass_Is);
            iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age] = adultsEnteringTheAgeClass_Is_alive + gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past[age]-adultsLeavingTheAgeClass_Is);
        }
        
        adultsEnteringTheAgeClass_Is = adultsLeavingTheAgeClass_Is;
        int adultsEnteringTheAgeClass_Is_alive = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[4-1])*(1-Parameters::cullingRatePositiveTestedAnimals), adultsEnteringTheAgeClass_Is);
        iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[Parameters::ageFirstCalving+4] = adultsEnteringTheAgeClass_Is_alive + gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving+4-Parameters::ageFirstCalving])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past[Parameters::ageFirstCalving+4]);
    }
    
    //Ic
    int numberOfAnimalsInIcPositiveTested = std::accumulate(iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past.begin(), iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past.end(), 0);
    if (numberOfAnimalsInIcPositiveTested > 0) {
        
        int firstAgeNotNull = 1;
        for (int i = 1; i < iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past.size(); i++) {
            if (iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[i] > 0) {
                firstAgeNotNull = i;
//                std::cout << "index of firstAgeNotNull for Ic: " << firstAgeNotNull << std::endl;
                break;
            }
        }
        
        for (int age = firstAgeNotNull; age <= (Parameters::ageFirstCalving-1); age++) {
            iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age] = gsl_ran_binomial(randomGenerator, (1-std::max(Parameters::cullingRatePositiveTestedAnimals,Parameters::cullingRateIc)), iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[age-1]);
        }
        
        iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[Parameters::ageFirstCalving] = gsl_ran_binomial(randomGenerator, (1-std::max(Parameters::cullingRatePositiveTestedAnimals,Parameters::cullingRateIc)), iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[Parameters::ageFirstCalving-1]);
        
        int adultsEnteringTheAgeClass_Ic = 0;
        int adultsLeavingTheAgeClass_Ic = 0;
        
        adultsLeavingTheAgeClass_Ic = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[Parameters::ageFirstCalving]);
        
        int buff1 = gsl_ran_binomial(randomGenerator, (1-std::max(Parameters::cullingRatePositiveTestedAnimals,Parameters::cullingRateIc)), iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[Parameters::ageFirstCalving]-adultsLeavingTheAgeClass_Ic);
        iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[Parameters::ageFirstCalving] = iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[Parameters::ageFirstCalving] + buff1;
        IcCulledThisTimeStep += (iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[Parameters::ageFirstCalving] - adultsLeavingTheAgeClass_Ic - buff1);
        
        for (int age = Parameters::ageFirstCalving+1; age <= Parameters::ageFirstCalving+3; age++) {
            adultsEnteringTheAgeClass_Ic = adultsLeavingTheAgeClass_Ic;
            adultsLeavingTheAgeClass_Ic = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[age]);
            int adultsEnteringTheAgeClass_Ic_alive = gsl_ran_binomial(randomGenerator, (1-std::max(Parameters::cullingRatePositiveTestedAnimals,Parameters::cullingRateIc)), adultsEnteringTheAgeClass_Ic);
            IcCulledThisTimeStep += (adultsEnteringTheAgeClass_Ic-adultsEnteringTheAgeClass_Ic_alive);
            int buff2 = gsl_ran_binomial(randomGenerator, (1-std::max(Parameters::cullingRatePositiveTestedAnimals,Parameters::cullingRateIc)), iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[age]-adultsLeavingTheAgeClass_Ic);
            iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age] = adultsEnteringTheAgeClass_Ic_alive + buff2;
            IcCulledThisTimeStep += (iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[age]-adultsLeavingTheAgeClass_Ic - buff2);
        }
        
        adultsEnteringTheAgeClass_Ic = adultsLeavingTheAgeClass_Ic;
        int adultsEnteringTheAgeClass_Ic_alive = gsl_ran_binomial(randomGenerator, (1-std::max(Parameters::cullingRatePositiveTestedAnimals,Parameters::cullingRateIc)), adultsEnteringTheAgeClass_Ic);
        IcCulledThisTimeStep += (adultsEnteringTheAgeClass_Ic-adultsEnteringTheAgeClass_Ic_alive);
        int buff3 = gsl_ran_binomial(randomGenerator, (1-std::max(Parameters::cullingRatePositiveTestedAnimals,Parameters::cullingRateIc)), iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[Parameters::ageFirstCalving+4]);
        iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[Parameters::ageFirstCalving+4] = adultsEnteringTheAgeClass_Ic_alive + buff3;
        IcCulledThisTimeStep += (iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[Parameters::ageFirstCalving+4] - buff3);
        
        //        for (int age = firstAgeNotNull; age <= (Parameters::ageFirstCalving-1); age++) {
        //            iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age] = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vDeathRate[age-1])*(1-std::max(Parameters::cullingRatePositiveTestedAnimals,Parameters::cullingRateIc)), iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[age-1]);
        //        }
        //
        //        iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[Parameters::ageFirstCalving] = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vDeathRate[Parameters::ageFirstCalving-1])*(1-std::max(Parameters::cullingRatePositiveTestedAnimals,Parameters::cullingRateIc)), iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[Parameters::ageFirstCalving-1]);
        //
        //        int adultsEnteringTheAgeClass_Ic = 0;
        //        int adultsLeavingTheAgeClass_Ic = 0;
        //
        //        adultsLeavingTheAgeClass_Ic = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[Parameters::ageFirstCalving]);
        //
        //        int buff1 = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving-Parameters::ageFirstCalving])*(1-std::max(Parameters::cullingRatePositiveTestedAnimals,Parameters::cullingRateIc)), iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[Parameters::ageFirstCalving]-adultsLeavingTheAgeClass_Ic);
        //        iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[Parameters::ageFirstCalving] = iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[Parameters::ageFirstCalving] + buff1;
        //        IcCulledThisTimeStep += (iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[Parameters::ageFirstCalving] - adultsLeavingTheAgeClass_Ic - buff1);
        //
        //        for (int age = Parameters::ageFirstCalving+1; age <= Parameters::ageFirstCalving+3; age++) {
        //            adultsEnteringTheAgeClass_Ic = adultsLeavingTheAgeClass_Ic;
        //            adultsLeavingTheAgeClass_Ic = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[age]);
        //            int adultsEnteringTheAgeClass_Ic_alive = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving-1])*(1-std::max(Parameters::cullingRatePositiveTestedAnimals,Parameters::cullingRateIc)), adultsEnteringTheAgeClass_Ic);
        //            IcCulledThisTimeStep += (adultsEnteringTheAgeClass_Ic-adultsEnteringTheAgeClass_Ic_alive);
        //            int buff2 = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving])*(1-std::max(Parameters::cullingRatePositiveTestedAnimals,Parameters::cullingRateIc)), iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[age]-adultsLeavingTheAgeClass_Ic);
        //            iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age] = adultsEnteringTheAgeClass_Ic_alive + buff2;
        //            IcCulledThisTimeStep += (iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[age]-adultsLeavingTheAgeClass_Ic - buff2);
        //        }
        //
        //        adultsEnteringTheAgeClass_Ic = adultsLeavingTheAgeClass_Ic;
        //        int adultsEnteringTheAgeClass_Ic_alive = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[4-1])*(1-std::max(Parameters::cullingRatePositiveTestedAnimals,Parameters::cullingRateIc)), adultsEnteringTheAgeClass_Ic);
        //        IcCulledThisTimeStep += (adultsEnteringTheAgeClass_Ic-adultsEnteringTheAgeClass_Ic_alive);
        //        int buff3 = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving+4-Parameters::ageFirstCalving])*(1-std::max(Parameters::cullingRatePositiveTestedAnimals,Parameters::cullingRateIc)), iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[Parameters::ageFirstCalving+4]);
        //        iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[Parameters::ageFirstCalving+4] = adultsEnteringTheAgeClass_Ic_alive + buff3;
        //        IcCulledThisTimeStep += (iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[Parameters::ageFirstCalving+4] - buff3);
    }
    
    return IcCulledThisTimeStep;
}



void HerdExit::UpdateSR(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& t, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer){
    
    UpdateSR_part1(randomGenerator, iParametersSpecificToEachHerd, t, iHealthStateCompartmentsContainer);
    
    UpdateSR_part2(randomGenerator, iParametersSpecificToEachHerd, t, iHealthStateCompartmentsContainer);
    
    UpdateSR_positiveTestedAnimals(randomGenerator, iParametersSpecificToEachHerd, t, iHealthStateCompartmentsContainer);
    
}


void HerdExit::UpdateSR_part1(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& t, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer){
    
    for (int age = 1; age <= Parameters::ageFirstCalving; age++) {
        iHealthStateCompartmentsContainer.iCompartmentSR.present[age] = gsl_ran_binomial(randomGenerator, (1.-iParametersSpecificToEachHerd.vDeathRate[age-1]), iHealthStateCompartmentsContainer.iCompartmentSR.past[age-1]);
    }
}

void HerdExit::UpdateSR_part2(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& t, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer){
    
    int adultsEnteringTheAgeClass = 0;
    int adultsLeavingTheAgeClass = 0;
    
    adultsLeavingTheAgeClass = gsl_ran_binomial(randomGenerator, (1. - std::exp( - (1./Parameters::durOneYear))), iHealthStateCompartmentsContainer.iCompartmentSR.past[Parameters::ageFirstCalving]);
    iHealthStateCompartmentsContainer.iCompartmentSR.present[Parameters::ageFirstCalving] = iHealthStateCompartmentsContainer.iCompartmentSR.present[Parameters::ageFirstCalving] + gsl_ran_binomial(randomGenerator, (1.-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving-Parameters::ageFirstCalving]), iHealthStateCompartmentsContainer.iCompartmentSR.past[Parameters::ageFirstCalving]-adultsLeavingTheAgeClass);
        
    for (int age = Parameters::ageFirstCalving+1; age <= Parameters::ageFirstCalving+3; age++) {
        adultsEnteringTheAgeClass = adultsLeavingTheAgeClass;
        adultsLeavingTheAgeClass = gsl_ran_binomial(randomGenerator, (1. - std::exp( - (1./Parameters::durOneYear))), iHealthStateCompartmentsContainer.iCompartmentSR.past[age]);
        int adultsEnteringTheAgeClass_alive = gsl_ran_binomial(randomGenerator, (1.-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving-1]), adultsEnteringTheAgeClass);
        int adultsNotLeavingTheAgeClass_alive = gsl_ran_binomial(randomGenerator, (1.-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving]), iHealthStateCompartmentsContainer.iCompartmentSR.past[age]-adultsLeavingTheAgeClass);
        iHealthStateCompartmentsContainer.iCompartmentSR.present[age] = adultsEnteringTheAgeClass_alive + adultsNotLeavingTheAgeClass_alive;
    }
    
    adultsEnteringTheAgeClass = adultsLeavingTheAgeClass;
    int adultsEnteringTheAgeClass_alive = gsl_ran_binomial(randomGenerator, (1.-iParametersSpecificToEachHerd.vCullingRate[4-1]), adultsEnteringTheAgeClass);
    int adultsNotLeavingTheAgeClass_alive = gsl_ran_binomial(randomGenerator, (1.-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving+4-Parameters::ageFirstCalving]), iHealthStateCompartmentsContainer.iCompartmentSR.past[Parameters::ageFirstCalving+4]);
    iHealthStateCompartmentsContainer.iCompartmentSR.present[Parameters::ageFirstCalving+4] = adultsEnteringTheAgeClass_alive + adultsNotLeavingTheAgeClass_alive;
}



void HerdExit::UpdateSR_positiveTestedAnimals(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& t, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer){

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Population dynamic concerning positive tested animals:
    
    int numberOfAnimalsInSRPositiveTested = std::accumulate(iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past.begin(), iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past.end(), 0);
    if (numberOfAnimalsInSRPositiveTested > 0) {
        std::cout << "numberOfAnimalsInSRPositiveTested > 0" << std::endl;
        
        int firstAgeNotNull = 0;
        for (int i = 0; i < iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past.size(); i++) {
            if (iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[i] > 0) {
                firstAgeNotNull = i;
                //                std::cout << "index of firstAgeNotNull: " << firstAgeNotNull << std::endl;
                break;
            }
        }
        
        for (int age = firstAgeNotNull; age <= Parameters::ageFirstCalving; age++) {
            iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[age] = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vDeathRate[age-1])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[age-1]);
        }
        
        int adultsEnteringTheAgeClass = 0;
        int adultsLeavingTheAgeClass = 0;
        
        adultsLeavingTheAgeClass = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[Parameters::ageFirstCalving]);
        iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[Parameters::ageFirstCalving] = iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[Parameters::ageFirstCalving] + gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving-Parameters::ageFirstCalving])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[Parameters::ageFirstCalving]-adultsLeavingTheAgeClass);
        
        for (int age = Parameters::ageFirstCalving+1; age <= Parameters::ageFirstCalving+3; age++) {
            adultsEnteringTheAgeClass = adultsLeavingTheAgeClass;
            adultsLeavingTheAgeClass = gsl_ran_binomial(randomGenerator, 1/Parameters::durOneYear, iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[age]);
            int adultsEnteringTheAgeClass_alive = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving-1])*(1-Parameters::cullingRatePositiveTestedAnimals), adultsEnteringTheAgeClass);
            int adultsNotLeavingTheAgeClass_alive = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[age-Parameters::ageFirstCalving])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[age]-adultsLeavingTheAgeClass);
            iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[age] = adultsEnteringTheAgeClass_alive + adultsNotLeavingTheAgeClass_alive;
        }
        
        adultsEnteringTheAgeClass = adultsLeavingTheAgeClass;
        int adultsEnteringTheAgeClass_alive = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[4-1])*(1-Parameters::cullingRatePositiveTestedAnimals), adultsEnteringTheAgeClass);
        int adultsNotLeavingTheAgeClass_alive = gsl_ran_binomial(randomGenerator, (1-iParametersSpecificToEachHerd.vCullingRate[Parameters::ageFirstCalving+4-Parameters::ageFirstCalving])*(1-Parameters::cullingRatePositiveTestedAnimals), iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[Parameters::ageFirstCalving+4]);
        iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[Parameters::ageFirstCalving+4] = adultsEnteringTheAgeClass_alive + adultsNotLeavingTheAgeClass_alive;
    }
    
}



