//
//  AnimalMoving.h
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunee on 26/04/2015.
//  Copyright (c) 2015 Gaël Beaunée. All rights reserved.
//

#ifndef __ParatubBetweenDairyHerd__AnimalMoving__
#define __ParatubBetweenDairyHerd__AnimalMoving__

#include <stdio.h>
#include <string>
#include <vector>

class AnimalMoving {
    
public:
    // Constructor =========================================================
    AnimalMoving();
    AnimalMoving(const std::vector<int>& vect, const std::string& theSourceId);

    // Variables ===========================================================
    int date;
    int healthState; // "SR" = 0; "T" = 1; "L" = 2; "Is" = 3;
    int age;
    int typeOfInfectedHerd;
    int presenceOfIcAtThisTime;
    int presenceOfAtLeastTwoIcAtThisTime;
    int numberOfOutgoingMovementsAboveTheMinimum;
    std::string herdSourceID;
    
};

#endif /* defined(__ParatubBetweenDairyHerd__AnimalMoving__) */
