//
//  PersistentResults.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 09/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

//=================================================================
// Accumulator for runs which persist at the end of the simulation
//=================================================================

#include "BufferPersistentResults.h"

// Constructor =========================================================
BufferPersistentResults::BufferPersistentResults(){
    // Vectors =============================================================
    bNbNewInfectedHerdsSinceSimulationStart = 0;
    bNbNewInfectedHerdsSinceTestsStart = 0;
    
    // accumulators ========================================================
    bInfectedHerds.resize(Parameters::simutime, 0); bInfectedHerds[0] = Parameters::nbHerdsInfected;
    bAffectedHerds.resize(Parameters::simutime, 0); bAffectedHerds[0] = 0;
    bNeverInfectedHerds.resize(Parameters::simutime, 0); bNeverInfectedHerds[0] = Parameters::nbHerds - Parameters::nbHerdsInfected;
    bAlreadyInfectedHerds.resize(Parameters::simutime, 0); bAlreadyInfectedHerds[0] = Parameters::nbHerdsInfected;
    
    bInfectedProportionMeta.resize(Parameters::simutime, 0);
    bInfectiousProportionMeta.resize(Parameters::simutime, 0);
    bAffectedProportionMeta.resize(Parameters::simutime, 0);
    
    bInfectedProportionFirst.resize(Parameters::simutime, 0);
    bInfectiousProportionFirst.resize(Parameters::simutime, 0);
    bAffectedProportionFirst.resize(Parameters::simutime, 0);
    
//    bInfectedProportionFirstInit.resize(Parameters::initializeSimutime, 0);
//    bInfectiousProportionFirstInit.resize(Parameters::initializeSimutime, 0);
//    bAffectedProportionFirstInit.resize(Parameters::initializeSimutime, 0);
    
    bHeadcountMeta.resize(Parameters::simutime, 0);
    bHeadcountFirst.resize(Parameters::simutime, 0);
//    bHeadcountFirstInit.resize(Parameters::initializeSimutime, 0);
    bHeadcountSecond.resize(Parameters::simutime, 0);
    
    
    bInfectedByFirst = 0;
    bInfectedBySecond = 0;
    bInfectedByExt = 0;
    
    bInfectedByTestedMov = 0;
    
    bNbOutgoingMov.resize(Parameters::simutime, 0);
    bNbOutgoingMovTested.resize(Parameters::simutime, 0);
    bNbOutgoingInfectedMov.resize(Parameters::simutime, 0);
    bNbOutgoingInfectedMovTested.resize(Parameters::simutime, 0);
    
    bNbIncomingMov.resize(Parameters::simutime, 0);
    bNbIncomingMovTested.resize(Parameters::simutime, 0);
    bNbIncomingInfectedMov.resize(Parameters::simutime, 0);
    bNbIncomingInfectedMovTested.resize(Parameters::simutime, 0);
    
    bIntroducionOfInfectiousAnimalsIfNoTest = 0;
    bIntroducionOfInfectiousAnimalsAvoided = 0;
    
    vBufferMatrixOfInfectedHerdsAfterHalfTheSimulationTime.resize(Parameters::nbHerds, 0);
    vBufferMatrixOfInfectedHerdsAtTheEndOfTheSimulationTime.resize(Parameters::nbHerds, 0);
    vBufferMatrixOfAlreadyInfectedHerdsAfterHalfTheSimulationTime.resize(Parameters::nbHerds, 0);
    vBufferMatrixOfAlreadyInfectedHerdsAtTheEndOfTheSimulationTime.resize(Parameters::nbHerds, 0);
    
    vBufferMatrixOfNumberOfInfection.resize(Parameters::nbHerds, 0);
    vBufferMatrixOfNumberOfInfectedAnimalsPurchased.resize(Parameters::nbHerds, 0);
    vBufferMatrixOfNumberOfWeeksWithInfection.resize(Parameters::nbHerds, 0);
    
    vBufferPrimaryCaseMatrix.resize(Parameters::nbHerds, 0);
    
//    vBufferMatrixOfNumberOfAnimalsTestedMovIn.resize(Parameters::nbHerds, 0);
//    vBufferMatrixOfNumberOfAnimalsTestedMovOut.resize(Parameters::nbHerds, 0);
    vBufferMatrixOfNumberOfIcAnimalsCulled.resize(Parameters::nbHerds, 0);
    vBufferMatrixOfNumberOfAnimalsCulledDuringTestAndCull.resize(Parameters::nbHerds, 0);
    vBufferMatrixOfNumberOfAnimalsTestedDuringTestAndCull.resize(Parameters::nbHerds, 0);


    for (auto theFarmID : Parameters::vFarmID) {
        buffer_mFarms_vectPrevalence[theFarmID] = std::vector<double>(Parameters::simutime, 0.0);
    }
}


void BufferPersistentResults::clear(){
    
    vBufferOutDegreeOfSelectedNode.clear();
    vBufferNbOutMovsOfSelectedNode.clear();
    vBufferSizeOfSelectedNode.clear();
    vBufferPrevalenceInSelectedNodes.clear();
    vBufferPrevalenceInInfectedHerdsAtTheEnd.clear();
    
    bNbNewInfectedHerdsSinceSimulationStart = 0;
    bNbNewInfectedHerdsSinceTestsStart = 0;
    
    bInfectedHerds.clear(); bInfectedHerds.resize(Parameters::simutime, 0); bInfectedHerds[0] = Parameters::nbHerdsInfected;
    bAffectedHerds.clear(); bAffectedHerds.resize(Parameters::simutime, 0); bAffectedHerds[0] = Parameters::nbHerdsInfected;
    bNeverInfectedHerds.clear(); bNeverInfectedHerds.resize(Parameters::simutime, 0); bNeverInfectedHerds[0] = Parameters::nbHerds - Parameters::nbHerdsInfected;
    bAlreadyInfectedHerds.clear(); bAlreadyInfectedHerds.resize(Parameters::simutime, 0); bAlreadyInfectedHerds[0] = Parameters::nbHerdsInfected;
    
    bInfectedProportionMeta.clear(); bInfectedProportionMeta.resize(Parameters::simutime, 0);
    bInfectiousProportionMeta.clear(); bInfectiousProportionMeta.resize(Parameters::simutime, 0);
    bAffectedProportionMeta.clear(); bAffectedProportionMeta.resize(Parameters::simutime, 0);
    
    bInfectedProportionFirst.clear(); bInfectedProportionFirst.resize(Parameters::simutime, 0);
    bInfectiousProportionFirst.clear(); bInfectiousProportionFirst.resize(Parameters::simutime, 0);
    bAffectedProportionFirst.clear(); bAffectedProportionFirst.resize(Parameters::simutime, 0);
    
//    bInfectedProportionFirstInit.clear(); bInfectedProportionFirstInit.resize(Parameters::initializeSimutime, 0);
//    bInfectiousProportionFirstInit.clear(); bInfectiousProportionFirstInit.resize(Parameters::initializeSimutime, 0);
//    bAffectedProportionFirstInit.clear(); bAffectedProportionFirstInit.resize(Parameters::initializeSimutime, 0);
    
    bHeadcountMeta.clear(); bHeadcountMeta.resize(Parameters::simutime, 0);
    bHeadcountFirst.clear(); bHeadcountFirst.resize(Parameters::simutime, 0);
//    bHeadcountFirstInit.clear(); bHeadcountFirstInit.resize(Parameters::initializeSimutime, 0);
    bHeadcountSecond.clear(); bHeadcountSecond.resize(Parameters::simutime, 0);
    
    
    bInfectedByFirst = 0;
    bInfectedBySecond = 0;
    bInfectedByExt = 0;
    
    bInfectedByTestedMov = 0;
    
    bNbOutgoingMov.clear(); bNbOutgoingMov.resize(Parameters::simutime, 0);;
    bNbOutgoingMovTested.clear(); bNbOutgoingMovTested.resize(Parameters::simutime, 0);;
    bNbOutgoingInfectedMov.clear(); bNbOutgoingInfectedMov.resize(Parameters::simutime, 0);;
    bNbOutgoingInfectedMovTested.clear(); bNbOutgoingInfectedMovTested.resize(Parameters::simutime, 0);;
    
    bNbIncomingMov.clear(); bNbIncomingMov.resize(Parameters::simutime, 0);;
    bNbIncomingMovTested.clear(); bNbIncomingMovTested.resize(Parameters::simutime, 0);;
    bNbIncomingInfectedMov.clear(); bNbIncomingInfectedMov.resize(Parameters::simutime, 0);;
    bNbIncomingInfectedMovTested.clear(); bNbIncomingInfectedMovTested.resize(Parameters::simutime, 0);;
    
    bIntroducionOfInfectiousAnimalsIfNoTest = 0;
    bIntroducionOfInfectiousAnimalsAvoided = 0;
    
    
    vBufferInfectionDurationWhenExtinction.clear();
    vBufferInfectionDurationIfNoExtinction.clear();
    
    vBufferExtinctionBefore1Years.clear();    
    vBufferExtinctionBefore2Years.clear();
    vBufferExtinctionBefore3Years.clear();
    
    vBufferExtinctionBefore5Years.clear();
    vBufferNoExtinctionBefore5Years.clear();
    
    
    vBufferSourceAndDestinationOfInfectedMovements_SourceIsFirst.clear();
    vBufferSourceAndDestinationOfInfectedMovements_SourceIsSecond.clear();
    vBufferSourceAndDestinationOfInfectedMovements_SourceIsExt.clear();
    
    vBufferNbInfectedMovementsPerHerd.clear();
    
    vBufferMatrixOfAlreadyInfectedHerdsAfterHalfTheSimulationTime.clear();
    vBufferMatrixOfAlreadyInfectedHerdsAtTheEndOfTheSimulationTime.clear();
    vBufferMatrixOfInfectedHerdsAfterHalfTheSimulationTime.clear();
    vBufferMatrixOfInfectedHerdsAtTheEndOfTheSimulationTime.clear();
    vBufferMatrixOfNumberOfInfection.clear();
    vBufferMatrixOfNumberOfInfectedAnimalsPurchased.clear();
    vBufferMatrixOfNumberOfWeeksWithInfection.clear();
    vBufferPrimaryCaseMatrix.clear();
    
//    vBufferMatrixOfNumberOfAnimalsTestedMovIn.clear();
//    vBufferMatrixOfNumberOfAnimalsTestedMovOut.clear();
    vBufferMatrixOfNumberOfIcAnimalsCulled.clear();
    vBufferMatrixOfNumberOfAnimalsCulledDuringTestAndCull.clear();
    vBufferMatrixOfNumberOfAnimalsTestedDuringTestAndCull.clear();
    
    vBufferMatrixOfInfectedHerdsAfterHalfTheSimulationTime.resize(Parameters::nbHerds, 0);
    vBufferMatrixOfInfectedHerdsAtTheEndOfTheSimulationTime.resize(Parameters::nbHerds, 0);
    vBufferMatrixOfAlreadyInfectedHerdsAfterHalfTheSimulationTime.resize(Parameters::nbHerds, 0);
    vBufferMatrixOfAlreadyInfectedHerdsAtTheEndOfTheSimulationTime.resize(Parameters::nbHerds, 0);
    vBufferMatrixOfNumberOfInfection.resize(Parameters::nbHerds, 0);
    vBufferMatrixOfNumberOfInfectedAnimalsPurchased.resize(Parameters::nbHerds, 0);
    vBufferMatrixOfNumberOfWeeksWithInfection.resize(Parameters::nbHerds, 0);
    vBufferPrimaryCaseMatrix.resize(Parameters::nbHerds, 0);
        
//    vBufferMatrixOfNumberOfAnimalsTestedMovIn.resize(Parameters::nbHerds, 0);
//    vBufferMatrixOfNumberOfAnimalsTestedMovOut.resize(Parameters::nbHerds, 0);
    vBufferMatrixOfNumberOfIcAnimalsCulled.resize(Parameters::nbHerds, 0);
    vBufferMatrixOfNumberOfAnimalsCulledDuringTestAndCull.resize(Parameters::nbHerds, 0);
    vBufferMatrixOfNumberOfAnimalsTestedDuringTestAndCull.resize(Parameters::nbHerds, 0);


    buffer_mFarms_vectPrevalence.clear();
    for (auto theFarmID : Parameters::vFarmID) {
        buffer_mFarms_vectPrevalence[theFarmID] = std::vector<double>(Parameters::simutime, 0.0);
    }
}


// Member functions ====================================================
void BufferPersistentResults::updateBuffer(const int& timeStep, const int& typeInfectedHerd, const SummaryDynamic& iSummaryDynamic){
    bInfectedProportionMeta[timeStep] += iSummaryDynamic.infected[timeStep];
    bInfectiousProportionMeta[timeStep] += iSummaryDynamic.infectious[timeStep];
    bAffectedProportionMeta[timeStep] += iSummaryDynamic.affected[timeStep];
    bHeadcountMeta[timeStep] += iSummaryDynamic.headcount[timeStep];
    
    if (typeInfectedHerd == 1) {
        bInfectedProportionFirst[timeStep] += iSummaryDynamic.infected[timeStep];
        bInfectiousProportionFirst[timeStep] += iSummaryDynamic.infectious[timeStep];
        bAffectedProportionFirst[timeStep] += iSummaryDynamic.affected[timeStep];
        bHeadcountFirst[timeStep] += iSummaryDynamic.headcount[timeStep];
    }
    
    if (timeStep == Parameters::simutime-1) {
        if (iSummaryDynamic.infected[Parameters::simutime-1]>0) {
            double prevalence = static_cast<double>(iSummaryDynamic.infected[Parameters::simutime-1])/static_cast<double>(iSummaryDynamic.headcount[Parameters::simutime-1]);
            vBufferPrevalenceInInfectedHerdsAtTheEnd.emplace_back(prevalence);
        }
    }
}


void BufferPersistentResults::updateBufferInit(const std::string& theFarmId, const int& theFarmIndex, const int& typeInfectedHerd, SummaryDynamic& iSummaryDynamic){
    if (typeInfectedHerd == 1) {
//        if (Parameters::initializeHerdsWithEndemicState) {
//            if (Parameters::withinHerdBiosecurity == false) {
//                for (int time = 0; time<Parameters::initializeSimutime; time++) {
//                    bInfectedProportionFirstInit[time] += iSummaryDynamic.infected[time];
//                    bInfectiousProportionFirstInit[time] += iSummaryDynamic.infectious[time];
//                    bAffectedProportionFirstInit[time] += iSummaryDynamic.affected[time];
//                    bHeadcountFirstInit[time] += iSummaryDynamic.headcount[time];
//                }
//            }
//        }
        
        vBufferSizeOfSelectedNode.emplace_back(Parameters::mFarmsCharacteristics[theFarmId].herdSize);
        vBufferOutDegreeOfSelectedNode.emplace_back(Parameters::mFarmsCharacteristics[theFarmId].outDegree);
        vBufferNbOutMovsOfSelectedNode.emplace_back(Parameters::mFarmsCharacteristics[theFarmId].outStrengthAll);
        
        vBufferInDegreeOfSelectedNode.emplace_back(Parameters::mFarmsCharacteristics[theFarmId].inDegree);
        vBufferNbInMovsOfSelectedNode.emplace_back(Parameters::mFarmsCharacteristics[theFarmId].inStrengthAll);
        
        double prevalence = static_cast<double>(iSummaryDynamic.infected[0])/static_cast<double>(iSummaryDynamic.headcount[0]);
        vBufferPrevalenceInSelectedNodes.emplace_back(prevalence);


//        std::cout << prevalence << std::endl; // DEBUG20170727

        int pos = theFarmIndex;
        vBufferPrimaryCaseMatrix[pos] = 1;
    }
}


//void BufferPersistentResults::updateBufferInit(const DairyHerd& theFarm){
//    if (theFarm.typeInfectedHerd == 1) {
//        //        if (Parameters::initializeHerdsWithEndemicState) {
//        //            if (Parameters::withinHerdBiosecurity == false) {
//        //                for (int time = 0; time<Parameters::initializeSimutime; time++) {
//        //                    bInfectedProportionFirstInit[time] += iSummaryDynamic.infected[time];
//        //                    bInfectiousProportionFirstInit[time] += iSummaryDynamic.infectious[time];
//        //                    bAffectedProportionFirstInit[time] += iSummaryDynamic.affected[time];
//        //                    bHeadcountFirstInit[time] += iSummaryDynamic.headcount[time];
//        //                }
//        //            }
//        //        }
//
//        vBufferSizeOfSelectedNode.emplace_back(theFarm.iParametersSpecificToEachHerd.herdSize);
//        vBufferOutDegreeOfSelectedNode.emplace_back(theFarm.iParametersSpecificToEachHerd.outgoingDegree);
//        vBufferNbOutMovsOfSelectedNode.emplace_back(theFarm.iParametersSpecificToEachHerd.numberOfOutgoingMovements);
//
//        vBufferInDegreeOfSelectedNode.emplace_back(theFarm.iParametersSpecificToEachHerd.incomingDegree);
//        vBufferNbInMovsOfSelectedNode.emplace_back(theFarm.iParametersSpecificToEachHerd.numberOfIncomingMovements);
//
//        double prevalence = static_cast<double>(theFarm.iSummaryDynamic.infected[0])/static_cast<double>(theFarm.iSummaryDynamic.headcount[0]);
//        vBufferPrevalenceInSelectedNodes.emplace_back(prevalence);
//
//        int pos = theFarm.iParametersSpecificToEachHerd.farmIndex;
//        vBufferPrimaryCaseMatrix[pos] = 1;
//    }
//}



