//
//  VectorPlus.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 01/08/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef VectorPlus_h
#define VectorPlus_h

#include <vector>
#include <algorithm>
#include <functional>
#include <assert.h>

template <typename T>
std::vector<T> operator+(const std::vector<T>& a, const std::vector<T>& b)
{
    assert(a.size() == b.size());
    
    std::vector<T> result;
    result.reserve(a.size());
    
    std::transform(a.begin(), a.end(), b.begin(),
                   std::back_inserter(result), std::plus<T>());
    return result;
}

#endif
