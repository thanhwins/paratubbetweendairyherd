//
//  PersistentResults.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 09/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

//=================================================================
// Accumulator for runs which persist at the end of the simulation
//=================================================================

#ifndef ParatubBetweenDairyHerd_PersistentResults_h
#define ParatubBetweenDairyHerd_PersistentResults_h

#include <vector>
#include <map>
#include <string>
#include <boost/accumulators/numeric/functional/vector.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/sum.hpp>
#include <boost/accumulators/statistics/count.hpp>
#include <boost/accumulators/statistics/median.hpp>
#include <boost/accumulators/statistics/extended_p_square.hpp>
#include <boost/array.hpp>

#include "Parameters.h"
#include "AgeCompartments.h"
#include "SummaryInfection.h"
#include "SummaryDynamic.h"
#include "Births.h"
#include "MapInEnvironments.h"
#include "UtilitiesFunctions.h"
#include "VectorPlus.h"
#include "VectorMinus.h"
#include "BufferPersistentResults.h"
#include "MetapopSummary.h"

using namespace boost::accumulators;

typedef accumulator_set<int, stats<tag::mean, tag::variance, tag::count, tag::sum > > accInt;
typedef accumulator_set<double, stats<tag::mean, tag::variance, tag::count, tag::sum  > > accDouble;
typedef accumulator_set<std::vector<int>, stats<tag::mean, tag::variance, tag::count, tag::sum  > > accVectInt;
typedef accumulator_set<std::vector<double>, stats<tag::mean, tag::variance, tag::count, tag::sum  > > accVectDouble;
typedef std::vector<accDouble> vectAccDouble;

typedef accumulator_set<double, stats<tag::extended_p_square> > accQuantiles;
typedef std::vector< accQuantiles > accVectQuantiles;

class PersistentResults {
public:
    // Constructor =========================================================
    PersistentResults();
    
//    void clear();
    
    // Vectors =============================================================
    std::vector<int> vInfectedHerdsVariation;
    std::vector<int> vAlreadyInfectedHerdsVariation;
    
    
    std::vector<int> vOutDegreeOfSelectedNode;
    std::vector<int> vInDegreeOfSelectedNode;
    
    std::vector<double> vNbOutMovsOfSelectedNode;
    std::vector<double> vNbInMovsOfSelectedNode;
    
    std::vector<int> vSizeOfSelectedNode;
    
    std::vector<double> vPrevalenceInSelectedNodes;
    
    std::vector<int>  vNbNewInfectedHerdsSinceSimulationStart;
    std::vector<int>  vNbNewInfectedHerdsSinceTestsStart;
    
    
    // accumulators ========================================================
    accVectInt accInfectedHerds;
    accVectInt accAffectedHerds;
    accVectInt accNeverInfectedHerds;
    accVectInt accAlreadyInfectedHerds;
    
    accVectDouble accInfectedProportionMeta;
    accVectDouble accInfectiousProportionMeta;
    accVectDouble accAffectedProportionMeta;
    
    accVectDouble accInfectedProportionFirst;
    accVectDouble accInfectiousProportionFirst;
    accVectDouble accAffectedProportionFirst;
    
//    accVectDouble accInfectedProportionFirstInit;
//    accVectDouble accInfectiousProportionFirstInit;
//    accVectDouble accAffectedProportionFirstInit;
    
    vectAccDouble accInfectedProportionSecond;
    vectAccDouble accInfectiousProportionSecond;
    vectAccDouble accAffectedProportionSecond;
    
//    std::vector<double> bHeadcountMeta;
//    std::vector<double> bHeadcountFirst;
//    std::vector<double> bHeadcountFirstInit;
//    std::vector<double> bHeadcountSecond;
    
    
    accInt accInfectedByFirst;
    accInt accInfectedBySecond;
    accInt accInfectedByExt;
        
    accInt accInfectedByTestedMov;    
    
    accVectInt accNbHerdsWithIc; //
    accVectInt accNbHerdsWithMoreThanTwoIc; //
    
    accVectInt accNbInitiallyInfectedHerds; //
    accVectInt accNbOtherInfectedHerds; //
    
    accVectDouble accProportionOfOutgoingMovTested; //
    accVectDouble accProportionOfOutgoingInfectedMovTested; //
    
    accVectDouble accProportionOfIncomingMovTested; //
    accVectDouble accProportionOfIncomingInfectedMovTested; //
    
    std::vector<int>  vNumberOfOutgoingMovTested; //
    std::vector<int>  vNumberOfOutgoingMov; //
    
    std::vector<int>  vNumberOfIncomingMovTested; //
    std::vector<int>  vNumberOfIncomingMov; //
    
    std::vector<int> vIntroducionOfInfectiousAnimalsIfNoTest;
    std::vector<int> vIntroducionOfInfectiousAnimalsAvoided;
    
    std::vector<double> vProportionOfIntroducionOfInfectiousAnimalsAvoided;
    
    accVectQuantiles accInfectedHerdsQuantiles;
    accVectQuantiles accAlreadyInfectedHerdsQuantiles;
    accVectQuantiles accAffectedHerdsQuantiles;
    
    accVectQuantiles accInfectedProportionMetaQuantiles;
    accVectQuantiles accInfectiousProportionMetaQuantiles;
    accVectQuantiles accAffectedProportionMetaQuantiles;
    
    std::vector<double> vPrevalenceInInfectedHerdsAtTheEnd;
    std::vector<std::vector<double>> vvPrevalenceInInfectedHerdsAtTheEnd;
    
    
    
    std::vector<std::vector<std::string>> vInfectionDurationWhenExtinction;
    std::vector<std::vector<std::string>> vInfectionDurationIfNoExtinction;
    
    std::vector<std::vector<std::string>> vExtinctionBefore1Years;
    
    std::vector<std::vector<std::string>> vExtinctionBefore2Years;
    
    std::vector<std::vector<std::string>> vExtinctionBefore3Years;
    
    std::vector<std::vector<std::string>> vExtinctionBefore5Years;
    std::vector<std::vector<std::string>> vNoExtinctionBefore5Years;

    
    
    std::vector<std::vector<std::string>> vvInfosHerdPrevalenceAfter5Years;
    
//    std::vector<std::vector<double>> vvDynamicHerdPrevalenceInfectedAfter5Years;
//    std::vector<std::vector<double>> vvDynamicHerdPrevalenceInfectiousAfter5Years;
//    std::vector<std::vector<double>> vvDynamicHerdPrevalenceAffectedAfter5Years;
    
    
//    std::vector<std::vector<double>> vvPersistenceDynamicOver5Years;
    std::vector<std::vector<std::string>> vvPersistenceOver5Years;
    
    
    std::vector<std::vector<std::string>> vSourceAndDestinationOfInfectedMovements_SourceIsFirst;
    std::vector<std::vector<std::string>> vSourceAndDestinationOfInfectedMovements_SourceIsSecond;
    std::vector<std::vector<std::string>> vSourceAndDestinationOfInfectedMovements_SourceIsExt;
    std::vector<std::vector<std::string>> vNbInfectedMovementsPerHerd; //
    
    
    std::vector<int> vNumberOfTSaleByPrimary;
    std::vector<int> vNumberOfLSaleByPrimary;
    std::vector<int> vNumberOfIsSaleByPrimary;
    
    std::vector<int> vNumberOfNewInfectionFromPrimary;
    
    
    std::vector<std::vector<int>> vvMatrixOfInfectedHerdsAfterHalfTheSimulationTime;
    std::vector<std::vector<int>> vvMatrixOfInfectedHerdsAtTheEndOfTheSimulationTime;
    std::vector<std::vector<int>> vvMatrixOfAlreadyInfectedHerdsAfterHalfTheSimulationTime;
    std::vector<std::vector<int>> vvMatrixOfAlreadyInfectedHerdsAtTheEndOfTheSimulationTime;
    
    std::vector<std::vector<int>> vvMatrixOfNumberOfInfection;
    std::vector<std::vector<int>> vvMatrixOfNumberOfInfectedAnimalsPurchased;
    std::vector<std::vector<int>> vvMatrixOfNumberOfWeeksWithInfection;
    
    std::vector<std::vector<int>> vvPrimaryCaseMatrix;
        
//    std::vector<std::vector<int>> vvMatrixOfNumberOfAnimalsTestedMovIn;
//    std::vector<std::vector<int>> vvMatrixOfNumberOfAnimalsTestedMovOut;
    std::vector<std::vector<int>> vvMatrixOfNumberOfIcAnimalsCulled;
    std::vector<std::vector<int>> vvMatrixOfNumberOfAnimalsCulledDuringTestAndCull;
    std::vector<std::vector<int>> vvMatrixOfNumberOfAnimalsTestedDuringTestAndCull;
    
    
    std::vector<double> vMedianInfectedHerds;
    std::vector<double> vInfectedHerds;
    std::vector<double> vAlreadyInfectedHerds;
    std::vector<double> vAffectedHerds;
    
    std::vector<double> vInfectedProportionMeta;
    std::vector<double> vInfectiousProportionMeta;
    std::vector<double> vAffectedProportionMeta;
    
    accVectInt accNbHerdWithTestOfMovements;
    accVectInt accNbHerdWithCullingImprovement;
    accVectInt accNbHerdWithHygieneImprovement;
    accVectInt accNbHerdWithCalfManagementImprovement;
    accVectInt accNbHerdWithTestAndCull;



    std::vector<std::vector<double>> vvMatrixOfWithinHerdPrevalenceOverTime;

    std::map<std::string, accVectDouble> mFarms_AccVectWithinHerdPrevalence;
    
    // Member functions ====================================================
    void updateAcc(const int& persistence, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary);
    
};

#endif
