//
//  DairyHerd.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 29/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_DairyHerd_h
#define ParatubBetweenDairyHerd_DairyHerd_h

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <random>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <stdexcept>
#include <assert.h>

#include "Parameters.h"
#include "ParametersSpecificToEachRepetition.h"
#include "ParametersSpecificToEachHerd.h"
//#include "AgeCompartments.h"
#include "HealthStateCompartmentsContainer.hpp"
#include "SummaryInfection.h"
#include "SummaryDynamic.h"
//#include "Births.h"
#include "BirthsContainer.hpp"
#include "HerdExit.h"
#include "MapShedding.h"
#include "MapInEnvironments.h"
#include "TransitionsTLIsIc.h"
#include "NewInfectionsST.h"
#include "UtilitiesFunctions.h"
#include "PersistentResults.h"
#include "BufferPersistentResults.h"
#include "MetapopSummary.h"
#include "VectorPlus.h"
#include "VectorMinus.h"
#include "MovementUnit.h"
#include "AnimalMoving.h"

class DairyHerd {
    // Variables ===========================================================
    
public:
    // Variables ===========================================================
    // Parameters specific of the herd
    ParametersSpecificToEachHerd iParametersSpecificToEachHerd;
    // Summary of headcounts for the health states and other
    HealthStateCompartmentsContainer iHealthStateCompartmentsContainer;
//    AgeCompartments iCompartmentSR, iCompartmentT, iCompartmentL, iCompartmentIs, iCompartmentIc;
    // Births objects
    BirthsContainer iBirthsContainer;
//    Births iBirthSR, iBirthT, iBirthL, iBirthIs, iBirthIc;
    // Exits objects
    HerdExit exit;
    // SummaryInfection
    SummaryDynamic iSummaryDynamic;
    SummaryInfection iSummaryInfection;
    // Bacteria shed by animals
    MapShedding QtyBacteria;
    // Amount of bacteria present in the different environments.
    MapInEnvironments Environments;
    // Transitions between health states
    TransitionsTLIsIc Transitions;
    // New infections
    NewInfectionsST NewInfections;
    // Persistence indicator
    bool persistenceIndicator;
    // Vectors of pending movements
    std::vector<AnimalMoving> vPendingMoveVectors;
//    std::vector<int> vMovesToReceiveAfterDynamics;
    
    int firstCaseDate;
    bool hasAlreadyBeenInfected;
    
    std::vector<short int> vInfectionDynamic; // 2: infected, 1:not infected but was previously infected in the past, 0: not infected and never infected
    
    bool firstInfectedHerd;
    int typeInfectedHerd;
    int numberOfInfection;
    int numberOfInfectedAnimalsPurchased;
    
    bool anExtinctionOccurred;
    
    int numberOfWeeksSinceInfected;
    int numberOfWeeksSinceInfectious;
    int numberOfWeeksWithInfection;
    int dateOfAppearanceOfTheFirstIc;
    int StartDateOfTheLastPeriodOfInfection;
    
    int numberOfPurchasesOfInfectedAnimalsT;
    int numberOfPurchasesOfInfectedAnimalsL;
    int numberOfPurchasesOfInfectedAnimalsIs;
    
    int numberOfSalesOfInfectedAnimalsT;
    int numberOfSalesOfInfectedAnimalsL;
    int numberOfSalesOfInfectedAnimalsIs;
    
    double prevalenceMaxForTheLastPeriodOfInfection;
    int cumulativeNumberOfIcForTheLastPeriodOfInfection;
    int numberOfWeeksWithInfectedAnimalsForTheLastPeriodOfInfection;
    
    int typeOfFirstAnimalPurchase;
    
    int cumulativeNumberOfIc;
    int presenceOfIcAtThisTime;
    
    int IcCulled;
    int numberOfAnimalsTestedDuringTestAndCull;
    int numberOfAnimalsCulledDuringTestAndCull;
    
    
//    std::vector<int> vNbBirthsPerYear;
//    std::vector<double> vNbBirthsPerYearPerAgeGroup;
//    std::vector<int> vNbBirthsPerAgeGroup;
    
    int birthFake;
    int birthTot;
    
    int numberOfAnimalInfectedSendWhenNotInitiallyInfected;
    int numberOfAnimalInfectedSendToMetapopWhenNotInitiallyInfected;
    
    double distanceFromObservation = 0;
    
    std::vector<double> vSummaryStatistics;
    
    std::vector<short int> vSummaryStatistics_N;
    std::vector<short int> vSummaryStatistics_N_pos;
//    std::vector<short int> vSummaryStatistics_N_neg;
    std::vector<short int> vSummaryStatistics_Nech;
    std::vector<short int> vSummaryStatistics_Nech_pos;
//    std::vector<short int> vSummaryStatistics_Nech_neg;
    
//    int summaryStatistic = 0;
    
    // Constructor =========================================================
    DairyHerd();
    DairyHerd(const ParametersSpecificToEachRepetition& ParamRep, const std::string& theId);
    
    // Member functions ====================================================
    void Initialize();
    
    void InitializeInfectionInHerd(const ParametersSpecificToEachRepetition& ParamRep, int initTime);


    bool InitializeInfectionInHerdForABC(const ParametersSpecificToEachRepetition& ParamRep);


    void Compute(const int& timeStep, const ParametersSpecificToEachRepetition& ParamRep, int& weeknumber);
    
    void UpdateSummary(const int& timeStep, MetapopSummary& iMetapopSummary);
    
    void initMetapopSummary(const int& timeStep,MetapopSummary& iMetapopSummary);
    
    void updateMetapopSummary(const int& timeStep, MetapopSummary& iMetapopSummary);
    
    void UpdatePastSummary(const int& timeStep);
    
     void AddingInfectedAnimals(const int& timeStep, int numberOfAnimalsIntroduced);
    
    void computeBirthEvents(const ParametersSpecificToEachRepetition& ParamRep, const int& timeStep);
    
    void SendOutMoves(const ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, std::map<std::string, std::map<int, std::vector<MovementUnit>> >& mmOutMoves, std::map<std::string, DairyHerd>& mFarms, std::vector<int>& vAgeMov, long double& countMov, std::vector<int>& vAgeFailure, long double& countAgeFailure, std::vector<int>& vAgeMovAfterCorrection, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary);
    
    void sendAnimal(std::string healthState, const int& timeStep, const int& age, const int& indexMov, std::map<std::string, DairyHerd>& mFarms,  std::map<std::string, std::map<int, std::vector<MovementUnit>> >& mmOutMoves, BufferPersistentResults& iBufferPersistentResults);
    
    void replaceMovementsByExtIfFailureOrNotYetInTheMetapopulation(const ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, const std::string& theFarmId, const int& age, MetapopSummary& iMetapopSummary, std::map<std::string, DairyHerd>& mFarms, std::map<std::string, std::map<int, std::vector<MovementUnit>> >& mmOutMoves, const int& movementIndex);
    
    void addPendingMov(const int& timeStep, const AnimalMoving& mov);
    
    int ageSelectionForTheMovement(const int& rawAge, const ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, const std::string& theFarmId);
    void ageSelection(const ParametersSpecificToEachRepetition& ParamRep, int& age, int& ageBuffer, int& lowerAgeLimit, int& upperAgeLimit, bool& allAgeCheck);
    
    void isTheTestAndCullMeasureShouldBeApplied(const ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, const int& firstAge, const int& lastAge, const double& proportionAnimalsTested, const MetapopSummary& iMetapopSummary);
    
    void testAndCull(const ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, const int& firstAge, const int& lastAge, const double& proportionAnimalsTested, const MetapopSummary& iMetapopSummary);
    
    void testAndRemoveAnimals(const ParametersSpecificToEachRepetition& ParamRep, const std::vector<int>& vNbIndivForEachHealthStates, const int& firstAge, const MetapopSummary& iMetapopSummary, const int& index);
    
    void RemoveAnimalsAfterTest(const ParametersSpecificToEachRepetition& ParamRep, AgeCompartments& iCompartment, const int& firstAge, const MetapopSummary& iMetapopSummary, const int& index);
    
    void statusChoice(const ParametersSpecificToEachRepetition& ParamRep, const MetapopSummary& iMetapopSummary, const int& age);
    
    void updateBufferPersistentResults(const ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, BufferPersistentResults& iBufferPersistentResults, const int& run);
    
    void simulateLastSampling(ParametersSpecificToEachRepetition& ParamRep, const int& timeStep);
    
    void simulateSamplingAndMoveAnimalsInPositiveTestedCompartment(ParametersSpecificToEachRepetition& ParamRep, const int& timeStep);
    
    void simulateSamplingAndMoveAnimalsInPositiveTestedCompartmentForFirstTimeStep(ParametersSpecificToEachRepetition& ParamRep, const int& timeStep);
    
};

#endif
