//
//  HerdCharacteristics.h
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunee on 26/04/2015.
//  Copyright (c) 2015 Gaël Beaunée. All rights reserved.
//

#ifndef __ParatubBetweenDairyHerd__HerdCharacteristics__
#define __ParatubBetweenDairyHerd__HerdCharacteristics__

#include <stdio.h>
#include <algorithm>

class HerdCharacteristics {

public:
    // Constructor =========================================================
    HerdCharacteristics();
    HerdCharacteristics(const int& theHerdSize, const double& theInDegree, const double& theOutDegree, const double& theInStrengthAll, const double& theOutStrengthAll);

    // Variables ===========================================================
    int herdSize;
    double inDegree;
    double outDegree;
    double inStrengthAll;
    double outStrengthAll;
    
    int beginDate = 0;
    double prevInit;
    int lastSampleDate = -999;
    int lastSampleHeadcount;
    int lastSamplesCount;
    int lastPositiveSamplesCount;
    bool dataForFirstAndLast;
    
    double proportionOfSample = 0;
    double proportionOfPositiveSample = 0;
};

#endif /* defined(__ParatubBetweenDairyHerd__HerdCharacteristics__) */
