//
//  MapShedding.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 04/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#include "MapShedding.h"

// Constructor =========================================================
MapShedding::MapShedding(){
    QtyTns = 0;
    QtyTs1 = 0;
    QtyTs2 = 0;
    QtyTy = 0;
    QtyTg = 0;
    
    QtyIsg = 0;
    QtyIs = 0;
    QtyIcg = 0;
    QtyIc = 0;
    
    milkTot = 0;
    QtyMilkIs = 0;
    QtyMilkIc = 0;
}


// Member functions ====================================================
void MapShedding::update(gsl_rng * randomGenerator, const int& t, const HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer){
    // Int and Ext
    int nbTns = iHealthStateCompartmentsContainer.iCompartmentT.calvesUnweaned[t-1] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.calvesUnweaned[t-1];
    int nbTs1 = iHealthStateCompartmentsContainer.iCompartmentT.calvesWeanedIn[t-1] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.calvesWeanedIn[t-1];
    int nbTs2 = iHealthStateCompartmentsContainer.iCompartmentT.calvesWeanedOut[t-1] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.calvesWeanedOut[t-1];
    int nbTy = iHealthStateCompartmentsContainer.iCompartmentT.yHeifer[t-1] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.yHeifer[t-1];
    int nbTg = iHealthStateCompartmentsContainer.iCompartmentT.heifer[t-1] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.heifer[t-1];
    
    QtyTns = 0; QtyTs1 = 0; QtyTs2 = 0; QtyTy = 0; QtyTg = 0;
    
    if (nbTns>0) {
        for (int i = 1; i <= nbTns; i++) {
            QtyTns += Parameters::qtyFaecesCnw * pow(10, 4) * 100 * gsl_ran_beta(randomGenerator, Parameters::alphaFaecesT, Parameters::betaFaecesT);
        }
    }
    if (nbTs1>0) {
        for (int i = 1; i <= nbTs1; i++) {
            QtyTs1 += Parameters::qtyFaecesCw * pow(10, 4) * 100 * gsl_ran_beta(randomGenerator, Parameters::alphaFaecesT, Parameters::betaFaecesT);
        }
    }
    if (nbTs2>0) {
        for (int i = 1; i <= nbTs2; i++) {
            QtyTs2 += Parameters::qtyFaecesCw * pow(10, 4) * 100 * gsl_ran_beta(randomGenerator, Parameters::alphaFaecesT, Parameters::betaFaecesT);
        }
    }
    if (nbTy>0) {
        for (int i = 1; i <= nbTy; i++) {
            QtyTy += Parameters::qtyFaecesHf * pow(10, 4) * 100 * gsl_ran_beta(randomGenerator, Parameters::alphaFaecesT, Parameters::betaFaecesT);
        }
    }
    if (nbTg>0) {
        for (int i = 1; i <= nbTg; i++) {
            QtyTg += Parameters::qtyFaecesAd * pow(10, 4) * 100 * gsl_ran_beta(randomGenerator, Parameters::alphaFaecesT, Parameters::betaFaecesT);
        }
    }
    
    
    
    int nbIsg = iHealthStateCompartmentsContainer.iCompartmentIs.heifer[t-1] + iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.heifer[t-1];
    int nbIs = iHealthStateCompartmentsContainer.iCompartmentIs.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.lactAll[t-1];
    int nbIcg = iHealthStateCompartmentsContainer.iCompartmentIc.heifer[t-1] + iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.heifer[t-1];
    int nbIc = iHealthStateCompartmentsContainer.iCompartmentIc.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.lactAll[t-1];
    
    QtyIsg = 0; QtyIs = 0; QtyIcg = 0; QtyIc = 0;
    
    if (nbIsg>0) {
        for (int i = 1; i <= nbIsg; i++) {
            QtyIsg += Parameters::qtyFaecesAd * pow(10, (4+10*gsl_ran_beta(randomGenerator, Parameters::alphaFaecesIs, Parameters::betaFaecesIs)));
        }
    }
    if (nbIs>0) {
        for (int i = 1; i <= nbIs; i++) {
            QtyIs += Parameters::qtyFaecesAd * pow(10, (4+10*gsl_ran_beta(randomGenerator, Parameters::alphaFaecesIs, Parameters::betaFaecesIs)));
        }
    }
    if (nbIcg>0) {
        for (int i = 1; i <= nbIcg; i++) {
            QtyIcg += Parameters::qtyFaecesAd * pow(10, (8+10*gsl_ran_beta(randomGenerator, Parameters::alphaFaecesIc, Parameters::betaFaecesIc)));
        }
    }
    if (nbIc>0) {
        for (int i = 1; i <= nbIc; i++) {
            QtyIc += Parameters::qtyFaecesAd * pow(10, (8+10*gsl_ran_beta(randomGenerator, Parameters::alphaFaecesIc, Parameters::betaFaecesIc)));
        }
    }
    

    
    
    QtyTg *= Parameters::adultsTogether;
    QtyIsg *= Parameters::adultsTogether;
    QtyIs *= Parameters::adultsTogether;
    QtyIcg *= Parameters::adultsTogether;
    QtyIc *= Parameters::adultsTogether;
    
    // Milk
    if (Parameters::milkTR == true) {
        QtyMilkIs = 0; QtyMilkIc = 0;
        
        double LactR = gsl_ran_binomial(randomGenerator, Parameters::propLactatingCows, (iHealthStateCompartmentsContainer.iCompartmentSR.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.lactAll[t-1]) );
        double LactL = gsl_ran_binomial(randomGenerator, Parameters::propLactatingCows, (iHealthStateCompartmentsContainer.iCompartmentL.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.lactAll[t-1]) );
        double LactIs = gsl_ran_binomial(randomGenerator, Parameters::propLactatingCows, (iHealthStateCompartmentsContainer.iCompartmentIs.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.lactAll[t-1]) );
        double LactIc = gsl_ran_binomial(randomGenerator, Parameters::propLactatingCows, (iHealthStateCompartmentsContainer.iCompartmentIc.lactAll[t-1] + iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.lactAll[t-1]) );
        
        milkTot = Parameters::qtyMilkProd * LactR + Parameters::qtyMilkProdL * LactL + Parameters::qtyMilkProdIs * LactIs + Parameters::qtyMilkProdIc * LactIc;
        
        double excrIs = gsl_ran_binomial(randomGenerator, Parameters::propExcreMilkIs, LactIs);
        for (int i = 1; i<=excrIs; i++) {
            QtyMilkIs += Parameters::qtyMilkProdIs * (pow(10, 5) * gsl_ran_beta(randomGenerator, Parameters::alphaMilkDirIs, Parameters::betaMilkDirIs));
        }
        for (int i = 1; i<=LactIs; i++) {
            QtyMilkIs += Parameters::qtyMilkProdIs * (1 + 1000 * gsl_ran_beta(randomGenerator, Parameters::alphaMilkIndirIs, Parameters::betaMilkIndirIs));
        }
        
        double excrIc = gsl_ran_binomial(randomGenerator, Parameters::propExcreMilkIc, LactIc);
        for (int i = 1; i<=excrIc; i++) {
            QtyMilkIc += Parameters::qtyMilkProdIc * (pow(10, 5) * gsl_ran_beta(randomGenerator, Parameters::alphaMilkDirIc, Parameters::betaMilkDirIc));
        }
        for (int i = 1; i<=LactIc; i++) {
            QtyMilkIc += Parameters::qtyMilkProdIc * pow(10, (3+10*gsl_ran_beta(randomGenerator, Parameters::alphaMilkIndirIc, Parameters::betaMilkIndirIc)));
        }
    }
    
}
