//
//  AnimalMoving.cpp
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunee on 26/04/2015.
//  Copyright (c) 2015 Gaël Beaunée. All rights reserved.
//

#include "AnimalMoving.h"

AnimalMoving::AnimalMoving(){}

AnimalMoving::AnimalMoving(const std::vector<int>& vect, const std::string& theSourceId){
    date = vect[0];
    healthState = vect[1];
    age = vect[2];
    typeOfInfectedHerd = vect[3];
    presenceOfIcAtThisTime = vect[4];
    presenceOfAtLeastTwoIcAtThisTime = vect[5];
    numberOfOutgoingMovementsAboveTheMinimum = vect[6];
    
    herdSourceID = theSourceId;
}