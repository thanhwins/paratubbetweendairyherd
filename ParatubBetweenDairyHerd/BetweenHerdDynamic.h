//
//  BetweenHerdDynamic.h
//  ParatubBetweenDairyHerd
//
//  Created by Gaël Beaunée on 03/01/2014.
//  Copyright (c) 2014 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_BetweenHerdDynamic_h
#define ParatubBetweenDairyHerd_BetweenHerdDynamic_h

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <random>
#include <gsl/gsl_randist.h>
#include <cmath>
#include <algorithm>
#include <stdexcept>
#include <boost/accumulators/numeric/functional/vector.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/sum.hpp>
#include <boost/accumulators/statistics/count.hpp>
#include <boost/accumulators/statistics/median.hpp>
#include <boost/accumulators/statistics/extended_p_square.hpp>

#ifdef ACTIVATE_PARALLEL
#include <omp.h>
#endif

#include "Parameters.h"
#include "MovementMatrix.h"
#include "ParametersSpecificToEachRepetition.h"
#include "DairyHerd.h"
#include "PersistentResults.h"
#include "BufferPersistentResults.h"
#include "GlobalResults.h"
#include "BufferGlobalResults.h"
#include "UtilitiesFunctions.h"
#include "Chronometer.h"
#include "MetapopSummary.h"
#include "MovementUnit.h"
#include "AnimalMoving.h"
#include "ABCInfosAndResults.h"
#include "ResultsWriting.h"

using namespace boost::accumulators;

typedef accumulator_set<int, stats<tag::mean, tag::variance, tag::count, tag::sum > > accInt;
typedef accumulator_set<double, stats<tag::mean, tag::variance, tag::count, tag::sum > > accDouble;
typedef accumulator_set<std::vector<int>, stats<tag::mean, tag::variance, tag::count, tag::sum > > accVectInt;
typedef accumulator_set<std::vector<double>, stats<tag::mean, tag::variance, tag::count, tag::sum > > accVectDouble;
typedef std::vector<accDouble> vectAccDouble;

typedef accumulator_set<double, stats<tag::extended_p_square> > accQuantiles;
typedef std::vector< accQuantiles > accVectQuantiles;

typedef std::vector<int> vInt;
typedef std::map<int, vInt> mInt2vInt;
typedef std::vector<mInt2vInt> vMapInt2vInt;
typedef std::vector<vInt> vIntVectors;
typedef std::vector<AnimalMoving> vAnimalMovingVectors;

class BetweenHerdDynamic {
    // Variables ===========================================================
    Chronometer chronoRun;//, chronoInit, chronoDynamic, chronoMovements, chronoComputeResults, chronoClearHerds;
    
    ////// Variables partagée de la section parallèle
    
    PersistentResults acc_persistentResults;
    GlobalResults acc_globalResults;
    
    ABCInfosAndResults iABCInfosAndResults;
    
    int nbPersistentSimu;
    
    int numberOfSimulationDone;
    
    // to check if there is a problem or not with the movements
    std::vector<int> vAgeMov;
    std::vector<int> vAgeMovAfterCorrection;
    std::vector<int> vAgeFailure;
    
    
    std::map<std::string, std::vector<double>> vvIntraHerdPrevalenceAtTheEnd;
    
    
public:
    // Constructor =========================================================
    BetweenHerdDynamic();
    
    // Member functions ====================================================

    void Initialize(ParametersSpecificToEachRepetition& ParamRep, std::map<std::string, DairyHerd>& mFarms, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary, BufferGlobalResults& iBufferGlobalResults, std::map<std::string, std::map<int, std::vector<MovementUnit>> >& mmOutMoves);
    


    void Run();



    void choiceOfHerdsInfectedUniform(ParametersSpecificToEachRepetition& ParamRep, std::map<std::string, DairyHerd>& mFarms, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary);
    
    void choiceOfHerdsInfectedPreferential(ParametersSpecificToEachRepetition& ParamRep, std::map<std::string, DairyHerd>& mFarms, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary);
    
    void choiceOfHerdsInfectedRandom(ParametersSpecificToEachRepetition& ParamRep, std::map<std::string, DairyHerd>& mFarms, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary);
    
    void choiceOfHerdsInfectedInABC(ParametersSpecificToEachRepetition& ParamRep, std::map<std::string, DairyHerd>& mFarms, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary);
    
    void choiceOfHerdsInfectedRandomAndPrevIntraRandom(ParametersSpecificToEachRepetition& ParamRep, std::map<std::string, DairyHerd>& mFarms, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary);
    
    
    void ReceiveInMoves(ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, const std::string& theFarmId, std::map<std::string, DairyHerd>& mFarms, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary, const int& run);
    
    void receiveAnimal(std::string healthState, const int& animalAge, ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, const std::string& theFarmId, std::map<std::string, DairyHerd>& mFarms, BufferPersistentResults& iBufferPersistentResults, const vAnimalMovingVectors::iterator& it, bool& numberOfIncomingMovsAboveTheMinimum, bool& numberOfOutgoingMovsAboveTheMinimum, MetapopSummary& iMetapopSummary, const int& run);
    
    void SendOutMovesExt(ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, std::map<std::string, DairyHerd>& mFarms, MetapopSummary& iMetapopSummary, BufferPersistentResults& iBufferPersistentResults, std::map<std::string, std::map<int, std::vector<MovementUnit>> >& mmOutMoves, long double& countMov, long double& countAgeFailure);
    
    
    bool Test(ParametersSpecificToEachRepetition& ParamRep, const int& status, const bool& aTestMustBeDone);
    
    
    void StatusChoice(ParametersSpecificToEachRepetition& ParamRep, const int& age, const std::string& theFarmId, const int& typeHerdSource, std::map<std::string, DairyHerd>& mFarms, MetapopSummary& iMetapopSummary, BufferPersistentResults& iBufferPersistentResults);
    
    void StatusChoiceBis(ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, const int& type, const int& age, const std::string& theFarmId, const int& typeHerdSource, std::map<std::string, DairyHerd>& mFarms, const std::string& herdSource, const int& typeOfInfectedHerdSource, MetapopSummary& iMetapopSummary, BufferPersistentResults& iBufferPersistentResults, const int& run);
    
    void ExecutionTimeDisplay();
    void SaveResults();
    


    
    void saveInfectionDynamic(int run, std::map<std::string, DairyHerd>& mFarms);
    
    void updateAccSecondPrev(std::map<std::string, DairyHerd>& mFarms);
    
    void updateSourceAndDestinationResults(const int& timeStep, const std::string& theFarmId, const int& type, const int& age, std::map<std::string, DairyHerd>& mFarms, const std::string& herdSource, const int& typeOfInfectedHerdSource, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary, const int& run);

    void updateIntraHerdPrevalenceAtTheEnd(std::map<std::string, DairyHerd>& mFarms);
    void saveIntraHerdPrevalenceAtTheEnd();

    void saveWithinHerdPrevalenceOverTime(std::map<std::string, DairyHerd>& mFarms, const int& theRun);
};

#endif
