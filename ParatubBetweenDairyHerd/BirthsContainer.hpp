//
//  BirthsContainer.hpp
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunée on 08/06/2016.
//  Copyright © 2016 Gaël Beaunée. All rights reserved.
//

#ifndef BirthsContainer_hpp
#define BirthsContainer_hpp

#include <stdio.h>

#include "Parameters.h"
#include "Births.h"
#include "UtilitiesFunctions.h"

struct BirthsContainer {
    // Constructor =========================================================
    BirthsContainer();
    
    // Variables ===========================================================
    Births iBirthSR, iBirthT, iBirthL, iBirthIs, iBirthIc;
    Births iBirthSR_positiveTested, iBirthT_positiveTested, iBirthL_positiveTested, iBirthIs_positiveTested, iBirthIc_positiveTested;
    
    // Member functions ====================================================
};

#endif /* BirthsContainer_hpp */
