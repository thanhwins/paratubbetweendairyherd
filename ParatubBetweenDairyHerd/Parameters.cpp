//
//  Parameters.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 25/06/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#include "Parameters.h"

// Static variables ====================================================
std::string Parameters::folderPath;
std::string Parameters::ResultsFolderPath;

#ifdef CLUSTER
std::string Parameters::ABCfolderPath = "../../";
#endif

#ifndef CLUSTER
std::string Parameters::ABCfolderPath = "/Users/gaelbeaunee/Work/PhD_Thesis/Models/BetweenHerds/ParatubBetweenDairyHerd/";
#endif

int Parameters::nbThreads;
// Seed and random number generator
gsl_rng * Parameters::randomGenerator;
long long Parameters::Seed;
// General information about simulations -------------------------------
int Parameters::nbRuns;
int Parameters::firstRunID;
int Parameters::nbRunsWithNan = 0;
double Parameters::nbHerds;
double Parameters::propHerdsInfected;
int Parameters::nbHerdsInfected;
int Parameters::nbInfectedAnimalsIntroduced;
int Parameters::nbYears;
int Parameters::simutime;
//std::vector<std::string> Parameters::vListOfHerdsId;
// Temporal parametres -------------------------------------------------
// Calving-to-calving interval
constexpr double Parameters::dLacta;
constexpr double Parameters::dLactaI;
// Herd size and its composition ---------------------------------------
//constexpr double Parameters::propSS;
//constexpr double Parameters::sexRatioFemale;
// Output parameters ---------------------------------------------------
// Sales
// Death-rate
constexpr double Parameters::deathRateIs;
constexpr double Parameters::deathRateIc;
// Culling-rate
constexpr double Parameters::cullingRateIs;
double Parameters::cullingRateIc;
// Bacteria death-rate
double Parameters::deathRateBactInside;
double Parameters::deathRateBactOutside;
// Cleaning
double Parameters::cleaningCP;
// Transmission parameters ---------------------------------------------
// Transmission routes
//bool Parameters::colostrumTR;
//bool Parameters::milkTR;
// Map infectious dose
double Parameters::infectiousDose;
// Infection rate
double Parameters::infGlobalEnv;
double Parameters::infGrazing;
double Parameters::infLocalEnv;
double Parameters::infIngestion;
double Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation;
double Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation_initialValue;
double Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope_global;
double Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope;
double Parameters::typeOfTheRiskToPurchaseInfectedAnimalOutsideTheMetapopulation;
// Vertical transmission
constexpr double Parameters::infinuteroT;
constexpr double Parameters::infinuteroL;
constexpr double Parameters::infinuteroIs;
constexpr double Parameters::infinuteroIc;
// Quantity of bacteria shed in the different environments -------------
// Quantity of faeces produced
constexpr double Parameters::qtyFaecesCnw;
constexpr double Parameters::qtyFaecesCw;
constexpr double Parameters::qtyFaecesHf;
constexpr double Parameters::qtyFaecesAd;
// Quantity of milk and colostrum drunk and produced
constexpr double Parameters::qtyMilkDrunk;
constexpr double Parameters::qtyColoDrunk;
constexpr double Parameters::qtyMilkProd;
constexpr double Parameters::qtyColoProd;
constexpr double Parameters::propLactatingCows;
constexpr double Parameters::qtyMilkProdL;
constexpr double Parameters::qtyMilkProdIs;
constexpr double Parameters::qtyMilkProdIc;
// Map shedding in faeces
double Parameters::alphaFaecesIs;
double Parameters::betaFaecesIs;
double Parameters::alphaFaecesIc;
double Parameters::betaFaecesIc;
double Parameters::alphaFaecesIss;
double Parameters::betaFaecesIss;
double Parameters::alphaFaecesIcs;
double Parameters::betaFaecesIcs;


std::map<std::string, HerdCharacteristics> Parameters::mFarmsCharacteristics;
std::vector<std::string> Parameters::vFarmID;

std::map<std::string, std::vector<short int>> Parameters::mHerdHeadcounts;
std::map<std::string, std::vector<double>> Parameters::mHerdCullingRates;
std::map<std::string, std::vector<double>> Parameters::mHerdBirthsRatio;
std::map<std::string, std::vector<short int>> Parameters::mHerdBirthEvents;


bool Parameters::SelectingInfectedHerdInAPreferentialWay;
int Parameters::TypeOfPreferentialWay;
std::vector<double> Parameters::vHerdDataForSelectingTheInfectedHerdsUniform;
std::vector<double> Parameters::vHerdDataForSelectingTheInfectedHerdsPreferentialOutDegree;
std::vector<double> Parameters::vHerdDataForSelectingTheInfectedHerdsPreferentialNumberOfOutMovements;

std::vector<std::vector<std::vector<std::vector<double>>>> Parameters::vvvProbaInitInfection;

bool Parameters::initializeHerdsWithEndemicState;
int Parameters::initializeSimutime;
int Parameters::endemicPeriodMin;
int Parameters::endemicPeriodMax;

double Parameters::muDistributionPrevIntra;
double Parameters::sigmaDistributionPrevIntra;

double Parameters::alphaBetaDistributionPrevIntra;
double Parameters::betaBetaDistributionPrevIntra;

std::string Parameters::typeOfWithinHerdPrevalenceDistribution;

int Parameters::maxPrevLevel;
std::map<int, std::vector<std::vector<std::vector<double>>>> Parameters::vvvPrevalenceLevelDistribution;
std::map<int, std::vector<std::vector<double>>> Parameters::vvPrevalenceLevelAverageDistribution;

int Parameters::startTest;
double Parameters::testSp;
double Parameters::testSeT;
double Parameters::testSeL;
double Parameters::testSeIs;

int Parameters::typeOfTestStrategy;
double Parameters::minimumOfIncomingMovements;
double Parameters::numberOfHerdsAboveTheMinimumOfIncomingMovements;
double Parameters::minimumOfOutgoingMovements;
double Parameters::numberOfHerdsAboveTheMinimumOfOutgoingMovements;

bool Parameters::replacementAfterTestAndCull;
double Parameters::testAndCullSp;
double Parameters::testAndCullSeT;
double Parameters::testAndCullSeL;
double Parameters::testAndCullSeIs;
double Parameters::testAndCullSeIc;
int Parameters::testAndCullMinimumAge;
int Parameters::testAndCullMaximumAge;
double Parameters::proportionOfAnimalsTested;
double Parameters::proportionOfAnimalsRemoved;


int Parameters::delayBetweenTheOnsetOfAnIcAndTheImplementationOfTheTestAndCull;

double Parameters::decreaseOfAdultToCalfContact;

bool Parameters::withinHerdBiosecurity;

double Parameters::cullingRateIcIfCullingImprovement;
double Parameters::deathRateBactInsideIfHygieneImprovement;
double Parameters::infGlobalEnvIfCalfManagementImprovement;

double Parameters::proportionOfFarmsWithTestAtPurchase;
double Parameters::proportionOfFarmsWithCullingImprovement;
double Parameters::proportionOfFarmsWithHygieneImprovement;
double Parameters::proportionOfFarmsWithCalfManagementImprovement;
double Parameters::proportionOfFarmsWithTestAndCull;



bool Parameters::ABCrejection;
bool Parameters::ABCsmc;
bool Parameters::ABCcompleteTrajectories;

bool Parameters::computeSummaryStatistics;

std::vector<std::string> Parameters::vHoldingsForResume;

int Parameters::ABCsmcRunNumber;
int Parameters::ABCsmcTotalNumberOfRuns;
int Parameters::numberOfAcceptedParticlesRequiredForTheFirstRun;
int Parameters::numberOfAcceptedParticlesRequiredToGoToTheNextRun;

bool Parameters::ABCsmcCurrentRunIsCompleted;
int Parameters::ABCsmcSequenceNumber;

int Parameters::ABCcompleteTrajectoriesSequenceNumber;

double Parameters::ABCsmcThreshold;

double Parameters::perturbationKernelparameter;

double Parameters::abcTestSensitivity;
double Parameters::abcTestSensitivity_min;
double Parameters::abcTestSensitivity_max;
double Parameters::abcTestSpecificity;

double Parameters::probaToPurchaseInfectedAnimal_initialValue_min;
double Parameters::probaToPurchaseInfectedAnimal_initialValue_max;

double Parameters::probaToPurchaseInfectedAnimal_slope_min_global;
double Parameters::probaToPurchaseInfectedAnimal_slope_min;
double Parameters::probaToPurchaseInfectedAnimal_slope_max_global;
double Parameters::probaToPurchaseInfectedAnimal_slope_max;

double Parameters::abcInfGlobalEnv_min;
double Parameters::abcInfGlobalEnv_max;

double Parameters::abcMuDistributionPrevIntra_min;
double Parameters::abcMuDistributionPrevIntra_max;
double Parameters::abcSigmaDistributionPrevIntra_min;
double Parameters::abcSigmaDistributionPrevIntra_max;


double Parameters::cullingRatePositiveTestedAnimals;

std::map<std::string, std::map<int, ObservedSampleInformations>> Parameters::mFarmsDateSampleCount;


// Constructor =========================================================
Parameters *Parameters::instance = NULL;


Parameters& Parameters::get_instance(int argc, const char * argv[]){
    if (instance == NULL) {
        instance = new Parameters(argc, argv);
    }
    return *instance;
}


void Parameters::kill(){
    if (instance != NULL) {
        delete instance;
    }
}


Parameters::Parameters(int argc, const char * argv[]){
    // ------------------------------------------------------------------------
    // Wrap everything in a try block.  Do this every time,
    // because exceptions will be thrown for problems.
    try {

        // Define the command line object, and insert a message
        // that describes the program. The "Command description message"
        // is printed last in the help text. The second argument is the
        // delimiter (usually space) and the last one is the version number.
        // The CmdLine object parses the argv array based on the Arg objects
        // that it contains.
        TCLAP::CmdLine cmd("This is the ParatubBetweenDairyHerd model.", ' ', "1.0");

        // Define a value argument and add it to the command line.
        // A value arg defines a flag and a type of value that it expects,
        // such as "-r 10".
        TCLAP::ValueArg<std::string> initPathDirectory("i", "init-data-dir", "path to the directory containing the data folder", true, "", "string");
//        TCLAP::ValueArg<std::string> getParamFilePath("p", "param-file-path", "path to the file containing the parameter values: association of key-value", false, "", "string", cmd);

        TCLAP::ValueArg<std::string> outputDirectory("o", "output-dir", "path to the ouput directory", true, "Results/", "string");
        TCLAP::ValueArg<int> trajectorySequenceNumber("", "trajSeqNumber", "the trajectory sequence number (scenario id) use to define output file name when results are kept for each run", false, 1, "int");

        TCLAP::ValueArg<double> proportionOfHerdInfected("", "p-propInf", "proportion of herd initially infected", false, 0.5, "double");

        TCLAP::ValueArg<double> withinHerdPrevalenceMean("", "p-prevMu", "within herd mean prevalence in initially infected herd", false, 1.7, "double");
        TCLAP::ValueArg<double> withinHerdPrevalenceSigma("", "p-prevSigma", "within herd sigma prevalence in initially infected herd", false, 10.00, "double");
        TCLAP::ValueArg<std::string> withinHerdPrevalenceTypeDist("", "p-typeDist", "type of the distribution for the within herd prevalence in initially infected herd ('gaussian' or 'beta' (default))", false, "beta", "std::string");

        TCLAP::ValueArg<double> probaToPurchaseAnInfectedAnimal("", "p-probPI", "probability to purchase an infected animal", false, 0.1, "double");
        TCLAP::ValueArg<double> slopeProbaToPurchaseAnInfectedAnimal("", "p-slopeProbPI", "slope of the probability to purchase an infected animal", false, 0.0, "double");

        TCLAP::ValueArg<double> infRateThroughGeneralEnv("", "p-betaG", "infection rate through the general environment", false, 9.5e-7, "double");
        TCLAP::ValueArg<double> sensitivityOfTheDiagnosticTest("", "p-diagTestSe", "sensitivity of the diagnostic test", false, 0.3, "double");

        TCLAP::ValueArg<int> numberOfRuns("r", "nbRuns", "number of repetition to be considered for the simulation", false, 2, "int");
        TCLAP::ValueArg<int> firstRunId("f", "firstRunId", "index of the first run (use when necessary to complete results)", false, 1, "int");
        TCLAP::ValueArg<int> numberOfThreads("j", "nbThreads", "the number of threads to use if compile with the right option (CFLAGS=-DACTIVATE_PARALLEL ...)", false, 1, "int");

        TCLAP::SwitchArg inferenceCLA("", "inferenceCLA", "use this arg. if the numerical experience concerns inference with composite likelihood approximation methods", false);
        TCLAP::SwitchArg cmptSmmrSttstcs("", "cmptSmmrSttstcs", "use this arg. if summary statistics have to be saved", false);

        // Add the arguments to the CmdLine object. The CmdLine object
        // uses this Arg to parse the command line.
        cmd.add( initPathDirectory );
        cmd.add( outputDirectory );
        cmd.add( trajectorySequenceNumber );

        cmd.add( proportionOfHerdInfected );
        cmd.add( withinHerdPrevalenceMean );
        cmd.add( withinHerdPrevalenceSigma );
        cmd.add( withinHerdPrevalenceTypeDist );
        cmd.add( probaToPurchaseAnInfectedAnimal );
        cmd.add( slopeProbaToPurchaseAnInfectedAnimal );
        cmd.add( infRateThroughGeneralEnv );
        cmd.add( sensitivityOfTheDiagnosticTest );

        cmd.add( numberOfRuns );
        cmd.add( firstRunId );
        cmd.add( numberOfThreads );

        cmd.add( inferenceCLA );
        cmd.add( cmptSmmrSttstcs );

        // Parse the argv array.
        cmd.parse( argc, argv );

        // Get the value parsed by each arg.
        folderPath = initPathDirectory.getValue();
        std::cout << "folderPath: " << folderPath << std::endl;

//        paramFilePath = getParamFilePath.getValue();
//        std::cout << "paramFilePath: " << paramFilePath << std::endl;

        ResultsFolderPath = outputDirectory.getValue();
        std::cout << "ResultsFolderPath: " << ResultsFolderPath << std::endl;

        ABCfolderPath = initPathDirectory.getValue();

        ABCcompleteTrajectoriesSequenceNumber = trajectorySequenceNumber.getValue();
        std::cout << "ABCcompleteTrajectoriesSequenceNumber: " << ABCcompleteTrajectoriesSequenceNumber << std::endl << std::endl;

        propHerdsInfected = proportionOfHerdInfected.getValue();
        std::cout << "propHerdsInfected: " << propHerdsInfected << std::endl;


        muDistributionPrevIntra = withinHerdPrevalenceMean.getValue();
        std::cout << "muDistributionPrevIntra: " << muDistributionPrevIntra << std::endl;
        abcMuDistributionPrevIntra_min = withinHerdPrevalenceMean.getValue();
        std::cout << "abcMuDistributionPrevIntra_min: " << abcMuDistributionPrevIntra_min << std::endl;
        abcMuDistributionPrevIntra_max = withinHerdPrevalenceMean.getValue();
        std::cout << "abcMuDistributionPrevIntra_max: " << abcMuDistributionPrevIntra_max << std::endl;

        sigmaDistributionPrevIntra = withinHerdPrevalenceSigma.getValue();
        std::cout << "sigmaDistributionPrevIntra: " << sigmaDistributionPrevIntra << std::endl;
        abcSigmaDistributionPrevIntra_min = withinHerdPrevalenceSigma.getValue();
        std::cout << "abcSigmaDistributionPrevIntra_min: " << abcSigmaDistributionPrevIntra_min << std::endl;
        abcSigmaDistributionPrevIntra_max = withinHerdPrevalenceSigma.getValue();
        std::cout << "abcSigmaDistributionPrevIntra_max: " << abcSigmaDistributionPrevIntra_max << std::endl;

        typeOfWithinHerdPrevalenceDistribution = withinHerdPrevalenceTypeDist.getValue();
        std::cout << "typeOfWithinHerdPrevalenceDistribution: " << typeOfWithinHerdPrevalenceDistribution << std::endl;


        probaToPurchaseInfectedAnimalOutsideTheMetapopulation_initialValue = probaToPurchaseAnInfectedAnimal.getValue();
        std::cout << "probaToPurchaseInfectedAnimalOutsideTheMetapopulation_initialValue: " << probaToPurchaseInfectedAnimalOutsideTheMetapopulation_initialValue << std::endl;
        probaToPurchaseInfectedAnimal_initialValue_min = probaToPurchaseAnInfectedAnimal.getValue();
        std::cout <<"probaToPurchaseInfectedAnimal_initialValue_min: " << probaToPurchaseInfectedAnimal_initialValue_min << std::endl;
        probaToPurchaseInfectedAnimal_initialValue_max = probaToPurchaseAnInfectedAnimal.getValue();
        std::cout << "probaToPurchaseInfectedAnimal_initialValue_max: " << probaToPurchaseInfectedAnimal_initialValue_max << std::endl;

        probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope_global = slopeProbaToPurchaseAnInfectedAnimal.getValue();
        std::cout << "probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope_global: " << probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope_global << std::endl;
        probaToPurchaseInfectedAnimal_slope_min_global = slopeProbaToPurchaseAnInfectedAnimal.getValue();
        std::cout <<"probaToPurchaseInfectedAnimal_slope_min_global: " << probaToPurchaseInfectedAnimal_slope_min_global << std::endl;
        probaToPurchaseInfectedAnimal_slope_max_global = slopeProbaToPurchaseAnInfectedAnimal.getValue();
        std::cout << "probaToPurchaseInfectedAnimal_slope_max_global: " << probaToPurchaseInfectedAnimal_slope_max_global << std::endl;


        abcInfGlobalEnv_min = infRateThroughGeneralEnv.getValue() * 7;
        std::cout << "abcInfGlobalEnv_min " << abcInfGlobalEnv_min << std::endl;
        abcInfGlobalEnv_max = infRateThroughGeneralEnv.getValue() * 7;
        std::cout << "abcInfGlobalEnv_max: " << abcInfGlobalEnv_max << std::endl;
        infGlobalEnv = infRateThroughGeneralEnv.getValue() * 7;
        std::cout << "infGlobalEnv: " << infGlobalEnv << std::endl;
        abcTestSensitivity_min = sensitivityOfTheDiagnosticTest.getValue();
        std::cout << "abcTestSensitivity_min: " << abcTestSensitivity_min << std::endl;
        abcTestSensitivity_max = sensitivityOfTheDiagnosticTest.getValue();
        std::cout << "abcTestSensitivity_max: " << abcTestSensitivity_max << std::endl << std::endl;

        nbRuns = numberOfRuns.getValue();
        std::cout << "nbRuns: " << nbRuns << std::endl;
        firstRunID = firstRunId.getValue();
        std::cout << "firstRunID: " << firstRunID << std::endl;
        nbThreads = numberOfThreads.getValue();
        std::cout << "nbThreads: " << nbThreads << std::endl << std::endl;

        ABCcompleteTrajectories = inferenceCLA.getValue();
        std::cout << "inferenceCLA: " << ABCcompleteTrajectories << std::endl;
        computeSummaryStatistics = cmptSmmrSttstcs.getValue();
        std::cout << "computeSummaryStatistics: " << computeSummaryStatistics << std::endl << std::endl;

    } catch (TCLAP::ArgException &e)  // catch any exceptions
    { std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; }
    // ------------------------------------------------------------------------




//    alphaBetaDistributionPrevIntra = muDistributionPrevIntra * (((muDistributionPrevIntra*(1-muDistributionPrevIntra))/(sigmaDistributionPrevIntra*sigmaDistributionPrevIntra)) - 1);
//    betaBetaDistributionPrevIntra = alphaBetaDistributionPrevIntra * ((1/muDistributionPrevIntra) - 1);

    alphaBetaDistributionPrevIntra = muDistributionPrevIntra;
    betaBetaDistributionPrevIntra = sigmaDistributionPrevIntra;




    cullingRateIc = 1. - std::exp( - (1./26.) );
    deathRateBactInside = 1. - std::exp( - (0.4) );
    deathRateBactOutside = 1. - std::exp( - (1./14.) );
    cleaningCP = 1. - std::exp( - (1./6.) );
    cullingRatePositiveTestedAnimals = 1. - std::exp( - (1./24.) );






    //
//    folderPath = argv[1];
//    ResultsFolderPath = argv[2];

    //
    Chronometer chronoParam;
    chronoParam.start();
    
    //Seeding the random number generator
    gsl_rng_env_setup();
    const gsl_rng_type * T;
    //T=gsl_rng_default;
    T=gsl_rng_mt19937;
    randomGenerator=gsl_rng_alloc(T);
    
    // create tm with 1/1/1970:
    //    std::tm timeinfo = std::tm();
    //    timeinfo.tm_year = 70;   // year: 1970 (year-1900)
    //    timeinfo.tm_mon = 0;      // month: january (month-1)
    //    timeinfo.tm_mday = 1;     // day: 1st
    //    std::time_t tt = std::mktime (&timeinfo);
    //    std::chrono::system_clock::time_point tp = std::chrono::system_clock::from_time_t (tt);
    //    std::chrono::system_clock::duration d = std::chrono::system_clock::now() - tp;
    //    Seed = std::chrono::duration_cast<std::chrono::seconds>(d).count();
    
    std::random_device rd;
    Seed = std::time(NULL) + rd(); // TODO: Add the (ABCcomplete)TrajectoriesSequenceNumber to this on order to ensure more difference between successive launch

//    //ONLY FOR TEST
//    Seed = 4641668117;
//    //ONLY FOR TEST
    
    gsl_rng_set(randomGenerator, Seed);
    std::cout << "Seed : " << Seed << std::endl;
    
    
    
    // initialize and set variables about ABC processes
    
    // load ABCparameters.csv
    std::vector<double> vABCparameters;
    std::ifstream fileABCparameters(folderPath + "data/ABCparameters.csv", std::fstream::in); // Open file in reading mode
    // Check if the file is open!
    if(!fileABCparameters){std::cerr << "[ERROR] Unable to open the file" + folderPath + "data/ABCparameters.csv" + " !!" << std::endl;}
    // For each line
    std::string lineABCparameters;
    // Construction of a line
    while (std::getline(fileABCparameters, lineABCparameters)){
        // Construction of a line
        std::stringstream ss(lineABCparameters);
        // Data extraction for the line
        double tempABCparameters;
        while(ss >> tempABCparameters){
            vABCparameters.emplace_back(tempABCparameters);
        }
    }
    
    ABCrejection = vABCparameters[0];
    ABCsmc = vABCparameters[1];
    if (ABCcompleteTrajectories == false) {
        ABCcompleteTrajectories = vABCparameters[2];
    }

    if (ABCrejection == true | ABCsmc == true | ABCcompleteTrajectories == true) {
        computeSummaryStatistics = true;
    }
    //
    ABCsmcTotalNumberOfRuns = vABCparameters[3];
    numberOfAcceptedParticlesRequiredForTheFirstRun = vABCparameters[4];
    numberOfAcceptedParticlesRequiredToGoToTheNextRun = vABCparameters[5];
    perturbationKernelparameter = vABCparameters[6];
    ABCsmcSequenceNumber = vABCparameters[7];
    //
    maxPrevLevel = vABCparameters[8];
    //
    if (ABCrejection == true | ABCsmc == true) {
        abcTestSensitivity_min = vABCparameters[9];
        abcTestSensitivity_max = vABCparameters[10];
        probaToPurchaseInfectedAnimal_initialValue_min = vABCparameters[12];
        probaToPurchaseInfectedAnimal_initialValue_max = vABCparameters[13];
        probaToPurchaseInfectedAnimal_slope_min_global = vABCparameters[14];
        probaToPurchaseInfectedAnimal_slope_max_global = vABCparameters[15];
        abcInfGlobalEnv_min = vABCparameters[16] * 7;
        abcInfGlobalEnv_max = vABCparameters[17] * 7;
        abcMuDistributionPrevIntra_min = vABCparameters[18];
        abcMuDistributionPrevIntra_max = vABCparameters[19];
        abcSigmaDistributionPrevIntra_min = vABCparameters[20];
        abcSigmaDistributionPrevIntra_max = vABCparameters[21];
        ABCcompleteTrajectoriesSequenceNumber = vABCparameters[22];
    }
    abcTestSpecificity = vABCparameters[11];
    //

    

    loadMatrixInfectionInitPrevalenceLevelDistribution();

    
    
    
    // Load some parameters values
    std::vector<double> vParamSimu;
    std::ifstream fichierParamSimu(folderPath + "data/SimulationParameters.csv", std::fstream::in); // Open file in reading mode
    // Check if the file is open!
    if(!fichierParamSimu){std::cerr << "[ERROR] Unable to open the file of the file 'SimulationParameters.csv' for the simulation parameters ..." << std::endl;}
    // For each line
    std::string lineParamSimu;
    // Construction of a line
    while (std::getline(fichierParamSimu, lineParamSimu)){
        // Construction of a line
        std::stringstream ss(lineParamSimu);
        // Data extraction for the line
        double tempParamSimu;
        while(ss >> tempParamSimu){
            vParamSimu.emplace_back(tempParamSimu);
        }
    }
    
    
    typeOfTheRiskToPurchaseInfectedAnimalOutsideTheMetapopulation = vParamSimu[0];
    if (typeOfTheRiskToPurchaseInfectedAnimalOutsideTheMetapopulation < 0 & typeOfTheRiskToPurchaseInfectedAnimalOutsideTheMetapopulation > 1) {
        std::cout << "wrong value for typeOfTheRiskToPurchaseInfectedAnimalOutsideTheMetapopulation" << std::endl;
    }
    std::cout << "typeOfTheRiskToPurchaseInfectedAnimalOutsideTheMetapopulation: " << typeOfTheRiskToPurchaseInfectedAnimalOutsideTheMetapopulation << std::endl;
    
    if (ABCrejection == true | ABCsmc == true) {
        probaToPurchaseInfectedAnimalOutsideTheMetapopulation_initialValue = vParamSimu[1];
        probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope_global = vParamSimu[2];
    }
    if (probaToPurchaseInfectedAnimalOutsideTheMetapopulation_initialValue < 0 & probaToPurchaseInfectedAnimalOutsideTheMetapopulation_initialValue > 1) {
        std::cout << "wrong value for probaToPurchaseInfectedAnimalOutsideTheMetapopulation_initialValue" << std::endl;
    }
    std::cout << "probaToPurchaseInfectedAnimalOutsideTheMetapopulation_initialValue: " << probaToPurchaseInfectedAnimalOutsideTheMetapopulation_initialValue << std::endl;

    if (probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope_global < 0 & probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope_global > 1) {
        std::cout << "wrong value for probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope_global" << std::endl;
    }
    std::cout << "probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope_global: " << probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope_global << std::endl;
    
    // Map infectious dose
    infectiousDose = std::pow(10, 6);
    // Infection rate
    if (ABCrejection == true | ABCsmc == true) {
        infGlobalEnv = vParamSimu[3] * 7; // infGlobalEnv = 9.5 * (std::pow(10, -7)) * 7;
    }
    infGrazing = 5* (std::pow(10, -6)) * 7;
    infLocalEnv = 5* (std::pow(10, -5)) * 7;
    infIngestion = 5* (std::pow(10, -4)) * 7;
    
    // Excretion des adultes dans les matières fécales (Faeces)
    alphaFaecesIs = 2.65;
    betaFaecesIs = 17;
    
    alphaFaecesIc = 2;
    betaFaecesIc = 17;
    
    alphaFaecesIss = alphaFaecesIs;
    betaFaecesIss = betaFaecesIs;
    
    alphaFaecesIcs = alphaFaecesIc;
    betaFaecesIcs = betaFaecesIc;
    
    
    // Construction of the general parameters
    std::cout << "Construction of the general parameters" << std::endl;
    std::vector<double> vParamGeneral;
    std::ifstream fichierParamGeneral(folderPath + "data/GeneralParameters.csv", std::fstream::in); // Open file in reading mode
    // Check if the file is open!
    if(!fichierParamGeneral){std::cerr << "[ERROR] Unable to open the file of the file 'GeneralParameters.csv' for the general parameters ..." << std::endl;}
    // For each line
    std::string lineParamGeneral;
    // Construction of a line
    while (std::getline(fichierParamGeneral, lineParamGeneral)){
        // Construction of a line
        std::stringstream ss(lineParamGeneral);
        // Data extraction for the line
        double tempParamGeneral;
        while(ss >> tempParamGeneral){
            vParamGeneral.emplace_back(tempParamGeneral);
        }
    }
    
    if (ABCrejection == true | ABCsmc == true) {
        nbRuns = vParamGeneral[0];
    }

    if (ABCrejection == true | ABCsmc == true) {
        firstRunID = 1;
    }
    
    nbYears = vParamGeneral[1];
    if (ABCrejection == true | ABCsmc == true) {
        propHerdsInfected = vParamGeneral[2];
    }
    nbInfectedAnimalsIntroduced = vParamGeneral[3];
    SelectingInfectedHerdInAPreferentialWay = vParamGeneral[4];
    TypeOfPreferentialWay = vParamGeneral[5];
    initializeHerdsWithEndemicState = vParamGeneral[6];
    initializeSimutime = vParamGeneral[7];
    endemicPeriodMin = vParamGeneral[8];
    endemicPeriodMax = vParamGeneral[9];
    if (ABCrejection == true | ABCsmc == true) {
        muDistributionPrevIntra = vParamGeneral[10];
        sigmaDistributionPrevIntra = vParamGeneral[11];
    }
    
    if (nbInfectedAnimalsIntroduced == 0) {
        initializeHerdsWithEndemicState = false;
    }
    
    if (initializeSimutime == 0) {
        initializeHerdsWithEndemicState = false;
    }
    
    simutime = nbYears*timestepInOneYear+1;


    //
    probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope = probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope_global / static_cast<double>(simutime);
    probaToPurchaseInfectedAnimal_slope_min = probaToPurchaseInfectedAnimal_slope_min_global / static_cast<double>(simutime);
    probaToPurchaseInfectedAnimal_slope_max = probaToPurchaseInfectedAnimal_slope_max_global / static_cast<double>(simutime);
    
    
    
    // Construction of the test parameters
    std::cout << "Construction of the test at purchase parameters" << std::endl;
    std::vector<double> vParamTest;
    std::ifstream fichierParamTest(folderPath + "data/TestParameters.csv", std::fstream::in); // Open file in reading mode
    // Check if the file is open!
    if(!fichierParamTest){std::cerr << "[ERROR] Unable to open the file of the file 'TestParameters.csv' for the test parameters ..." << std::endl;}
    // For each line
    std::string lineParamTest;
    // Construction of a line
    while (std::getline(fichierParamTest, lineParamTest)){
        // Construction of a line
        std::stringstream ss(lineParamTest);
        // Data extraction for the line
        double tempParamTest;
        while(ss >> tempParamTest){
            vParamTest.emplace_back(tempParamTest);
        }
    }
    
    startTest = timestepInOneYear*vParamTest[0];
    testSp = vParamTest[1];
    testSeT = vParamTest[2];
    testSeL = vParamTest[3];
    testSeIs = vParamTest[4];
    typeOfTestStrategy = vParamTest[5];
    minimumOfIncomingMovements = vParamTest[6];
    minimumOfOutgoingMovements = vParamTest[7];
    proportionOfFarmsWithTestAtPurchase = vParamTest[8];
    
    
    
    
    
    // Construction of the initial headcounts array
    std::cout << "Construction of the initial headcounts array" << std::endl;
    std::string headcountsFileName;
    if (ABCrejection == true | ABCsmc == true) {
        headcountsFileName = "data/mvHeadcountsOnFirstExhaustiveSampling.csv";
    } else {
        headcountsFileName = "data/Headcounts_withId.csv";
    }
    std::ifstream fileHeadcountsWithId(folderPath + headcountsFileName, std::fstream::in); // Open file in reading mode
    if(!fileHeadcountsWithId){std::cerr << "[ERROR] Unable to open the file 'Headcounts.csv' !!" << std::endl;} // Check if the file is open!
    // For each line:
    std::string lineHeadcountsWithId;
    while (std::getline(fileHeadcountsWithId, lineHeadcountsWithId)){
        // Construction of a line
        std::stringstream ss(lineHeadcountsWithId);
        // Data extraction for the line
        std::string theFarmId;
        ss >> theFarmId;
        std::vector<short int> vHeadcounts;
        short int tempHeadcounts;
        while(ss >> tempHeadcounts){
            vHeadcounts.emplace_back(tempHeadcounts);
        }
        mHerdHeadcounts.insert(std::pair<std::string, std::vector<short int>>(theFarmId,vHeadcounts));
    }
    
    
//    // Construction of the initial sexRatio array
//    std::cout << "Construction of the initial sexRatio array" << std::endl;
//    std::ifstream fileSexRatioWithId(folderPath + "data/BirthRatio_withId.csv", std::fstream::in); // Open file in reading mode
//    if(!fileSexRatioWithId){std::cerr << "[ERROR] Unable to open the file 'BirthRatio.csv' !!" << std::endl;} // Check if the file is open!
//    // For each line:
//    std::string lineSexRatioWithId;
//    while (std::getline(fileSexRatioWithId, lineSexRatioWithId)){
//        // Construction of a line
//        std::stringstream ss(lineSexRatioWithId);
//        // Data extraction for the line
//        std::string theFarmId;
//        ss >> theFarmId;
//        std::vector<double> vSexRatio;
//        double tempSexRatio;
//        while(ss >> tempSexRatio){
//            vSexRatio.emplace_back(tempSexRatio);
//        }
//        mHerdBirthsRatio.insert(std::pair<std::string, std::vector<double>>(theFarmId,vSexRatio));
//    }
    
    
    // Construction of the birth events array
    std::cout << "Construction of the birth events array" << std::endl;
//    std::ifstream fileBirthEventsWithId(folderPath + "data/BirthEvents_withId.csv", std::fstream::in); // Open file in reading mode // in case we want to use the sex ratio data
    std::ifstream fileBirthEventsWithId(folderPath + "data/BirthFemaleEvents_withId.csv", std::fstream::in); // Open file in reading mode
    if(!fileBirthEventsWithId){std::cerr << "[ERROR] Unable to open the file 'BirthEvents.csv' !!" << std::endl;} // Check if the file is open!
    // For each line:
    std::string lineBirthEventsWithId;
    while (std::getline(fileBirthEventsWithId, lineBirthEventsWithId)){
        // Construction of a line
        std::stringstream ss(lineBirthEventsWithId);
        // Data extraction for the line
        std::string theFarmId;
        ss >> theFarmId;
        std::vector<short int> vBirthEvents;
        short int tempBirthEvents;
        while(ss >> tempBirthEvents){
            vBirthEvents.emplace_back(tempBirthEvents);
        }
        mHerdBirthEvents.insert(std::pair<std::string, std::vector<short int>>(theFarmId,vBirthEvents));
    }
    
    
    // Construction of the culling rates array
    std::cout << "Construction of the culling rates array" << std::endl;
    std::ifstream fileCullingRatesWithId(folderPath + "data/CullingRates_withId.csv", std::fstream::in); // Open file in reading mode
    if(!fileCullingRatesWithId){std::cerr << "[ERROR] Unable to open the file 'cullingRates.csv' !!" << std::endl;} // Check if the file is open!
    // For each line:
    std::string lineCullingRatesWithId;
    while (std::getline(fileCullingRatesWithId, lineCullingRatesWithId)){
        // Construction of a line
        std::stringstream ss(lineCullingRatesWithId);
        // Data extraction for the line
        std::string theFarmId;
        ss >> theFarmId;
        std::vector<double> vCullingRates;
        double tempCullingRates;
        while(ss >> tempCullingRates){
            vCullingRates.emplace_back(tempCullingRates);
        }
        mHerdCullingRates.insert(std::pair<std::string, std::vector<double>>(theFarmId,vCullingRates));
    }
    
    
    
    
    numberOfHerdsAboveTheMinimumOfIncomingMovements = 0;
    numberOfHerdsAboveTheMinimumOfOutgoingMovements = 0;
    
    // Construction of the farm characteristics map
    std::cout << "Construction of the farm characteristics map" << std::endl;
    std::ifstream fileTableOfCharacteristics(folderPath + "data/TableOfCharacteristics.csv", std::fstream::in); // Open file in reading mode
    if(!fileTableOfCharacteristics){std::cerr << "[ERROR] Unable to open the file 'cullingRates.csv' !!" << std::endl;} // Check if the file is open!
    // For each line:
    std::string lineTableOfCharacteristics;
    while (std::getline(fileTableOfCharacteristics, lineTableOfCharacteristics)){
        // Construction of a line
        std::stringstream ss(lineTableOfCharacteristics);
        // Data extraction for the line
        std::string theFarmId;
        ss >> theFarmId;
        
        double selectUnif;
        ss >> selectUnif;
        
        double inDegree;
        ss >> inDegree;
        double outDegree;
        ss >> outDegree;
        double inStrengthAll;
        ss >> inStrengthAll;
        inStrengthAll /= static_cast<double>(Parameters::nbYears);
        double outStrengthAll;
        ss >> outStrengthAll;
        outStrengthAll /= static_cast<double>(Parameters::nbYears);
        int herdSize = std::accumulate(mHerdHeadcounts[theFarmId].begin(), mHerdHeadcounts[theFarmId].end(), 0);
        
        HerdCharacteristics theCharacteristics = HerdCharacteristics(herdSize, inDegree, outDegree, inStrengthAll, outStrengthAll);
        mFarmsCharacteristics.insert(std::pair<std::string, HerdCharacteristics>(theFarmId,theCharacteristics));
        
        vFarmID.emplace_back(theFarmId);
        vHerdDataForSelectingTheInfectedHerdsUniform.emplace_back(selectUnif);
        vHerdDataForSelectingTheInfectedHerdsPreferentialOutDegree.emplace_back(outDegree);
        vHerdDataForSelectingTheInfectedHerdsPreferentialNumberOfOutMovements.emplace_back(outStrengthAll);
        
        if (inStrengthAll >= minimumOfIncomingMovements) {
            numberOfHerdsAboveTheMinimumOfIncomingMovements ++;
        }
        
        if (outStrengthAll >= minimumOfOutgoingMovements) {
            numberOfHerdsAboveTheMinimumOfOutgoingMovements ++;
        }
        
    }
    
    //    std::cout << vFarmID.size() << std::endl;
    //
    //    for (int i = 0; i < vFarmID.size(); i++) {
    //        std::cout << vFarmID[i] << "  ";
    //    }
    //    std::cout << std::endl;

    
    
    
    
    // Set the values of herd Biosecurity Parameters
    std::cout << "Set the values of herd Biosecurity Parameters" << std::endl;
    std::vector<double> vParamBiosecurity;
    std::ifstream fichierParamBiosecurity(folderPath + "data/herdBiosecurityParameters.csv", std::fstream::in); // Open file in reading mode
    // Check if the file is open!
    if(!fichierParamBiosecurity){std::cerr << "[ERROR] Unable to open the file of the file 'herdBiosecurityParameters.csv' for the test parameters ..." << std::endl;}
    // For each line
    std::string lineParamBiosecurity;
    // Construction of a line
    while (std::getline(fichierParamBiosecurity, lineParamBiosecurity)){
        // Construction of a line
        std::stringstream ss(lineParamBiosecurity);
        // Data extraction for the line
        double tempParamBiosecurity;
        while(ss >> tempParamBiosecurity){
            vParamBiosecurity.emplace_back(tempParamBiosecurity);
        }
    }
    
    withinHerdBiosecurity = vParamBiosecurity[0];
    
    if (withinHerdBiosecurity) {
        proportionOfFarmsWithCullingImprovement = vParamBiosecurity[1];
        cullingRateIcIfCullingImprovement = 1. - std::exp( - (1./vParamBiosecurity[2]) ); // 1./vParamBiosecurity[2];
        
        proportionOfFarmsWithHygieneImprovement = vParamBiosecurity[3];
        deathRateBactInsideIfHygieneImprovement = vParamBiosecurity[4];
        
        proportionOfFarmsWithCalfManagementImprovement = vParamBiosecurity[5];
        decreaseOfAdultToCalfContact = vParamBiosecurity[6];
        infGlobalEnvIfCalfManagementImprovement = infGlobalEnv - (infGlobalEnv*decreaseOfAdultToCalfContact);
        
        proportionOfFarmsWithTestAndCull = vParamBiosecurity[7];
        testAndCullSp = vParamBiosecurity[8];
        testAndCullSeT = vParamBiosecurity[9];
        testAndCullSeL = vParamBiosecurity[10];
        testAndCullSeIs = vParamBiosecurity[11];
        testAndCullSeIc = vParamBiosecurity[12];
        testAndCullMinimumAge = static_cast<int>(vParamBiosecurity[13]);
        testAndCullMaximumAge = static_cast<int>(vParamBiosecurity[14]);
        proportionOfAnimalsTested = vParamBiosecurity[15];
        proportionOfAnimalsRemoved = vParamBiosecurity[16];
        replacementAfterTestAndCull = vParamBiosecurity[17];
        delayBetweenTheOnsetOfAnIcAndTheImplementationOfTheTestAndCull = vParamBiosecurity[18];
        
    } else {
        proportionOfFarmsWithCullingImprovement = 0.0;
        cullingRateIcIfCullingImprovement = cullingRateIc;
        
        proportionOfFarmsWithHygieneImprovement = 0.0;
        deathRateBactInsideIfHygieneImprovement = deathRateBactInside;
        
        proportionOfFarmsWithCalfManagementImprovement = 0.0;
        decreaseOfAdultToCalfContact = 0.0;
        infGlobalEnvIfCalfManagementImprovement = infGlobalEnv;
        
        proportionOfFarmsWithTestAndCull = 0.0;
        testAndCullSp = vParamBiosecurity[8];
        testAndCullSeT = vParamBiosecurity[9];
        testAndCullSeL = vParamBiosecurity[10];
        testAndCullSeIs = vParamBiosecurity[11];
        testAndCullSeIc = vParamBiosecurity[12];
        testAndCullMinimumAge = static_cast<int>(vParamBiosecurity[13]);
        testAndCullMaximumAge = static_cast<int>(vParamBiosecurity[14]);
        proportionOfAnimalsTested = vParamBiosecurity[15];
        proportionOfAnimalsRemoved = vParamBiosecurity[16];
        replacementAfterTestAndCull = vParamBiosecurity[17];
        delayBetweenTheOnsetOfAnIcAndTheImplementationOfTheTestAndCull = vParamBiosecurity[18];
    }
    
    
    
//#ifndef ABCreduce
//    if (ABCrejection == false & ABCsmc == false & ABCcompleteTrajectories == false) {
//        vvvProbaInitInfection.resize(10);
//        addDataToMatrixOfProbaInitInfection("matrixPropInfectionInit_1.csv", 1);
//        addDataToMatrixOfProbaInitInfection("matrixPropInfectionInit_2.csv", 2);
//        addDataToMatrixOfProbaInitInfection("matrixPropInfectionInit_3.csv", 3);
//        addDataToMatrixOfProbaInitInfection("matrixPropInfectionInit_4.csv", 4);
//        addDataToMatrixOfProbaInitInfection("matrixPropInfectionInit_5.csv", 5);
//        addDataToMatrixOfProbaInitInfection("matrixPropInfectionInit_6.csv", 6);
//        addDataToMatrixOfProbaInitInfection("matrixPropInfectionInit_7.csv", 7);
//        addDataToMatrixOfProbaInitInfection("matrixPropInfectionInit_8.csv", 8);
//        addDataToMatrixOfProbaInitInfection("matrixPropInfectionInit_9.csv", 9);
//        addDataToMatrixOfProbaInitInfection("matrixPropInfectionInit_10.csv", 10);
//    }
//#endif
    
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    abcTestSensitivity = abcTestSensitivity_min;
    std::cout << "abcTestSensitivity: " << abcTestSensitivity << std::endl;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    
    
    
    if (ABCrejection == true | ABCsmc == true) {
        // load ABCsmcNextRunInfos.csv
        std::vector<double> vInfos;
        std::ifstream fileABCsmcInformations(Parameters::ABCfolderPath + "ABCsmcInfosAndParticlesForTheNextRun/ABCsmcNextRunInfos.csv", std::fstream::in); // Open file in reading mode
        // Check if the file is open!
        if(!fileABCsmcInformations){std::cerr << "[ERROR] Unable to open the file 'ABCsmcNextRunInfos.csv' !!" << std::endl;} // Check if the file is open!
        // For each line
        std::string lineABCsmcInformations;
        // Construction of a line
        while (std::getline(fileABCsmcInformations, lineABCsmcInformations)){
        // Construction of a line
        std::stringstream ss(lineABCsmcInformations);
        // Data extraction for the line
        double tempParamGeneral;
        while(ss >> tempParamGeneral){
            vInfos.emplace_back(tempParamGeneral);
        }
    }
    
    ABCsmcRunNumber = vInfos[0];
    ABCsmcThreshold = vInfos[1];
    
    
    ABCsmcCurrentRunIsCompleted = false;
    }
    
    
    
    // Add information to the farm characteristics map for the ABC processes and set parameters to references if not taking into account
//    if (ABCrejection == true | ABCsmc == true | ABCcompleteTrajectories == true) {
        if (ABCrejection == true | ABCsmc == true) {
            vFarmID.clear();
        }

        if (ABCrejection == true | ABCsmc == true | ABCcompleteTrajectories == true | computeSummaryStatistics == true) {
            std::ifstream fileTableOfPrevInit(folderPath + "data/abcInitAndResume.csv", std::fstream::in); // Open file in reading mode
            if(!fileTableOfPrevInit){std::cerr << "[ERROR] Unable to open the file 'abcInitAndResume.csv' !!" << std::endl;} // Check if the file is open!
            // For each line:
            std::string lineTableOfPrevInit;
            while (std::getline(fileTableOfPrevInit, lineTableOfPrevInit)){
                // Construction of a line
                std::stringstream ss(lineTableOfPrevInit);
                // Data extraction for the line
                std::string theFarmId;
                ss >> theFarmId;

                ss >> mFarmsCharacteristics[theFarmId].beginDate;
                if (ABCrejection == false & ABCsmc == false) {
                    mFarmsCharacteristics[theFarmId].beginDate = 0;
                }
                double observedPrevalence;
                ss >> observedPrevalence;
                //double truePrevalence = (observedPrevalence + (abcTestSpecificity - 1.)) / (abcTestSensitivity + abcTestSpecificity - 1.);
                //truePrevalence = std::min(truePrevalence, 1.0);
                //mFarmsCharacteristics[theFarmId].prevInit = truePrevalence;
                mFarmsCharacteristics[theFarmId].prevInit = observedPrevalence;
                ss >> mFarmsCharacteristics[theFarmId].lastSampleDate;
                ss >> mFarmsCharacteristics[theFarmId].lastSampleHeadcount;
                ss >> mFarmsCharacteristics[theFarmId].lastSamplesCount;
                ss >> mFarmsCharacteristics[theFarmId].lastPositiveSamplesCount;
                // TODO:
                if (mFarmsCharacteristics[theFarmId].lastSampleDate != -999) {
                    mFarmsCharacteristics[theFarmId].dataForFirstAndLast = true;
                    vHoldingsForResume.emplace_back(theFarmId);

                    mFarmsCharacteristics[theFarmId].proportionOfSample = std::min(static_cast<double>(mFarmsCharacteristics[theFarmId].lastSamplesCount) / static_cast<double>(mFarmsCharacteristics[theFarmId].lastSampleHeadcount), 1.0);
                    mFarmsCharacteristics[theFarmId].proportionOfPositiveSample = static_cast<double>(mFarmsCharacteristics[theFarmId].lastPositiveSamplesCount) / static_cast<double>(mFarmsCharacteristics[theFarmId].lastSamplesCount);
                }

                if (ABCrejection == true | ABCsmc == true) {
                    vFarmID.emplace_back(theFarmId);
                }

            }
        }
        
        std::cout << "vHoldingsForResumeSize: " << vHoldingsForResume.size() << std::endl;
//    } else {
//        
//        
//        ////////////////////////////////////////////////////////////////////////
//        ////////////////////////////////////////////////////////////////////////
//        ////////////////////////////////////////////////////////////////////////
//        /// ONLY FOR TEST !!
////        vFarmID.clear();
////        std::ifstream fileTableOfPrevInit(folderPath + "data/abcInitAndResume.csv", std::fstream::in); // Open file in reading mode
////        if(!fileTableOfPrevInit){std::cerr << "[ERROR] Unable to open the file 'abcInitAndResume.csv' !!" << std::endl;} // Check if the file is open!
////        // For each line:
////        std::string lineTableOfPrevInit;
////        while (std::getline(fileTableOfPrevInit, lineTableOfPrevInit)){
////            // Construction of a line
////            std::stringstream ss(lineTableOfPrevInit);
////            // Data extraction for the line
////            std::string theFarmId;
////            ss >> theFarmId;
////            vFarmID.emplace_back(theFarmId);
////        }
//        /// ONLY FOR TEST !!
//        ////////////////////////////////////////////////////////////////////////
//        ////////////////////////////////////////////////////////////////////////
//        ////////////////////////////////////////////////////////////////////////
//        
//        
//        for (auto it = mFarmsCharacteristics.begin(); it != mFarmsCharacteristics.end(); ++it) {
//            it->second.beginDate = 0;
//            it->second.prevInit = 0;
////            it->second.lastSampleDate = -999;
////            it->second.lastSamplesCount = 0;
////            it->second.lastPositiveSamplesCount = 0;
////            it->second.dataForFirstAndLast = false;
//        }
//    }





    nbHerds = static_cast<double>(vFarmID.size());
    
    
    
    
    
    nbHerdsInfected = round(static_cast<double>(nbHerds)*propHerdsInfected);
    
    
    
    
    
    // Construction of mFarmsDateSampleCount

    if (ABCrejection == true | ABCsmc == true | ABCcompleteTrajectories == true | computeSummaryStatistics == true) {
        for (auto holdingID: vFarmID) {
            std::string filePath = folderPath + "data/holdingID_DateSampleCount/dateSampleCount" + holdingID + ".csv";
    //        std::cout << filePath << std::endl;
            std::ifstream fileHoldingID_DateSampleCount(filePath, std::fstream::in); // Open file in reading mode
            if(fileHoldingID_DateSampleCount){ // Check if the file is open!
                // For each line:
                std::string lineHoldingID_DateSampleCount;
                std::map<int, ObservedSampleInformations> mDateSampleCount;
                while (std::getline(fileHoldingID_DateSampleCount, lineHoldingID_DateSampleCount)){
                    // Construction of a line
                    std::stringstream ss(lineHoldingID_DateSampleCount);
                    // Data extraction for the line
                    int date;
                    ss >> date;
                    int observedHeadcount;
                    ss >> observedHeadcount;
                    int observedSampleCount;
                    ss >> observedSampleCount;
                    int observedPositiveSampleCount;
                    ss >> observedPositiveSampleCount;
                    // Add date-sampleCount in mFarmsDateSampleCount
                    ObservedSampleInformations theSamplesCountData = ObservedSampleInformations(observedHeadcount, observedSampleCount, observedPositiveSampleCount);
                    mDateSampleCount[date]=theSamplesCountData;
                }

                mFarmsDateSampleCount.insert(std::pair<std::string, std::map<int, ObservedSampleInformations>>(holdingID,mDateSampleCount));
            } //else {
                //std::cout << "[ERROR] Unable to open the file 'abcInitAndResume.csv' !!" << std::endl;
            //}
        }
    }
    
    std::cout << "size of mFarmsDateSampleCount: " << mFarmsDateSampleCount.size() << std::endl;
//    for (auto holdingID : mFarmsDateSampleCount) {
//        std::cout << holdingID.first << ": " << std::endl;
//        for(auto date : holdingID.second) {
//            std::cout << date.first << " - " << date.second << std::endl;
//        }
//    }
    
    
    
    
    
    
    
    
    
    std::cout << std::endl;
    std::cout << "Network characteristics: \n";
    std::cout << "nbHerds: " << nbHerds << std::endl;
    std::cout << std::endl;
    
    
    
    std::cout << std::endl;
    std::cout << "General Parameters: \n";
    std::cout << "------------------- \n";
    if (ABCsmc == true) {
        std::cout << "Nb runs: -" << std::endl;
    } else {
        std::cout << "Nb runs: " << nbRuns << std::endl;
    }
    std::cout << "Nb years: " << nbYears << std::endl;
    if (ABCrejection == false & ABCsmc == false) {
        std::cout << "Nb herds infected: " << nbHerdsInfected << std::endl;
        std::cout << "Nb infected animals introduced: " << nbInfectedAnimalsIntroduced << std::endl;
        std::cout << "Selecting infected herds in a preferential way: " << std::boolalpha << SelectingInfectedHerdInAPreferentialWay << std::endl;
        if (SelectingInfectedHerdInAPreferentialWay == true) {
            std::cout << "Type of preferential way: " << TypeOfPreferentialWay << std::endl;
        }
        std::cout << "Initialize herds with endemic state: " << std::boolalpha << initializeHerdsWithEndemicState << std::endl;
//        std::cout << "Endemic state level: " << initializeSimutime << std::endl;
        std::cout << "Within herd mean level: " << muDistributionPrevIntra << std::endl;
    }
    std::cout << std::endl;
    
    
    
    
    if (ABCrejection == true | ABCsmc == true) {
        std::cout << std::endl;
        std::cout << "ABC Parameters: \n";
        std::cout << "------------------- \n";
        if (ABCrejection == true) {
            std::cout << "ABC algo : rejection" << std::endl;
        }
        if (ABCsmc == true) {
            std::cout << "ABC algo : smc" << std::endl;
            std::cout << "ABCsmcRunNumber: " << ABCsmcRunNumber << std::endl;
            //std::cout << "ABCsmcThreshold: " << ABCsmcThreshold << std::endl;
            std::cout << std::endl;
            std::cout << "ABCsmcTotalNumberOfRuns : " << ABCsmcTotalNumberOfRuns << std::endl;
            std::cout << "numberOfAcceptedParticlesRequiredForTheFirstRun : " << numberOfAcceptedParticlesRequiredForTheFirstRun << std::endl;
            std::cout << "numberOfAcceptedParticlesRequiredToGoToTheNextRun : " << numberOfAcceptedParticlesRequiredToGoToTheNextRun << std::endl;
            std::cout << "perturbationKernelparameter : " << perturbationKernelparameter << std::endl;
            std::cout << "ABCsmcSequenceNumber : " << ABCsmcSequenceNumber << std::endl;
            std::cout << std::endl;
            std::cout << "maxPrevLevel : " << maxPrevLevel << std::endl;
            std::cout << std::endl;
            std::cout << "abcTestSensitivity_min : " << abcTestSensitivity_min << std::endl;
            std::cout << "abcTestSensitivity_max : " << abcTestSensitivity_max << std::endl;
            std::cout << "abcTestSpecificity : " << abcTestSpecificity << std::endl;
            std::cout << "probaToPurchaseInfectedAnimal_initialValue_min : " << probaToPurchaseInfectedAnimal_initialValue_min << std::endl;
            std::cout << "probaToPurchaseInfectedAnimal_initialValue_max : " << probaToPurchaseInfectedAnimal_initialValue_max << std::endl;
            std::cout << "probaToPurchaseInfectedAnimal_slope_min_global : " << probaToPurchaseInfectedAnimal_slope_min_global << std::endl;
            std::cout << "probaToPurchaseInfectedAnimal_slope_max_global : " << probaToPurchaseInfectedAnimal_slope_max_global << std::endl;
            std::cout << "abcInfGlobalEnv_min : " << abcInfGlobalEnv_min << std::endl;
            std::cout << "abcInfGlobalEnv_max : " << abcInfGlobalEnv_max << std::endl;
        }
        std::cout << std::endl;
    }
    
    if (ABCcompleteTrajectories == true) {
        std::cout << std::endl;
        std::cout << "ABCcompleteTrajectories Parameters: \n";
        std::cout << "------------------- \n";

        std::cout << "ABC algo : completeTrajectories" << std::endl;
//        std::cout << "ABCsmcRunNumber: " << ABCsmcRunNumber << std::endl;
        //std::cout << "ABCsmcThreshold: " << ABCsmcThreshold << std::endl;
        std::cout << std::endl;
//        std::cout << "ABCsmcTotalNumberOfRuns : " << ABCsmcTotalNumberOfRuns << std::endl;
//        std::cout << "numberOfAcceptedParticlesRequiredForTheFirstRun : " << numberOfAcceptedParticlesRequiredForTheFirstRun << std::endl;
//        std::cout << "numberOfAcceptedParticlesRequiredToGoToTheNextRun : " << numberOfAcceptedParticlesRequiredToGoToTheNextRun << std::endl;
//        std::cout << "perturbationKernelparameter : " << perturbationKernelparameter << std::endl;
        std::cout << "ABCcompleteTrajectoriesSequenceNumber : " << ABCcompleteTrajectoriesSequenceNumber << std::endl;
        std::cout << std::endl;
        std::cout << "maxPrevLevel : " << maxPrevLevel << std::endl;
        std::cout << std::endl;
        std::cout << "propHerdsInfected : " << propHerdsInfected << std::endl;
        std::cout << "muDistributionPrevIntra : " << muDistributionPrevIntra << std::endl;
        std::cout << "sigmaDistributionPrevIntra : " << sigmaDistributionPrevIntra << std::endl;
        std::cout << std::endl;
        std::cout << "abcTestSensitivity : " << abcTestSensitivity << std::endl;
//        std::cout << "abcTestSensitivity_min : " << abcTestSensitivity_min << std::endl;
//        std::cout << "abcTestSensitivity_max : " << abcTestSensitivity_max << std::endl;
        std::cout << "abcTestSpecificity : " << abcTestSpecificity << std::endl;
        std::cout << "probaToPurchaseInfectedAnimalOutsideTheMetapopulation_initialValue : " << probaToPurchaseInfectedAnimalOutsideTheMetapopulation_initialValue << std::endl;
//        std::cout << "probaToPurchaseInfectedAnimal_initialValue_min : " << probaToPurchaseInfectedAnimal_initialValue_min << std::endl;
        //        std::cout << "probaToPurchaseInfectedAnimal_initialValue_max : " << probaToPurchaseInfectedAnimal_initialValue_max << std::endl;
        std::cout << "probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope_global : " << probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope_global << std::endl;
//        std::cout << "probaToPurchaseInfectedAnimal_slope_min_global : " << probaToPurchaseInfectedAnimal_slope_min_global << std::endl;
        //        std::cout << "probaToPurchaseInfectedAnimal_slope_max_global : " << probaToPurchaseInfectedAnimal_slope_max_global << std::endl;
        std::cout << "infGlobalEnv : " << infGlobalEnv << std::endl;
//        std::cout << "abcInfGlobalEnv_min : " << abcInfGlobalEnv_min << std::endl;
//        std::cout << "abcInfGlobalEnv_max : " << abcInfGlobalEnv_max << std::endl;
        std::cout << std::endl;
    }
    
    if ((ABCrejection == true & ABCsmc == true) | (ABCrejection == true & ABCcompleteTrajectories == true) | (ABCsmc == true & ABCcompleteTrajectories == true)) {
        std::cout << "ABC Parameters errors!\n" << std::endl;
        std::exit(EXIT_FAILURE);
    }
    
    
    
    
    if (startTest <= simutime) {
        std::cout << std::endl;
        std::cout << "Parameters of the test: \n";
        std::cout << "----------------------- \n";
        std::cout << "Start date for the tests: " << startTest/timestepInOneYear << std::endl;
        std::cout << "Sp: " << testSp << std::endl;
        std::cout << "Se for T: " << testSeT << std::endl;
        std::cout << "Se for L: " << testSeL << std::endl;
        std::cout << "Se for Is: " << testSeIs << std::endl;
        std::cout << "Type of test strategy: ";
        if (typeOfTestStrategy == strategyHerdRandomlyChosenMovIn) {
            std::cout << "herd randomly chosen - mov in";
        } else if (typeOfTestStrategy == strategyHerdRandomlyChosenMovOut) {
            std::cout << "herd randomly chosen - mov out";
        } else if (typeOfTestStrategy == strategyHerdRandomlyChosenMovInOut) {
            std::cout << "herd randomly chosen - mov in and out";
        } else if (typeOfTestStrategy == strategyHerdWithIc) {
            std::cout << "herd with Ic";
        } else if (typeOfTestStrategy == strategyHerdWithMoreThanTwoIc) {
            std::cout << "herd with more than two Ic";
        } else if (typeOfTestStrategy == strategyBasedOnNumberOfIncomingMovements) {
            std::cout << "based on the number of incoming movements | ";
            std::cout << "minimum of in movements: " << minimumOfIncomingMovements << " | ";
            printf("number of herds concerned: %.2f %% \n", numberOfHerdsAboveTheMinimumOfIncomingMovements/nbHerds*100.0);
        } else if (typeOfTestStrategy == strategyBasedOnNumberOfOutgoingMovements) {
            std::cout << "based on the number of outgoing movements | ";
            std::cout << "minimum of out movements: " << minimumOfOutgoingMovements << " | ";
            printf("number of herds concerned: %.2f %% \n", numberOfHerdsAboveTheMinimumOfOutgoingMovements/nbHerds*100.0);
        } else {
            throw std::runtime_error("Wrong value for 'typeOfTestStrategy'.");
        }
        std::cout << std::endl;
        
        //        std::cout << "ProportionOfFarmsWithTestAtPurchase: " << proportionOfFarmsWithTestAtPurchase * 100. << " %" << std::endl;
        printf("Proportion of farms with test at purchase/sale: %.2f %% \n", proportionOfFarmsWithTestAtPurchase * 100.);
        
        std::cout << std::endl;
    }
    
    if (withinHerdBiosecurity) {
        std::cout << std::endl;
        std::cout << "Parameters of the within herd biosecurity: \n";
        std::cout << "------------------------------------------ \n";
        
        std::cout << "proportionOfFarmsWithCullingImprovement: " << proportionOfFarmsWithCullingImprovement << std::endl;
        std::cout << "cullingRateIcIfCullingImprovement: 1/" << 1/cullingRateIcIfCullingImprovement << std::endl;
        
        std::cout << "proportionOfFarmsWithHygieneImprovement: " << proportionOfFarmsWithHygieneImprovement << std::endl;
        std::cout << "deathRateBactInsideIfHygieneImprovement: " << deathRateBactInsideIfHygieneImprovement << std::endl;
        
        std::cout << "proportionOfFarmsWithCalfManagementImprovement: " << proportionOfFarmsWithCalfManagementImprovement << std::endl;
        std::cout << "decreaseOfAdultToCalfContact: " << decreaseOfAdultToCalfContact << std::endl;
        
        std::cout << "proportionOfFarmsWithTestAndCull: " << proportionOfFarmsWithTestAndCull << std::endl;
        std::cout << "testAndCullSp: " << testAndCullSp << std::endl;
        std::cout << "testAndCullSeT: " << testAndCullSeT << std::endl;
        std::cout << "testAndCullSeL: " << testAndCullSeL << std::endl;
        std::cout << "testAndCullSeIs: " << testAndCullSeIs << std::endl;
        std::cout << "testAndCullSeIc: " << testAndCullSeIc << std::endl;
        std::cout << "testAndCullMinimumAge: " << testAndCullMinimumAge << std::endl;
        std::cout << "testAndCullMaximumAge: " << testAndCullMaximumAge << std::endl;
        std::cout << "proportionOfAnimalsTested: " << proportionOfAnimalsTested << std::endl;
        std::cout << "proportionOfAnimalsRemoved: " << proportionOfAnimalsRemoved << std::endl;
        std::cout << "replacementAfterTestAndCull: " << std::boolalpha << replacementAfterTestAndCull << std::endl;
        std::cout << "delayBetweenTheOnsetOfAnIcAndTheImplementationOfTheTestAndCull: " << delayBetweenTheOnsetOfAnIcAndTheImplementationOfTheTestAndCull << std::endl;
    }
    
    
    chronoParam.stop();
    std::cout << std::endl << std::endl << "Parameters (time of construction): " << std::setprecision(2) << chronoParam.display() << " seconds" << std::endl << std::endl;
    std::cout << std::setprecision(6);
    
}


// Member functions ====================================================
void Parameters::addDataToMatrixOfProbaInitInfection(std::string fileName, int timeInYear){
    // Construction of vvvProbaInitInfection
    std::ifstream fileMatPropInf(folderPath + "data/" + fileName, std::fstream::in); // Open file in reading mode
    if(!fileMatPropInf){std::cerr << "[ERROR] Unable to open the file '" + fileName + "' !!" << std::endl;} // Check if the file is open!
    // For each line:
    std::string lineMatPropInf;
    while (std::getline(fileMatPropInf, lineMatPropInf)){
        // Construction of a line
        std::stringstream ss(lineMatPropInf);
        std::vector<double> vBuffer;
        // Data extraction for the line
        double temp;
        while(ss >> temp){
            vBuffer.emplace_back(temp);
        }
        
        std::vector<double> vBufferHealthStates(vBuffer.begin(), vBuffer.begin()+5); assert(vBufferHealthStates.size() == 5);
        std::vector<double> vBufferdistributionSR(vBuffer.begin()+5, vBuffer.begin()+5+135); assert(vBufferdistributionSR.size() == 135);
        std::vector<double> vBufferdistributionT(vBuffer.begin()+5+135, vBuffer.begin()+5+135*2); assert(vBufferdistributionT.size() == 135);
        std::vector<double> vBufferdistributionL(vBuffer.begin()+5+135*2, vBuffer.begin()+5+135*3); assert(vBufferdistributionL.size() == 135);
        std::vector<double> vBufferdistributionIs(vBuffer.begin()+5+135*3, vBuffer.begin()+5+135*4); assert(vBufferdistributionIs.size() == 135);
        std::vector<double> vBufferdistributionIc(vBuffer.begin()+5+135*4, vBuffer.begin()+5+135*5); assert(vBufferdistributionIc.size() == 135);
        std::vector<double> vBufferEnvironments(vBuffer.begin()+5+135*5, vBuffer.end()); assert(vBufferEnvironments.size() == 12);
        
        std::vector<std::vector<double>> vvBuffer;
        vvBuffer.emplace_back(vBufferHealthStates);
        vvBuffer.emplace_back(vBufferdistributionSR);
        vvBuffer.emplace_back(vBufferdistributionT);
        vvBuffer.emplace_back(vBufferdistributionL);
        vvBuffer.emplace_back(vBufferdistributionIs);
        vvBuffer.emplace_back(vBufferdistributionIc);
        vvBuffer.emplace_back(vBufferEnvironments);
        
        vvvProbaInitInfection[timeInYear-1].emplace_back(vvBuffer); // The lines are added to the array
        
    }
}


void Parameters::loadMatrixInfectionInitPrevalenceLevelDistribution(){
    // Construction of vvvPrevalenceLevelDistribution

    // read the file
    std::ifstream fileMatPrevLevel(folderPath + "data/" + "selectedInitScenarioForPrev_1to90.csv", std::fstream::in); // Open file in reading mode
    if(!fileMatPrevLevel){std::cerr << "[ERROR] Unable to open the file 'selectedInitScenarioForPrev_1to90.csv' !!" << std::endl;} // Check if the file is open!

    // build vector for the average scenario
    std::vector<double> vBufferHealthStates_avg(5, 0.);
    std::vector<double> vBufferdistributionSR_avg(135, 0.);
    std::vector<double> vBufferdistributionT_avg(135, 0.);
    std::vector<double> vBufferdistributionL_avg(135, 0.);
    std::vector<double> vBufferdistributionIs_avg(135, 0.);
    std::vector<double> vBufferdistributionIc_avg(135, 0.);
    std::vector<double> vBufferEnvironments_avg(12, 0.);
    // init the average scenario
    std::vector<std::vector<double>> vvBuffer_avg;
    vvBuffer_avg.emplace_back(vBufferHealthStates_avg);
    vvBuffer_avg.emplace_back(vBufferdistributionSR_avg);
    vvBuffer_avg.emplace_back(vBufferdistributionT_avg);
    vvBuffer_avg.emplace_back(vBufferdistributionL_avg);
    vvBuffer_avg.emplace_back(vBufferdistributionIs_avg);
    vvBuffer_avg.emplace_back(vBufferdistributionIc_avg);
    vvBuffer_avg.emplace_back(vBufferEnvironments_avg);
    // for each level of prevalence, initialize the average scenario
    for (int thePrev = 1; thePrev<=Parameters::maxPrevLevel; thePrev++) {
        vvPrevalenceLevelAverageDistribution[thePrev] = vvBuffer_avg;
    }

    // For each line in the file:
    std::string lineMatPrevLevel;
    std::getline(fileMatPrevLevel, lineMatPrevLevel); // read the first line (header)
    while (std::getline(fileMatPrevLevel, lineMatPrevLevel)){
        // Construction of a line
        std::stringstream ss(lineMatPrevLevel);
        std::vector<double> vBuffer;
        // Data extraction for the line
        std::string temp;
        while(std::getline(ss, temp, ',')) {
            vBuffer.emplace_back(std::stod(temp));
        }
        // build the different vector
        std::vector<double> vBufferHealthStates(vBuffer.begin(), vBuffer.begin()+5); assert(vBufferHealthStates.size() == 5);
        std::vector<double> vBufferdistributionSR(vBuffer.begin()+5, vBuffer.begin()+5+135); assert(vBufferdistributionSR.size() == 135);
        std::vector<double> vBufferdistributionT(vBuffer.begin()+5+135, vBuffer.begin()+5+135*2); assert(vBufferdistributionT.size() == 135);
        std::vector<double> vBufferdistributionL(vBuffer.begin()+5+135*2, vBuffer.begin()+5+135*3); assert(vBufferdistributionL.size() == 135);
        std::vector<double> vBufferdistributionIs(vBuffer.begin()+5+135*3, vBuffer.begin()+5+135*4); assert(vBufferdistributionIs.size() == 135);
        std::vector<double> vBufferdistributionIc(vBuffer.begin()+5+135*4, vBuffer.begin()+5+135*5); assert(vBufferdistributionIc.size() == 135);
        std::vector<double> vBufferEnvironments(vBuffer.begin()+5+135*5, vBuffer.end()); assert(vBufferEnvironments.size() == 12);
        // build the scenario
        std::vector<std::vector<double>> vvBuffer;
        vvBuffer.emplace_back(vBufferHealthStates);
        vvBuffer.emplace_back(vBufferdistributionSR);
        vvBuffer.emplace_back(vBufferdistributionT);
        vvBuffer.emplace_back(vBufferdistributionL);
        vvBuffer.emplace_back(vBufferdistributionIs);
        vvBuffer.emplace_back(vBufferdistributionIc);
        vvBuffer.emplace_back(vBufferEnvironments);
        // get the prevalence level
        int prevLevel = round((1.0-vBufferHealthStates[0])*100);
        // add the scenario in the map
        vvvPrevalenceLevelDistribution[prevLevel].emplace_back(vvBuffer); // The lines are added to the array

        // accumulate the scenarios values for the average scenario
        vvPrevalenceLevelAverageDistribution[prevLevel][0] = vvPrevalenceLevelAverageDistribution[prevLevel][0] + vBufferHealthStates;
        vvPrevalenceLevelAverageDistribution[prevLevel][1] = vvPrevalenceLevelAverageDistribution[prevLevel][1] + vBufferdistributionSR;
        vvPrevalenceLevelAverageDistribution[prevLevel][2] = vvPrevalenceLevelAverageDistribution[prevLevel][2] + vBufferdistributionT;
        vvPrevalenceLevelAverageDistribution[prevLevel][3] = vvPrevalenceLevelAverageDistribution[prevLevel][3] + vBufferdistributionL;
        vvPrevalenceLevelAverageDistribution[prevLevel][4] = vvPrevalenceLevelAverageDistribution[prevLevel][4] + vBufferdistributionIs;
        vvPrevalenceLevelAverageDistribution[prevLevel][5] = vvPrevalenceLevelAverageDistribution[prevLevel][5] + vBufferdistributionIc;
        vvPrevalenceLevelAverageDistribution[prevLevel][6] = vvPrevalenceLevelAverageDistribution[prevLevel][6] + vBufferEnvironments;
    }

    // normalize the average scenario
    for (int thePrev = 1; thePrev<=Parameters::maxPrevLevel; thePrev++) {
        int theDenum = static_cast<int>(vvvPrevalenceLevelDistribution[thePrev].size());
        vvPrevalenceLevelAverageDistribution[thePrev][0] = U_functions::VectorDivision(vvPrevalenceLevelAverageDistribution[thePrev][0], theDenum);
        vvPrevalenceLevelAverageDistribution[thePrev][1] = U_functions::VectorDivision(vvPrevalenceLevelAverageDistribution[thePrev][1], theDenum);
        vvPrevalenceLevelAverageDistribution[thePrev][2] = U_functions::VectorDivision(vvPrevalenceLevelAverageDistribution[thePrev][2], theDenum);
        vvPrevalenceLevelAverageDistribution[thePrev][3] = U_functions::VectorDivision(vvPrevalenceLevelAverageDistribution[thePrev][3], theDenum);
        vvPrevalenceLevelAverageDistribution[thePrev][4] = U_functions::VectorDivision(vvPrevalenceLevelAverageDistribution[thePrev][4], theDenum);
        vvPrevalenceLevelAverageDistribution[thePrev][5] = U_functions::VectorDivision(vvPrevalenceLevelAverageDistribution[thePrev][5], theDenum);
        vvPrevalenceLevelAverageDistribution[thePrev][6] = U_functions::VectorDivision(vvPrevalenceLevelAverageDistribution[thePrev][6], theDenum);

//        std::cout << std::endl << "prev: ";
//        for (auto element : vvPrevalenceLevelAverageDistribution[thePrev][0]) {
//            std::cout << element << " ";
//        }
//        std::cout << std::endl;
//
//        std::cout << "SR: ";
//        for (auto element : vvPrevalenceLevelAverageDistribution[thePrev][1]) {
//            std::cout << element << " ";
//        }
//        std::cout << std::endl;
//
//        std::cout << "T: ";
//        for (auto element : vvPrevalenceLevelAverageDistribution[thePrev][2]) {
//            std::cout << element << " ";
//        }
//        std::cout << std::endl;
//
//        std::cout << "L: ";
//        for (auto element : vvPrevalenceLevelAverageDistribution[thePrev][3]) {
//            std::cout << element << " ";
//        }
//        std::cout << std::endl;
//
//        std::cout << "Is: ";
//        for (auto element : vvPrevalenceLevelAverageDistribution[thePrev][4]) {
//            std::cout << element << " ";
//        }
//        std::cout << std::endl;
//
//        std::cout << "Ic: ";
//        for (auto element : vvPrevalenceLevelAverageDistribution[thePrev][5]) {
//            std::cout << element << " ";
//        }
//        std::cout << std::endl;
//
//        std::cout << "env: ";
//        for (auto element : vvPrevalenceLevelAverageDistribution[thePrev][6]) {
//            std::cout << element << " ";
//        }
//        std::cout << std::endl;
    }
    

}


void Parameters::updateTheABCsmcRunNumber(){
    ABCsmcRunNumber++;
}


void Parameters::updateABCsmcCurrentRunIsCompleted(){
    ABCsmcCurrentRunIsCompleted = true;
}


void Parameters::updateProbaToPurchaseInfectedAnimalOutsideTheMetapopulation(const int& timeStep){
    probaToPurchaseInfectedAnimalOutsideTheMetapopulation = std::max(0.0, std::min(1.0, probaToPurchaseInfectedAnimalOutsideTheMetapopulation_initialValue + probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope * timeStep));

//    if (probaToPurchaseInfectedAnimalOutsideTheMetapopulation > 1.0) {
//        probaToPurchaseInfectedAnimalOutsideTheMetapopulation = 1.0;
//    }
//
//    if (probaToPurchaseInfectedAnimalOutsideTheMetapopulation < 0.0) {
//        probaToPurchaseInfectedAnimalOutsideTheMetapopulation = 0.0;
//    }
}



