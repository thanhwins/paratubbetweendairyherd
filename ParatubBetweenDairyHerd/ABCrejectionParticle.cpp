//
//  ABCrejectionParticle.cpp
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunée on 27/05/2015.
//  Copyright (c) 2015 Gaël Beaunée. All rights reserved.
//

#include "ABCrejectionParticle.h"

// Constructor =========================================================
ABCrejectionParticle::ABCrejectionParticle(const double& theParam, const double& theDistance){
    paramValue = theParam;
    distance = theDistance;
}

// Member functions ====================================================


