//
//  ObservedSampleInformations.hpp
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunée on 14/06/2016.
//  Copyright © 2016 Gaël Beaunée. All rights reserved.
//

#ifndef ObservedSampleInformations_hpp
#define ObservedSampleInformations_hpp

#include <stdio.h>
#include <algorithm>

class ObservedSampleInformations {
    
public:
    // Constructor =========================================================
    ObservedSampleInformations();
    ObservedSampleInformations(const int& theObservedHeadount, const int& theObservedSamplesCount, const double& theObservedPositiveSamplesCount);
    
    // Variables ===========================================================
    int observedHeadount;
    int observedSamplesCount;
    int observedPositiveSamplesCount;
    
    double proportionOfSample;
    double proportionOfPositiveSample;
};

#endif /* ObservedSampleInformations_hpp */
