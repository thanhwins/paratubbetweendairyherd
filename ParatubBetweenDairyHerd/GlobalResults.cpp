//
//  GlobalResults.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 08/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#include "GlobalResults.h"

GlobalResults::GlobalResults(){
    // Variables ===========================================================
    accMetapopPersistence = accVectInt(std::vector<int>(Parameters::simutime));
    for (int i=0; i<Parameters::nbHerds; i++) {
        vAccHeadcounts.emplace_back(accVectInt(std::vector<int>(Parameters::simutime)));
        vAccAnnualHeadcounts.emplace_back(accVectInt(std::vector<int>(Parameters::nbYears*2)));
        vAccAnnualHeadcountsLact.emplace_back(accVectInt(std::vector<int>(Parameters::nbYears*2)));
        vAccNbPres.emplace_back(accVectDouble(std::vector<double>(Parameters::nbYears)));
        vAccNbPresLact.emplace_back(accVectDouble(std::vector<double>(Parameters::nbYears)));
//        vAccAnnualBirths.emplace_back(accVectInt(std::vector<int>(Parameters::nbYears)));
//        vAccAnnualBirthsPerAgeGroup.emplace_back(accVectDouble(std::vector<double>(Parameters::nbYears*6)));
//        vAccBirthsPerAgeGroup.emplace_back(accVectDouble(std::vector<double>(6)));
    }
}


// Member functions ====================================================
void GlobalResults::updateAcc(BufferGlobalResults& iBufferGlobalResults, std::map<std::string, DairyHerd>& mFarms){
    
    accMetapopPersistence(iBufferGlobalResults.vMetapopPersistence);
    
    for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
        int pos = it->second.iParametersSpecificToEachHerd.farmIndex;

        vAccHeadcounts[pos](it->second.iSummaryDynamic.headcount);
        
        std::vector<int> vAnnualHeadcounts;
        std::vector<int> vAnnualHeadcountsLact;
        for (int time = 0; time < Parameters::nbYears*2; time ++) {
            vAnnualHeadcounts.emplace_back(it->second.iSummaryDynamic.headcount[time*(Parameters::timestepInOneYear/2)]);
            vAnnualHeadcountsLact.emplace_back(it->second.iSummaryDynamic.headcountLact[time*(Parameters::timestepInOneYear/2)]);
        }
        vAccAnnualHeadcounts[pos](vAnnualHeadcounts);
        vAccAnnualHeadcountsLact[pos](vAnnualHeadcountsLact);
        
        
        std::vector<double> vNbPres;
        std::vector<double> vNbPresLact;
        for (int time = 0; time < Parameters::nbYears; time ++) {
            double nbPresBuffer = 0.0;
            double nbPresLactBuffer = 0.0;
            for (int t = time*Parameters::timestepInOneYear; t < time*Parameters::timestepInOneYear+Parameters::timestepInOneYear; t++) {
                nbPresBuffer += it->second.iSummaryDynamic.headcount[t];
                nbPresLactBuffer += it->second.iSummaryDynamic.headcountLact[t];
            }
            nbPresBuffer = nbPresBuffer/Parameters::timestepInOneYear;
            nbPresLactBuffer = nbPresLactBuffer/Parameters::timestepInOneYear;
            vNbPres.emplace_back(nbPresBuffer);
            vNbPresLact.emplace_back(nbPresLactBuffer);
        }
        vAccNbPres[pos](vNbPres);
        vAccNbPresLact[pos](vNbPresLact);
        
        
//        vAccAnnualBirths[pos](it->second.vNbBirthsPerYear);
//        vAccAnnualBirthsPerAgeGroup[pos](it->second.vNbBirthsPerYearPerAgeGroup);
//        vAccBirthsPerAgeGroup[pos](U_functions::getProportionsFromHeadcount(it->second.vNbBirthsPerAgeGroup));
    }
}




