//
//  ABCInfosAndResults.h
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunée on 23/05/2015.
//  Copyright (c) 2015 Gaël Beaunée. All rights reserved.
//

#ifndef __ParatubBetweenDairyHerd__ABCInfosAndResults__
#define __ParatubBetweenDairyHerd__ABCInfosAndResults__

#include <stdio.h>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <math.h>
#include <gsl/gsl_statistics.h>
#include <algorithm>

#include "ABCsmcSetOfPreviousParticles.h"
#include "ABCrejectionParticle.h"
#include "ABCsmcParticle.h"
#include "Parameters.h"
#include "DairyHerd.h"
#include "UtilitiesFunctions.h"

class ABCInfosAndResults {
    
public:
    // Constructor =========================================================
    ABCInfosAndResults();
    
    // Variables ===========================================================
    // ABC-rejection
    std::vector<ABCrejectionParticle> vABCrejectionResults;
    
    // ABC-smc
    std::map<std::string, ABCsmcSetOfPreviousParticles> previousParticles;
    double threshold;
    
    std::vector<ABCsmcParticle> vABCsmcAllParticles;
    std::vector<ABCsmcParticle> vABCsmcSelectedParticles;
    double numberOfGeneratedParticles = 0.;
    double numberOfSelectedParticles = 0.;
    
    //
//    std::map<std::string, std::vector<int>> simulatedsummaryStatistic; // A remplacer par la sauvegarde des distances par troupeaux
    
    // Member functions ====================================================
    void initializeABCsmc();
    void loadABCsmcParticlesForTheRun(const std::string& param);
    
    void computeAndStoreDistanceForTheRun(ParametersSpecificToEachRepetition& ParamRep, std::map<std::string, DairyHerd>& mFarms);
//    double computeDistanceBetweenSimulationAndObservation(std::map<std::string, DairyHerd>& mFarms);
    
    void saveABCrejectionResults();
    
    void particlesProcessingForNextRunOfABCsmc();
    void computeNextPreviousParticles(const std::string& param);
    void saveABCsmcResults(const std::string& param);
    
    void saveInformationsOnTheCurrentRun();
    void saveParticlesOfABCsmc(const std::string& param);
    
//    void storeSimulatedSummaryStatistics(std::map<std::string, DairyHerd>& mFarms); // A remplacer par la sauvegarde des distances par troupeaux
//    void saveSimulatedSummaryStatistics(); // A remplacer par la sauvegarde des distances par troupeaux
    
};

#endif /* defined(__ParatubBetweenDairyHerd__ABCResults__) */
