//
//  ResultsWriting.hpp
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunée on 25/07/2017.
//  Copyright © 2017 Gaël Beaunée. All rights reserved.
//

#ifndef ResultsWriting_hpp
#define ResultsWriting_hpp

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <random>
#include <gsl/gsl_randist.h>
#include <cmath>
#include <algorithm>
#include <stdexcept>
#include <boost/accumulators/numeric/functional/vector.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/sum.hpp>
#include <boost/accumulators/statistics/count.hpp>
#include <boost/accumulators/statistics/median.hpp>
#include <boost/accumulators/statistics/extended_p_square.hpp>

#ifdef ACTIVATE_PARALLEL
#include <omp.h>
#endif

#include "Parameters.h"
#include "ParametersSpecificToEachRepetition.h"
#include "DairyHerd.h"
#include "PersistentResults.h"
#include "BufferPersistentResults.h"
#include "GlobalResults.h"
#include "BufferGlobalResults.h"
#include "UtilitiesFunctions.h"


using namespace boost::accumulators;

typedef accumulator_set<int, stats<tag::mean, tag::variance, tag::count, tag::sum > > accInt;
typedef accumulator_set<double, stats<tag::mean, tag::variance, tag::count, tag::sum > > accDouble;
typedef accumulator_set<std::vector<int>, stats<tag::mean, tag::variance, tag::count, tag::sum > > accVectInt;
typedef accumulator_set<std::vector<double>, stats<tag::mean, tag::variance, tag::count, tag::sum > > accVectDouble;
typedef std::vector<accDouble> vectAccDouble;

typedef accumulator_set<double, stats<tag::extended_p_square> > accQuantiles;
typedef std::vector< accQuantiles > accVectQuantiles;

typedef std::vector<int> vInt;
typedef std::map<int, vInt> mInt2vInt;
typedef std::vector<mInt2vInt> vMapInt2vInt;
typedef std::vector<vInt> vIntVectors;
typedef std::vector<AnimalMoving> vAnimalMovingVectors;


namespace ResultsWriting {
//    void SaveResults(PersistentResults& acc_persistentResults, GlobalResults& acc_globalResults);

    void openFileAndWrite(std::string fileName, const accVectDouble& AccumulatorsVector);
    void openFileAndWrite(std::string fileName, const accVectInt& AccumulatorsVector);
    void openFileAndWrite(std::string fileName, const std::vector<double>& Results);
    void openFileAndWrite(std::string fileName, const std::vector<int>& Results);
    void openFileAndWrite(std::string fileName, const std::vector<std::string>& Results);
    void openFileAndWrite(std::string fileName, const vectAccDouble& VectorAccumulators);
    void openFileAndWrite(std::string fileName, const accInt& AccumulatorsInt);

    void openFileAndWrite(std::string fileName, const accVectQuantiles& AccumulatorsQuantiles);

    void openFileAndWriteMatrix_FirstIndexThenSecondIndex(std::string fileName, std::vector<std::vector<double>>& Results);
    void openFileAndWriteMatrix_FirstIndexThenSecondIndex(std::string fileName, std::vector<std::vector<int>>& Results);
    void openFileAndWriteMatrix_FirstIndexThenSecondIndex(std::string fileName, std::vector<std::vector<std::string>>& Results);
    void openFileAndWriteMatrix_SecondIndexThenFirstIndex(std::string fileName, std::vector<std::vector<int>>& Results);

    void openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID(std::string fileName, std::vector<std::vector<int>>& Results);

    void openFileAndWrite(std::string fileName, const std::vector<accVectInt>& vectAccVectInt);
    void openFileAndWrite(std::string fileName, const std::vector<accVectDouble>& vectAccVectDouble);

//    void saveInfectionDynamic(int run, std::map<std::string, DairyHerd>& mFarms);
//    void updateAccSecondPrev(std::map<std::string, DairyHerd>& mFarms);
//    void updateSourceAndDestinationResults(const int& timeStep, const std::string& theFarmId, const int& type, const int& age, std::map<std::string, DairyHerd>& mFarms, const std::string& herdSource, const int& typeOfInfectedHerdSource, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary, const int& run);
//    void updateIntraHerdPrevalenceAtTheEnd(std::map<std::string, DairyHerd>& mFarms);
//    void saveIntraHerdPrevalenceAtTheEnd();

    bool saveABCcompleteTrajectoriesResults(std::map<std::string, DairyHerd>& mFarms, const int& run);

    void openFileAndWriteMatrix_farmsInLine_timeInColumn_forTheCurrentRun(std::string fileName, std::map<std::string, std::vector<double>>& Results, const int& run);

    void openFileAndWrite(std::string fileName, const std::vector<int>& Results, const int& run);

//    void openFileAndWrite_mAcc(std::string fileName, std::map<std::string, accVectDouble>& mFarms_AccumulatorsVector);
};

#endif /* ResultsWriting_hpp */
