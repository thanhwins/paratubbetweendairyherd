//
//  NewInfectionsST.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 05/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#include "NewInfectionsST.h"

// Constructor =========================================================
NewInfectionsST::NewInfectionsST(){
    hcInBuilding = 0;
    hcUnweaned = 0;
    hcWeanedIn = 0;
    hcWeanedOut = 0;
    hcYheifer = 0;
    hcHeifer = 0;
    
    newInfColo = 0;
    newInfMilk = 0;
    newInfLocal = 0;
    newInfGlobal = 0;
    
//    InfOut6monthCC.resize(Parameters::simutime,0);
//    InfIn6monthCC.resize(Parameters::simutime,0);
//    InfIn6monthAC.resize(Parameters::simutime,0);
}


// Member functions ====================================================
void NewInfectionsST::updateVariables(const int& t, const int& weeknumber, const HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer){
    if (Parameters::managementOfPositiveTestedTakeIntoAccount) {
        hcUnweaned = U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR.present, 1, Parameters::ageWeaning) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT.present, 1, Parameters::ageWeaning) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL.present, 1, Parameters::ageWeaning) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present, 1, Parameters::ageWeaning) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present, 1, Parameters::ageWeaning) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present, 1, Parameters::ageWeaning);
        
        hcWeanedIn = U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR.present, Parameters::ageWeaning+1, Parameters::ageGrazing) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT.present, Parameters::ageWeaning+1, Parameters::ageGrazing) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL.present, Parameters::ageWeaning+1, Parameters::ageGrazing) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present, Parameters::ageWeaning+1, Parameters::ageGrazing) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present, Parameters::ageWeaning+1, Parameters::ageGrazing) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present, Parameters::ageWeaning+1, Parameters::ageGrazing);
        
        hcWeanedOut = U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR.present, Parameters::ageGrazing+1, Parameters::ageCalf) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT.present, Parameters::ageGrazing+1, Parameters::ageCalf) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL.present, Parameters::ageGrazing+1, Parameters::ageCalf) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present, Parameters::ageGrazing+1, Parameters::ageCalf) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present, Parameters::ageGrazing+1, Parameters::ageCalf) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present, Parameters::ageGrazing+1, Parameters::ageCalf);
        
        hcYheifer = U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR.present, Parameters::ageCalf+1, Parameters::ageYheifer) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT.present, Parameters::ageCalf+1, Parameters::ageYheifer) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL.present, Parameters::ageCalf+1, Parameters::ageYheifer) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present, Parameters::ageCalf+1, Parameters::ageYheifer) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present, Parameters::ageCalf+1, Parameters::ageYheifer) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present, Parameters::ageCalf+1, Parameters::ageYheifer);
        
        hcHeifer = U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR.present, Parameters::ageYheifer+1, Parameters::ageFirstCalving) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT.present, Parameters::ageYheifer+1, Parameters::ageFirstCalving) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL.present, Parameters::ageYheifer+1, Parameters::ageFirstCalving) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentIs.present, Parameters::ageYheifer+1, Parameters::ageFirstCalving) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentIc.present, Parameters::ageYheifer+1, Parameters::ageFirstCalving) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present, Parameters::ageYheifer+1, Parameters::ageFirstCalving) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present, Parameters::ageYheifer+1, Parameters::ageFirstCalving) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present, Parameters::ageYheifer+1, Parameters::ageFirstCalving) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present, Parameters::ageYheifer+1, Parameters::ageFirstCalving) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present, Parameters::ageYheifer+1, Parameters::ageFirstCalving);
        
        if ((weeknumber >= Parameters::startGrazing) && (weeknumber <= Parameters::endGrazing)) {
            // in the grazing period
            hcInBuilding = U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR.present, 1, Parameters::ageGrazing) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT.present, 1, Parameters::ageGrazing) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL.present, 1, Parameters::ageGrazing) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present, 1, Parameters::ageGrazing) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present, 1, Parameters::ageGrazing) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present, 1, Parameters::ageGrazing);
        } else {
            // out of the grazing period
            hcInBuilding = U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR.present, 1, Parameters::nbStage) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT.present, 1, Parameters::nbStage) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL.present, 1, Parameters::nbStage) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentIs.present, 1, Parameters::nbStage) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentIc.present, 1, Parameters::nbStage) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present, 1, Parameters::nbStage) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present, 1, Parameters::nbStage) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present, 1, Parameters::nbStage) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present, 1, Parameters::nbStage) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present, 1, Parameters::nbStage);
        }
    } else {
        hcUnweaned = U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR.present, 1, Parameters::ageWeaning) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT.present, 1, Parameters::ageWeaning) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL.present, 1, Parameters::ageWeaning);
        
        hcWeanedIn = U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR.present, Parameters::ageWeaning+1, Parameters::ageGrazing) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT.present, Parameters::ageWeaning+1, Parameters::ageGrazing) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL.present, Parameters::ageWeaning+1, Parameters::ageGrazing);
        
        hcWeanedOut = U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR.present, Parameters::ageGrazing+1, Parameters::ageCalf) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT.present, Parameters::ageGrazing+1, Parameters::ageCalf) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL.present, Parameters::ageGrazing+1, Parameters::ageCalf);
        
        hcYheifer = U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR.present, Parameters::ageCalf+1, Parameters::ageYheifer) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT.present, Parameters::ageCalf+1, Parameters::ageYheifer) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL.present, Parameters::ageCalf+1, Parameters::ageYheifer);
        
        hcHeifer = U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR.present, Parameters::ageYheifer+1, Parameters::ageFirstCalving) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT.present, Parameters::ageYheifer+1, Parameters::ageFirstCalving) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL.present, Parameters::ageYheifer+1, Parameters::ageFirstCalving) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentIs.present, Parameters::ageYheifer+1, Parameters::ageFirstCalving) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentIc.present, Parameters::ageYheifer+1, Parameters::ageFirstCalving);
        
        if ((weeknumber >= Parameters::startGrazing) && (weeknumber <= Parameters::endGrazing)) {
            // in the grazing period
            hcInBuilding = U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR.present, 1, Parameters::ageGrazing) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT.present, 1, Parameters::ageGrazing) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL.present, 1, Parameters::ageGrazing);
        } else {
            // out of the grazing period
            hcInBuilding = U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentSR.present, 1, Parameters::nbStage) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentT.present, 1, Parameters::nbStage) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentL.present, 1, Parameters::nbStage) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentIs.present, 1, Parameters::nbStage) + U_functions::SumOfAnimalsByAge(iHealthStateCompartmentsContainer.iCompartmentIc.present, 1, Parameters::nbStage);
        }
    }
}




void NewInfectionsST::update(gsl_rng * randomGenerator, const int& t, const int& age, const int& weeknumber, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const MapInEnvironments& Environments, const HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer){
    
    newInfColo = 0;
    newInfMilk = 0;
    newInfLocal = 0;
    newInfGlobal = 0;
    
    
    // in and out the grazing period +++++++++++++++++++++++++++++++++++++++
    
    // colostrum ---------------------------------------------------
    if (Parameters::colostrumTR == true) {
        if (age == 0) {
            for (int i = 0; i<Environments.colostrum.size(); i++) {
                newInfColo += gsl_ran_bernoulli(randomGenerator, (1 - exp( -Parameters::infIngestion * Environments.colostrum[i] / Parameters::infectiousDose)));
            }
        }
    }
    
    // milk --------------------------------------------------------
    if (Parameters::milkTR == true) {
        if ((iHealthStateCompartmentsContainer.iCompartmentSR.present[age]-newInfColo)>0) {
            if (age < Parameters::ageWeaning) {
                if (age == 0) {
                    newInfMilk = gsl_ran_binomial(randomGenerator, (1 - exp( -Parameters::infIngestion * (4./7.) * Environments.milk[t] / Parameters::infectiousDose)), (iHealthStateCompartmentsContainer.iCompartmentSR.present[age]-newInfColo));
                } else {
                    newInfMilk = gsl_ran_binomial(randomGenerator, (1 - exp( -exp(-Parameters::susceptibilityCoeff*(age)) * Parameters::infIngestion * Environments.milk[t] / Parameters::infectiousDose)), (iHealthStateCompartmentsContainer.iCompartmentSR.present[age]-newInfColo));
                }
            }
        }
    }
    
    if ((weeknumber >= Parameters::startGrazing) && (weeknumber <= Parameters::endGrazing)) {
        // in the grazing period +++++++++++++++++++++++++++++++++++++++
        
        // local -------------------------------------------------------
        if (Parameters::localTR == true) {
            if ((iHealthStateCompartmentsContainer.iCompartmentSR.present[age]-newInfColo-newInfMilk)>0) {
                // common part
                if (age < Parameters::ageWeaning) {
                    if (hcUnweaned > 0) {
                        newInfLocal = gsl_ran_binomial(randomGenerator, (1 - exp( -exp(-Parameters::susceptibilityCoeff*(age)) * Parameters::infLocalEnv * Environments.int_1[t] / hcUnweaned / Parameters::infectiousDose)), (iHealthStateCompartmentsContainer.iCompartmentSR.present[age]-newInfColo-newInfMilk));
                    }
                }
                
                // during the grazing period
                if ((age >= Parameters::ageWeaning) & (age < Parameters::ageGrazing)) {
                    if (hcWeanedIn > 0) {
                        newInfLocal = gsl_ran_binomial(randomGenerator, (1 - exp( -exp(-Parameters::susceptibilityCoeff*(age)) * Parameters::infLocalEnv * Environments.int_2[t] / hcWeanedIn / Parameters::infectiousDose)), (iHealthStateCompartmentsContainer.iCompartmentSR.present[age]-newInfColo-newInfMilk));
                    }
                }
                
                if ((age >= Parameters::ageGrazing) & (age < Parameters::ageYheifer)) {
                    if ((hcWeanedOut + hcYheifer) > 0) {
                        newInfLocal = gsl_ran_binomial(randomGenerator, (1 - exp( -exp(-Parameters::susceptibilityCoeff*(age)) * Parameters::infGrazing * (Environments.ext_1[t] + Environments.ext_2[t]) / (hcWeanedOut + hcYheifer) / Parameters::infectiousDose)), (iHealthStateCompartmentsContainer.iCompartmentSR.present[age]-newInfColo-newInfMilk));
                    }
                }
                
                if ((age >= Parameters::ageYheifer) & (age < Parameters::ageFirstCalving)) {
                    if (hcHeifer > 0) {
                        newInfLocal = gsl_ran_binomial(randomGenerator, (1 - exp( -exp(-Parameters::susceptibilityCoeff*(age)) * Parameters::infGrazing * Environments.ext_3[t] / hcHeifer / Parameters::infectiousDose)), (iHealthStateCompartmentsContainer.iCompartmentSR.present[age]-newInfColo-newInfMilk));
                    }
                }
            }
        }
        
        // global ------------------------------------------------------
        if (hcInBuilding > 0) {
            if ((iHealthStateCompartmentsContainer.iCompartmentSR.present[age]-newInfColo-newInfMilk-newInfLocal)>0) {
                if (age < Parameters::ageGrazing) {
                    newInfGlobal = gsl_ran_binomial(randomGenerator, (1 - exp( -exp(-Parameters::susceptibilityCoeff*(age)) * iParametersSpecificToEachHerd.infGlobalEnvInThisFarm * Environments.global[t] / hcInBuilding / Parameters::infectiousDose)), (iHealthStateCompartmentsContainer.iCompartmentSR.present[age]-newInfColo-newInfMilk-newInfLocal));
                }
            }
        }
        
        // an other transmission route ---------------------------------
        
    } else {
        // out of the grazing period +++++++++++++++++++++++++++++++++++
        
        // local -------------------------------------------------------
        if (Parameters::localTR == true) {
            if ((iHealthStateCompartmentsContainer.iCompartmentSR.present[age]-newInfColo-newInfMilk)>0) {
                // common part
                if (age < Parameters::ageWeaning) {
                    if (hcUnweaned > 0) {
                        newInfLocal = gsl_ran_binomial(randomGenerator, (1 - exp( -exp(-Parameters::susceptibilityCoeff*(age)) * Parameters::infLocalEnv * Environments.int_1[t] / hcUnweaned / Parameters::infectiousDose)), (iHealthStateCompartmentsContainer.iCompartmentSR.present[age]-newInfColo-newInfMilk));
                    }
                }
                
                // during the grazing period
                if ((age >= Parameters::ageWeaning) & (age < Parameters::ageCalf)) {
                    if ((hcWeanedIn + hcWeanedOut) > 0) {
                        newInfLocal = gsl_ran_binomial(randomGenerator, (1 - exp( -exp(-Parameters::susceptibilityCoeff*(age)) * Parameters::infLocalEnv * Environments.int_2[t] / (hcWeanedIn + hcWeanedOut) / Parameters::infectiousDose)), (iHealthStateCompartmentsContainer.iCompartmentSR.present[age]-newInfColo-newInfMilk));
                    }
                }
                
                if ((age >= Parameters::ageCalf) & (age < Parameters::ageYheifer)) {
                    if (hcYheifer > 0) {
                        newInfLocal = gsl_ran_binomial(randomGenerator, (1 - exp( -exp(-Parameters::susceptibilityCoeff*(age)) * Parameters::infLocalEnv * Environments.int_3[t] / hcYheifer / Parameters::infectiousDose)), (iHealthStateCompartmentsContainer.iCompartmentSR.present[age]-newInfColo-newInfMilk));
                    }
                }
                
                if ((age >= Parameters::ageYheifer) & (age < Parameters::ageFirstCalving)) {
                    if (hcHeifer > 0) {
                        newInfLocal = gsl_ran_binomial(randomGenerator, (1 - exp( -exp(-Parameters::susceptibilityCoeff*(age)) * Parameters::infLocalEnv * Environments.int_4[t] / hcHeifer / Parameters::infectiousDose)), (iHealthStateCompartmentsContainer.iCompartmentSR.present[age]-newInfColo-newInfMilk));
                    }
                }
            }
        }
        
        // global ------------------------------------------------------
        if (hcInBuilding > 0) {
            if ((iHealthStateCompartmentsContainer.iCompartmentSR.present[age]-newInfColo-newInfMilk-newInfLocal)>0) {
                newInfGlobal = gsl_ran_binomial(randomGenerator, (1 - exp( -exp(-Parameters::susceptibilityCoeff*(age)) * iParametersSpecificToEachHerd.infGlobalEnvInThisFarm * Environments.global[t] / hcInBuilding / Parameters::infectiousDose)), (iHealthStateCompartmentsContainer.iCompartmentSR.present[age]-newInfColo-newInfMilk-newInfLocal));
            }
        }
        
        // an other transmission route ---------------------------------
        
    }
    
}




void NewInfectionsST::transfer(const int& t, const int& age, const int& weeknumber, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer, SummaryInfection& DynInf){
    if ((newInfColo + newInfMilk + newInfLocal + newInfGlobal)>0) {
        
        iHealthStateCompartmentsContainer.iCompartmentSR.present[age] = iHealthStateCompartmentsContainer.iCompartmentSR.present[age] - (newInfColo + newInfMilk + newInfLocal + newInfGlobal);
        iHealthStateCompartmentsContainer.iCompartmentT.present[age] = iHealthStateCompartmentsContainer.iCompartmentT.present[age] + (newInfColo + newInfMilk + newInfLocal + newInfGlobal);
        
        DynInf.ageAtInfection_colostrum[age] += newInfColo;
        DynInf.ageAtInfection_milk[age] += newInfMilk;
        DynInf.ageAtInfection_local[age] += newInfLocal;
        DynInf.ageAtInfection_global[age] += newInfGlobal;
        DynInf.ageAtInfection[age] += (newInfColo + newInfMilk + newInfLocal + newInfGlobal);
        
        DynInf.transmissionRoutes_colostrum[t] += newInfColo;
        DynInf.transmissionRoutes_milk[t] += newInfMilk;
        DynInf.transmissionRoutes_local[t] += newInfLocal;
        DynInf.transmissionRoutes_global[t] += newInfGlobal;
        DynInf.IncidenceT[t] += (newInfColo + newInfMilk + newInfLocal + newInfGlobal);
        
//        if ((weeknumber >= Parameters::startGrazing) && (weeknumber <= Parameters::endGrazing)) {
//            // in the grazing period +++++++++++++++++++++++++++++++++++
//            
//            if (Parameters::localTR == true) {
//                if (age >= Parameters::ageGrazing) {
//                    InfOut6monthCC[t] += newInfLocal;
//                }
//            }
//            
//        } else {
//            // out of the grazing period +++++++++++++++++++++++++++++++
//            
//            if (age >= Parameters::ageGrazing) {
//                InfIn6monthAC[t] += newInfLocal;
//                if (Parameters::localTR == true) {
//                    InfIn6monthCC[t] += newInfLocal;
//                }
//            }
//        }
    }
}





