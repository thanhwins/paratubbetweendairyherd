//
//  DairyHerd.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 29/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#include "DairyHerd.h"


// Constructor =========================================================
DairyHerd::DairyHerd(){}

DairyHerd::DairyHerd(const ParametersSpecificToEachRepetition& ParamRep, const std::string& theId) : iParametersSpecificToEachHerd(ParamRep, theId) {
    persistenceIndicator = false;
    //    vMovesToReceiveAfterDynamics.resize(5, 0);
    firstCaseDate = Parameters::simutime;
    hasAlreadyBeenInfected = false;
    vInfectionDynamic.resize(Parameters::simutime,0);
    firstInfectedHerd = false;
    typeInfectedHerd = 2;
    numberOfWeeksSinceInfected = 0;
    numberOfWeeksSinceInfectious = 0;
    numberOfWeeksWithInfection = 0;
    numberOfInfection = 0;
    dateOfAppearanceOfTheFirstIc = 0;
    cumulativeNumberOfIc = 0;
    presenceOfIcAtThisTime = false;
    
    IcCulled = 0;
    numberOfAnimalsTestedDuringTestAndCull = 0;
    numberOfAnimalsCulledDuringTestAndCull = 0;
    
    StartDateOfTheLastPeriodOfInfection = 0;
    numberOfPurchasesOfInfectedAnimalsT = 0;
    numberOfPurchasesOfInfectedAnimalsL = 0;
    numberOfPurchasesOfInfectedAnimalsIs = 0;
    numberOfSalesOfInfectedAnimalsT = 0;
    numberOfSalesOfInfectedAnimalsL = 0;
    numberOfSalesOfInfectedAnimalsIs = 0;
    prevalenceMaxForTheLastPeriodOfInfection = 0;
    cumulativeNumberOfIcForTheLastPeriodOfInfection = 0;
    numberOfWeeksWithInfectedAnimalsForTheLastPeriodOfInfection = 0;
    typeOfFirstAnimalPurchase = 0;
    
    numberOfInfectedAnimalsPurchased = 0;
    
    anExtinctionOccurred = false;
    
//    vNbBirthsPerYear.resize(Parameters::nbYears,0);
//    vNbBirthsPerYearPerAgeGroup.resize(Parameters::nbYears*6,0);
//    vNbBirthsPerAgeGroup.resize(6,0);
    
    birthFake = 0;
    birthTot = 0;
    
    numberOfAnimalInfectedSendWhenNotInitiallyInfected = 0;
    numberOfAnimalInfectedSendToMetapopWhenNotInitiallyInfected = 0;
    
    vSummaryStatistics.resize(Parameters::simutime,-1);

    vSummaryStatistics_N.resize(Parameters::simutime,-1);
    vSummaryStatistics_N_pos.resize(Parameters::simutime,-1);
//    vSummaryStatistics_N_neg.resize(Parameters::simutime,-1);
    vSummaryStatistics_Nech.resize(Parameters::simutime,-1);
    vSummaryStatistics_Nech_pos.resize(Parameters::simutime,-1);
//    vSummaryStatistics_Nech_neg.resize(Parameters::simutime,-1);
    
//    vSummaryStatistics_N;
//    vSummaryStatistics_N_pos;
//    vSummaryStatistics_N_neg;
//    vSummaryStatistics_N_ech;
//    vSummaryStatistics_Nech_pos;
//    vSummaryStatistics_Nech_neg;
}


// Member functions ====================================================
void DairyHerd::Initialize(){
    // Initialization
    iHealthStateCompartmentsContainer.iCompartmentSR.present.assign(Parameters::mHerdHeadcounts[iParametersSpecificToEachHerd.farmId].begin(), Parameters::mHerdHeadcounts[iParametersSpecificToEachHerd.farmId].end());
    
    // First update for HerdDynamic, SummaryDynamic and SummaryInfection
    iHealthStateCompartmentsContainer.iCompartmentSR.update(0);// iHealthStateCompartmentsContainer.iCompartmentT.update(0); iHealthStateCompartmentsContainer.iCompartmentL.update(0); iHealthStateCompartmentsContainer.iCompartmentIs.update(0); iHealthStateCompartmentsContainer.iCompartmentIc.update(0);
    iSummaryDynamic.update(0, iHealthStateCompartmentsContainer);
}


void DairyHerd::InitializeInfectionInHerd(const ParametersSpecificToEachRepetition& ParamRep, int initTime){
    int nbYearOfInit = initTime;
//    std::random_device rd;
//    std::mt19937 gen(rd());
    std::mt19937 gen(static_cast<int>(ParamRep.Seed * iParametersSpecificToEachHerd.farmIndex));
    std::uniform_int_distribution<> distUnif(0, static_cast<int>(Parameters::vvvProbaInitInfection[nbYearOfInit-1].size())-1);
    int indexInitSelection;
    
    int numberOfAttempts = 0;
    bool attemptWithSuccess = false;
    
    while (attemptWithSuccess == false & numberOfAttempts < 1000) {
        
        indexInitSelection = distUnif(gen);
        
        std::vector<double> vProbaHealthStatesDistribution(Parameters::vvvProbaInitInfection[nbYearOfInit-1][indexInitSelection][0].begin(), Parameters::vvvProbaInitInfection[nbYearOfInit-1][indexInitSelection][0].end());
        std::vector<double> vProbaSR_Distribution(Parameters::vvvProbaInitInfection[nbYearOfInit-1][indexInitSelection][1].begin(), Parameters::vvvProbaInitInfection[nbYearOfInit-1][indexInitSelection][1].end());
        std::vector<double> vProbaT_Distribution(Parameters::vvvProbaInitInfection[nbYearOfInit-1][indexInitSelection][2].begin(), Parameters::vvvProbaInitInfection[nbYearOfInit-1][indexInitSelection][2].end());
        std::vector<double> vProbaL_Distribution(Parameters::vvvProbaInitInfection[nbYearOfInit-1][indexInitSelection][3].begin(), Parameters::vvvProbaInitInfection[nbYearOfInit-1][indexInitSelection][3].end());
        std::vector<double> vProbaIs_Distribution(Parameters::vvvProbaInitInfection[nbYearOfInit-1][indexInitSelection][4].begin(), Parameters::vvvProbaInitInfection[nbYearOfInit-1][indexInitSelection][4].end());
        std::vector<double> vProbaIc_Distribution(Parameters::vvvProbaInitInfection[nbYearOfInit-1][indexInitSelection][5].begin(), Parameters::vvvProbaInitInfection[nbYearOfInit-1][indexInitSelection][5].end());
        
        
        int nbT = round(static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentSR.total[0] * vProbaHealthStatesDistribution[1]));
        int nbL = round(static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentSR.total[0] * vProbaHealthStatesDistribution[2]));
        int nbIs = round(static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentSR.total[0] * vProbaHealthStatesDistribution[3]));
        int nbIc = round(static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentSR.total[0] * vProbaHealthStatesDistribution[4]));
        
        if(nbT+nbL+nbIs+nbIc > iHealthStateCompartmentsContainer.iCompartmentSR.total[0]){
            nbT = floor(static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentSR.total[0] * vProbaHealthStatesDistribution[1]));
            nbL = floor(static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentSR.total[0] * vProbaHealthStatesDistribution[2]));
            nbIs = floor(static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentSR.total[0] * vProbaHealthStatesDistribution[3]));
            nbIc = floor(static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentSR.total[0] * vProbaHealthStatesDistribution[4]));
        }
        
        if(nbT+nbL+nbIs+nbIc == 0){
            nbT = ceil(static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentSR.total[0] * vProbaHealthStatesDistribution[1]));
            nbL = ceil(static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentSR.total[0] * vProbaHealthStatesDistribution[2]));
            nbIs = ceil(static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentSR.total[0] * vProbaHealthStatesDistribution[3]));
            nbIc = ceil(static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentSR.total[0] * vProbaHealthStatesDistribution[4]));
        }
        
        
        if(nbT+nbL+nbIs+nbIc > iHealthStateCompartmentsContainer.iCompartmentSR.total[0]){
            std::cout << "Problem during initialization of infection in herd!" << std::endl;
        }
        
        if (nbT+nbL+nbIs+nbIc == 0) {
            //        AddingInfectedAnimals(0, Parameters::nbInfectedAnimalsIntroduced);
            std::cout << "\033[1;31mProblem in InitializeInfectionInHerd: nbT+nbL+nbIs+nbIc == 0!\033[1;m" << std::endl;
        }
        
        int gardeFou = 0;
        while (nbT+nbL+nbIs+nbIc > 0) {
            gardeFou ++;
            if (gardeFou > 10000) {
                std::cout << "\033[1;31mgarde-fou - InitializeInfectionInHerd\033[1;m" << std::endl;
                std::exit(EXIT_FAILURE);
            }
            
            if (nbT > 0) {
                std::vector<double> vSRZeroOne = U_functions::getZeroAndOneFromVector(iHealthStateCompartmentsContainer.iCompartmentSR.past);
                std::vector<double> vProb = U_functions::VectorMultiplication(vProbaT_Distribution, vSRZeroOne);
                
                //            std::vector<unsigned short int> distributionOfT = U_functions::multinomialDistribution_shortInt(ParamRep.randomGenerator, 1, vProb);
                //            iHealthStateCompartmentsContainer.iCompartmentT.past = iHealthStateCompartmentsContainer.iCompartmentT.past + distributionOfT;
                //            iHealthStateCompartmentsContainer.iCompartmentSR.past = iHealthStateCompartmentsContainer.iCompartmentSR.past - distributionOfT;
                //            nbT--;
                if (std::accumulate(vProb.begin(), vProb.end(), 0.0) == 0) {
                    std::vector<double> vProbBuffer = U_functions::getProportionsFromHeadcount(iHealthStateCompartmentsContainer.iCompartmentSR.past);
                    std::vector<double> vOne; vOne.resize(Parameters::ageFirstCalving, 1);
                    std::vector<double> vZero; vZero.resize(Parameters::nbStage-Parameters::ageFirstCalving, 0);
                    std::vector<double> vZeroOneType;
                    vZeroOneType.insert(vZeroOneType.end(), vOne.begin(), vOne.end());
                    vZeroOneType.insert(vZeroOneType.end(), vZero.begin(), vZero.end());
                    assert(vZeroOneType.size() == Parameters::nbStage);
                    std::vector<double> vProb2 = U_functions::VectorMultiplication(vProbBuffer, vZeroOneType);
                    
                    if (std::accumulate(vProb2.begin(), vProb2.end(), 0.0) > 0) {
                        std::vector<unsigned short int> distributionOfT = U_functions::multinomialDistribution_shortInt(ParamRep.randomGenerator, 1, vProb2);
                        assert(std::accumulate(distributionOfT.begin(), distributionOfT.end(), 0) == 1);
                        iHealthStateCompartmentsContainer.iCompartmentT.past = iHealthStateCompartmentsContainer.iCompartmentT.past + distributionOfT;
                        iHealthStateCompartmentsContainer.iCompartmentSR.past = iHealthStateCompartmentsContainer.iCompartmentSR.past - distributionOfT;
                    }
                    nbT--;
                } else {
                    std::vector<unsigned short int> distributionOfT = U_functions::multinomialDistribution_shortInt(ParamRep.randomGenerator, 1, vProb);
                    assert(std::accumulate(distributionOfT.begin(), distributionOfT.end(), 0) == 1);
                    iHealthStateCompartmentsContainer.iCompartmentT.past = iHealthStateCompartmentsContainer.iCompartmentT.past + distributionOfT;
                    iHealthStateCompartmentsContainer.iCompartmentSR.past = iHealthStateCompartmentsContainer.iCompartmentSR.past - distributionOfT;
                    nbT--;
                }
            }
            
            if (nbL > 0) {
                std::vector<double> vSRZeroOne = U_functions::getZeroAndOneFromVector(iHealthStateCompartmentsContainer.iCompartmentSR.past);
                std::vector<double> vProb = U_functions::VectorMultiplication(vProbaL_Distribution, vSRZeroOne);
                
                //            std::vector<unsigned short int> distributionOfL = U_functions::multinomialDistribution_shortInt(ParamRep.randomGenerator, 1, vProb);
                //            iHealthStateCompartmentsContainer.iCompartmentL.past = iHealthStateCompartmentsContainer.iCompartmentL.past + distributionOfL;
                //            iHealthStateCompartmentsContainer.iCompartmentSR.past = iHealthStateCompartmentsContainer.iCompartmentSR.past - distributionOfL;
                //            nbL--;
                if (std::accumulate(vProb.begin(), vProb.end(), 0.0) == 0) {
                    std::vector<double> vProbBuffer = U_functions::getProportionsFromHeadcount(iHealthStateCompartmentsContainer.iCompartmentSR.past);
                    std::vector<double> vOne; vOne.resize(1, 1);
                    std::vector<double> vZero; vZero.resize(Parameters::nbStage-1, 0);
                    std::vector<double> vZeroOneType;
                    vZeroOneType.insert(vZeroOneType.end(), vOne.begin(), vOne.end());
                    vZeroOneType.insert(vZeroOneType.end(), vZero.begin(), vZero.end());
                    assert(vZeroOneType.size() == Parameters::nbStage);
                    std::vector<double> vProb2 = U_functions::VectorMultiplication(vProbBuffer, vZeroOneType);
                    if (std::accumulate(vProb2.begin(), vProb2.end(), 0.0) > 0) {
                        std::vector<unsigned short int> distributionOfL = U_functions::multinomialDistribution_shortInt(ParamRep.randomGenerator, 1, vProb2);
                        assert(std::accumulate(distributionOfL.begin(), distributionOfL.end(), 0) == 1);
                        iHealthStateCompartmentsContainer.iCompartmentL.past = iHealthStateCompartmentsContainer.iCompartmentL.past + distributionOfL;
                        iHealthStateCompartmentsContainer.iCompartmentSR.past = iHealthStateCompartmentsContainer.iCompartmentSR.past - distributionOfL;
                    }
                    nbL--;
                } else {
                    std::vector<unsigned short int> distributionOfL = U_functions::multinomialDistribution_shortInt(ParamRep.randomGenerator, 1, vProb);
                    assert(std::accumulate(distributionOfL.begin(), distributionOfL.end(), 0) == 1);
                    iHealthStateCompartmentsContainer.iCompartmentL.past = iHealthStateCompartmentsContainer.iCompartmentL.past + distributionOfL;
                    iHealthStateCompartmentsContainer.iCompartmentSR.past = iHealthStateCompartmentsContainer.iCompartmentSR.past - distributionOfL;
                    nbL--;
                }
            }
            
            if (nbIs > 0) {
                std::vector<double> vSRZeroOne = U_functions::getZeroAndOneFromVector(iHealthStateCompartmentsContainer.iCompartmentSR.past);
                std::vector<double> vProb = U_functions::VectorMultiplication(vProbaIs_Distribution, vSRZeroOne);
                
                //            std::vector<unsigned short int> distributionOfIs = U_functions::multinomialDistribution_shortInt(ParamRep.randomGenerator, 1, vProb);
                //            iHealthStateCompartmentsContainer.iCompartmentIs.past = iHealthStateCompartmentsContainer.iCompartmentIs.past + distributionOfIs;
                //            iHealthStateCompartmentsContainer.iCompartmentSR.past = iHealthStateCompartmentsContainer.iCompartmentSR.past - distributionOfIs;
                //            nbIs--;
                if (std::accumulate(vProb.begin(), vProb.end(), 0.0) == 0) {
                    std::vector<double> vProbBuffer = U_functions::getProportionsFromHeadcount(iHealthStateCompartmentsContainer.iCompartmentSR.past);
                    std::vector<double> vZero; vZero.resize(Parameters::ageYheifer+1, 0);
                    std::vector<double> vOne; vOne.resize(Parameters::nbStage-Parameters::ageYheifer-1, 1);
                    std::vector<double> vZeroOneType;
                    vZeroOneType.insert(vZeroOneType.end(), vZero.begin(), vZero.end());
                    vZeroOneType.insert(vZeroOneType.end(), vOne.begin(), vOne.end());
                    assert(vZeroOneType.size() == Parameters::nbStage);
                    std::vector<double> vProb2 = U_functions::VectorMultiplication(vProbBuffer, vZeroOneType);
                    
                    if (std::accumulate(vProb2.begin(), vProb2.end(), 0.0) > 0) {
                        std::vector<unsigned short int> distributionOfIs = U_functions::multinomialDistribution_shortInt(ParamRep.randomGenerator, 1, vProb2);
                        assert(std::accumulate(distributionOfIs.begin(), distributionOfIs.end(), 0) == 1);
                        iHealthStateCompartmentsContainer.iCompartmentIs.past = iHealthStateCompartmentsContainer.iCompartmentIs.past + distributionOfIs;
                        iHealthStateCompartmentsContainer.iCompartmentSR.past = iHealthStateCompartmentsContainer.iCompartmentSR.past - distributionOfIs;
                    }
                    nbIs--;
                } else {
                    std::vector<unsigned short int> distributionOfIs = U_functions::multinomialDistribution_shortInt(ParamRep.randomGenerator, 1, vProb);
                    assert(std::accumulate(distributionOfIs.begin(), distributionOfIs.end(), 0) == 1);
                    iHealthStateCompartmentsContainer.iCompartmentIs.past = iHealthStateCompartmentsContainer.iCompartmentIs.past + distributionOfIs;
                    iHealthStateCompartmentsContainer.iCompartmentSR.past = iHealthStateCompartmentsContainer.iCompartmentSR.past - distributionOfIs;
                    nbIs--;
                }
            }
            
            if (nbIc > 0) {
                std::vector<double> vSRZeroOne = U_functions::getZeroAndOneFromVector(iHealthStateCompartmentsContainer.iCompartmentSR.past);
                std::vector<double> vProb = U_functions::VectorMultiplication(vProbaIc_Distribution, vSRZeroOne);
                
                //            std::vector<unsigned short int> distributionOfIc = U_functions::multinomialDistribution_shortInt(ParamRep.randomGenerator, 1, vProb);
                //            iHealthStateCompartmentsContainer.iCompartmentIc.past = iHealthStateCompartmentsContainer.iCompartmentIc.past + distributionOfIc;
                //            iHealthStateCompartmentsContainer.iCompartmentSR.past = iHealthStateCompartmentsContainer.iCompartmentSR.past - distributionOfIc;
                //            nbIc--;
                if (std::accumulate(vProb.begin(), vProb.end(), 0.0) == 0) {
                    std::vector<double> vProbBuffer = U_functions::getProportionsFromHeadcount(iHealthStateCompartmentsContainer.iCompartmentSR.past);
                    std::vector<double> vZero; vZero.resize(Parameters::ageYheifer+2, 0);
                    std::vector<double> vOne; vOne.resize(Parameters::nbStage-Parameters::ageYheifer-2, 1);
                    std::vector<double> vZeroOneType;
                    vZeroOneType.insert(vZeroOneType.end(), vZero.begin(), vZero.end());
                    vZeroOneType.insert(vZeroOneType.end(), vOne.begin(), vOne.end());
                    assert(vZeroOneType.size() == Parameters::nbStage);
                    std::vector<double> vProb2 = U_functions::VectorMultiplication(vProbBuffer, vZeroOneType);
                    
                    if (std::accumulate(vProb2.begin(), vProb2.end(), 0.0) > 0) {
                        std::vector<unsigned short int> distributionOfIc = U_functions::multinomialDistribution_shortInt(ParamRep.randomGenerator, 1, vProb2);
                        assert(std::accumulate(distributionOfIc.begin(), distributionOfIc.end(), 0) == 1);
                        iHealthStateCompartmentsContainer.iCompartmentIc.past = iHealthStateCompartmentsContainer.iCompartmentIc.past + distributionOfIc;
                        iHealthStateCompartmentsContainer.iCompartmentSR.past = iHealthStateCompartmentsContainer.iCompartmentSR.past - distributionOfIc;
                    }
                    nbIc--;
                } else {
                    std::vector<unsigned short int> distributionOfIc = U_functions::multinomialDistribution_shortInt(ParamRep.randomGenerator, 1, vProb);
                    assert(std::accumulate(distributionOfIc.begin(), distributionOfIc.end(), 0) == 1);
                    iHealthStateCompartmentsContainer.iCompartmentIc.past = iHealthStateCompartmentsContainer.iCompartmentIc.past + distributionOfIc;
                    iHealthStateCompartmentsContainer.iCompartmentSR.past = iHealthStateCompartmentsContainer.iCompartmentSR.past - distributionOfIc;
                    nbIc--;
                }
            }
            
        }
        
        numberOfAttempts ++;
        
        int numberOfInfectedAnimals = 0;
        numberOfInfectedAnimals += std::accumulate(iHealthStateCompartmentsContainer.iCompartmentT.past.begin(), iHealthStateCompartmentsContainer.iCompartmentT.past.end(), 0);
        numberOfInfectedAnimals += std::accumulate(iHealthStateCompartmentsContainer.iCompartmentL.past.begin(), iHealthStateCompartmentsContainer.iCompartmentL.past.end(), 0);
        numberOfInfectedAnimals += std::accumulate(iHealthStateCompartmentsContainer.iCompartmentIs.past.begin(), iHealthStateCompartmentsContainer.iCompartmentIs.past.end(), 0);
        numberOfInfectedAnimals += std::accumulate(iHealthStateCompartmentsContainer.iCompartmentIc.past.begin(), iHealthStateCompartmentsContainer.iCompartmentIc.past.end(), 0);
        
        if (numberOfInfectedAnimals > 0) {
            attemptWithSuccess = true;
        }
        
    }
    
    
    if (attemptWithSuccess == false & numberOfAttempts >= 100) {
        iHealthStateCompartmentsContainer.iCompartmentIs.past[Parameters::ageFirstCalving] += Parameters::nbInfectedAnimalsIntroduced;
        if (iHealthStateCompartmentsContainer.iCompartmentSR.past[Parameters::ageFirstCalving]>=Parameters::nbInfectedAnimalsIntroduced) {
            iHealthStateCompartmentsContainer.iCompartmentSR.past[Parameters::ageFirstCalving] -= Parameters::nbInfectedAnimalsIntroduced;
        } else {
            iHealthStateCompartmentsContainer.iCompartmentSR.past[Parameters::ageFirstCalving] = 0;
        }
        
        std::cout << "warning: problem when initialize infection! No possible to dispatch animals in herd: " << iParametersSpecificToEachHerd.farmId << std::endl;
    }
    
    
    
    int numberOfIc = std::accumulate(iHealthStateCompartmentsContainer.iCompartmentIc.past.begin(), iHealthStateCompartmentsContainer.iCompartmentIc.past.end(), 0);
    cumulativeNumberOfIc = numberOfIc;
    cumulativeNumberOfIcForTheLastPeriodOfInfection = numberOfIc;
    
    iParametersSpecificToEachHerd.initializeParametersOfControlMeasures(numberOfIc);
    
}


bool DairyHerd::InitializeInfectionInHerdForABC(const ParametersSpecificToEachRepetition& ParamRep){
    // variable used as an indicator if the herd has really been infected or not
    bool bHerdHasBeenInfected = false;

    //
    double observedPrevalence = Parameters::mFarmsCharacteristics[iParametersSpecificToEachHerd.farmId].prevInit;
    double truePrevalence;
    int prevLevelOfInit = 0.0;

    // following the type of simulation, set the prevalence level
    if (Parameters::ABCrejection == true | Parameters::ABCsmc == true) {
        truePrevalence = (observedPrevalence + (Parameters::abcTestSpecificity - 1.)) / (ParamRep.abcTestSensitivity + Parameters::abcTestSpecificity - 1.);
        truePrevalence = std::min(truePrevalence, 1.0);
        prevLevelOfInit = static_cast<int>(round(truePrevalence * 100));
        prevLevelOfInit = std::max(prevLevelOfInit, 1);
        prevLevelOfInit = std::min(prevLevelOfInit, Parameters::maxPrevLevel);
    } else {//if ((Parameters::ABCcompleteTrajectories == true) | (Parameters::ABCrejection == false & Parameters::ABCsmc == false)) {
        if (Parameters::typeOfWithinHerdPrevalenceDistribution == "gaussian") {
            truePrevalence = gsl_ran_gaussian(ParamRep.randomGenerator, Parameters::sigmaDistributionPrevIntra) + Parameters::muDistributionPrevIntra;
            prevLevelOfInit = static_cast<int>(round(truePrevalence * 100));
            while ((truePrevalence < 0) | (prevLevelOfInit > Parameters::maxPrevLevel)) {
                truePrevalence = gsl_ran_gaussian(ParamRep.randomGenerator, Parameters::sigmaDistributionPrevIntra) + Parameters::muDistributionPrevIntra;
                prevLevelOfInit = static_cast<int>(round(truePrevalence * 100));
            }
        } else { // Parameters::typeOfWithinHerdPrevalenceDistribution == "beta"
            truePrevalence = gsl_ran_beta(ParamRep.randomGenerator, Parameters::alphaBetaDistributionPrevIntra, Parameters::betaBetaDistributionPrevIntra);
            prevLevelOfInit = static_cast<int>(std::floor(truePrevalence * 100));
            if (prevLevelOfInit > Parameters::maxPrevLevel) {
                prevLevelOfInit = Parameters::maxPrevLevel;
            }
        }
    }
    
    int prevLevelOfInitIndex = prevLevelOfInit;

    if ((prevLevelOfInit < 0) | (prevLevelOfInit > Parameters::maxPrevLevel)) {
        std::cout << "prevLevelOfInit: " << prevLevelOfInit << " - prevLevelOfInitIndex: " << prevLevelOfInitIndex << std::endl;
    }


    ////////////////////////////////////////////////////////////////////////////
    // get whole prevalence and compute the number of infected animals
    double Ntot = static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentSR.total[0]);
    int nbInfectedAnimals = 0;
    int indexInitSelection;

    if (prevLevelOfInit>0) {
        // random sampling of one scenario for the prevalence chosen
        std::mt19937 gen(static_cast<int>(ParamRep.Seed * iParametersSpecificToEachHerd.farmIndex));
        std::uniform_int_distribution<> distUnif(0, static_cast<int>(Parameters::vvvPrevalenceLevelDistribution[prevLevelOfInitIndex].size())-1);
        indexInitSelection = distUnif(gen);
        // get the prevalences in health status
        // std::cout << prevLevelOfInitIndex << " | " << indexInitSelection << std::endl;
        std::vector<double> vProbaHealthStatesDistribution(Parameters::vvvPrevalenceLevelDistribution[prevLevelOfInitIndex][indexInitSelection][0].begin(), Parameters::vvvPrevalenceLevelDistribution[prevLevelOfInitIndex][indexInitSelection][0].end());
        // compute the number of infected animals, based on the whole prevalence
        nbInfectedAnimals = round( Ntot * (1.-vProbaHealthStatesDistribution[0]) );
    }


    ////////////////////////////////////////////////////////////////////////////
    // dispatch infected animals in health status and age group
    if (nbInfectedAnimals>0) {
        bHerdHasBeenInfected = true;

        ////////////////////////////////////////////////////////////////////////
        // sampling, at random, health status and age group using the distributions of animals by health status and age group
        for (int i = 0; i < nbInfectedAnimals; i++) {
            // get distributions by health status and age group for infected animals
            std::vector<double> vProbaT_Distribution(Parameters::vvvPrevalenceLevelDistribution[prevLevelOfInitIndex][indexInitSelection][2].begin(), Parameters::vvvPrevalenceLevelDistribution[prevLevelOfInitIndex][indexInitSelection][2].end());
            std::vector<double> vProbaL_Distribution(Parameters::vvvPrevalenceLevelDistribution[prevLevelOfInitIndex][indexInitSelection][3].begin(), Parameters::vvvPrevalenceLevelDistribution[prevLevelOfInitIndex][indexInitSelection][3].end());
            std::vector<double> vProbaIs_Distribution(Parameters::vvvPrevalenceLevelDistribution[prevLevelOfInitIndex][indexInitSelection][4].begin(), Parameters::vvvPrevalenceLevelDistribution[prevLevelOfInitIndex][indexInitSelection][4].end());
            std::vector<double> vProbaIc_Distribution(Parameters::vvvPrevalenceLevelDistribution[prevLevelOfInitIndex][indexInitSelection][5].begin(), Parameters::vvvPrevalenceLevelDistribution[prevLevelOfInitIndex][indexInitSelection][5].end());

            // create a vector of animals presence in age group: 0 if no animal and 1 if at least one animal
            std::vector<double> v_indicator_of_presence_in_the_age_groups = U_functions::getZeroAndOneFromVector(iHealthStateCompartmentsContainer.iCompartmentSR.past);

            // correction of the distributions by health status and age group for infected animals
            vProbaT_Distribution = U_functions::VectorMultiplication(vProbaT_Distribution, v_indicator_of_presence_in_the_age_groups);
            vProbaL_Distribution = U_functions::VectorMultiplication(vProbaL_Distribution, v_indicator_of_presence_in_the_age_groups);
            vProbaIs_Distribution = U_functions::VectorMultiplication(vProbaIs_Distribution, v_indicator_of_presence_in_the_age_groups);
            vProbaIc_Distribution = U_functions::VectorMultiplication(vProbaIc_Distribution, v_indicator_of_presence_in_the_age_groups);

            // build of the whole vector of probability (health status x age group)
            std::vector<double> vProbaAll;
            vProbaAll.insert(std::end(vProbaAll), std::begin(vProbaT_Distribution), std::end(vProbaT_Distribution));
            vProbaAll.insert(std::end(vProbaAll), std::begin(vProbaL_Distribution), std::end(vProbaL_Distribution));
            vProbaAll.insert(std::end(vProbaAll), std::begin(vProbaIs_Distribution), std::end(vProbaIs_Distribution));
            vProbaAll.insert(std::end(vProbaAll), std::begin(vProbaIc_Distribution), std::end(vProbaIc_Distribution));


            ////////////////////////////////////////////////////////////////////
            // check if the vector contain at least one item > 0, (condition to use the multinomial distribution)
            // if the probability vector contains only values = 0, apply a correction (+minProbaCorrection)
            if (std::accumulate(vProbaAll.begin(), vProbaAll.end(), 0.0) == 0) {
                // std::cout << "!p0 | " << prevLevelOfInitIndex << " (" << indexInitSelection << ") | " << Ntot << " - " << nbInfectedAnimals << std::endl;
                // get the mean scenario in order to keep an adequat distribution
                vProbaT_Distribution.clear(); vProbaT_Distribution.insert(vProbaT_Distribution.end(), Parameters::vvPrevalenceLevelAverageDistribution[prevLevelOfInitIndex][2].begin(), Parameters::vvPrevalenceLevelAverageDistribution[prevLevelOfInitIndex][2].end());
                vProbaL_Distribution.clear(); vProbaL_Distribution.insert(vProbaL_Distribution.end(), Parameters::vvPrevalenceLevelAverageDistribution[prevLevelOfInitIndex][3].begin(), Parameters::vvPrevalenceLevelAverageDistribution[prevLevelOfInitIndex][3].end());
                vProbaIs_Distribution.clear(); vProbaIs_Distribution.insert(vProbaIs_Distribution.end(), Parameters::vvPrevalenceLevelAverageDistribution[prevLevelOfInitIndex][4].begin(), Parameters::vvPrevalenceLevelAverageDistribution[prevLevelOfInitIndex][4].end());
                vProbaIc_Distribution.clear(); vProbaIc_Distribution.insert(vProbaIc_Distribution.end(), Parameters::vvPrevalenceLevelAverageDistribution[prevLevelOfInitIndex][5].begin(), Parameters::vvPrevalenceLevelAverageDistribution[prevLevelOfInitIndex][5].end());
                // in order to avoid creation of a probability vector (vProbaAll) with only values equal to zero (in the case where there is not animal in tha age group compatible with health distribution), a minimum is set for the probability: 1e-9
                // T
                std::vector<double> minProbaCorrection_T1; minProbaCorrection_T1.resize(Parameters::durSusceptible, std::pow(10, -9)); // 0 to Parameters::durSusceptible
                std::vector<double> minProbaCorrection_T2; minProbaCorrection_T2.resize(Parameters::nbStage-Parameters::durSusceptible, 0);
                std::vector<double> minProbaCorrection_T;
                minProbaCorrection_T.insert(std::end(minProbaCorrection_T), std::begin(minProbaCorrection_T1), std::end(minProbaCorrection_T1));
                minProbaCorrection_T.insert(std::end(minProbaCorrection_T), std::begin(minProbaCorrection_T2), std::end(minProbaCorrection_T2));
//                assert(minProbaCorrection_T.size() == 135);
                vProbaT_Distribution = vProbaT_Distribution + minProbaCorrection_T;
                // L
                std::vector<double> minProbaCorrection_L; minProbaCorrection_L.resize(Parameters::nbStage,std::pow(10, -9)); // 0 to 135 weeks
//                assert(minProbaCorrection_L.size() == 135);
                vProbaL_Distribution = vProbaL_Distribution + minProbaCorrection_L;
                // Is and Ic
                std::vector<double> minProbaCorrection_IsIc1; minProbaCorrection_IsIc1.resize(Parameters::ageSign, 0);
                std::vector<double> minProbaCorrection_IsIc2; minProbaCorrection_IsIc2.resize(Parameters::nbStage-Parameters::ageSign, std::pow(10, -9)); // ageSign to 135 weeks
                std::vector<double> minProbaCorrection_IsIc;
                minProbaCorrection_IsIc.insert(std::end(minProbaCorrection_IsIc), std::begin(minProbaCorrection_IsIc1), std::end(minProbaCorrection_IsIc1));
                minProbaCorrection_IsIc.insert(std::end(minProbaCorrection_IsIc), std::begin(minProbaCorrection_IsIc2), std::end(minProbaCorrection_IsIc2));
//                assert(minProbaCorrection_IsIc.size() == 135);
                vProbaIs_Distribution = vProbaIs_Distribution + minProbaCorrection_IsIc;
                vProbaIc_Distribution = vProbaIc_Distribution + minProbaCorrection_IsIc;
                //
                // correction of the distributions by health status and age group for infected animals
                vProbaT_Distribution = U_functions::VectorMultiplication(vProbaT_Distribution, v_indicator_of_presence_in_the_age_groups);
                vProbaL_Distribution = U_functions::VectorMultiplication(vProbaL_Distribution, v_indicator_of_presence_in_the_age_groups);
                vProbaIs_Distribution = U_functions::VectorMultiplication(vProbaIs_Distribution, v_indicator_of_presence_in_the_age_groups);
                vProbaIc_Distribution = U_functions::VectorMultiplication(vProbaIc_Distribution, v_indicator_of_presence_in_the_age_groups);
                // rebuild the whole vector of probability (health status x age group)
                vProbaAll.clear();
                vProbaAll.insert(std::end(vProbaAll), std::begin(vProbaT_Distribution), std::end(vProbaT_Distribution));
                vProbaAll.insert(std::end(vProbaAll), std::begin(vProbaL_Distribution), std::end(vProbaL_Distribution));
                vProbaAll.insert(std::end(vProbaAll), std::begin(vProbaIs_Distribution), std::end(vProbaIs_Distribution));
                vProbaAll.insert(std::end(vProbaAll), std::begin(vProbaIc_Distribution), std::end(vProbaIc_Distribution));
            }


            ////////////////////////////////////////////////////////////////////
            // sample with the multinomial distribution
            std::vector<unsigned short int> ageAndHealthStateOfInfectedAnimal = U_functions::multinomialDistribution_shortInt(ParamRep.randomGenerator, 1, vProbaAll);
            // check if their is one item = 1 (for debug)
            assert(std::accumulate(ageAndHealthStateOfInfectedAnimal.begin(), ageAndHealthStateOfInfectedAnimal.end(), 0) == 1);
            // find the position of the animal
            auto iter = std::find(std::begin(ageAndHealthStateOfInfectedAnimal), std::end(ageAndHealthStateOfInfectedAnimal), 1);
            int position = static_cast<int>( std::distance(ageAndHealthStateOfInfectedAnimal.begin(), iter) );
            // compute the health status
            int healthState = position / Parameters::nbStage;
            // compute the age
            int age = position % Parameters::nbStage; // int age = position - (healthState*Parameters::nbStage);
            // std::cout << std::endl << position << " | " << healthState << " | " << age << std::endl;


            ////////////////////////////////////////////////////////////////////
            // Transfer
            // remove the animal from the SR group
            iHealthStateCompartmentsContainer.iCompartmentSR.past[age] --;
            // add the animal in the corresponding health status
            switch (healthState) {
                case 0: iHealthStateCompartmentsContainer.iCompartmentT.past[age]++; break; // T
                case 1: iHealthStateCompartmentsContainer.iCompartmentL.past[age]++; break; // L
                case 2: iHealthStateCompartmentsContainer.iCompartmentIs.past[age]++; break; // Is
                case 3: iHealthStateCompartmentsContainer.iCompartmentIc.past[age]++; break; // Ic
                default:  break;
            }
        }


        ////////////////////////////////////////////////////////////////////////
        // init. of env. here:
        // Summary of headcounts
        iHealthStateCompartmentsContainer.iCompartmentSR.UpdateAfterAddingInfectiousAnimals(0);
        iHealthStateCompartmentsContainer.iCompartmentT.UpdateAfterAddingInfectiousAnimals(0);
        iHealthStateCompartmentsContainer.iCompartmentL.UpdateAfterAddingInfectiousAnimals(0);
        iHealthStateCompartmentsContainer.iCompartmentIs.UpdateAfterAddingInfectiousAnimals(0);
        iHealthStateCompartmentsContainer.iCompartmentIc.UpdateAfterAddingInfectiousAnimals(0);

        // compute size of group relative to env.
        double nb_calvesUnweaned = iHealthStateCompartmentsContainer.iCompartmentT.calvesUnweaned[0] + iHealthStateCompartmentsContainer.iCompartmentL.calvesUnweaned[0] + iHealthStateCompartmentsContainer.iCompartmentIs.calvesUnweaned[0] + iHealthStateCompartmentsContainer.iCompartmentIc.calvesUnweaned[0];
        double nb_calvesWeanedInOut = iHealthStateCompartmentsContainer.iCompartmentT.calvesWeanedIn[0] + iHealthStateCompartmentsContainer.iCompartmentT.calvesWeanedOut[0] + iHealthStateCompartmentsContainer.iCompartmentL.calvesWeanedIn[0] + iHealthStateCompartmentsContainer.iCompartmentL.calvesWeanedOut[0] + iHealthStateCompartmentsContainer.iCompartmentIs.calvesWeanedIn[0] + iHealthStateCompartmentsContainer.iCompartmentIs.calvesWeanedOut[0] + iHealthStateCompartmentsContainer.iCompartmentIc.calvesWeanedIn[0] + iHealthStateCompartmentsContainer.iCompartmentIc.calvesWeanedOut[0];
        double nb_yHeifer = iHealthStateCompartmentsContainer.iCompartmentT.yHeifer[0] + iHealthStateCompartmentsContainer.iCompartmentL.yHeifer[0] + iHealthStateCompartmentsContainer.iCompartmentIs.yHeifer[0] + iHealthStateCompartmentsContainer.iCompartmentIc.yHeifer[0];
        double nb_heifer = iHealthStateCompartmentsContainer.iCompartmentT.heifer[0] + iHealthStateCompartmentsContainer.iCompartmentL.heifer[0] + iHealthStateCompartmentsContainer.iCompartmentIs.heifer[0] + iHealthStateCompartmentsContainer.iCompartmentIc.heifer[0];
        double nb_lactAll = iHealthStateCompartmentsContainer.iCompartmentT.lactAll[0] + iHealthStateCompartmentsContainer.iCompartmentL.lactAll[0] + iHealthStateCompartmentsContainer.iCompartmentIs.lactAll[0] + iHealthStateCompartmentsContainer.iCompartmentIc.lactAll[0];

        // get amount of bacteria per animal for each env.
        std::vector<double> vBactQttyInEnv(Parameters::vvvPrevalenceLevelDistribution[prevLevelOfInitIndex][indexInitSelection][6].begin(), Parameters::vvvPrevalenceLevelDistribution[prevLevelOfInitIndex][indexInitSelection][6].end());

        // set the amount of bacteria in the env.
        Environments.int_1[0] = vBactQttyInEnv[0]*nb_calvesUnweaned;
        Environments.int_2[0] = vBactQttyInEnv[1]*nb_calvesWeanedInOut;
        Environments.int_3[0] = vBactQttyInEnv[2]*nb_yHeifer;
        Environments.int_4[0] = vBactQttyInEnv[3]*nb_heifer;
        Environments.int_5[0] = vBactQttyInEnv[4]*nb_lactAll;

        // compute amount of bacteria in the general env.
        Environments.global[0] = Environments.int_1[0] + Environments.int_2[0] + Environments.int_3[0] + Environments.int_4[0] + Environments.int_5[0];

        // no bacteria in env. ext at this period of the year
        Environments.ext_1[0] = 0;
        Environments.ext_2[0] = 0;
        Environments.ext_3[0] = 0;
        Environments.ext[0] = Environments.ext_1[0] + Environments.ext_2[0] + Environments.ext_3[0];

        // amount of bacteria in milk and colostrum compute each day
        // Environments.milk[0] = 0;
        // Environments.colostrum[0] = 0;


        ////////////////////////////////////////////////////////////////////////
        // compute the number of Ic and set variables using it
        int numberOfIc = std::accumulate(iHealthStateCompartmentsContainer.iCompartmentIc.past.begin(), iHealthStateCompartmentsContainer.iCompartmentIc.past.end(), 0);
        cumulativeNumberOfIc = numberOfIc;
        cumulativeNumberOfIcForTheLastPeriodOfInfection = numberOfIc;
        iParametersSpecificToEachHerd.initializeParametersOfControlMeasures(numberOfIc);
    }

    return bHerdHasBeenInfected;
}


void DairyHerd::Compute(const int& timeStep, const ParametersSpecificToEachRepetition& ParamRep, int& weeknumber){
    
    // Update parameters specific to the herd
    iParametersSpecificToEachHerd.Update(ParamRep, timeStep, cumulativeNumberOfIc);
    
    // first Case Date
    if (!hasAlreadyBeenInfected) {
        if (persistenceIndicator) { //if (iSummaryDynamic.infected[timeStep-1] > 0) {
            firstCaseDate = timeStep;
            hasAlreadyBeenInfected = true;
        }
    }
    
    
    // If infection
    if (persistenceIndicator == true) {
        
        // Births
        computeBirthEvents(ParamRep, timeStep);
        
        // Exits
        IcCulled += exit.Update_bis(ParamRep.randomGenerator, iParametersSpecificToEachHerd, timeStep, iHealthStateCompartmentsContainer);
        
        // Update quantity of bacteria shed
        QtyBacteria.update(ParamRep.randomGenerator, timeStep, iHealthStateCompartmentsContainer);
        
        // Update qunatity of bacteria in environment
        Environments.update(ParamRep.randomGenerator, timeStep, weeknumber, iParametersSpecificToEachHerd, QtyBacteria, iHealthStateCompartmentsContainer, iBirthsContainer);
        
        // Age loop for transitions between health states
        for (int age=1; age<Parameters::nbStage; age++) {
            // Transitions between health states : T>L , L>Is , Is>Ic
            Transitions.update(ParamRep.randomGenerator, timeStep, age, iHealthStateCompartmentsContainer);
            Transitions.transfer(timeStep, age, iHealthStateCompartmentsContainer, iSummaryInfection);
        } // End age loop for exits and transitions between health states
        
        // New infections
        NewInfections.updateVariables(timeStep, weeknumber, iHealthStateCompartmentsContainer);
        
        // Age loop for new infections
        for (int age=0; age<Parameters::durSusceptible; age++) {
            NewInfections.update(ParamRep.randomGenerator, timeStep, age, weeknumber, iParametersSpecificToEachHerd, Environments, iHealthStateCompartmentsContainer);
            NewInfections.transfer(timeStep, age, weeknumber, iHealthStateCompartmentsContainer, iSummaryInfection);
            //                NewInfections.update_and_transfer(ParamRep.randomGenerator, tnow, age, ParamRep.weeknumber, Environments, matSR, matT);
        } // End age loop for new infections
        
        cumulativeNumberOfIc += iSummaryInfection.IncidenceIc[timeStep];
        cumulativeNumberOfIcForTheLastPeriodOfInfection += iSummaryInfection.IncidenceIc[timeStep];
        
        iParametersSpecificToEachHerd.updateParametersOfControlMeasures(timeStep, iSummaryInfection.IncidenceIc[timeStep]);
        
        if (dateOfAppearanceOfTheFirstIc == 0) {
            if (iSummaryInfection.IncidenceIc[timeStep]>0) {
                dateOfAppearanceOfTheFirstIc = numberOfWeeksSinceInfected;
            }
        }
        
    } else {
        
        // Births
        computeBirthEvents(ParamRep, timeStep);
        
        // Exits
        exit.UpdateSR(ParamRep.randomGenerator, iParametersSpecificToEachHerd, timeStep, iHealthStateCompartmentsContainer);
        
    }
        
}


void DairyHerd::UpdateSummary(const int& timeStep, MetapopSummary& iMetapopSummary){
    // Summary of headcounts
    iHealthStateCompartmentsContainer.iCompartmentSR.update(timeStep); iHealthStateCompartmentsContainer.iCompartmentT.update(timeStep); iHealthStateCompartmentsContainer.iCompartmentL.update(timeStep); iHealthStateCompartmentsContainer.iCompartmentIs.update(timeStep); iHealthStateCompartmentsContainer.iCompartmentIc.update(timeStep);
    if (Parameters::managementOfPositiveTestedTakeIntoAccount) {
        iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.update(timeStep); iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.update(timeStep); iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.update(timeStep); iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.update(timeStep); iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.update(timeStep);
    }
    iSummaryDynamic.update(timeStep, iHealthStateCompartmentsContainer);
    
    double actualPrevalence = static_cast<double>(iSummaryDynamic.infected[timeStep])/static_cast<double>(iSummaryDynamic.headcount[timeStep]);
    prevalenceMaxForTheLastPeriodOfInfection = std::max(prevalenceMaxForTheLastPeriodOfInfection, actualPrevalence);
    
    // Checking if infection persists
    if (iSummaryDynamic.infected[timeStep] == 0 & (Environments.global[timeStep]+Environments.ext[timeStep])<1) {
        if (persistenceIndicator == true & typeInfectedHerd == 2) {
            anExtinctionOccurred = true;
        }
        
        persistenceIndicator = false;
        if (hasAlreadyBeenInfected) {
            vInfectionDynamic[timeStep] = 1;
        }
        //        else {
        //            vInfectionDynamic[timeStep] = 0;
        //        }
        if (typeInfectedHerd == 1) {
            typeInfectedHerd = 2;
            
            numberOfWeeksSinceInfected = 0;
            numberOfWeeksSinceInfectious = 0;
            dateOfAppearanceOfTheFirstIc = 0;
            
            numberOfPurchasesOfInfectedAnimalsT = 0;
            numberOfPurchasesOfInfectedAnimalsL = 0;
            numberOfPurchasesOfInfectedAnimalsIs = 0;
            numberOfSalesOfInfectedAnimalsT = 0;
            numberOfSalesOfInfectedAnimalsL = 0;
            numberOfSalesOfInfectedAnimalsIs = 0;
            prevalenceMaxForTheLastPeriodOfInfection = 0;
            cumulativeNumberOfIcForTheLastPeriodOfInfection = 0;
            numberOfWeeksWithInfectedAnimalsForTheLastPeriodOfInfection = 0;
            typeOfFirstAnimalPurchase = 0;
        }
    } else {
        if (persistenceIndicator == false) {
            numberOfInfection ++;
            StartDateOfTheLastPeriodOfInfection = timeStep;
        }
        persistenceIndicator = true;
        iSummaryInfection.persistenceUpdate(timeStep);
        vInfectionDynamic[timeStep] = 2;
        
        numberOfWeeksSinceInfected++;
        numberOfWeeksWithInfection++;
        
        if (iSummaryDynamic.infectious[timeStep] > 0 | (Environments.global[timeStep]+Environments.ext[timeStep])>0) {
            numberOfWeeksSinceInfectious++;
        }
        
        if (iSummaryDynamic.infected[timeStep]>0) {
            numberOfWeeksWithInfectedAnimalsForTheLastPeriodOfInfection++;
        }
    }
    
    if (iSummaryDynamic.affected[timeStep] > 0) {
        presenceOfIcAtThisTime = true;
    } else {
        presenceOfIcAtThisTime = false;
    }
    
    updateMetapopSummary(timeStep, iMetapopSummary);
    
}


void DairyHerd::initMetapopSummary(const int& timeStep, MetapopSummary& iMetapopSummary){
    updateMetapopSummary(timeStep, iMetapopSummary);
}


void DairyHerd::updateMetapopSummary(const int& timeStep, MetapopSummary& iMetapopSummary){
    for (int ageAnimal=0; ageAnimal<Parameters::nbStage; ageAnimal++) {
        iMetapopSummary.presentSR[ageAnimal] += static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentSR.past[ageAnimal]);
        iMetapopSummary.presentT[ageAnimal] += static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentT.past[ageAnimal]);
        iMetapopSummary.presentL[ageAnimal] += static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentL.past[ageAnimal]);
        iMetapopSummary.presentIs[ageAnimal] += static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentIs.past[ageAnimal]);
        iMetapopSummary.presentIc[ageAnimal] += static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentIc.past[ageAnimal]);
        if (Parameters::managementOfPositiveTestedTakeIntoAccount) {
            iMetapopSummary.presentSR[ageAnimal] += static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[ageAnimal]);
            iMetapopSummary.presentT[ageAnimal] += static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.past[ageAnimal]);
            iMetapopSummary.presentL[ageAnimal] += static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past[ageAnimal]);
            iMetapopSummary.presentIs[ageAnimal] += static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past[ageAnimal]);
            iMetapopSummary.presentIc[ageAnimal] += static_cast<double>(iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[ageAnimal]);
        }
    }
    
    if (presenceOfIcAtThisTime) {
        iMetapopSummary.presentNbHerdsWithIc ++;
    }
    if (cumulativeNumberOfIc >= 2) {
        iMetapopSummary.presentNbHerdsWithMoreThanTwoIc ++;
    }
    
    if (persistenceIndicator) {
        if (typeInfectedHerd == 1) {
            iMetapopSummary.NbInitiallyInfectedHerds ++;
        } else {
            iMetapopSummary.NbOtherInfectedHerds ++;
        }
    }
    
    iMetapopSummary.updateControlStrategyCensus(timeStep, iParametersSpecificToEachHerd);

    if (iSummaryDynamic.infected[timeStep] > 0) {
        double theWithinHerdPrevalence = static_cast<double>(iSummaryDynamic.infected[timeStep]) / static_cast<double>(iSummaryDynamic.headcount[timeStep]);
//        std::cout << theWithinHerdPrevalence << " - ";
        iMetapopSummary.vPrevalenceIntraHerd[timeStep] += theWithinHerdPrevalence;
        iMetapopSummary.vNbHerdInfected[timeStep] += 1;
    }

}


void DairyHerd::UpdatePastSummary(const int& timeStep){
    // Summary of headcounts
    iHealthStateCompartmentsContainer.iCompartmentSR.PastUpdate(timeStep); iHealthStateCompartmentsContainer.iCompartmentT.PastUpdate(timeStep); iHealthStateCompartmentsContainer.iCompartmentL.PastUpdate(timeStep); iHealthStateCompartmentsContainer.iCompartmentIs.PastUpdate(timeStep); iHealthStateCompartmentsContainer.iCompartmentIc.PastUpdate(timeStep);
    if (Parameters::managementOfPositiveTestedTakeIntoAccount) {
        iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.PastUpdate(timeStep); iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.PastUpdate(timeStep); iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.PastUpdate(timeStep); iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.PastUpdate(timeStep); iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.PastUpdate(timeStep);
    }
    iSummaryDynamic.PastUpdate(timeStep, iHealthStateCompartmentsContainer);
    
    // Checking if infection persists
    if (iSummaryDynamic.infected[timeStep-1] == 0 & (Environments.global[timeStep-1]+Environments.ext[timeStep-1])<1) {
        persistenceIndicator = false;
        if (hasAlreadyBeenInfected) {
            vInfectionDynamic[timeStep-1] = 1;
        }
        else {
            vInfectionDynamic[timeStep-1] = 0;
        }
        if (typeInfectedHerd == 1) {
            typeInfectedHerd = 2;
            
            numberOfWeeksSinceInfected = 0;
            numberOfWeeksSinceInfectious = 0;
            dateOfAppearanceOfTheFirstIc = 0;
            
            numberOfPurchasesOfInfectedAnimalsT = 0;
            numberOfPurchasesOfInfectedAnimalsL = 0;
            numberOfPurchasesOfInfectedAnimalsIs = 0;
            numberOfSalesOfInfectedAnimalsT = 0;
            numberOfSalesOfInfectedAnimalsL = 0;
            numberOfSalesOfInfectedAnimalsIs = 0;
            prevalenceMaxForTheLastPeriodOfInfection = 0;
            cumulativeNumberOfIcForTheLastPeriodOfInfection = 0;
            numberOfWeeksWithInfectedAnimalsForTheLastPeriodOfInfection = 0;
            typeOfFirstAnimalPurchase = 0;
        }
    } else {
        if (persistenceIndicator == false) {
            numberOfInfection ++;
            numberOfWeeksSinceInfected++;
            numberOfWeeksWithInfection++;
            StartDateOfTheLastPeriodOfInfection = timeStep-1;
        }
        persistenceIndicator = true;
        //        iSummaryInfection.persistencePastUpdate(timeStep);
        
        vInfectionDynamic[timeStep-1] = 2;
    }
    
    if (iSummaryDynamic.affected[timeStep-1] > 0) {
        presenceOfIcAtThisTime = true;
    } else {
        presenceOfIcAtThisTime = false;
    }
}


void DairyHerd::AddingInfectedAnimals(const int& timeStep, int numberOfAnimalsIntroduced){
    // Introduction of the infected animal
    iHealthStateCompartmentsContainer.iCompartmentIs.past[Parameters::ageFirstCalving] += numberOfAnimalsIntroduced;
    if (iHealthStateCompartmentsContainer.iCompartmentSR.past[Parameters::ageFirstCalving]>=numberOfAnimalsIntroduced) {
        iHealthStateCompartmentsContainer.iCompartmentSR.past[Parameters::ageFirstCalving] -= numberOfAnimalsIntroduced;
    } else {
        iHealthStateCompartmentsContainer.iCompartmentSR.past[Parameters::ageFirstCalving] = 0;
    }
    // Summary of headcounts
    iHealthStateCompartmentsContainer.iCompartmentIs.UpdateAfterAddingInfectiousAnimals(timeStep); iHealthStateCompartmentsContainer.iCompartmentSR.UpdateAfterAddingInfectiousAnimals(timeStep);
    iSummaryDynamic.update(timeStep, iHealthStateCompartmentsContainer);
    // Persistence indicator
    iSummaryInfection.persistenceUpdate(timeStep);
    persistenceIndicator = true;
}


void DairyHerd::computeBirthEvents(const ParametersSpecificToEachRepetition& ParamRep, const int& timeStep){
    // determine the year
//    int year = floor(static_cast<double>((timeStep-1))/static_cast<double>(Parameters::timestepInOneYear));
    // get the number of births
    int birthSum = iParametersSpecificToEachHerd.vAnnualBirthEvents[timeStep-1];
    
    if (birthSum > 0) {
        int birthsS = 0;
        int birthsT = 0;
        // int birthsS_Female;
        int birthsS_FemaleAlive;
        // int birthsT_Female;
        int birthsT_FemaleAlive;

        std::vector<int> vBirthsSHealthStatesDispatching; vBirthsSHealthStatesDispatching.resize(5, 0);

        if (persistenceIndicator == true) {
            //get headcount in age group
            std::vector<int>  vFirstCalvingCows = {iHealthStateCompartmentsContainer.iCompartmentSR.past[Parameters::ageFirstCalving-1], iHealthStateCompartmentsContainer.iCompartmentT.past[Parameters::ageFirstCalving-1], iHealthStateCompartmentsContainer.iCompartmentL.past[Parameters::ageFirstCalving-1], iHealthStateCompartmentsContainer.iCompartmentIs.past[Parameters::ageFirstCalving-1], iHealthStateCompartmentsContainer.iCompartmentIc.past[Parameters::ageFirstCalving-1]};
            std::vector<int> vAdultCow1 = {iHealthStateCompartmentsContainer.iCompartmentSR.lact1[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentT.lact1[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentL.lact1[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIs.lact1[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIc.lact1[timeStep-1]};
            std::vector<int> vAdultCow2 = {iHealthStateCompartmentsContainer.iCompartmentSR.lact2[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentT.lact2[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentL.lact2[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIs.lact2[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIc.lact2[timeStep-1]};
            std::vector<int> vAdultCow3 = {iHealthStateCompartmentsContainer.iCompartmentSR.lact3[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentT.lact3[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentL.lact3[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIs.lact3[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIc.lact3[timeStep-1]};
            std::vector<int> vAdultCow4 = {iHealthStateCompartmentsContainer.iCompartmentSR.lact4[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentT.lact4[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentL.lact4[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIs.lact4[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIc.lact4[timeStep-1]};
            std::vector<int> vAdultCow5 = {iHealthStateCompartmentsContainer.iCompartmentSR.lact5[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentT.lact5[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentL.lact5[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIs.lact5[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIc.lact5[timeStep-1]};

            if (Parameters::managementOfPositiveTestedTakeIntoAccount) {
                std::vector<int>  vFirstCalvingCows_positiveTested =  {iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[Parameters::ageFirstCalving-1], iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.past[Parameters::ageFirstCalving-1], iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past[Parameters::ageFirstCalving-1], iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past[Parameters::ageFirstCalving-1], iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[Parameters::ageFirstCalving-1]};
                vFirstCalvingCows = vFirstCalvingCows + vFirstCalvingCows_positiveTested;
                std::vector<int> vAdultCow1_positiveTested = {iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.lact1[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.lact1[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.lact1[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.lact1[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.lact1[timeStep-1]};
                vAdultCow1 = vAdultCow1 + vAdultCow1_positiveTested;
                std::vector<int> vAdultCow2_positiveTested = {iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.lact2[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.lact2[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.lact2[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.lact2[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.lact2[timeStep-1]};
                vAdultCow2 = vAdultCow2 + vAdultCow2_positiveTested;
                std::vector<int> vAdultCow3_positiveTested = {iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.lact3[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.lact3[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.lact3[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.lact3[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.lact3[timeStep-1]};
                vAdultCow3 = vAdultCow3 + vAdultCow3_positiveTested;
                std::vector<int> vAdultCow4_positiveTested = {iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.lact4[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.lact4[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.lact4[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.lact4[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.lact4[timeStep-1]};
                vAdultCow4 = vAdultCow4 + vAdultCow4_positiveTested;
                std::vector<int> vAdultCow5_positiveTested = {iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.lact5[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.lact5[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.lact5[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.lact5[timeStep-1], iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.lact5[timeStep-1]};
                vAdultCow5 = vAdultCow5 + vAdultCow5_positiveTested;
            }

            // dispatch births in age group
            std::vector<int> vAgeGroupDispatchingProb; vAgeGroupDispatchingProb.resize(6, 0);
            std::vector<int> vAgeGroupDispatching; vAgeGroupDispatching.resize(6, 0);

            int firstCalvingAll = std::accumulate(vFirstCalvingCows.begin(), vFirstCalvingCows.end(), 0);
            int Lact1all = std::accumulate(vAdultCow1.begin(), vAdultCow1.end(), 0);
            int Lact2all = std::accumulate(vAdultCow2.begin(), vAdultCow2.end(), 0);
            int Lact3all = std::accumulate(vAdultCow3.begin(), vAdultCow3.end(), 0);
            int Lact4all = std::accumulate(vAdultCow4.begin(), vAdultCow4.end(), 0);
            int Lact5all = std::accumulate(vAdultCow5.begin(), vAdultCow5.end(), 0);

            vAgeGroupDispatchingProb[0] = firstCalvingAll;
            vAgeGroupDispatchingProb[1] = Lact1all;
            vAgeGroupDispatchingProb[2] = Lact2all;
            vAgeGroupDispatchingProb[3] = Lact3all;
            vAgeGroupDispatchingProb[4] = Lact4all;
            vAgeGroupDispatchingProb[5] = Lact5all;

            int nbAnimalsIn_vAgeGroupDispatchingProb = std::accumulate(vAgeGroupDispatchingProb.begin(), vAgeGroupDispatchingProb.end(), 0);
            int birthSumExcess = 0;

            if (nbAnimalsIn_vAgeGroupDispatchingProb < birthSum) {
                birthSumExcess = birthSum - nbAnimalsIn_vAgeGroupDispatchingProb;
                birthSum = nbAnimalsIn_vAgeGroupDispatchingProb;
            }
            birthFake += birthSumExcess; // représente très peu de cas, si devient non négligeable il faudrait repartir entre S et T


            if (birthSumExcess > 0) {
                // std::cout << birthSumExcess << " / " << birthSum << std::endl;
                if (nbAnimalsIn_vAgeGroupDispatchingProb == 0) {
                    birthsS += birthSumExcess;
                } else {
                    // std::cout << birthSumExcess << " / " << birthSum << std::endl;
                    std::vector<double> vProbs = U_functions::getProportionsFromHeadcount(vAgeGroupDispatchingProb);
                    std::vector<int> vDistributionOfAdditionalBirths = U_functions::multinomialDistribution(ParamRep.randomGenerator, birthSumExcess, vProbs);

                    birthSum += birthSumExcess;
                    nbAnimalsIn_vAgeGroupDispatchingProb += birthSumExcess;
                    vAgeGroupDispatchingProb = vAgeGroupDispatchingProb + vDistributionOfAdditionalBirths;
                }
            }


            if (nbAnimalsIn_vAgeGroupDispatchingProb == birthSum) {
                vAgeGroupDispatching = vAgeGroupDispatching + vAgeGroupDispatchingProb;
            } else {
                // Ici on utilise une boucle de multinomiale (pour chaque naissance) à a place d'une seule multinomiale pour la totalité des birthSum afin d'éviter d'avoir plus de naissance dans un état de santé qu'il n'y a d'animaux présent
                for (int birth = 0; birth < birthSum; birth++) {
                    std::vector<double> vProbs = U_functions::getProportionsFromHeadcount(vAgeGroupDispatchingProb);
                    std::vector<int> vBirth = U_functions::multinomialDistribution(ParamRep.randomGenerator, 1, vProbs);

                    vAgeGroupDispatching = vAgeGroupDispatching + vBirth;
                    vAgeGroupDispatchingProb = vAgeGroupDispatchingProb - vBirth;
                }
            }

            //    vNbBirthsPerAgeGroup[0] += vAgeGroupDispatching[0];
            //    vNbBirthsPerAgeGroup[1] += vAgeGroupDispatching[1];
            //    vNbBirthsPerAgeGroup[2] += vAgeGroupDispatching[2];
            //    vNbBirthsPerAgeGroup[3] += vAgeGroupDispatching[3];
            //    vNbBirthsPerAgeGroup[4] += vAgeGroupDispatching[4];
            //    vNbBirthsPerAgeGroup[5] += vAgeGroupDispatching[5];

            //    vNbBirthsPerYearPerAgeGroup[year*6+0] += vAgeGroupDispatching[0];
            //    vNbBirthsPerYearPerAgeGroup[year*6+1] += vAgeGroupDispatching[1];
            //    vNbBirthsPerYearPerAgeGroup[year*6+2] += vAgeGroupDispatching[2];
            //    vNbBirthsPerYearPerAgeGroup[year*6+3] += vAgeGroupDispatching[3];
            //    vNbBirthsPerYearPerAgeGroup[year*6+4] += vAgeGroupDispatching[4];
            //    vNbBirthsPerYearPerAgeGroup[year*6+5] += vAgeGroupDispatching[5];

            // for each age group, dispatch health states
            std::vector<int> vBirthsFromfirstCalvingCows; vBirthsFromfirstCalvingCows.resize(5, 0);
            std::vector<int> vBirthsFromLact1; vBirthsFromLact1.resize(5, 0);
            std::vector<int> vBirthsFromLact2; vBirthsFromLact2.resize(5, 0);
            std::vector<int> vBirthsFromLact3; vBirthsFromLact3.resize(5, 0);
            std::vector<int> vBirthsFromLact4; vBirthsFromLact4.resize(5, 0);
            std::vector<int> vBirthsFromLact5; vBirthsFromLact5.resize(5, 0);

            if (vAgeGroupDispatching[0] > 0) {
                std::vector<double> vProb = U_functions::getProportionsFromHeadcount(vFirstCalvingCows);
                vBirthsFromfirstCalvingCows = U_functions::multinomialDistribution(ParamRep.randomGenerator, vAgeGroupDispatching[0], vProb);
            }
            if (vAgeGroupDispatching[1]>0) {
                std::vector<double> vProb = U_functions::getProportionsFromHeadcount(vAdultCow1);
                vBirthsFromLact1 = U_functions::multinomialDistribution(ParamRep.randomGenerator, vAgeGroupDispatching[1], vProb);
            }
            if (vAgeGroupDispatching[2]>0) {
                std::vector<double> vProb = U_functions::getProportionsFromHeadcount(vAdultCow2);
                vBirthsFromLact2 = U_functions::multinomialDistribution(ParamRep.randomGenerator, vAgeGroupDispatching[2], vProb);
            }
            if (vAgeGroupDispatching[3]>0) {
                std::vector<double> vProb = U_functions::getProportionsFromHeadcount(vAdultCow3);
                vBirthsFromLact3 = U_functions::multinomialDistribution(ParamRep.randomGenerator, vAgeGroupDispatching[3], vProb);
            }
            if (vAgeGroupDispatching[4]>0) {
                std::vector<double> vProb = U_functions::getProportionsFromHeadcount(vAdultCow4);
                vBirthsFromLact4 = U_functions::multinomialDistribution(ParamRep.randomGenerator, vAgeGroupDispatching[4], vProb);
            }
            if (vAgeGroupDispatching[5]>0) {
                std::vector<double> vProb = U_functions::getProportionsFromHeadcount(vAdultCow5);
                vBirthsFromLact5 = U_functions::multinomialDistribution(ParamRep.randomGenerator, vAgeGroupDispatching[5], vProb);
            }

            // Transfer births

            int birthsFromSR = vBirthsFromfirstCalvingCows[0] + vBirthsFromLact1[0] + vBirthsFromLact2[0] + vBirthsFromLact3[0] + vBirthsFromLact4[0] + vBirthsFromLact5[0];
            int birthsFromT = vBirthsFromfirstCalvingCows[1] + vBirthsFromLact1[1] + vBirthsFromLact2[1] + vBirthsFromLact3[1] + vBirthsFromLact4[1] + vBirthsFromLact5[1];
            int birthsFromL = vBirthsFromfirstCalvingCows[2] + vBirthsFromLact1[2] + vBirthsFromLact2[2] + vBirthsFromLact3[2] + vBirthsFromLact4[2] + vBirthsFromLact5[2];
            int birthsFromIs = vBirthsFromfirstCalvingCows[3] + vBirthsFromLact1[3] + vBirthsFromLact2[3] + vBirthsFromLact3[3] + vBirthsFromLact4[3] + vBirthsFromLact5[3];
            int birthsFromIc = vBirthsFromfirstCalvingCows[4] + vBirthsFromLact1[4] + vBirthsFromLact2[4] + vBirthsFromLact3[4] + vBirthsFromLact4[4] + vBirthsFromLact5[4];

//            if (birthsFromT > 0) {
//                std::cout << birthsFromT << " (" << vBirthsFromfirstCalvingCows[1] << ", " << vBirthsFromLact1[1] << ", " << vBirthsFromLact2[1] << ", " << vBirthsFromLact3[1] << ", " << vBirthsFromLact4[1] << ", " << vBirthsFromLact5[1] << ")" << std::endl;
//            }

            // in case we want to use the sex ratio data
            // int birthsFromSR_FemaleAlive = gsl_ran_binomial(ParamRep.randomGenerator, (1.-iParametersSpecificToEachHerd.deathRateBirth)*(iParametersSpecificToEachHerd.sexRatio), birthsFromSR);
            // int birthsFromT_FemaleAlive = gsl_ran_binomial(ParamRep.randomGenerator, (1.-iParametersSpecificToEachHerd.deathRateBirth)*(iParametersSpecificToEachHerd.sexRatio), birthsFromT);
            // int birthsFromL_FemaleAlive = gsl_ran_binomial(ParamRep.randomGenerator, (1.-iParametersSpecificToEachHerd.deathRateBirth)*(iParametersSpecificToEachHerd.sexRatio), birthsFromL);
            // int birthsFromIs_FemaleAlive = gsl_ran_binomial(ParamRep.randomGenerator, (1.-iParametersSpecificToEachHerd.deathRateBirth)*(iParametersSpecificToEachHerd.sexRatio), birthsFromIs);
            // int birthsFromIc_FemaleAlive = gsl_ran_binomial(ParamRep.randomGenerator, (1.-iParametersSpecificToEachHerd.deathRateBirth)*(iParametersSpecificToEachHerd.sexRatio), birthsFromIc);

            int birthsFromSR_FemaleAlive = gsl_ran_binomial(ParamRep.randomGenerator, (1.-iParametersSpecificToEachHerd.deathRateBirth), birthsFromSR);
            int birthsFromT_FemaleAlive = gsl_ran_binomial(ParamRep.randomGenerator, (1.-iParametersSpecificToEachHerd.deathRateBirth), birthsFromT);
            int birthsFromL_FemaleAlive = gsl_ran_binomial(ParamRep.randomGenerator, (1.-iParametersSpecificToEachHerd.deathRateBirth), birthsFromL);
            int birthsFromIs_FemaleAlive = gsl_ran_binomial(ParamRep.randomGenerator, (1.-iParametersSpecificToEachHerd.deathRateBirth), birthsFromIs);
            int birthsFromIc_FemaleAlive = gsl_ran_binomial(ParamRep.randomGenerator, (1.-iParametersSpecificToEachHerd.deathRateBirth), birthsFromIc);

            int birthsFromSR_FemaleAliveInfectedInutero = 0;
            int birthsFromT_FemaleAliveInfectedInutero = gsl_ran_binomial(ParamRep.randomGenerator, Parameters::infinuteroT, birthsFromT_FemaleAlive);
            birthsFromT_FemaleAlive -= birthsFromT_FemaleAliveInfectedInutero;
            int birthsFromL_FemaleAliveInfectedInutero = gsl_ran_binomial(ParamRep.randomGenerator, Parameters::infinuteroL, birthsFromL_FemaleAlive);
            birthsFromL_FemaleAlive -= birthsFromL_FemaleAliveInfectedInutero;
            int birthsFromIs_FemaleAliveInfectedInutero = gsl_ran_binomial(ParamRep.randomGenerator, Parameters::infinuteroIs, birthsFromIs_FemaleAlive);
            birthsFromIs_FemaleAlive -= birthsFromIs_FemaleAliveInfectedInutero;
            int birthsFromIc_FemaleAliveInfectedInutero = gsl_ran_binomial(ParamRep.randomGenerator, Parameters::infinuteroIc, birthsFromIc_FemaleAlive);
            birthsFromIc_FemaleAlive -= birthsFromIc_FemaleAliveInfectedInutero;

            birthsS += birthsFromSR_FemaleAlive + birthsFromT_FemaleAlive + birthsFromL_FemaleAlive + birthsFromIs_FemaleAlive + birthsFromIc_FemaleAlive;
            birthsS_FemaleAlive = birthsS;

            birthsT += birthsFromSR_FemaleAliveInfectedInutero + birthsFromT_FemaleAliveInfectedInutero + birthsFromL_FemaleAliveInfectedInutero + birthsFromIs_FemaleAliveInfectedInutero + birthsFromIc_FemaleAliveInfectedInutero;
            birthsT_FemaleAlive = birthsT;

            // vNbBirthsPerYear[year] += (birthsFromSR_Female + birthsFromT_Female + birthsFromL_Female + birthsFromIs_Female + birthsFromIc_Female);

            birthTot += birthSum;

            // Transfer
            iHealthStateCompartmentsContainer.iCompartmentSR.present[0]+=birthsS_FemaleAlive;
            iHealthStateCompartmentsContainer.iCompartmentT.present[0]+=birthsT_FemaleAlive;

            vBirthsSHealthStatesDispatching[0] = birthsFromSR_FemaleAlive;
            vBirthsSHealthStatesDispatching[1] = birthsFromT_FemaleAlive;
            vBirthsSHealthStatesDispatching[2] = birthsFromL_FemaleAlive;
            vBirthsSHealthStatesDispatching[3] = birthsFromIs_FemaleAlive;
            vBirthsSHealthStatesDispatching[4] = birthsFromIc_FemaleAlive;

        } else {
            //get headcount in age group
            int firstCalvingCows = iHealthStateCompartmentsContainer.iCompartmentSR.past[Parameters::ageFirstCalving-1];
            int adultCow1 = iHealthStateCompartmentsContainer.iCompartmentSR.lact1[timeStep-1];
            int adultCow2 = iHealthStateCompartmentsContainer.iCompartmentSR.lact2[timeStep-1];
            int adultCow3 = iHealthStateCompartmentsContainer.iCompartmentSR.lact3[timeStep-1];
            int adultCow4 = iHealthStateCompartmentsContainer.iCompartmentSR.lact4[timeStep-1];
            int adultCow5 = iHealthStateCompartmentsContainer.iCompartmentSR.lact5[timeStep-1];

            if (Parameters::managementOfPositiveTestedTakeIntoAccount) {
                firstCalvingCows = firstCalvingCows + iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[Parameters::ageFirstCalving-1];
                adultCow1 = adultCow1 + iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.lact1[timeStep-1];
                adultCow2 = adultCow2 + iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.lact2[timeStep-1];
                adultCow3 = adultCow3 + iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.lact3[timeStep-1];
                adultCow4 = adultCow4 + iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.lact4[timeStep-1];
                adultCow5 = adultCow5 + iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.lact5[timeStep-1];
            }

            int nbAnimalsLikelyToCalve = firstCalvingCows + adultCow1 + adultCow2 + adultCow3 + adultCow4 + adultCow5;
            int birthSumExcess = 0;
            if (nbAnimalsLikelyToCalve < birthSum) {
                birthSumExcess = birthSum - nbAnimalsLikelyToCalve;
            }

            birthsS += birthSum;

            // birthsS_Female = gsl_ran_binomial(ParamRep.randomGenerator, iParametersSpecificToEachHerd.sexRatio, birthsS);
            // birthsS_FemaleAlive = gsl_ran_binomial(ParamRep.randomGenerator, 1-iParametersSpecificToEachHerd.deathRateBirth, birthsS_Female);
            // birthsS_FemaleAlive = gsl_ran_binomial(ParamRep.randomGenerator, (1.-iParametersSpecificToEachHerd.deathRateBirth)*(iParametersSpecificToEachHerd.sexRatio), birthsS); // in case we want to use the sex ratio data
            birthsS_FemaleAlive = gsl_ran_binomial(ParamRep.randomGenerator, (1.-iParametersSpecificToEachHerd.deathRateBirth), birthsS);
            birthsT_FemaleAlive = 0;

            // vNbBirthsPerYear[year] += birthsS_Female;// + birthsT_Female;

            birthFake += birthSumExcess;
            birthTot += birthSum;
            
            // Transfer
            iHealthStateCompartmentsContainer.iCompartmentSR.present[0]+=birthsS_FemaleAlive;
            
            vBirthsSHealthStatesDispatching[0] = birthsS_FemaleAlive;
        }

        // update summary
        iSummaryInfection.ageAtInfection_inutero[0] += birthsT_FemaleAlive;
        iSummaryInfection.ageAtInfection[0] += birthsT_FemaleAlive;
        iSummaryInfection.transmissionRoutes_inutero[timeStep] += birthsT_FemaleAlive;
        iSummaryInfection.IncidenceT[timeStep] += birthsT_FemaleAlive;

        // transfer cows
        iBirthsContainer.iBirthSR.setBirthsS(vBirthsSHealthStatesDispatching[0]);
        
        iBirthsContainer.iBirthT.setBirthsS(vBirthsSHealthStatesDispatching[1]);
        iBirthsContainer.iBirthL.setBirthsS(vBirthsSHealthStatesDispatching[2]);
        iBirthsContainer.iBirthIs.setBirthsS(vBirthsSHealthStatesDispatching[3]);
        iBirthsContainer.iBirthIc.setBirthsS(vBirthsSHealthStatesDispatching[4]);

        //    iBirthsContainer.iBirthIs.setBirthsS(0);
        //    iBirthsContainer.iBirthIc.setBirthsS(0);
    } else {
        iBirthsContainer.iBirthSR.setBirthsS(0);
        iBirthsContainer.iBirthT.setBirthsS(0);
        iBirthsContainer.iBirthL.setBirthsS(0);
        iBirthsContainer.iBirthIs.setBirthsS(0);
        iBirthsContainer.iBirthIc.setBirthsS(0);
    }
}



void DairyHerd::SendOutMoves(const ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, std::map<std::string, std::map<int, std::vector<MovementUnit>> >& mmOutMoves, std::map<std::string, DairyHerd>& mFarms, std::vector<int>& vAgeMov, long double& countMov, std::vector<int>& vAgeFailure, long double& countAgeFailure, std::vector<int>& vAgeMovAfterCorrection, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary){
    
    for (int i = 0; i<static_cast<int>(mmOutMoves[iParametersSpecificToEachHerd.farmId][timeStep].size()); i++) {
        
        int rawAge = mmOutMoves[iParametersSpecificToEachHerd.farmId][timeStep][i].age;
        assert(rawAge>= 0 & rawAge < 135);
        
        if (timeStep >= iParametersSpecificToEachHerd.beginDate) {
#ifndef ACTIVATE_PARALLEL
            vAgeMov[rawAge]++;
#endif
            countMov++;
            
            float checkSize = iHealthStateCompartmentsContainer.iCompartmentSR.present[rawAge] + iHealthStateCompartmentsContainer.iCompartmentT.present[rawAge] + iHealthStateCompartmentsContainer.iCompartmentL.present[rawAge] + iHealthStateCompartmentsContainer.iCompartmentIs.present[rawAge];
            
            int age;
            if (checkSize > 0) {
                age = rawAge;
            } else {
                age = ageSelectionForTheMovement(rawAge, ParamRep, timeStep, iParametersSpecificToEachHerd.farmId);
            }
            
            
            if (age < 999) {
                assert(rawAge>= 0 & rawAge < 135);
                double ageHeadcount = iHealthStateCompartmentsContainer.iCompartmentSR.present[age] + iHealthStateCompartmentsContainer.iCompartmentT.present[age] + iHealthStateCompartmentsContainer.iCompartmentL.present[age] + iHealthStateCompartmentsContainer.iCompartmentIs.present[age];
                
                if (ageHeadcount >= 1) {
#ifndef ACTIVATE_PARALLEL
                    vAgeMovAfterCorrection[age]++;
#endif
                    
                    std::vector<double> vProbs(4);
                    vProbs[0] = iHealthStateCompartmentsContainer.iCompartmentSR.present[age] / ageHeadcount;
                    vProbs[1] = iHealthStateCompartmentsContainer.iCompartmentT.present[age] / ageHeadcount;
                    vProbs[2] = iHealthStateCompartmentsContainer.iCompartmentL.present[age] / ageHeadcount;
                    vProbs[3] = iHealthStateCompartmentsContainer.iCompartmentIs.present[age] / ageHeadcount;
                    //                std::cout << "Is proba" << vProbs[3] << std::endl;
                    std::vector<int> vIndivNbForEachCompart = U_functions::multinomialDistribution(ParamRep.randomGenerator, 1, vProbs);
                    // implémenter un choix de la destination au hasard dans les destinations possibles?
                    
                    // SR
                    if (vIndivNbForEachCompart[0] > 0){
                        sendAnimal("SR", timeStep, age, i, mFarms, mmOutMoves, iBufferPersistentResults);
                    } else
                        // T
                        if (vIndivNbForEachCompart[1] > 0){
                            sendAnimal("T", timeStep, age, i, mFarms, mmOutMoves, iBufferPersistentResults);
                        } else
                            // L
                            if (vIndivNbForEachCompart[2] > 0){
                                sendAnimal("L", timeStep, age, i, mFarms, mmOutMoves, iBufferPersistentResults);
                            } else
                                // Is
                                if (vIndivNbForEachCompart[3] > 0){
                                    sendAnimal("Is", timeStep, age, i, mFarms, mmOutMoves, iBufferPersistentResults);
                                }
                }
                else {
#ifndef ACTIVATE_PARALLEL
                    vAgeFailure[rawAge]++;
#endif
                    countAgeFailure++;
                }
            } else {
                if (mmOutMoves[iParametersSpecificToEachHerd.farmId][timeStep][i].destination != "outside") {
#ifndef ACTIVATE_PARALLEL
                    vAgeFailure[rawAge]++;
#endif
                    countAgeFailure++;
                }
                replaceMovementsByExtIfFailureOrNotYetInTheMetapopulation(ParamRep, timeStep, iParametersSpecificToEachHerd.farmId, rawAge, iMetapopSummary, mFarms, mmOutMoves, i);
            }
        } else {
            replaceMovementsByExtIfFailureOrNotYetInTheMetapopulation(ParamRep, timeStep, iParametersSpecificToEachHerd.farmId, rawAge, iMetapopSummary, mFarms, mmOutMoves, i);
            std::cout << "replaceMovementsByExtIfFailureOrNotYetInTheMetapopulation" << std::endl;
        }
        
    }
}



void DairyHerd::sendAnimal(std::string healthState, const int& timeStep, const int& age, const int& indexMov, std::map<std::string, DairyHerd>& mFarms,  std::map<std::string, std::map<int, std::vector<MovementUnit>> >& mmOutMoves, BufferPersistentResults& iBufferPersistentResults){
    
    if (healthState != "SR" & healthState != "T" & healthState != "L" & healthState != "Is") {
        throw std::runtime_error("Wrong format for 'healthState' when sending animal!");
    }
    
    if (healthState == "SR") {
        iHealthStateCompartmentsContainer.iCompartmentSR.present[age] -= 1;
    } else if (healthState == "T") {
        iHealthStateCompartmentsContainer.iCompartmentT.present[age] -= 1;
        numberOfSalesOfInfectedAnimalsT++;
    } else if (healthState == "L") {
        iHealthStateCompartmentsContainer.iCompartmentL.present[age] -= 1;
        numberOfSalesOfInfectedAnimalsL++;
    } else if (healthState == "Is") {
        iHealthStateCompartmentsContainer.iCompartmentIs.present[age] -= 1;
        numberOfSalesOfInfectedAnimalsIs++;
    }
    
    if (healthState != "SR") {
        if (typeInfectedHerd == 2) {
            numberOfAnimalInfectedSendWhenNotInitiallyInfected ++;
        }
    }
    
    std::vector<int> v(7);
    v[0] = timeStep;
    if (healthState == "SR") {
        v[1] = 0;
    } else if (healthState == "T") {
        v[1] = 1;
    } else if (healthState == "L") {
        v[1] = 2;
    } else if (healthState == "Is") {
        v[1] = 3;
    }
    v[2] = age;
    v[3] = typeInfectedHerd; if (!persistenceIndicator) {v[3] = 3;}
    v[4] = presenceOfIcAtThisTime;
    if (cumulativeNumberOfIc >= 2) {v[5] = 1;} else {v[5] = 0;}
    if (iParametersSpecificToEachHerd.numberOfOutgoingMovementsAboveTheMinimum) {v[6] = 1;} else {v[6] = 0;}
    
    AnimalMoving pendingMov = AnimalMoving(v, iParametersSpecificToEachHerd.farmId);
    
    if (mmOutMoves[iParametersSpecificToEachHerd.farmId][timeStep][indexMov].destination != "outside") {
//        mFarms[mmOutMoves[iParametersSpecificToEachHerd.farmId][timeStep][indexMov].destination].vPendingMoveVectors.emplace_back(pendingMov);
        if (mFarms.count(mmOutMoves[iParametersSpecificToEachHerd.farmId][timeStep][indexMov].destination) != 0) {
            mFarms[mmOutMoves[iParametersSpecificToEachHerd.farmId][timeStep][indexMov].destination].addPendingMov(timeStep, pendingMov);
        }
        
        if (healthState != "SR") {
            if (typeInfectedHerd == 2) {
                numberOfAnimalInfectedSendToMetapopWhenNotInitiallyInfected ++;
            }
        }
        
#ifndef ABCreduce
        if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
            if (timeStep >= Parameters::startTest) {
                if (Parameters::typeOfTestStrategy == Parameters::strategyHerdRandomlyChosenMovIn) {
                    if (mFarms[mmOutMoves[iParametersSpecificToEachHerd.farmId][timeStep][indexMov].destination].iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm) {
                        iBufferPersistentResults.bNbOutgoingMovTested[timeStep]++;
                        if (healthState != "SR") {
                            iBufferPersistentResults.bNbOutgoingInfectedMovTested[timeStep]++;
                        }
                    }
                } else
                    if (Parameters::typeOfTestStrategy == Parameters::strategyHerdRandomlyChosenMovOut) {
                        if (iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm) {
                            iBufferPersistentResults.bNbOutgoingMovTested[timeStep]++;
                            if (healthState != "SR") {
                                iBufferPersistentResults.bNbOutgoingInfectedMovTested[timeStep]++;
                            }
                        }
                    } else
                        if (Parameters::typeOfTestStrategy == Parameters::strategyHerdRandomlyChosenMovInOut) {
                            if (iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm | mFarms[mmOutMoves[iParametersSpecificToEachHerd.farmId][timeStep][indexMov].destination].iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm) {
                                iBufferPersistentResults.bNbOutgoingMovTested[timeStep]++;
                                if (healthState != "SR") {
                                    iBufferPersistentResults.bNbOutgoingInfectedMovTested[timeStep]++;
                                }
                            }
                        } else
                            if (Parameters::typeOfTestStrategy == Parameters::strategyHerdWithIc) {
                                if (iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm) {
                                    if (presenceOfIcAtThisTime) {
                                        iBufferPersistentResults.bNbOutgoingMovTested[timeStep]++;
                                        if (healthState != "SR") {
                                            iBufferPersistentResults.bNbOutgoingInfectedMovTested[timeStep]++;
                                        }
                                    }
                                }
                            } else
                                if (Parameters::typeOfTestStrategy == Parameters::strategyHerdWithMoreThanTwoIc) {
                                    if (iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm) {
                                        if (cumulativeNumberOfIc >= 2) {
                                            iBufferPersistentResults.bNbOutgoingMovTested[timeStep]++;
                                            if (healthState != "SR") {
                                                iBufferPersistentResults.bNbOutgoingInfectedMovTested[timeStep]++;
                                            }
                                        }
                                    }
                                } else
                                    if (Parameters::typeOfTestStrategy == Parameters::strategyBasedOnNumberOfIncomingMovements) {
                                        if (mFarms[mmOutMoves[iParametersSpecificToEachHerd.farmId][timeStep][indexMov].destination].iParametersSpecificToEachHerd.numberOfIncomingMovementsAboveTheMinimum) {
                                            iBufferPersistentResults.bNbOutgoingMovTested[timeStep]++;
                                            if (healthState != "SR") {
                                                iBufferPersistentResults.bNbOutgoingInfectedMovTested[timeStep]++;
                                            }
                                        }
                                    } else
                                        if (Parameters::typeOfTestStrategy == Parameters::strategyBasedOnNumberOfOutgoingMovements) {
                                            if (iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm) {
                                                if (iParametersSpecificToEachHerd.numberOfOutgoingMovementsAboveTheMinimum) {
                                                    iBufferPersistentResults.bNbOutgoingMovTested[timeStep]++;
                                                    if (healthState != "SR") {
                                                        iBufferPersistentResults.bNbOutgoingInfectedMovTested[timeStep]++;
                                                    }
                                                }
                                            }
                                        } else {
                                            throw std::runtime_error("Wrong strategies in SendOutMoves function");
                                        }
            }
        }
#endif
    }
#ifndef ABCreduce
    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
        iBufferPersistentResults.bNbOutgoingMov[timeStep]++;
        
        if (healthState != "SR") {
            iBufferPersistentResults.bNbOutgoingInfectedMov[timeStep]++;
        }
        
        if (healthState != "SR") {
            if (typeInfectedHerd == 1) {
                iBufferPersistentResults.bNumberOfTSaleByPrimary++;
            }
        }
    }
#endif
}



void DairyHerd::replaceMovementsByExtIfFailureOrNotYetInTheMetapopulation(const ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, const std::string& theFarmId, const int& age, MetapopSummary& iMetapopSummary, std::map<std::string, DairyHerd>& mFarms, std::map<std::string, std::map<int, std::vector<MovementUnit>> >& mmOutMoves, const int& movementIndex){
    std::vector<double> vProbs(4);
    
    double SR = static_cast<double>(iMetapopSummary.pastSR[age]);
    double T = static_cast<double>(iMetapopSummary.pastT[age]);
    double L = static_cast<double>(iMetapopSummary.pastL[age]);
    double Is = static_cast<double>(iMetapopSummary.pastIs[age]);
    double proportionOfHerdsWithIc = iMetapopSummary.pastNbHerdsWithIc / Parameters::nbHerds;
    double proportionOfHerdsWithMoreThanTwoIc = iMetapopSummary.pastNbHerdsWithMoreThanTwoIc / Parameters::nbHerds;
    double proportionOfHerdsWithOutgoingMovementsAboveTheMinimum = Parameters::numberOfHerdsAboveTheMinimumOfOutgoingMovements/Parameters::nbHerds;
    if ((T + L + Is) > 0) {
        if (Parameters::ABCrejection == true | Parameters::ABCsmc == true | Parameters::ABCcompleteTrajectories == true | Parameters::typeOfTheRiskToPurchaseInfectedAnimalOutsideTheMetapopulation == 1) {
            if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false & Parameters::typeOfTheRiskToPurchaseInfectedAnimalOutsideTheMetapopulation == 1) {
                SR = ((1-Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation) * (T + L + Is)) / (Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation);
            } else {
                SR = ((1-ParamRep.probaToPurchaseInfectedAnimal_currentValue) * (T + L + Is)) / (ParamRep.probaToPurchaseInfectedAnimal_currentValue);
            }
        }
    }
    double N = SR + T + L + Is;
    
    vProbs[0] = (SR / N);
    vProbs[1] = (T / N);
    vProbs[2] = (L / N);
    vProbs[3] = (Is / N);
    
    //    std::cout << SR << " | " << T << " | " << L << " | " << Is << " | " << std::endl;
    //    std::cout << vProbs[0] << " | " << vProbs[1] << " | " << vProbs[2] << " | " << vProbs[3] << " | " << std::endl;
    
    if (N == 0) {
        std::cout << timeStep << " | " << iParametersSpecificToEachHerd.farmId << " | " << "age: " << age << " - ZERO! (replaceMovementsByExtIfFailureOrNotYetInTheMetapopulation)" << std::endl;
        
        vProbs[0] = 0;
        vProbs[1] = 0;
        vProbs[2] = 0;
        vProbs[3] = 0;

        if (Parameters::ABCrejection == true | Parameters::ABCsmc == true | Parameters::ABCcompleteTrajectories == true | Parameters::typeOfTheRiskToPurchaseInfectedAnimalOutsideTheMetapopulation == 1) {
            if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false & Parameters::typeOfTheRiskToPurchaseInfectedAnimalOutsideTheMetapopulation == 1) {
                vProbs[0] = 1-Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation;
                if (age < Parameters::ageYheifer) {
                    vProbs[1] = Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation/2.;
                    vProbs[2] = Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation/2.;
                    vProbs[3] = 0.;
                } else if (age >= Parameters::ageYheifer & age < Parameters::ageAdults) {
                    vProbs[1] = Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation/3.;
                    vProbs[2] = Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation/3.;
                    vProbs[3] = Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation/3.;
                } else { // age >= Parameters::ageAdults
                    vProbs[1] = 0.;
                    vProbs[2] = Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation/2.;
                    vProbs[3] = Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation/2.;
                }
            } else {
                vProbs[0] = 1-ParamRep.probaToPurchaseInfectedAnimal_currentValue;
                if (age < Parameters::ageYheifer) {
                    vProbs[1] = ParamRep.probaToPurchaseInfectedAnimal_currentValue/2.;
                    vProbs[2] = ParamRep.probaToPurchaseInfectedAnimal_currentValue/2.;
                    vProbs[3] = 0.;
                } else if (age >= Parameters::ageYheifer & age < Parameters::ageAdults) {
                    vProbs[1] = ParamRep.probaToPurchaseInfectedAnimal_currentValue/3.;
                    vProbs[2] = ParamRep.probaToPurchaseInfectedAnimal_currentValue/3.;
                    vProbs[3] = ParamRep.probaToPurchaseInfectedAnimal_currentValue/3.;
                } else { // age >= Parameters::ageAdults
                    vProbs[1] = 0.;
                    vProbs[2] = ParamRep.probaToPurchaseInfectedAnimal_currentValue/2.;
                    vProbs[3] = ParamRep.probaToPurchaseInfectedAnimal_currentValue/2.;
                }
            }
        } else {
            vProbs[0] = 1;
            vProbs[1] = 0;
            vProbs[2] = 0;
            vProbs[3] = 0;
        }
    }

    //    assert(N > 0);

    std::vector<int> vIndivNbForEachCompart = U_functions::multinomialDistribution(ParamRep.randomGenerator, 1, vProbs);

    std::vector<int> v(7);
    v[0] = timeStep;

    if (vIndivNbForEachCompart[0] > 0){ // SR
        v[1] = 0;
    } else if (vIndivNbForEachCompart[1] > 0){ // T
        v[1] = 1;
    } else if (vIndivNbForEachCompart[2] > 0){ // L
        v[1] = 2;
    } else if (vIndivNbForEachCompart[3] > 0){ // Is
        v[1] = 3;
    }
    
    v[2] = age;
    v[3] = 0;
    v[4] = gsl_ran_bernoulli(ParamRep.randomGenerator, proportionOfHerdsWithIc); // presenceOfIcAtThisTime
    v[5] = gsl_ran_bernoulli(ParamRep.randomGenerator, proportionOfHerdsWithMoreThanTwoIc); // cumulativeNumberOfIc
    v[6] = gsl_ran_bernoulli(ParamRep.randomGenerator, proportionOfHerdsWithOutgoingMovementsAboveTheMinimum);
    
    AnimalMoving pendingMov = AnimalMoving(v, "outside");
    
    if (mmOutMoves[theFarmId][timeStep][movementIndex].destination != "outside") {
        //        assert(mmOutMoves[theFarmId][timeStep][movementIndex].destination != "outside");
//        mFarms[mmOutMoves[theFarmId][timeStep][movementIndex].destination].vPendingMoveVectors.emplace_back(pendingMov);
        if (mFarms.count(mmOutMoves[theFarmId][timeStep][movementIndex].destination) != 0) {
            mFarms[mmOutMoves[theFarmId][timeStep][movementIndex].destination].addPendingMov(timeStep, pendingMov);
        }
    }
}



void DairyHerd::addPendingMov(const int& timeStep, const AnimalMoving& mov){
    if (timeStep >= iParametersSpecificToEachHerd.beginDate) {
        vPendingMoveVectors.emplace_back(mov);
    }
}



int DairyHerd::ageSelectionForTheMovement(const int& rawAge, const ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, const std::string& theFarmId) {
    
    int age = 999;
    int ageBuffer;
    int lowerAgeLimit = 0;
    int upperAgeLimit = rawAge-1;
    bool allAgeCheck = false;
    
    //    if (rawAge < Parameters::ageFirstCalving) {
    int gardeFou = 0;
    while ((age == 999) & (allAgeCheck!=true)) {
        gardeFou ++;
        if (gardeFou > 10000) {
            std::cout << "\033[1;31mgarde-fou - ageSelectionForTheMovement\033[1;m" << std::endl;
            std::exit(EXIT_FAILURE);
        }
        ageBuffer = upperAgeLimit+1;
        ageSelection(ParamRep, age, ageBuffer, lowerAgeLimit, upperAgeLimit, allAgeCheck);
    }
    //    }
    
    
    //    if (age == 999) {
    //        std::cout << rawAge << " | " << herdNum << " | " << iParametersSpecificToEachHerd.herdSize << " | " << iSummaryDynamic.headcount[timeStep-1] << " || " << iParametersSpecificToEachHerd.Kcow << "|" << iHealthStateCompartmentsContainer.iCompartmentSR.lactAll[1] << " - " << iHealthStateCompartmentsContainer.iCompartmentSR.lact5[1] << std::endl;
    //    }
    
    return age;
}



void DairyHerd::ageSelection(const ParametersSpecificToEachRepetition& ParamRep, int& age, int& ageBuffer, int& lowerAgeLimit, int& upperAgeLimit, bool& allAgeCheck) {
    
    std::vector<double> vProbs;
    
    if (ageBuffer < Parameters::ageWeaning) {
        vProbs.resize(Parameters::ageWeaning, 0);
        lowerAgeLimit = 0;
        upperAgeLimit = Parameters::ageWeaning-1;
        allAgeCheck = true;
    } else if (ageBuffer >= Parameters::ageWeaning & ageBuffer < Parameters::ageCalf) {
        vProbs.resize(Parameters::ageCalf-Parameters::ageWeaning, 0);
        lowerAgeLimit = Parameters::ageWeaning;
        upperAgeLimit = Parameters::ageCalf-1;
        allAgeCheck = true;
    } else if (ageBuffer >= Parameters::ageCalf & ageBuffer < Parameters::ageYheifer) {
        vProbs.resize(Parameters::ageYheifer-Parameters::ageCalf, 0);
        lowerAgeLimit = Parameters::ageCalf;
        upperAgeLimit = Parameters::ageYheifer-1;
        allAgeCheck = true;
    } else if (ageBuffer >= Parameters::ageYheifer & ageBuffer < Parameters::ageFirstCalving) {
        vProbs.resize(Parameters::ageFirstCalving-Parameters::ageYheifer, 0);
        lowerAgeLimit = Parameters::ageYheifer;
        upperAgeLimit = Parameters::ageFirstCalving-1;
        allAgeCheck = true;
    } else if (ageBuffer >= Parameters::ageFirstCalving) {
        vProbs.resize(Parameters::nbStage-Parameters::ageFirstCalving, 0);
        lowerAgeLimit = Parameters::ageFirstCalving;
        upperAgeLimit = Parameters::nbStage-1;
        allAgeCheck = true;
    }
    
    
    int indexProbs = 0;
    for (int ageIndex = lowerAgeLimit; ageIndex <= upperAgeLimit; ageIndex++) {
        vProbs[indexProbs] = iHealthStateCompartmentsContainer.iCompartmentSR.present[ageIndex] + iHealthStateCompartmentsContainer.iCompartmentT.present[ageIndex] + iHealthStateCompartmentsContainer.iCompartmentL.present[ageIndex] + iHealthStateCompartmentsContainer.iCompartmentIs.present[ageIndex];
        indexProbs++;
    }
    
    float sumProbs = 0;
    for (int ageIndex = 0; ageIndex < vProbs.size(); ageIndex++) {
        sumProbs += vProbs[ageIndex];
    }
    
    for (int ageIndex = 0; ageIndex < vProbs.size(); ageIndex++) {
        vProbs[ageIndex] = vProbs[ageIndex]/sumProbs;
    }
    
    std::vector<int> result = U_functions::multinomialDistribution(ParamRep.randomGenerator, 1, vProbs);
    
    std::vector<int>::iterator it = std::find(result.begin(), result.end(), 1);
    if (it == result.end()) {
        age = 999;
    } else {
        int position = static_cast<int>(std::distance( result.begin(), it));
        age = lowerAgeLimit + position;
    }
    
}



void DairyHerd::isTheTestAndCullMeasureShouldBeApplied(const ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, const int& firstAge, const int& lastAge, const double& proportionAnimalsTested, const MetapopSummary& iMetapopSummary){
    
    if (iParametersSpecificToEachHerd.testAndCullImplementedInThisFarm) {
        if (timeStep == iParametersSpecificToEachHerd.testAndCullDate) {
            testAndCull(ParamRep, timeStep, firstAge, lastAge, proportionAnimalsTested, iMetapopSummary);
            iParametersSpecificToEachHerd.resetParametersOfControlMeasures();
        }
    }
    
}



void DairyHerd::testAndCull(const ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, const int& firstAge, const int& lastAge, const double& proportionAnimalsTested, const MetapopSummary& iMetapopSummary){
    
    std::vector<int> vProbs(lastAge-firstAge+1, 0);
    for (int age = firstAge-1; age < lastAge; age++) {
        vProbs[age-firstAge+1] = iHealthStateCompartmentsContainer.iCompartmentSR.present[age]+iHealthStateCompartmentsContainer.iCompartmentT.present[age]+iHealthStateCompartmentsContainer.iCompartmentL.present[age]+iHealthStateCompartmentsContainer.iCompartmentIs.present[age]+iHealthStateCompartmentsContainer.iCompartmentIc.present[age];
    }
    double N = std::accumulate(vProbs.begin(), vProbs.end(), 0);
    double nbAnimalsTested = gsl_ran_binomial(ParamRep.randomGenerator, proportionAnimalsTested, N);
    //    std::cout << nbAnimalsTested << std::endl;
    numberOfAnimalsTestedDuringTestAndCull += nbAnimalsTested;
    
    if (nbAnimalsTested < N) {
        
        std::vector<int> vAgeList = U_functions::ListAgeOfAnimals(vProbs);
        //        std::vector<int> vAgeBuffer = U_functions::RandomSamplingWithoutReplacement(vAgeList, nbAnimalsTested);
        std::vector<int> vAgeBuffer = U_functions::RandomSamplingWithoutReplacement(ParamRep.randomGenerator, vAgeList, nbAnimalsTested);
        std::vector<int> vNbIndivForEachAge(vProbs.size(),0);
        for (int index = 0; index < vAgeBuffer.size(); index++) {
            vNbIndivForEachAge[vAgeBuffer[index]] ++;
        }
        
        
        for (int index = 0; index < vNbIndivForEachAge.size(); index++) {
            if (vNbIndivForEachAge[index] > 0) {
                std::vector<int> vHealthStates(5, 0);
                
                vHealthStates[0] = iHealthStateCompartmentsContainer.iCompartmentSR.present[index+firstAge-1];
                vHealthStates[1] = iHealthStateCompartmentsContainer.iCompartmentT.present[index+firstAge-1];
                vHealthStates[2] = iHealthStateCompartmentsContainer.iCompartmentL.present[index+firstAge-1];
                vHealthStates[3] = iHealthStateCompartmentsContainer.iCompartmentIs.present[index+firstAge-1];
                vHealthStates[4] = iHealthStateCompartmentsContainer.iCompartmentIc.present[index+firstAge-1];
                
                std::vector<int> vHealthStatesList = U_functions::ListHealthStatesOfAnimals(vHealthStates);
                //                std::vector<int> vHealthStatesBuffer = U_functions::RandomSamplingWithoutReplacement(vHealthStatesList, vNbIndivForEachAge[index]);
                std::vector<int> vHealthStatesBuffer = U_functions::RandomSamplingWithoutReplacement(ParamRep.randomGenerator, vHealthStatesList, vNbIndivForEachAge[index]);
                std::vector<int> vNbIndivForEachHealthStates(vHealthStates.size(),0);
                for (int index = 0; index < vHealthStatesBuffer.size(); index++) {
                    vNbIndivForEachHealthStates[vHealthStatesBuffer[index]] ++;
                }
                
                
//                if (vHealthStates[0]-vNbIndivForEachHealthStates[0] < 0) {
//                    std::cout << vNbIndivForEachHealthStates[0] << " | " << vHealthStates[0] << std::endl;
//                }
//                if (vHealthStates[1]-vNbIndivForEachHealthStates[1] < 0) {
//                    std::cout << vNbIndivForEachHealthStates[1] << " | " << vHealthStates[1] << std::endl;
//                }
//                if (vHealthStates[2]-vNbIndivForEachHealthStates[2] < 0) {
//                    std::cout << vNbIndivForEachHealthStates[2] << " | " << vHealthStates[2] << std::endl;
//                }
//                if (vHealthStates[3]-vNbIndivForEachHealthStates[3] < 0) {
//                    std::cout << vNbIndivForEachHealthStates[3] << " | " << vHealthStates[3] << std::endl;
//                }
//                if (vHealthStates[4]-vNbIndivForEachHealthStates[4] < 0) {
//                    std::cout << vNbIndivForEachHealthStates[4] << " | " << vHealthStates[4] << std::endl;
//                }
                
                testAndRemoveAnimals(ParamRep, vNbIndivForEachHealthStates, firstAge, iMetapopSummary, index);
            }
        }
        
    } else {
        std::vector<int> vNbIndivForEachAge(vProbs.begin(), vProbs.end());
        
        for (int index = 0; index < vNbIndivForEachAge.size(); index++) {
            if (vNbIndivForEachAge[index] > 0) {
                std::vector<int> vNbIndivForEachHealthStates(5, 0);
                
                vNbIndivForEachHealthStates[0] = iHealthStateCompartmentsContainer.iCompartmentSR.present[index+firstAge-1];
                vNbIndivForEachHealthStates[1] = iHealthStateCompartmentsContainer.iCompartmentT.present[index+firstAge-1];
                vNbIndivForEachHealthStates[2] = iHealthStateCompartmentsContainer.iCompartmentL.present[index+firstAge-1];
                vNbIndivForEachHealthStates[3] = iHealthStateCompartmentsContainer.iCompartmentIs.present[index+firstAge-1];
                vNbIndivForEachHealthStates[4] = iHealthStateCompartmentsContainer.iCompartmentIc.present[index+firstAge-1];
                
                testAndRemoveAnimals(ParamRep, vNbIndivForEachHealthStates, firstAge, iMetapopSummary, index);
            }
        }
    }
}



void DairyHerd::testAndRemoveAnimals(const ParametersSpecificToEachRepetition& ParamRep, const std::vector<int>& vNbIndivForEachHealthStates, const int& firstAge, const MetapopSummary& iMetapopSummary, const int& index){
    for (int healthState = 0; healthState < vNbIndivForEachHealthStates.size(); healthState++) {
        if (vNbIndivForEachHealthStates[healthState] > 0) {
            for (int animal = 0; animal < vNbIndivForEachHealthStates[healthState]; animal++) {
                bool result = false;
                switch (healthState) {
                    case 0 :
                        result = gsl_ran_bernoulli(ParamRep.randomGenerator, 1-Parameters::testAndCullSp);
                        if (result == true) {
                            RemoveAnimalsAfterTest(ParamRep, iHealthStateCompartmentsContainer.iCompartmentSR, firstAge, iMetapopSummary, index);
                        }
                        break;
                    case 1 :
                        result = gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::testAndCullSeT);
                        if (result == true) {
                            RemoveAnimalsAfterTest(ParamRep, iHealthStateCompartmentsContainer.iCompartmentT, firstAge, iMetapopSummary, index);
                        }
                        break;
                    case 2 :
                        result = gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::testAndCullSeL);
                        if (result == true) {
                            RemoveAnimalsAfterTest(ParamRep, iHealthStateCompartmentsContainer.iCompartmentL, firstAge, iMetapopSummary, index);
                        }
                        break;
                    case 3 :
                        result = gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::testAndCullSeIs);
                        if (result == true) {
                            RemoveAnimalsAfterTest(ParamRep, iHealthStateCompartmentsContainer.iCompartmentIs, firstAge, iMetapopSummary, index);
                        }
                        break;
                    case 4 :
                        result = gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::testAndCullSeIc);
                        if (result == true) {
                            RemoveAnimalsAfterTest(ParamRep, iHealthStateCompartmentsContainer.iCompartmentIc, firstAge, iMetapopSummary, index);
                        }
                        break;
                    default : throw std::runtime_error("Wrong status in test-and-cull function.");
                }
            }
        }
    }
}



void DairyHerd::RemoveAnimalsAfterTest(const ParametersSpecificToEachRepetition& ParamRep, AgeCompartments& iCompartment, const int& firstAge, const MetapopSummary& iMetapopSummary, const int& index){
    assert(iCompartment.present[index+firstAge-1]>0);
    if (Parameters::proportionOfAnimalsRemoved < 1.0) {
        if (gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::proportionOfAnimalsRemoved)) {
            iCompartment.present[index+firstAge-1]--;
            numberOfAnimalsCulledDuringTestAndCull++;
            if (Parameters::replacementAfterTestAndCull) {
                statusChoice(ParamRep, iMetapopSummary, index+firstAge-1);
                //statusChoice(ParamRep, iMetapopSummary, Parameters::ageFirstCalving+1);
            }
        }
    } else {
        iCompartment.present[index+firstAge-1]--;
        numberOfAnimalsCulledDuringTestAndCull++;
        if (Parameters::replacementAfterTestAndCull) {
            statusChoice(ParamRep, iMetapopSummary, index+firstAge-1);
            //statusChoice(ParamRep, iMetapopSummary, Parameters::ageFirstCalving+1);
        }
    }
}



void DairyHerd::statusChoice(const ParametersSpecificToEachRepetition& ParamRep, const MetapopSummary& iMetapopSummary, const int& age){
    std::vector<double> vProbs(4);
    
    double SR = static_cast<double>(iMetapopSummary.pastSR[age]);
    double T = static_cast<double>(iMetapopSummary.pastT[age]);
    double L = static_cast<double>(iMetapopSummary.pastL[age]);
    double Is = static_cast<double>(iMetapopSummary.pastIs[age]);
    
    double N = SR + T + L + Is;
    
    vProbs[0] = (SR / N) * Parameters::testAndCullSp;
    vProbs[1] = (T / N) * (1-Parameters::testAndCullSeT);
    vProbs[2] = (L / N) * (1-Parameters::testAndCullSeL);
    vProbs[3] = (Is / N) * (1-Parameters::testAndCullSeIs);
    
    std::vector<int> vIndivNbForEachCompart = U_functions::multinomialDistribution(ParamRep.randomGenerator, 1, vProbs);
    
    //SR
    if (vIndivNbForEachCompart[0]>0) {
        ++iHealthStateCompartmentsContainer.iCompartmentSR.present[age];
    } else if (vIndivNbForEachCompart[1]>0) {
        ++iHealthStateCompartmentsContainer.iCompartmentT.present[age];
    } else if (vIndivNbForEachCompart[2]>0) {
        ++iHealthStateCompartmentsContainer.iCompartmentL.present[age];
    } else if (vIndivNbForEachCompart[3]>0) {
        ++iHealthStateCompartmentsContainer.iCompartmentIs.present[age];
    }
}



void DairyHerd::updateBufferPersistentResults(const ParametersSpecificToEachRepetition& ParamRep, const int& timeStep, BufferPersistentResults& iBufferPersistentResults, const int& run){
    
    if (persistenceIndicator == true) {
        iBufferPersistentResults.bInfectedHerds[timeStep]++;
        if (iSummaryDynamic.affected[timeStep] > 0) {
            iBufferPersistentResults.bAffectedHerds[timeStep]++;
        }
    }
    
    if (hasAlreadyBeenInfected == false) {
        iBufferPersistentResults.bNeverInfectedHerds[timeStep]++;
    } else {
        iBufferPersistentResults.bAlreadyInfectedHerds[timeStep]++;
    }
    
    
    if (typeInfectedHerd == 2) {
        if (anExtinctionOccurred == true) {
            anExtinctionOccurred = false;
            
            std::vector<std::string> v(16);
            
            v[0] = iParametersSpecificToEachHerd.farmId;
            v[1] = std::to_string(run);
            v[2] = std::to_string(StartDateOfTheLastPeriodOfInfection);
            v[3] = std::to_string(numberOfWeeksSinceInfected);
            v[4] = std::to_string(numberOfInfection);
            v[5] = std::to_string(numberOfPurchasesOfInfectedAnimalsT + numberOfPurchasesOfInfectedAnimalsL + numberOfPurchasesOfInfectedAnimalsIs);
            v[6] = std::to_string(numberOfPurchasesOfInfectedAnimalsT);
            v[7] = std::to_string(numberOfPurchasesOfInfectedAnimalsL);
            v[8] = std::to_string(numberOfPurchasesOfInfectedAnimalsIs);
            v[9] = std::to_string(numberOfSalesOfInfectedAnimalsT);
            v[10] = std::to_string(numberOfSalesOfInfectedAnimalsL);
            v[11] = std::to_string(numberOfSalesOfInfectedAnimalsIs);
            v[12] = std::to_string(prevalenceMaxForTheLastPeriodOfInfection);
            v[13] = std::to_string(cumulativeNumberOfIcForTheLastPeriodOfInfection);
            v[14] = std::to_string(numberOfWeeksWithInfectedAnimalsForTheLastPeriodOfInfection);
            v[15] = std::to_string(typeOfFirstAnimalPurchase);
            
            iBufferPersistentResults.vBufferInfectionDurationWhenExtinction.emplace_back(v);
            
            if (numberOfWeeksSinceInfected <= Parameters::timestepInOneYear*1) {
                iBufferPersistentResults.vBufferExtinctionBefore1Years.emplace_back(v);
            } else if (numberOfWeeksSinceInfected <= Parameters::timestepInOneYear*2) {
                iBufferPersistentResults.vBufferExtinctionBefore2Years.emplace_back(v);
            } else if (numberOfWeeksSinceInfected <= Parameters::timestepInOneYear*3) {
                iBufferPersistentResults.vBufferExtinctionBefore3Years.emplace_back(v);
            } else if (numberOfWeeksSinceInfected <= Parameters::timestepInOneYear*5) {
                iBufferPersistentResults.vBufferExtinctionBefore5Years.emplace_back(v);
            }
            
            
            if (numberOfWeeksSinceInfected < Parameters::timestepInOneYear*5) {
                if (StartDateOfTheLastPeriodOfInfection <= Parameters::simutime - Parameters::timestepInOneYear*5) {
                    std::vector<std::string> vPersistenceDynamic;
                    vPersistenceDynamic.emplace_back(iParametersSpecificToEachHerd.farmId);
                    vPersistenceDynamic.emplace_back(std::to_string(numberOfWeeksSinceInfected));
                    vPersistenceDynamic.emplace_back(std::to_string(StartDateOfTheLastPeriodOfInfection));
                    vPersistenceDynamic.emplace_back(std::to_string(numberOfPurchasesOfInfectedAnimalsT));
                    vPersistenceDynamic.emplace_back(std::to_string(numberOfPurchasesOfInfectedAnimalsL));
                    vPersistenceDynamic.emplace_back(std::to_string(numberOfPurchasesOfInfectedAnimalsIs));
                    vPersistenceDynamic.emplace_back(std::to_string(numberOfPurchasesOfInfectedAnimalsT + numberOfPurchasesOfInfectedAnimalsL + numberOfPurchasesOfInfectedAnimalsIs));
                    
                    iBufferPersistentResults.vvBufferPersistenceOver5Years.emplace_back(vPersistenceDynamic);
                    
                    //                    std::vector<double> vPersistence;
                    //                    vPersistence.resize(Parameters::timestepInOneYear*5, 0);
                    //                    for (int i = 0; i < numberOfWeeksSinceInfected; i++) {
                    //                        vPersistence[i] = 1;
                    //                    }
                    
                    //                    vPersistenceDynamic.insert(vPersistenceDynamic.end(), vPersistence.begin(), vPersistence.end());
                    
                    //                    iBufferPersistentResults.vvBufferPersistenceDynamicOver5Years.emplace_back(vPersistenceDynamic);
                }
            }
            
            numberOfWeeksSinceInfected = 0;
            numberOfWeeksSinceInfectious = 0;
            dateOfAppearanceOfTheFirstIc = 0;
            
            numberOfPurchasesOfInfectedAnimalsT = 0;
            numberOfPurchasesOfInfectedAnimalsL = 0;
            numberOfPurchasesOfInfectedAnimalsIs = 0;
            numberOfSalesOfInfectedAnimalsT = 0;
            numberOfSalesOfInfectedAnimalsL = 0;
            numberOfSalesOfInfectedAnimalsIs = 0;
            prevalenceMaxForTheLastPeriodOfInfection = 0;
            cumulativeNumberOfIcForTheLastPeriodOfInfection = 0;
            numberOfWeeksWithInfectedAnimalsForTheLastPeriodOfInfection = 0;
            typeOfFirstAnimalPurchase = 0;
            
        }
        
        
        
        if (persistenceIndicator) {
            
            if (timeStep == Parameters::simutime-1) {
                std::vector<std::string> v(16);
                
                v[0] = iParametersSpecificToEachHerd.farmId;
                v[1] = std::to_string(run);
                v[2] = std::to_string(StartDateOfTheLastPeriodOfInfection);
                v[3] = std::to_string(numberOfWeeksSinceInfected);
                v[4] = std::to_string(numberOfInfection);
                v[5] = std::to_string(numberOfPurchasesOfInfectedAnimalsT + numberOfPurchasesOfInfectedAnimalsL + numberOfPurchasesOfInfectedAnimalsIs);
                v[6] = std::to_string(numberOfPurchasesOfInfectedAnimalsT);
                v[7] = std::to_string(numberOfPurchasesOfInfectedAnimalsL);
                v[8] = std::to_string(numberOfPurchasesOfInfectedAnimalsIs);
                v[9] = std::to_string(numberOfSalesOfInfectedAnimalsT);
                v[10] = std::to_string(numberOfSalesOfInfectedAnimalsL);
                v[11] = std::to_string(numberOfSalesOfInfectedAnimalsIs);
                v[12] = std::to_string(prevalenceMaxForTheLastPeriodOfInfection);
                v[13] = std::to_string(cumulativeNumberOfIcForTheLastPeriodOfInfection);
                v[14] = std::to_string(numberOfWeeksWithInfectedAnimalsForTheLastPeriodOfInfection);
                v[15] = std::to_string(static_cast<double>(iSummaryDynamic.infected[Parameters::simutime-1])/static_cast<double>(iSummaryDynamic.headcount[Parameters::simutime-1]));
                
                iBufferPersistentResults.vBufferInfectionDurationIfNoExtinction.emplace_back(v);
            }
            
            if (numberOfWeeksSinceInfected == Parameters::timestepInOneYear*5+1) {
                std::vector<std::string> v(16);
                
                v[0] = iParametersSpecificToEachHerd.farmId;
                v[1] = std::to_string(run);
                v[2] = std::to_string(StartDateOfTheLastPeriodOfInfection);
                v[3] = std::to_string(numberOfWeeksSinceInfected);
                v[4] = std::to_string(numberOfInfection);
                v[5] = std::to_string(numberOfPurchasesOfInfectedAnimalsT + numberOfPurchasesOfInfectedAnimalsL + numberOfPurchasesOfInfectedAnimalsIs);
                v[6] = std::to_string(numberOfPurchasesOfInfectedAnimalsT);
                v[7] = std::to_string(numberOfPurchasesOfInfectedAnimalsL);
                v[8] = std::to_string(numberOfPurchasesOfInfectedAnimalsIs);
                v[9] = std::to_string(numberOfSalesOfInfectedAnimalsT);
                v[10] = std::to_string(numberOfSalesOfInfectedAnimalsL);
                v[11] = std::to_string(numberOfSalesOfInfectedAnimalsIs);
                v[12] = std::to_string(prevalenceMaxForTheLastPeriodOfInfection);
                v[13] = std::to_string(cumulativeNumberOfIcForTheLastPeriodOfInfection);
                v[14] = std::to_string(numberOfWeeksWithInfectedAnimalsForTheLastPeriodOfInfection);
                //                v[15] = static_cast<double>(iSummaryDynamic.infected[timeStep-1])/static_cast<double>(iSummaryDynamic.headcount[timeStep-1]);
                v[15] = std::to_string(typeOfFirstAnimalPurchase);
                
                iBufferPersistentResults.vBufferNoExtinctionBefore5Years.emplace_back(v);
                
            }
            
            
            if (numberOfWeeksSinceInfected == Parameters::timestepInOneYear*5) {
                
                int endDate = timeStep;
                
                double prevalenceInfected = static_cast<double>(iSummaryDynamic.infected[endDate]) / static_cast<double>(iSummaryDynamic.headcount[endDate]);
                double prevalenceInfectious = static_cast<double>(iSummaryDynamic.infectious[endDate]) / static_cast<double>(iSummaryDynamic.headcount[endDate]);
                double prevalenceAffected = static_cast<double>(iSummaryDynamic.affected[endDate]) / static_cast<double>(iSummaryDynamic.headcount[endDate]);
                
                std::vector<std::string> v2(14);
                v2[0] = iParametersSpecificToEachHerd.farmId;
                v2[1] = std::to_string(StartDateOfTheLastPeriodOfInfection);
                v2[2] = std::to_string(typeOfFirstAnimalPurchase);
                v2[3] = std::to_string(dateOfAppearanceOfTheFirstIc);
                v2[4] = std::to_string(cumulativeNumberOfIcForTheLastPeriodOfInfection);
                v2[5] = std::to_string(numberOfPurchasesOfInfectedAnimalsT);
                v2[6] = std::to_string(numberOfPurchasesOfInfectedAnimalsL);
                v2[7] = std::to_string(numberOfPurchasesOfInfectedAnimalsIs);
                v2[8] = std::to_string(numberOfSalesOfInfectedAnimalsT);
                v2[9] = std::to_string(numberOfSalesOfInfectedAnimalsL);
                v2[10] = std::to_string(numberOfSalesOfInfectedAnimalsIs);
                v2[11] = std::to_string(prevalenceInfected);
                v2[12] = std::to_string(prevalenceInfectious);
                v2[13] = std::to_string(prevalenceAffected);
                
                iBufferPersistentResults.vvBufferInfosHerdPrevalenceAfter5Years.emplace_back(v2);
                
                
                //                int startDate = StartDateOfTheLastPeriodOfInfection; //timeStep-5*Parameters::timestepInOneYear; // ajouter une verification sur la durée avec la différence entre startDate et endDate
                //                std::vector<double> vPrevalenceInfected = U_functions::VectorDivision(iSummaryDynamic.infected, iSummaryDynamic.headcount);
                //                std::vector<double> vPrevalenceInfected5years;
                //                vPrevalenceInfected5years.emplace_back(iParametersSpecificToEachHerd.herdID);
                //                vPrevalenceInfected5years.emplace_back(StartDateOfTheLastPeriodOfInfection);
                //                vPrevalenceInfected5years.emplace_back(numberOfPurchasesOfInfectedAnimalsT);
                //                vPrevalenceInfected5years.emplace_back(numberOfPurchasesOfInfectedAnimalsL);
                //                vPrevalenceInfected5years.emplace_back(numberOfPurchasesOfInfectedAnimalsIs);
                //                vPrevalenceInfected5years.insert(vPrevalenceInfected5years.end(), vPrevalenceInfected.begin()+startDate, vPrevalenceInfected.begin()+endDate);
                //                iBufferPersistentResults.vvBufferDynamicHerdPrevalenceInfectedAfter5Years.emplace_back(vPrevalenceInfected5years);
                
                //                std::vector<double> vPrevalenceInfectious = U_functions::VectorDivision(iSummaryDynamic.infectious, iSummaryDynamic.headcount);
                //                std::vector<double> vPrevalenceInfectious5years;
                //                vPrevalenceInfectious5years.insert(vPrevalenceInfectious5years.end(), vPrevalenceInfectious.begin()+startDate, vPrevalenceInfectious.begin()+endDate);
                //                iBufferPersistentResults.vvBufferDynamicHerdPrevalenceInfectiousAfter5Years.emplace_back(vPrevalenceInfectious5years);
                //
                //                std::vector<double> vPrevalenceAffected = U_functions::VectorDivision(iSummaryDynamic.affected, iSummaryDynamic.headcount);
                //                std::vector<double> vPrevalenceAffected5years;
                //                vPrevalenceAffected5years.insert(vPrevalenceAffected5years.end(), vPrevalenceAffected.begin()+startDate, vPrevalenceAffected.begin()+endDate);
                //                iBufferPersistentResults.vvBufferDynamicHerdPrevalenceAffectedAfter5Years.emplace_back(vPrevalenceAffected5years);
                
                
                if (StartDateOfTheLastPeriodOfInfection <= Parameters::simutime - Parameters::timestepInOneYear*5) {
                    std::vector<std::string> vPersistenceDynamic;
                    vPersistenceDynamic.emplace_back(iParametersSpecificToEachHerd.farmId);
                    vPersistenceDynamic.emplace_back(std::to_string(numberOfWeeksSinceInfected));
                    vPersistenceDynamic.emplace_back(std::to_string(StartDateOfTheLastPeriodOfInfection));
                    vPersistenceDynamic.emplace_back(std::to_string(numberOfPurchasesOfInfectedAnimalsT));
                    vPersistenceDynamic.emplace_back(std::to_string(numberOfPurchasesOfInfectedAnimalsL));
                    vPersistenceDynamic.emplace_back(std::to_string(numberOfPurchasesOfInfectedAnimalsIs));
                    vPersistenceDynamic.emplace_back(std::to_string(numberOfPurchasesOfInfectedAnimalsT + numberOfPurchasesOfInfectedAnimalsL + numberOfPurchasesOfInfectedAnimalsIs));
                    
                    iBufferPersistentResults.vvBufferPersistenceOver5Years.emplace_back(vPersistenceDynamic);
                    
                    //                    std::vector<double> vPersistence;
                    //                    vPersistence.resize(Parameters::timestepInOneYear*5, 1);
                    
                    //                    vPersistenceDynamic.insert(vPersistenceDynamic.end(), vPersistence.begin(), vPersistence.end());
                    
                    //                    iBufferPersistentResults.vvBufferPersistenceDynamicOver5Years.emplace_back(vPersistenceDynamic);
                }
                
            }
            
        }
        
    }
    
    int pos = iParametersSpecificToEachHerd.farmIndex;
    
    if (timeStep == round((Parameters::simutime-1)/2)) {
        iBufferPersistentResults.vBufferMatrixOfInfectedHerdsAfterHalfTheSimulationTime[pos] = iSummaryInfection.persistence[timeStep];
        iBufferPersistentResults.vBufferMatrixOfAlreadyInfectedHerdsAfterHalfTheSimulationTime[pos] = hasAlreadyBeenInfected;
    }
    
    if (timeStep == Parameters::simutime-1) {
        iBufferPersistentResults.vBufferMatrixOfInfectedHerdsAtTheEndOfTheSimulationTime[pos] = iSummaryInfection.persistence[timeStep];
        iBufferPersistentResults.vBufferMatrixOfAlreadyInfectedHerdsAtTheEndOfTheSimulationTime[pos] = hasAlreadyBeenInfected;
        
        iBufferPersistentResults.vBufferMatrixOfNumberOfInfection[pos] = numberOfInfection;
        iBufferPersistentResults.vBufferMatrixOfNumberOfInfectedAnimalsPurchased[pos] = numberOfInfectedAnimalsPurchased;
        iBufferPersistentResults.vBufferMatrixOfNumberOfWeeksWithInfection[pos] = numberOfWeeksWithInfection;
        
        iBufferPersistentResults.vBufferMatrixOfNumberOfIcAnimalsCulled[pos] = IcCulled;
        iBufferPersistentResults.vBufferMatrixOfNumberOfAnimalsCulledDuringTestAndCull[pos] = numberOfAnimalsCulledDuringTestAndCull;
        iBufferPersistentResults.vBufferMatrixOfNumberOfAnimalsTestedDuringTestAndCull[pos] = numberOfAnimalsTestedDuringTestAndCull;
        
//    }
//    
//    if (timeStep == Parameters::simutime-1) {
        if (typeInfectedHerd == 2) {
            if (numberOfAnimalInfectedSendWhenNotInitiallyInfected > 0) {
                std::vector<std::string> v(4);
                v[0] = iParametersSpecificToEachHerd.farmId;
                v[1] = std::to_string(numberOfAnimalInfectedSendWhenNotInitiallyInfected);
                v[2] = std::to_string(numberOfAnimalInfectedSendToMetapopWhenNotInitiallyInfected);
                v[3] = std::to_string(numberOfWeeksWithInfection);
                
                iBufferPersistentResults.vBufferNbInfectedMovementsPerHerd.emplace_back(v);
            }
        }


        iBufferPersistentResults.buffer_mFarms_vectPrevalence[iParametersSpecificToEachHerd.farmId] = U_functions::VectorDivision(iSummaryDynamic.infected, iSummaryDynamic.headcount);
    }
}


void DairyHerd::simulateLastSampling(ParametersSpecificToEachRepetition& ParamRep, const int& timeStep){
    if (timeStep == iParametersSpecificToEachHerd.lastSamplingDate) {
         double observedSamplesCount = Parameters::mFarmsCharacteristics[iParametersSpecificToEachHerd.farmId].lastSamplesCount;
        // double observedPositiveSamplesCount = Parameters::mFarmsCharacteristics[iParametersSpecificToEachHerd.farmId].lastPositiveSamplesCount;
        double observedProportionOfSample = Parameters::mFarmsCharacteristics[iParametersSpecificToEachHerd.farmId].proportionOfSample;
        double observedProportionOfPositiveSample = Parameters::mFarmsCharacteristics[iParametersSpecificToEachHerd.farmId].proportionOfPositiveSample;
        double simulatedPositiveSampleCount = 0;
        
        int firstAge = 104;
        int lastAge = 135;
        
        std::vector<int> vProbs(lastAge-firstAge+1, 0);
        std::vector<int> vInfCount(lastAge-firstAge+1, 0);
        for (int age = firstAge-1; age < lastAge; age++) {
            vProbs[age-firstAge+1] = iHealthStateCompartmentsContainer.iCompartmentSR.present[age]+iHealthStateCompartmentsContainer.iCompartmentT.present[age]+iHealthStateCompartmentsContainer.iCompartmentL.present[age]+iHealthStateCompartmentsContainer.iCompartmentIs.present[age]+iHealthStateCompartmentsContainer.iCompartmentIc.present[age];
            vInfCount[age-firstAge+1] = iHealthStateCompartmentsContainer.iCompartmentT.present[age]+iHealthStateCompartmentsContainer.iCompartmentL.present[age]+iHealthStateCompartmentsContainer.iCompartmentIs.present[age]+iHealthStateCompartmentsContainer.iCompartmentIc.present[age];
        }
        double N = std::accumulate(vProbs.begin(), vProbs.end(), 0);
        double Ninf = std::accumulate(vInfCount.begin(), vInfCount.end(), 0);
        // double nbAnimalsTested = std::min(observedSamplesCount,N);
        double nbAnimalsTested = round(observedProportionOfSample*N);

        //FOR TEST
        // nbAnimalsTested = std::min(observedSamplesCount, N);
        //FOR TEST
        
        if (nbAnimalsTested < N) {
            
            std::vector<int> vAgeList = U_functions::ListAgeOfAnimals(vProbs);
            std::vector<int> vAgeBuffer = U_functions::RandomSamplingWithoutReplacement(ParamRep.randomGenerator, vAgeList, nbAnimalsTested);
            std::vector<int> vNbIndivForEachAge(vProbs.size(),0);
            for (int index = 0; index < vAgeBuffer.size(); index++) {
                vNbIndivForEachAge[vAgeBuffer[index]] ++;
            }
            
            
            for (int index = 0; index < vNbIndivForEachAge.size(); index++) {
                if (vNbIndivForEachAge[index] > 0) {
                    std::vector<int> vHealthStates(5, 0);
                    
                    vHealthStates[0] = iHealthStateCompartmentsContainer.iCompartmentSR.present[index+firstAge-1];
                    vHealthStates[1] = iHealthStateCompartmentsContainer.iCompartmentT.present[index+firstAge-1];
                    vHealthStates[2] = iHealthStateCompartmentsContainer.iCompartmentL.present[index+firstAge-1];
                    vHealthStates[3] = iHealthStateCompartmentsContainer.iCompartmentIs.present[index+firstAge-1];
                    vHealthStates[4] = iHealthStateCompartmentsContainer.iCompartmentIc.present[index+firstAge-1];
                    
                    std::vector<int> vHealthStatesList = U_functions::ListHealthStatesOfAnimals(vHealthStates);
                    //                std::vector<int> vHealthStatesBuffer = U_functions::RandomSamplingWithoutReplacement(vHealthStatesList, vNbIndivForEachAge[index]);
                    std::vector<int> vHealthStatesBuffer = U_functions::RandomSamplingWithoutReplacement(ParamRep.randomGenerator, vHealthStatesList, vNbIndivForEachAge[index]);
                    std::vector<int> vNbIndivForEachHealthStates(vHealthStates.size(),0);
                    for (int index = 0; index < vHealthStatesBuffer.size(); index++) {
                        vNbIndivForEachHealthStates[vHealthStatesBuffer[index]] ++;
                    }
                    
                    int countSR = vNbIndivForEachHealthStates[0];
                    int countT = vNbIndivForEachHealthStates[1];
                    int countL = vNbIndivForEachHealthStates[2];
                    int countIs = vNbIndivForEachHealthStates[3];
                    int countIc = vNbIndivForEachHealthStates[4];
                    
                    int simulatedPositiveSR = gsl_ran_binomial(ParamRep.randomGenerator, 1.-Parameters::abcTestSpecificity, countSR);
                    int simulatedPositiveT = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countT);
                    int simulatedPositiveL = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countL);
                    int simulatedPositiveIs = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countIs);
                    int simulatedPositiveIc = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countIc);
                    
                    simulatedPositiveSampleCount += (simulatedPositiveSR + simulatedPositiveT + simulatedPositiveL + simulatedPositiveIs + simulatedPositiveIc);
                    
                    iHealthStateCompartmentsContainer.iCompartmentSR.present[index+firstAge-1] -= simulatedPositiveSR;
                    iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[index+firstAge-1] += simulatedPositiveSR;
                    
                    iHealthStateCompartmentsContainer.iCompartmentT.present[index+firstAge-1] -= simulatedPositiveT;
                    iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[index+firstAge-1] += simulatedPositiveT;
                    
                    iHealthStateCompartmentsContainer.iCompartmentL.present[index+firstAge-1] -= simulatedPositiveL;
                    iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[index+firstAge-1] += simulatedPositiveL;
                    
                    iHealthStateCompartmentsContainer.iCompartmentIs.present[index+firstAge-1] -= simulatedPositiveIs;
                    iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[index+firstAge-1] += simulatedPositiveIs;
                    
                    iHealthStateCompartmentsContainer.iCompartmentIc.present[index+firstAge-1] -= simulatedPositiveIc;
                    iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[index+firstAge-1] += simulatedPositiveIc;
                }
            }
            
        } else {
            std::vector<int> vNbIndivForEachAge(vProbs.begin(), vProbs.end());
            
            for (int index = 0; index < vNbIndivForEachAge.size(); index++) {
                if (vNbIndivForEachAge[index] > 0) {
                    std::vector<int> vNbIndivForEachHealthStates(5, 0);
                    
                    vNbIndivForEachHealthStates[0] = iHealthStateCompartmentsContainer.iCompartmentSR.present[index+firstAge-1];
                    vNbIndivForEachHealthStates[1] = iHealthStateCompartmentsContainer.iCompartmentT.present[index+firstAge-1];
                    vNbIndivForEachHealthStates[2] = iHealthStateCompartmentsContainer.iCompartmentL.present[index+firstAge-1];
                    vNbIndivForEachHealthStates[3] = iHealthStateCompartmentsContainer.iCompartmentIs.present[index+firstAge-1];
                    vNbIndivForEachHealthStates[4] = iHealthStateCompartmentsContainer.iCompartmentIc.present[index+firstAge-1];
                    
                    int countSR = vNbIndivForEachHealthStates[0];
                    int countT = vNbIndivForEachHealthStates[1];
                    int countL = vNbIndivForEachHealthStates[2];
                    int countIs = vNbIndivForEachHealthStates[3];
                    int countIc = vNbIndivForEachHealthStates[4];
                    
                    int simulatedPositiveSR = gsl_ran_binomial(ParamRep.randomGenerator, 1.-Parameters::abcTestSpecificity, countSR);
                    int simulatedPositiveT = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countT);
                    int simulatedPositiveL = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countL);
                    int simulatedPositiveIs = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countIs);
                    int simulatedPositiveIc = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countIc);
                    
                    simulatedPositiveSampleCount += (simulatedPositiveSR + simulatedPositiveT + simulatedPositiveL + simulatedPositiveIs + simulatedPositiveIc);
                    
                    iHealthStateCompartmentsContainer.iCompartmentSR.present[index+firstAge-1] -= simulatedPositiveSR;
                    iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[index+firstAge-1] += simulatedPositiveSR;
                    
                    iHealthStateCompartmentsContainer.iCompartmentT.present[index+firstAge-1] -= simulatedPositiveT;
                    iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[index+firstAge-1] += simulatedPositiveT;
                    
                    iHealthStateCompartmentsContainer.iCompartmentL.present[index+firstAge-1] -= simulatedPositiveL;
                    iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[index+firstAge-1] += simulatedPositiveL;
                    
                    iHealthStateCompartmentsContainer.iCompartmentIs.present[index+firstAge-1] -= simulatedPositiveIs;
                    iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[index+firstAge-1] += simulatedPositiveIs;
                    
                    iHealthStateCompartmentsContainer.iCompartmentIc.present[index+firstAge-1] -= simulatedPositiveIc;
                    iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[index+firstAge-1] += simulatedPositiveIc;
                }
            }
        }
        
        if (Parameters::ABCrejection == true | Parameters::ABCsmc == true | Parameters::ABCcompleteTrajectories == true | Parameters::computeSummaryStatistics == true) {
            if (((timeStep != iParametersSpecificToEachHerd.beginDate) &  (Parameters::ABCrejection == true | Parameters::ABCsmc == true)) | (Parameters::ABCcompleteTrajectories == true | Parameters::computeSummaryStatistics == true)) {
                // double temp = observedPositiveSamplesCount - simulatedPositiveSampleCount;
                double temp = simulatedPositiveSampleCount/nbAnimalsTested - observedProportionOfPositiveSample;
                double theDistance = pow(temp, 2.0);
                distanceFromObservation += theDistance;
                ParamRep.distanceBetweenSimulationAndObservationForThisRun += theDistance;
                
                if (Parameters::ABCcompleteTrajectories == true | Parameters::computeSummaryStatistics == true) {
                    double summaryStat = simulatedPositiveSampleCount/nbAnimalsTested;
                    vSummaryStatistics[timeStep] = summaryStat;
                    
                    vSummaryStatistics_N[timeStep] = static_cast<short int>(N);
                    vSummaryStatistics_N_pos[timeStep] = static_cast<short int>(Ninf);
                    //vSummaryStatistics_N_neg[timeStep];
                    vSummaryStatistics_Nech[timeStep] = static_cast<short int>(nbAnimalsTested);
                    vSummaryStatistics_Nech_pos[timeStep] = static_cast<short int>(simulatedPositiveSampleCount);
                    //vSummaryStatistics_Nech_neg[timeStep];
                }
            }
        }
    }
}


void DairyHerd::simulateSamplingAndMoveAnimalsInPositiveTestedCompartment(ParametersSpecificToEachRepetition& ParamRep, const int& timeStep){
    if (Parameters::managementOfPositiveTestedTakeIntoAccount) {
        if (iParametersSpecificToEachHerd.samplingDates.find(timeStep) != iParametersSpecificToEachHerd.samplingDates.end()) {
            double observedSamplesCount = iParametersSpecificToEachHerd.samplingDates[timeStep].observedSamplesCount;
//            double observedPositiveSamplesCount = iParametersSpecificToEachHerd.samplingDates[timeStep].observedPositiveSamplesCount;
            double observedProportionOfSample = iParametersSpecificToEachHerd.samplingDates[timeStep].proportionOfSample;
            double observedProportionOfPositiveSample = iParametersSpecificToEachHerd.samplingDates[timeStep].proportionOfPositiveSample;
            double simulatedPositiveSampleCount = 0;
            
            int firstAge = 104;
            int lastAge = 135;
            
            std::vector<int> vProbs(lastAge-firstAge+1, 0);
            std::vector<int> vInfCount(lastAge-firstAge+1, 0);
            for (int age = firstAge-1; age < lastAge; age++) {
                vProbs[age-firstAge+1] = iHealthStateCompartmentsContainer.iCompartmentSR.present[age]+iHealthStateCompartmentsContainer.iCompartmentT.present[age]+iHealthStateCompartmentsContainer.iCompartmentL.present[age]+iHealthStateCompartmentsContainer.iCompartmentIs.present[age]+iHealthStateCompartmentsContainer.iCompartmentIc.present[age];
                vInfCount[age-firstAge+1] = iHealthStateCompartmentsContainer.iCompartmentT.present[age]+iHealthStateCompartmentsContainer.iCompartmentL.present[age]+iHealthStateCompartmentsContainer.iCompartmentIs.present[age]+iHealthStateCompartmentsContainer.iCompartmentIc.present[age];
            }
            double N = std::accumulate(vProbs.begin(), vProbs.end(), 0);
            double Ninf = std::accumulate(vInfCount.begin(), vInfCount.end(), 0);
            // double nbAnimalsTested = std::min(observedSamplesCount,N);
            double nbAnimalsTested = round(observedProportionOfSample*N);

            //FOR TEST
            // nbAnimalsTested = std::min(observedSamplesCount, N);
            //FOR TEST

            if (nbAnimalsTested == 0) {
                std::cout << "nbAnimalsTested == 0 for farm: " << iParametersSpecificToEachHerd.farmId << " (N = " << N << ")" << std::endl;
            }
            
            if (nbAnimalsTested < N) {
                
                std::vector<int> vAgeList = U_functions::ListAgeOfAnimals(vProbs);
                std::vector<int> vAgeBuffer = U_functions::RandomSamplingWithoutReplacement(ParamRep.randomGenerator, vAgeList, nbAnimalsTested);
                std::vector<int> vNbIndivForEachAge(vProbs.size(),0);
                for (int index = 0; index < vAgeBuffer.size(); index++) {
                    vNbIndivForEachAge[vAgeBuffer[index]] ++;
                }
                
                
                for (int index = 0; index < vNbIndivForEachAge.size(); index++) {
                    if (vNbIndivForEachAge[index] > 0) {
                        std::vector<int> vHealthStates(5, 0);
                        
                        vHealthStates[0] = iHealthStateCompartmentsContainer.iCompartmentSR.present[index+firstAge-1];
                        vHealthStates[1] = iHealthStateCompartmentsContainer.iCompartmentT.present[index+firstAge-1];
                        vHealthStates[2] = iHealthStateCompartmentsContainer.iCompartmentL.present[index+firstAge-1];
                        vHealthStates[3] = iHealthStateCompartmentsContainer.iCompartmentIs.present[index+firstAge-1];
                        vHealthStates[4] = iHealthStateCompartmentsContainer.iCompartmentIc.present[index+firstAge-1];
                        
                        std::vector<int> vHealthStatesList = U_functions::ListHealthStatesOfAnimals(vHealthStates);
                        // std::vector<int> vHealthStatesBuffer = U_functions::RandomSamplingWithoutReplacement(vHealthStatesList, vNbIndivForEachAge[index]);
                        std::vector<int> vHealthStatesBuffer = U_functions::RandomSamplingWithoutReplacement(ParamRep.randomGenerator, vHealthStatesList, vNbIndivForEachAge[index]);
                        std::vector<int> vNbIndivForEachHealthStates(vHealthStates.size(),0);
                        for (int index = 0; index < vHealthStatesBuffer.size(); index++) {
                            vNbIndivForEachHealthStates[vHealthStatesBuffer[index]] ++;
                        }
                        
                        int countSR = vNbIndivForEachHealthStates[0];
                        int countT = vNbIndivForEachHealthStates[1];
                        int countL = vNbIndivForEachHealthStates[2];
                        int countIs = vNbIndivForEachHealthStates[3];
                        int countIc = vNbIndivForEachHealthStates[4];
                        
                        int simulatedPositiveSR = gsl_ran_binomial(ParamRep.randomGenerator, 1.-Parameters::abcTestSpecificity, countSR);
                        int simulatedPositiveT = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countT);
                        int simulatedPositiveL = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countL);
                        int simulatedPositiveIs = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countIs);
                        int simulatedPositiveIc = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countIc);
                        
                        simulatedPositiveSampleCount += (simulatedPositiveSR + simulatedPositiveT + simulatedPositiveL + simulatedPositiveIs + simulatedPositiveIc);
                        
                        iHealthStateCompartmentsContainer.iCompartmentSR.present[index+firstAge-1] -= simulatedPositiveSR;
                        iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[index+firstAge-1] += simulatedPositiveSR;
                        
                        iHealthStateCompartmentsContainer.iCompartmentT.present[index+firstAge-1] -= simulatedPositiveT;
                        iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[index+firstAge-1] += simulatedPositiveT;
                        
                        iHealthStateCompartmentsContainer.iCompartmentL.present[index+firstAge-1] -= simulatedPositiveL;
                        iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[index+firstAge-1] += simulatedPositiveL;
                        
                        iHealthStateCompartmentsContainer.iCompartmentIs.present[index+firstAge-1] -= simulatedPositiveIs;
                        iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[index+firstAge-1] += simulatedPositiveIs;
                        
                        iHealthStateCompartmentsContainer.iCompartmentIc.present[index+firstAge-1] -= simulatedPositiveIc;
                        iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[index+firstAge-1] += simulatedPositiveIc;
                    }
                }
                
            } else {
                std::vector<int> vNbIndivForEachAge(vProbs.begin(), vProbs.end());
                
                for (int index = 0; index < vNbIndivForEachAge.size(); index++) {
                    if (vNbIndivForEachAge[index] > 0) {
                        std::vector<int> vNbIndivForEachHealthStates(5, 0);
                        
                        vNbIndivForEachHealthStates[0] = iHealthStateCompartmentsContainer.iCompartmentSR.present[index+firstAge-1];
                        vNbIndivForEachHealthStates[1] = iHealthStateCompartmentsContainer.iCompartmentT.present[index+firstAge-1];
                        vNbIndivForEachHealthStates[2] = iHealthStateCompartmentsContainer.iCompartmentL.present[index+firstAge-1];
                        vNbIndivForEachHealthStates[3] = iHealthStateCompartmentsContainer.iCompartmentIs.present[index+firstAge-1];
                        vNbIndivForEachHealthStates[4] = iHealthStateCompartmentsContainer.iCompartmentIc.present[index+firstAge-1];
                        
                        int countSR = vNbIndivForEachHealthStates[0];
                        int countT = vNbIndivForEachHealthStates[1];
                        int countL = vNbIndivForEachHealthStates[2];
                        int countIs = vNbIndivForEachHealthStates[3];
                        int countIc = vNbIndivForEachHealthStates[4];
                        
                        int simulatedPositiveSR = gsl_ran_binomial(ParamRep.randomGenerator, 1.-Parameters::abcTestSpecificity, countSR);
                        int simulatedPositiveT = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countT);
                        int simulatedPositiveL = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countL);
                        int simulatedPositiveIs = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countIs);
                        int simulatedPositiveIc = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countIc);
                        
                        simulatedPositiveSampleCount += (simulatedPositiveSR + simulatedPositiveT + simulatedPositiveL + simulatedPositiveIs + simulatedPositiveIc);
                        
                        iHealthStateCompartmentsContainer.iCompartmentSR.present[index+firstAge-1] -= simulatedPositiveSR;
                        iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.present[index+firstAge-1] += simulatedPositiveSR;
                        
                        iHealthStateCompartmentsContainer.iCompartmentT.present[index+firstAge-1] -= simulatedPositiveT;
                        iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[index+firstAge-1] += simulatedPositiveT;
                        
                        iHealthStateCompartmentsContainer.iCompartmentL.present[index+firstAge-1] -= simulatedPositiveL;
                        iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[index+firstAge-1] += simulatedPositiveL;
                        
                        iHealthStateCompartmentsContainer.iCompartmentIs.present[index+firstAge-1] -= simulatedPositiveIs;
                        iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[index+firstAge-1] += simulatedPositiveIs;
                        
                        iHealthStateCompartmentsContainer.iCompartmentIc.present[index+firstAge-1] -= simulatedPositiveIc;
                        iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[index+firstAge-1] += simulatedPositiveIc;
                    }
                }
            }

            
            if (Parameters::ABCrejection == true | Parameters::ABCsmc == true | Parameters::ABCcompleteTrajectories == true | Parameters::computeSummaryStatistics == true) {
                if (((timeStep != iParametersSpecificToEachHerd.beginDate) &  (Parameters::ABCrejection == true | Parameters::ABCsmc == true)) | (Parameters::ABCcompleteTrajectories == true | Parameters::computeSummaryStatistics == true)) {
                    // double temp = observedPositiveSamplesCount - simulatedPositiveSampleCount;
                    double temp = simulatedPositiveSampleCount/nbAnimalsTested - observedProportionOfPositiveSample;
                    double theDistance = pow(temp, 2.0);
                    distanceFromObservation += theDistance;
                    ParamRep.distanceBetweenSimulationAndObservationForThisRun += theDistance;
                    
                    if (Parameters::ABCcompleteTrajectories == true | Parameters::computeSummaryStatistics == true) {
                        double summaryStat = simulatedPositiveSampleCount/nbAnimalsTested;
                        vSummaryStatistics[timeStep] = summaryStat;
                        
                        vSummaryStatistics_N[timeStep] = static_cast<short int>(N);
                        vSummaryStatistics_N_pos[timeStep] = static_cast<short int>(Ninf);
                        //vSummaryStatistics_N_neg[timeStep];
                        vSummaryStatistics_Nech[timeStep] = static_cast<short int>(nbAnimalsTested);
                        vSummaryStatistics_Nech_pos[timeStep] = static_cast<short int>(simulatedPositiveSampleCount);
                        //vSummaryStatistics_Nech_neg[timeStep];
                    }
                }
            }
            
            
        }
    }
}


void DairyHerd::simulateSamplingAndMoveAnimalsInPositiveTestedCompartmentForFirstTimeStep(ParametersSpecificToEachRepetition& ParamRep, const int& timeStep){
    if (Parameters::managementOfPositiveTestedTakeIntoAccount) {
        if (iParametersSpecificToEachHerd.samplingDates.find(timeStep) != iParametersSpecificToEachHerd.samplingDates.end()) {
            //std::cout << "simulateSamplingAndMoveAnimalsInPositiveTestedCompartmentForFirstTimeStep - farmId: " << iParametersSpecificToEachHerd.farmId << std::endl;
            
            double observedSamplesCount = iParametersSpecificToEachHerd.samplingDates[timeStep].observedSamplesCount;
//            double observedPositiveSamplesCount = iParametersSpecificToEachHerd.samplingDates[timeStep].observedPositiveSamplesCount;
            double observedProportionOfSample = iParametersSpecificToEachHerd.samplingDates[timeStep].proportionOfSample;
            double observedProportionOfPositiveSample = iParametersSpecificToEachHerd.samplingDates[timeStep].proportionOfPositiveSample;
            double simulatedPositiveSampleCount = 0;
            
            int firstAge = 104;
            int lastAge = 135;
            
            std::vector<int> vProbs(lastAge-firstAge+1, 0);
            std::vector<int> vInfCount(lastAge-firstAge+1, 0);
            for (int age = firstAge-1; age < lastAge; age++) {
                vProbs[age-firstAge+1] = iHealthStateCompartmentsContainer.iCompartmentSR.past[age]+iHealthStateCompartmentsContainer.iCompartmentT.past[age]+iHealthStateCompartmentsContainer.iCompartmentL.past[age]+iHealthStateCompartmentsContainer.iCompartmentIs.past[age]+iHealthStateCompartmentsContainer.iCompartmentIc.past[age];
                vInfCount[age-firstAge+1] = iHealthStateCompartmentsContainer.iCompartmentT.present[age]+iHealthStateCompartmentsContainer.iCompartmentL.present[age]+iHealthStateCompartmentsContainer.iCompartmentIs.present[age]+iHealthStateCompartmentsContainer.iCompartmentIc.present[age];
            }
            double N = std::accumulate(vProbs.begin(), vProbs.end(), 0);
            double Ninf = std::accumulate(vInfCount.begin(), vInfCount.end(), 0);
            // double nbAnimalsTested = std::min(observedSamplesCount,N);
            double nbAnimalsTested = round(observedProportionOfSample*N);

            //FOR TEST
            // nbAnimalsTested = std::min(observedSamplesCount, N);
            //FOR TEST
            
            if (nbAnimalsTested < N) {
                
                std::vector<int> vAgeList = U_functions::ListAgeOfAnimals(vProbs);
                std::vector<int> vAgeBuffer = U_functions::RandomSamplingWithoutReplacement(ParamRep.randomGenerator, vAgeList, nbAnimalsTested);
                std::vector<int> vNbIndivForEachAge(vProbs.size(),0);
                for (int index = 0; index < vAgeBuffer.size(); index++) {
                    vNbIndivForEachAge[vAgeBuffer[index]] ++;
                }
                
                
                for (int index = 0; index < vNbIndivForEachAge.size(); index++) {
                    if (vNbIndivForEachAge[index] > 0) {
                        std::vector<int> vHealthStates(5, 0);
                        
                        vHealthStates[0] = iHealthStateCompartmentsContainer.iCompartmentSR.past[index+firstAge-1];
                        vHealthStates[1] = iHealthStateCompartmentsContainer.iCompartmentT.past[index+firstAge-1];
                        vHealthStates[2] = iHealthStateCompartmentsContainer.iCompartmentL.past[index+firstAge-1];
                        vHealthStates[3] = iHealthStateCompartmentsContainer.iCompartmentIs.past[index+firstAge-1];
                        vHealthStates[4] = iHealthStateCompartmentsContainer.iCompartmentIc.past[index+firstAge-1];
                        
                        std::vector<int> vHealthStatesList = U_functions::ListHealthStatesOfAnimals(vHealthStates);
                        // std::vector<int> vHealthStatesBuffer = U_functions::RandomSamplingWithoutReplacement(vHealthStatesList, vNbIndivForEachAge[index]);
                        std::vector<int> vHealthStatesBuffer = U_functions::RandomSamplingWithoutReplacement(ParamRep.randomGenerator, vHealthStatesList, vNbIndivForEachAge[index]);
                        std::vector<int> vNbIndivForEachHealthStates(vHealthStates.size(),0);
                        for (int index = 0; index < vHealthStatesBuffer.size(); index++) {
                            vNbIndivForEachHealthStates[vHealthStatesBuffer[index]] ++;
                        }
                        
                        int countSR = vNbIndivForEachHealthStates[0];
                        int countT = vNbIndivForEachHealthStates[1];
                        int countL = vNbIndivForEachHealthStates[2];
                        int countIs = vNbIndivForEachHealthStates[3];
                        int countIc = vNbIndivForEachHealthStates[4];
                        
                        int simulatedPositiveSR = gsl_ran_binomial(ParamRep.randomGenerator, 1.-Parameters::abcTestSpecificity, countSR);
                        int simulatedPositiveT = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countT);
                        int simulatedPositiveL = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countL);
                        int simulatedPositiveIs = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countIs);
                        int simulatedPositiveIc = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countIc);
                        
                        simulatedPositiveSampleCount += (simulatedPositiveSR + simulatedPositiveT + simulatedPositiveL + simulatedPositiveIs + simulatedPositiveIc);
                        
                        iHealthStateCompartmentsContainer.iCompartmentSR.past[index+firstAge-1] -= simulatedPositiveSR;
                        iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[index+firstAge-1] += simulatedPositiveSR;
                        
                        iHealthStateCompartmentsContainer.iCompartmentT.past[index+firstAge-1] -= simulatedPositiveT;
                        iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.past[index+firstAge-1] += simulatedPositiveT;
                        
                        iHealthStateCompartmentsContainer.iCompartmentL.past[index+firstAge-1] -= simulatedPositiveL;
                        iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past[index+firstAge-1] += simulatedPositiveL;
                        
                        iHealthStateCompartmentsContainer.iCompartmentIs.past[index+firstAge-1] -= simulatedPositiveIs;
                        iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past[index+firstAge-1] += simulatedPositiveIs;
                        
                        iHealthStateCompartmentsContainer.iCompartmentIc.past[index+firstAge-1] -= simulatedPositiveIc;
                        iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[index+firstAge-1] += simulatedPositiveIc;
                    }
                }
                
            } else {
                std::vector<int> vNbIndivForEachAge(vProbs.begin(), vProbs.end());
                
                for (int index = 0; index < vNbIndivForEachAge.size(); index++) {
                    if (vNbIndivForEachAge[index] > 0) {
                        std::vector<int> vNbIndivForEachHealthStates(5, 0);
                        
                        vNbIndivForEachHealthStates[0] = iHealthStateCompartmentsContainer.iCompartmentSR.past[index+firstAge-1];
                        vNbIndivForEachHealthStates[1] = iHealthStateCompartmentsContainer.iCompartmentT.past[index+firstAge-1];
                        vNbIndivForEachHealthStates[2] = iHealthStateCompartmentsContainer.iCompartmentL.past[index+firstAge-1];
                        vNbIndivForEachHealthStates[3] = iHealthStateCompartmentsContainer.iCompartmentIs.past[index+firstAge-1];
                        vNbIndivForEachHealthStates[4] = iHealthStateCompartmentsContainer.iCompartmentIc.past[index+firstAge-1];
                        
                        int countSR = vNbIndivForEachHealthStates[0];
                        int countT = vNbIndivForEachHealthStates[1];
                        int countL = vNbIndivForEachHealthStates[2];
                        int countIs = vNbIndivForEachHealthStates[3];
                        int countIc = vNbIndivForEachHealthStates[4];
                        
                        int simulatedPositiveSR = gsl_ran_binomial(ParamRep.randomGenerator, 1.-Parameters::abcTestSpecificity, countSR);
                        int simulatedPositiveT = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countT);
                        int simulatedPositiveL = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countL);
                        int simulatedPositiveIs = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countIs);
                        int simulatedPositiveIc = gsl_ran_binomial(ParamRep.randomGenerator, ParamRep.abcTestSensitivity, countIc);
                        
                        simulatedPositiveSampleCount += (simulatedPositiveSR + simulatedPositiveT + simulatedPositiveL + simulatedPositiveIs + simulatedPositiveIc);
                        
                        iHealthStateCompartmentsContainer.iCompartmentSR.past[index+firstAge-1] -= simulatedPositiveSR;
                        iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.past[index+firstAge-1] += simulatedPositiveSR;
                        
                        iHealthStateCompartmentsContainer.iCompartmentT.past[index+firstAge-1] -= simulatedPositiveT;
                        iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.past[index+firstAge-1] += simulatedPositiveT;
                        
                        iHealthStateCompartmentsContainer.iCompartmentL.past[index+firstAge-1] -= simulatedPositiveL;
                        iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.past[index+firstAge-1] += simulatedPositiveL;
                        
                        iHealthStateCompartmentsContainer.iCompartmentIs.past[index+firstAge-1] -= simulatedPositiveIs;
                        iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.past[index+firstAge-1] += simulatedPositiveIs;
                        
                        iHealthStateCompartmentsContainer.iCompartmentIc.past[index+firstAge-1] -= simulatedPositiveIc;
                        iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.past[index+firstAge-1] += simulatedPositiveIc;
                    }
                }
            }
            
            if (Parameters::ABCrejection == true | Parameters::ABCsmc == true | Parameters::ABCcompleteTrajectories == true | Parameters::computeSummaryStatistics == true) {
                if (((timeStep != iParametersSpecificToEachHerd.beginDate) &  (Parameters::ABCrejection == true | Parameters::ABCsmc == true)) | (Parameters::ABCcompleteTrajectories == true | Parameters::computeSummaryStatistics == true)) {
                    // double temp = observedPositiveSamplesCount - simulatedPositiveSampleCount;
                    double temp = simulatedPositiveSampleCount/nbAnimalsTested - observedProportionOfPositiveSample;
                    double theDistance = pow(temp, 2.0);
                    distanceFromObservation += theDistance;
                    ParamRep.distanceBetweenSimulationAndObservationForThisRun += theDistance;
                    
                    if (Parameters::ABCcompleteTrajectories == true | Parameters::computeSummaryStatistics == true) {
                        double summaryStat = simulatedPositiveSampleCount/nbAnimalsTested;
                        vSummaryStatistics[timeStep] = summaryStat;
                        
                        vSummaryStatistics_N[timeStep] = static_cast<short int>(N);
                        vSummaryStatistics_N_pos[timeStep] = static_cast<short int>(Ninf);
                        //vSummaryStatistics_N_neg[timeStep];
                        vSummaryStatistics_Nech[timeStep] = static_cast<short int>(nbAnimalsTested);
                        vSummaryStatistics_Nech_pos[timeStep] = static_cast<short int>(simulatedPositiveSampleCount);
                        //vSummaryStatistics_Nech_neg[timeStep];
                    }
                }
            }
            
            
        }
    }
}



