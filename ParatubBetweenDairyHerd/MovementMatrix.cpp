//
//  MovementMatrix.cpp
//  ParatubBetweenDairyHerd
//
//  Created by Gaël Beaunée on 03/01/2014.
//  Copyright (c) 2014 Gaël Beaunée. All rights reserved.
//

#include "MovementMatrix.h"

// Static variables ====================================================
std::map<std::string, std::map<int, std::vector<MovementUnit>> > MovementMatrix::mmOutMoves;

// Constructor =========================================================
MovementMatrix *MovementMatrix::instance = NULL;


MovementMatrix& MovementMatrix::get_instance(){
    if (instance == NULL) {
        instance = new MovementMatrix();
    }
    return *instance;
}


void MovementMatrix::kill(){
    if (instance != NULL) {
        delete instance;
    }
}


MovementMatrix::MovementMatrix(){
    std::cout << "Build the MovementMatrix..." << std::endl << std::endl;
//    k = 1;
//    step= 7;
    
    loadTheNetworkData();
}


void MovementMatrix::loadTheNetworkData(){
    
    std::vector< std::vector<std::string> > matMov;
    
    // Construction of the initial headcounts array
    std::ifstream fichier(Parameters::folderPath + "data/Network_withId.csv", std::fstream::in); // Open file in reading mode
    // Check if the file is open!
    if(!fichier){std::cerr << "[ERROR] Unable to open the file of movements ..." << std::endl;}
    // For each line
    std::string line;
    while (std::getline(fichier, line)){
        // Construction of a line
        std::stringstream ss(line);
        std::vector<std::string> v;
        // Data extraction for the line
        std::string temp;
        while(ss >> temp){
            v.emplace_back(temp);
        }
        // The line is added to the array
        matMov.emplace_back(v);
    }
    
    
    for (auto & theMov : matMov){
//        for (int rep = 0; rep < ceil(Parameters::nbYears/9.); rep++) {
            std::string sourceID = theMov[0];
            std::string destination = theMov[1]; // Destination
            int age = std::stoi(theMov[3])-1; // Age (-1 pour correspondre à l'indexation du programme en c++)
//            int dateMov = std::stoi(theMov[2])+(rep*9*52)-1;
            int dateMov = std::stoi(theMov[2])-1; // Date (-1 pour correspondre à l'indexation du programme en c++)

            std::map<int, std::vector< MovementUnit>> mOutMoves;
            mmOutMoves.insert(std::pair< std::string,std::map<int, std::vector<MovementUnit>> >(sourceID,mOutMoves));
            
            MovementUnit mov = MovementUnit(destination, age);
            
            mmOutMoves[sourceID][dateMov].emplace_back(mov);
            
//        }
    }
    
}






