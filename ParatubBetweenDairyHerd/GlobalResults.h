//
//  GlobalResults.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 08/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_GlobalResults_h
#define ParatubBetweenDairyHerd_GlobalResults_h

#include <vector>
#include <boost/accumulators/numeric/functional/vector.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/sum.hpp>
#include <boost/accumulators/statistics/count.hpp>
#include <boost/accumulators/statistics/median.hpp>

#include "Parameters.h"
#include "DairyHerd.h"
#include "BufferGlobalResults.h"
#include "UtilitiesFunctions.h"

using namespace boost::accumulators;

typedef accumulator_set<std::vector<int>, stats<tag::mean, tag::variance, tag::count, tag::sum > > accVectInt;
typedef accumulator_set<std::vector<double>, stats<tag::mean, tag::variance, tag::count, tag::sum > > accVectDouble;

class GlobalResults {
public:
    // Constructor =========================================================
    GlobalResults();
        
    // Variables ===========================================================
    accVectInt accMetapopPersistence; //
    std::vector<accVectInt> vAccHeadcounts;
    std::vector<accVectInt> vAccAnnualHeadcounts;
    std::vector<accVectInt> vAccAnnualHeadcountsLact;
    std::vector<accVectDouble> vAccNbPres;
    std::vector<accVectDouble> vAccNbPresLact;
//    std::vector<accVectInt> vAccAnnualBirths;
//    std::vector<accVectDouble> vAccAnnualBirthsPerAgeGroup;
//    std::vector<accVectDouble> vAccBirthsPerAgeGroup;
    
    // Member functions ====================================================
    void updateAcc(BufferGlobalResults& iBufferGlobalResults, std::map<std::string, DairyHerd>& mFarms);
    
};

#endif
