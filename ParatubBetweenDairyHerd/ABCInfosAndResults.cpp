//
//  ABCInfosAndResults.cpp
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunée on 23/05/2015.
//  Copyright (c) 2015 Gaël Beaunée. All rights reserved.
//

#include "ABCInfosAndResults.h"

// Constructor =========================================================
ABCInfosAndResults::ABCInfosAndResults(){
    initializeABCsmc();
}

// Member functions ====================================================
void ABCInfosAndResults::initializeABCsmc(){
    
    threshold = Parameters::ABCsmcThreshold;
    
    if (Parameters::ABCsmcRunNumber > 1) {
        loadABCsmcParticlesForTheRun("probaToPurchaseInfectedAnimal_initialValue");
        loadABCsmcParticlesForTheRun("probaToPurchaseInfectedAnimal_slope");
        loadABCsmcParticlesForTheRun("abcTestSensitivity");
        loadABCsmcParticlesForTheRun("abcInfGlobalEnv");
    }
    
}


void ABCInfosAndResults::loadABCsmcParticlesForTheRun(const std::string& param){
    // load ABCsmcNextParticles_param.csv
    std::ifstream fileABCsmcNextParticles(Parameters::ABCfolderPath + "ABCsmcInfosAndParticlesForTheNextRun/ABCsmcNextParticles_" + param + ".csv", std::fstream::in); // Open file in reading mode
    if(!fileABCsmcNextParticles){std::cerr << "[ERROR] Unable to open the file 'ABCsmcNextParticles_" + param + ".csv' !!" << std::endl;} // Check if the file is open!
    // For each line:
    std::string lineABCsmcNextParticles;
    
    // previousParticleValues
    std::getline(fileABCsmcNextParticles, lineABCsmcNextParticles);
    std::stringstream ssPreviousParticleValues(lineABCsmcNextParticles);
    std::vector<double> vTempPreviousParticleValues;
    double tempPreviousParticleValues;
    while(ssPreviousParticleValues >> tempPreviousParticleValues){
        vTempPreviousParticleValues.emplace_back(tempPreviousParticleValues);
    }
    // previousParticleWeights
    std::getline(fileABCsmcNextParticles, lineABCsmcNextParticles);
    std::stringstream ssPreviousParticleWeights(lineABCsmcNextParticles);
    std::vector<double> vTempPreviousParticleWeights;
    double tempPreviousParticleWeights;
    while(ssPreviousParticleWeights >> tempPreviousParticleWeights){
        vTempPreviousParticleWeights.emplace_back(tempPreviousParticleWeights);
    }
    // previousParticleNormalizedWeights
    std::getline(fileABCsmcNextParticles, lineABCsmcNextParticles);
    std::stringstream ssPreviousParticleNormalizedWeights(lineABCsmcNextParticles);
    std::vector<double> vTempPreviousParticleNormalizedWeights;
    double tempPreviousParticleNormalizedWeights;
    while(ssPreviousParticleNormalizedWeights >> tempPreviousParticleNormalizedWeights){
        vTempPreviousParticleNormalizedWeights.emplace_back(tempPreviousParticleNormalizedWeights);
    }
    // sumWeights
    std::getline(fileABCsmcNextParticles, lineABCsmcNextParticles);
    std::stringstream ssSumWeights(lineABCsmcNextParticles);
    double tempSumWeights;
    ssSumWeights >> tempSumWeights;
    // empiricalStandardDeviation
    std::getline(fileABCsmcNextParticles, lineABCsmcNextParticles);
    std::stringstream ssEmpiricalStandardDeviation(lineABCsmcNextParticles);
    double tempEmpiricalStandardDeviation;
    ssEmpiricalStandardDeviation >> tempEmpiricalStandardDeviation;
    // ess
    std::getline(fileABCsmcNextParticles, lineABCsmcNextParticles);
    std::stringstream ssEss(lineABCsmcNextParticles);
    double tempEss;
    ssEss >> tempEss;
    
    
    previousParticles[param].previousParticleValues = vTempPreviousParticleValues;
    previousParticles[param].previousParticleWeights = vTempPreviousParticleWeights;
    previousParticles[param].previousParticleNormalizedWeights = vTempPreviousParticleNormalizedWeights;
    previousParticles[param].sumWeights = tempSumWeights;
    previousParticles[param].empiricalStandardDeviation = tempEmpiricalStandardDeviation;
    previousParticles[param].ess = tempEss;
}


void ABCInfosAndResults::computeAndStoreDistanceForTheRun(ParametersSpecificToEachRepetition& ParamRep, std::map<std::string, DairyHerd>& mFarms){
    
//    double theDistance = computeDistanceBetweenSimulationAndObservation(mFarms);
    double theDistance = ParamRep.distanceBetweenSimulationAndObservationForThisRun;
    
    if (Parameters::ABCrejection == true) {
        ABCrejectionParticle theParticle = ABCrejectionParticle(ParamRep.probaToPurchaseInfectedAnimal_initialValue, theDistance);
        vABCrejectionResults.emplace_back(theParticle);
    }
    
    if (Parameters::ABCsmc == true) {
        if (Parameters::ABCsmcRunNumber == 0) {
            std::cout << "computeAndStoreDistanceForTheRun (smc) error!\n" << std::endl;
            std::exit(EXIT_FAILURE);
        } else if (Parameters::ABCsmcRunNumber == 1) {
            if (numberOfGeneratedParticles < Parameters::numberOfAcceptedParticlesRequiredForTheFirstRun) {
                ParamRep.theABCsmcParticle.distance = theDistance;
                ParamRep.theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_initialValue"].PerturbedWeight = 1.;
                ParamRep.theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_slope"].PerturbedWeight = 1.;
                ParamRep.theABCsmcParticle.parameters["abcTestSensitivity"].PerturbedWeight = 1.;
                ParamRep.theABCsmcParticle.parameters["abcInfGlobalEnv"].PerturbedWeight = 1.;
                vABCsmcSelectedParticles.emplace_back(ParamRep.theABCsmcParticle);
                numberOfGeneratedParticles++;
            }
        } else {
            if (numberOfSelectedParticles < Parameters::numberOfAcceptedParticlesRequiredToGoToTheNextRun) {
                ParamRep.theABCsmcParticle.distance = theDistance;
                if (theDistance <= threshold) {
                    // ProbaToPurchaseInfectedAnimal
                    double priorProbabilityForProbaToPurchaseInfectedAnimal_initialValue = 1 / (Parameters::probaToPurchaseInfectedAnimal_initialValue_max - Parameters::probaToPurchaseInfectedAnimal_initialValue_min);
                    double sigmaForProbaToPurchaseInfectedAnimal_initialValue = previousParticles["probaToPurchaseInfectedAnimal_initialValue"].empiricalStandardDeviation * Parameters::perturbationKernelparameter;
                    double perturbedForProbaToPurchaseInfectedAnimal_initialValue = ParamRep.theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_initialValue"].PerturbedValue;
                    
                    // ProbaToPurchaseInfectedAnimal_slope
                    double priorProbabilityForProbaToPurchaseInfectedAnimal_slope = 1 / (Parameters::probaToPurchaseInfectedAnimal_slope_max - Parameters::probaToPurchaseInfectedAnimal_slope_min);
                    double sigmaForProbaToPurchaseInfectedAnimal_slope = previousParticles["probaToPurchaseInfectedAnimal_slope"].empiricalStandardDeviation * Parameters::perturbationKernelparameter;
                    double perturbedForProbaToPurchaseInfectedAnimal_slope = ParamRep.theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_slope"].PerturbedValue;
                    
                    // TestSensitivity
                    double priorProbabilityForAbcTestSensitivity = 1 / (Parameters::abcTestSensitivity_max - Parameters::abcTestSensitivity_min);
                    double sigmaForAbcTestSensitivity = previousParticles["abcTestSensitivity"].empiricalStandardDeviation * Parameters::perturbationKernelparameter;
                    double perturbedForAbcTestSensitivity = ParamRep.theABCsmcParticle.parameters["abcTestSensitivity"].PerturbedValue;
                    
                    // InfGlobalEnv
                    double priorProbabilityForAbcInfGlobalEnv = 1 / (std::log10(Parameters::abcInfGlobalEnv_max) - std::log10(Parameters::abcInfGlobalEnv_min));
                    double sigmaForAbcInfGlobalEnv = previousParticles["abcInfGlobalEnv"].empiricalStandardDeviation * Parameters::perturbationKernelparameter;
                    double perturbedForAbcInfGlobalEnv = ParamRep.theABCsmcParticle.parameters["abcInfGlobalEnv"].PerturbedValue;
                    
                    
                    // ProbaToPurchaseInfectedAnimal
                    double tempForProbaToPurchaseInfectedAnimal_initialValue = 0;
                    for (int i = 0; i<previousParticles["probaToPurchaseInfectedAnimal_initialValue"].previousParticleWeights.size(); i++) {
                        double previousWeight = previousParticles["probaToPurchaseInfectedAnimal_initialValue"].previousParticleWeights[i];
                        double previous = previousParticles["probaToPurchaseInfectedAnimal_initialValue"].previousParticleValues[i];
                        double probaDensityForMovingFromPreviousToPerturbedParticle = U_functions::gaussianPdf(perturbedForProbaToPurchaseInfectedAnimal_initialValue, previous, sigmaForProbaToPurchaseInfectedAnimal_initialValue);
                        tempForProbaToPurchaseInfectedAnimal_initialValue += previousWeight * probaDensityForMovingFromPreviousToPerturbedParticle;
                    }
                    double perturbedWeightForProbaToPurchaseInfectedAnimal_initialValue = priorProbabilityForProbaToPurchaseInfectedAnimal_initialValue / tempForProbaToPurchaseInfectedAnimal_initialValue;
                    ParamRep.theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_initialValue"].PerturbedWeight = perturbedWeightForProbaToPurchaseInfectedAnimal_initialValue;
                    if (Parameters::probaToPurchaseInfectedAnimal_initialValue_min == Parameters::probaToPurchaseInfectedAnimal_initialValue_max) {
                        ParamRep.theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_initialValue"].PerturbedWeight = 1.;
                    }
                    
                    // ProbaToPurchaseInfectedAnimal_slope
                    double tempForProbaToPurchaseInfectedAnimal_slope = 0;
                    for (int i = 0; i<previousParticles["probaToPurchaseInfectedAnimal_slope"].previousParticleWeights.size(); i++) {
                        double previousWeight = previousParticles["probaToPurchaseInfectedAnimal_slope"].previousParticleWeights[i];
                        double previous = previousParticles["probaToPurchaseInfectedAnimal_slope"].previousParticleValues[i];
                        double probaDensityForMovingFromPreviousToPerturbedParticle = U_functions::gaussianPdf(perturbedForProbaToPurchaseInfectedAnimal_slope, previous, sigmaForProbaToPurchaseInfectedAnimal_slope);
                        tempForProbaToPurchaseInfectedAnimal_slope += previousWeight * probaDensityForMovingFromPreviousToPerturbedParticle;
                    }
                    double perturbedWeightForProbaToPurchaseInfectedAnimal_slope = priorProbabilityForProbaToPurchaseInfectedAnimal_slope / tempForProbaToPurchaseInfectedAnimal_slope;
                    ParamRep.theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_slope"].PerturbedWeight = perturbedWeightForProbaToPurchaseInfectedAnimal_slope;
                    if (Parameters::probaToPurchaseInfectedAnimal_slope_min == Parameters::probaToPurchaseInfectedAnimal_slope_max) {
                        ParamRep.theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_slope"].PerturbedWeight = 1.;
                    }
                    
                    // TestSensitivity
                    double tempForAbcTestSensitivity = 0;
                    for (int i = 0; i<previousParticles["abcTestSensitivity"].previousParticleWeights.size(); i++) {
                        double previousWeight = previousParticles["abcTestSensitivity"].previousParticleWeights[i];
                        double previous = previousParticles["abcTestSensitivity"].previousParticleValues[i];
                        double probaDensityForMovingFromPreviousToPerturbedParticle = U_functions::gaussianPdf(perturbedForAbcTestSensitivity, previous, sigmaForAbcTestSensitivity);
                        tempForAbcTestSensitivity += previousWeight * probaDensityForMovingFromPreviousToPerturbedParticle;
                    }
                    double perturbedWeightForAbcTestSensitivity = priorProbabilityForAbcTestSensitivity / tempForAbcTestSensitivity;
                    ParamRep.theABCsmcParticle.parameters["abcTestSensitivity"].PerturbedWeight = perturbedWeightForAbcTestSensitivity;
                    if (Parameters::abcTestSensitivity_min == Parameters::abcTestSensitivity_max) {
                        ParamRep.theABCsmcParticle.parameters["abcTestSensitivity"].PerturbedWeight = 1.;
                    }
                    
                    // InfGlobalEnv
                    double tempForAbcInfGlobalEnv = 0;
                    for (int i = 0; i<previousParticles["abcInfGlobalEnv"].previousParticleWeights.size(); i++) {
                        double previousWeight = previousParticles["abcInfGlobalEnv"].previousParticleWeights[i];
                        double previous = previousParticles["abcInfGlobalEnv"].previousParticleValues[i];
                        double probaDensityForMovingFromPreviousToPerturbedParticle = U_functions::gaussianPdf(perturbedForAbcInfGlobalEnv, previous, sigmaForAbcInfGlobalEnv);
                        tempForAbcInfGlobalEnv += previousWeight * probaDensityForMovingFromPreviousToPerturbedParticle;
                    }
                    double perturbedWeightForAbcInfGlobalEnv = priorProbabilityForAbcInfGlobalEnv / tempForAbcInfGlobalEnv;
                    ParamRep.theABCsmcParticle.parameters["abcInfGlobalEnv"].PerturbedWeight = perturbedWeightForAbcInfGlobalEnv;
                    if (Parameters::abcInfGlobalEnv_min == Parameters::abcInfGlobalEnv_max) {
                        ParamRep.theABCsmcParticle.parameters["abcInfGlobalEnv"].PerturbedWeight = 1.;
                    }
                    
//                if (theDistance <= threshold) {
                    numberOfSelectedParticles++;
                    vABCsmcSelectedParticles.emplace_back(ParamRep.theABCsmcParticle);
                }
                    vABCsmcAllParticles.emplace_back(ParamRep.theABCsmcParticle);
                    numberOfGeneratedParticles++;
//                }
            }
        }
    }
}


//double ABCInfosAndResults::computeDistanceBetweenSimulationAndObservation(std::map<std::string, DairyHerd>& mFarms){
//    double theDistance = 0;
//    
//    for (auto it = Parameters::vHoldingsForResume.begin(); it != Parameters::vHoldingsForResume.end(); ++it) {
////        double observedSamplesCount = Parameters::mFarmsCharacteristics[*it].lastSamplesCount;
////        double observedPositiveSamplesCount = Parameters::mFarmsCharacteristics[*it].lastPositiveSamplesCount;
////        double simulatedPositiveSamplesCount = simulateSampling(mFarms[*it]);
////        double dist = observedPositiveSamplesCount - simulatedPositiveSamplesCount;
//        
//        theDistance += mFarms[*it].distanceFromObservation;
//    }
//    
//    return theDistance;
//}


void ABCInfosAndResults::saveABCrejectionResults(){
    if (Parameters::ABCrejection == true) {
        // Open file in writing mode
        std::ofstream ost(Parameters::ResultsFolderPath + "vABCrejectionResults.csv", std::fstream::out);
        if(!ost){std::cerr << "[ERROR] can't open output file : vABCrejectionResults" << std::endl;} // Check if the file is open!
        // Writing ...
        for (auto rep = vABCrejectionResults.begin(); rep != vABCrejectionResults.end(); ++rep) {
            ost << rep->paramValue << "\t" << rep->distance << "\n";
        }
    }
}


void ABCInfosAndResults::particlesProcessingForNextRunOfABCsmc(){
    if ((Parameters::ABCsmcRunNumber == 1 & numberOfGeneratedParticles == Parameters::numberOfAcceptedParticlesRequiredForTheFirstRun) | (Parameters::ABCsmcRunNumber > 1 & numberOfSelectedParticles == Parameters::numberOfAcceptedParticlesRequiredToGoToTheNextRun)) {
        // Compute next previous particles and replace the old by the new one, for each parameters
        computeNextPreviousParticles("probaToPurchaseInfectedAnimal_initialValue");
        computeNextPreviousParticles("probaToPurchaseInfectedAnimal_slope");
        computeNextPreviousParticles("abcTestSensitivity");
        computeNextPreviousParticles("abcInfGlobalEnv");
        
        // Calculate new threshold(s)
        std::vector<double> vDistance;
        for (auto it = vABCsmcSelectedParticles.begin(); it != vABCsmcSelectedParticles.end(); ++it) {
            vDistance.emplace_back(it->distance);
        }
        std::sort (vDistance.begin(), vDistance.end());
//        double newThreshold = gsl_stats_quantile_from_sorted_data(&vDistance[0], 1, vDistance.size(), 0.80);
        int index = static_cast<int>(round(static_cast<double>(vDistance.size())/100.*80.)) - 1;
        double newThreshold = vDistance[index];
        // Replace threshold by newThreshold
        threshold = newThreshold;
        
        // Save run results
        saveABCsmcResults("probaToPurchaseInfectedAnimal_initialValue");
        saveABCsmcResults("probaToPurchaseInfectedAnimal_slope");
        saveABCsmcResults("abcTestSensitivity");
        saveABCsmcResults("abcInfGlobalEnv");
        
        // Update the run number
        Parameters::updateTheABCsmcRunNumber();
        
        // Clear variables
        vABCsmcAllParticles.clear();
        vABCsmcSelectedParticles.clear();
        numberOfGeneratedParticles = 0.;
        numberOfSelectedParticles = 0.;
    }
}


void ABCInfosAndResults::computeNextPreviousParticles(const std::string& param){
    ABCsmcSetOfPreviousParticles nextSetOfpreviousParticles;
    
    // Normalise the weights and calculate the effective sample size (ESS)
    double theEss = 0.;
    double theEmpiricalStandardDeviation = 0.;
    std::vector<double> vPerturbedValue;
    
    double theSumOfParticleWeights = 0.;
    
    for (auto it = vABCsmcSelectedParticles.begin(); it != vABCsmcSelectedParticles.end(); ++it) {
        theSumOfParticleWeights += it->parameters[param].PerturbedWeight;
    }
    
    for (auto it = vABCsmcSelectedParticles.begin(); it != vABCsmcSelectedParticles.end(); ++it) {
        double thePerturbedNormalizedWeight = it->parameters[param].PerturbedWeight / theSumOfParticleWeights;
        it->parameters[param].PerturbedNormalizedWeight = thePerturbedNormalizedWeight;
        theEss += std::pow(thePerturbedNormalizedWeight, 2);
        vPerturbedValue.emplace_back(it->parameters[param].PerturbedValue);
        
        nextSetOfpreviousParticles.previousParticleValues.emplace_back(it->parameters[param].PerturbedValue);
        nextSetOfpreviousParticles.previousParticleWeights.emplace_back(it->parameters[param].PerturbedWeight);
        nextSetOfpreviousParticles.previousParticleNormalizedWeights.emplace_back(it->parameters[param].PerturbedNormalizedWeight);
    }
    
    nextSetOfpreviousParticles.sumWeights = theSumOfParticleWeights;
    theEmpiricalStandardDeviation = gsl_stats_sd(&vPerturbedValue[0], 1, vPerturbedValue.size());
    nextSetOfpreviousParticles.empiricalStandardDeviation = theEmpiricalStandardDeviation;
    theEss  = 1. / theEss;
    nextSetOfpreviousParticles.ess = theEss;
    
    // Replace old previous particles by the new previous particles
    previousParticles[param] = nextSetOfpreviousParticles;
}


void ABCInfosAndResults::saveABCsmcResults(const std::string& param){
    if (Parameters::ABCsmc == true) {
        // Save ABCsmcSelectedParticles
        std::ofstream ost1(Parameters::ResultsFolderPath + "ABCsmcSelectedParticles_" + std::to_string(Parameters::ABCsmcRunNumber) + ".csv", std::fstream::out);
        if(!ost1){std::cerr << "[ERROR] can't open output file : ABCsmcSelectedParticles_" + std::to_string(Parameters::ABCsmcRunNumber) << std::endl;} // Check if the file is open!
        // Writing ...
        ost1 << param << "\t" << "distance" << "\n";
        for (auto it = vABCsmcSelectedParticles.begin(); it != vABCsmcSelectedParticles.end(); ++it) {
            double theValue = it->parameters[param].PerturbedValue;
            double theDistance = it->distance;
            
            ost1 << theValue << "\t" << theDistance << "\n";
        }
        
        // Save threshold
        std::ofstream ost2(Parameters::ResultsFolderPath + "threshold_" + std::to_string(Parameters::ABCsmcRunNumber) + ".csv", std::fstream::out);
        if(!ost2){std::cerr << "[ERROR] can't open output file : threshold_" + std::to_string(Parameters::ABCsmcRunNumber) << std::endl;} // Check if the file is open!
        // Writing ...
        ost2 << threshold << "\n";
        
        // Save previousParticles
        for (auto param = previousParticles.begin(); param != previousParticles.end(); ++param) {
            std::ofstream ost3(Parameters::ResultsFolderPath + param->first + "_ess_" + std::to_string(Parameters::ABCsmcRunNumber) + ".csv", std::fstream::out);
            if(!ost3){std::cerr << "[ERROR] can't open output file : " + param->first + "_ess_"  + std::to_string(Parameters::ABCsmcRunNumber) << std::endl;} // Check if the file is open!
            // Writing ...
            ost3 << param->second.ess << "\n";
            
            
            std::ofstream ost4(Parameters::ResultsFolderPath + param->first + "_particles_" + std::to_string(Parameters::ABCsmcRunNumber) + ".csv", std::fstream::out);
            if(!ost4){std::cerr << "[ERROR] can't open output file : " + param->first + "_particles_"  + std::to_string(Parameters::ABCsmcRunNumber) << std::endl;} // Check if the file is open!
            // Writing ...
            for (int i = 0; i < param->second.previousParticleValues.size(); ++i) {
                double theValue = param->second.previousParticleValues[i];
                double theWeight = param->second.previousParticleWeights[i];
                double theNormalizedWeight = param->second.previousParticleNormalizedWeights[i];
                
                ost4 << theValue << "\t" << theWeight << "\t" << theNormalizedWeight << "\n";
            }
        }
        
        std::cout << "ABCsmc run " << Parameters::ABCsmcRunNumber << " | total number of particles generated: " << numberOfGeneratedParticles << std::endl;
    }
}


void ABCInfosAndResults::saveInformationsOnTheCurrentRun(){
    if (Parameters::ABCsmc == true) {
        if ((Parameters::ABCsmcRunNumber == 1 & numberOfGeneratedParticles == Parameters::numberOfAcceptedParticlesRequiredForTheFirstRun) | (Parameters::ABCsmcRunNumber > 1 & numberOfSelectedParticles == Parameters::numberOfAcceptedParticlesRequiredToGoToTheNextRun)) {
            
            saveParticlesOfABCsmc("probaToPurchaseInfectedAnimal_initialValue");
            saveParticlesOfABCsmc("probaToPurchaseInfectedAnimal_slope");
            saveParticlesOfABCsmc("abcTestSensitivity");
            saveParticlesOfABCsmc("abcInfGlobalEnv");
            
            // Save ABCsmcInformations
            if (Parameters::ABCsmcSequenceNumber == 1) {
                std::ofstream ost_infos(Parameters::ABCfolderPath + "ABCsmcParticlesGeneratedDuringTheRun/ABCsmcInformations.csv", std::fstream::out);
                if(!ost_infos){std::cerr << "[ERROR] can't open output file : ABCsmcInformations.csv" << std::endl;} // Check if the file is open!
                // Writing ...
                ost_infos << Parameters::ABCsmcRunNumber << "\n";
            }
             
            Parameters::updateABCsmcCurrentRunIsCompleted();
            
            std::cout << "ABCsmc run " << Parameters::ABCsmcRunNumber << " | total number of particles generated: " << numberOfGeneratedParticles << std::endl;
            
        }
    }
}


void ABCInfosAndResults::saveParticlesOfABCsmc(const std::string& param){
    if (Parameters::ABCsmc == true) {
        if ((Parameters::ABCsmcRunNumber == 1 & numberOfGeneratedParticles == Parameters::numberOfAcceptedParticlesRequiredForTheFirstRun) | (Parameters::ABCsmcRunNumber > 1 & numberOfSelectedParticles == Parameters::numberOfAcceptedParticlesRequiredToGoToTheNextRun)) {
            
            // Save ABCsmcAllParticles
            std::ofstream ost_all(Parameters::ABCfolderPath + "ABCsmcParticlesGeneratedDuringTheRun/ABCsmcAllParticles_" + param + "_" + std::to_string(Parameters::ABCsmcSequenceNumber) + ".csv", std::fstream::out);
            if(!ost_all){std::cerr << "[ERROR] can't open output file : ABCsmcAllParticles_" + param + "_" + std::to_string(Parameters::ABCsmcSequenceNumber) << std::endl;} // Check if the file is open!
            // Writing ...
            ost_all << "previousValue" << "\t" << "previousWeight" << "\t" << "previousNormalizedWeight" << "\t" << "perturbedValue" << "\t" << "perturbedWeight" << "\t" << "distance" << "\n";
            for (auto it = vABCsmcAllParticles.begin(); it != vABCsmcAllParticles.end(); ++it) {
                double thePreviousValue = it->parameters[param].PreviousValue;
                double thePreviousWeight = it->parameters[param].PreviousWeight;
                double thePreviousNormalizedWeight = it->parameters[param].PreviousNormalizedWeight;
                double thePerturbedValue = it->parameters[param].PerturbedValue;
                double thePerturbedWeight = it->parameters[param].PerturbedWeight;
                double theDistance = it->distance;
                
                ost_all << thePreviousValue << "\t" << thePreviousWeight << "\t" << thePreviousNormalizedWeight << "\t" << thePerturbedValue << "\t" << thePerturbedWeight << "\t" << theDistance << "\n";
            }
            
            // Save ABCsmcSelectedParticles
            std::ofstream ost_selected(Parameters::ABCfolderPath + "ABCsmcParticlesGeneratedDuringTheRun/ABCsmcSelectedParticles_" + param + "_" + std::to_string(Parameters::ABCsmcSequenceNumber) + ".csv", std::fstream::out);
            if(!ost_selected){std::cerr << "[ERROR] can't open output file : ABCsmcSelectedParticles_" + param + "_" + std::to_string(Parameters::ABCsmcSequenceNumber) << std::endl;} // Check if the file is open!
            // Writing ...
            ost_selected << "previousValue" << "\t" << "previousWeight" << "\t" << "previousNormalizedWeight" << "\t" << "perturbedValue" << "\t" << "perturbedWeight" << "\t" << "distance" << "\n";
            for (auto it = vABCsmcSelectedParticles.begin(); it != vABCsmcSelectedParticles.end(); ++it) {
                double thePreviousValue = it->parameters[param].PreviousValue;
                double thePreviousWeight = it->parameters[param].PreviousWeight;
                double thePreviousNormalizedWeight = it->parameters[param].PreviousNormalizedWeight;
                double thePerturbedValue = it->parameters[param].PerturbedValue;
                double thePerturbedWeight = it->parameters[param].PerturbedWeight;
                double theDistance = it->distance;
                
                ost_selected << thePreviousValue << "\t" << thePreviousWeight << "\t" << thePreviousNormalizedWeight << "\t" << thePerturbedValue << "\t" << thePerturbedWeight << "\t" << theDistance << "\n";
            }
            
        }
    }
}


//void ABCInfosAndResults::storeSimulatedSummaryStatistics(std::map<std::string, DairyHerd>& mFarms){
//    for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
//        std::string farmID = it->first;
//        int summaryStatistic = it->second.summaryStatistic;
//        
//        if (simulatedsummaryStatistic.find(farmID) == simulatedsummaryStatistic.end()) {
//            std::vector<int> vect;
//            simulatedsummaryStatistic[farmID] = vect;
//            simulatedsummaryStatistic[farmID].emplace_back(summaryStatistic);
//        } else {
//            simulatedsummaryStatistic[farmID].emplace_back(summaryStatistic);
//        }
//    }
//}


//void ABCInfosAndResults::saveSimulatedSummaryStatistics(){
//    if (Parameters::ABCsmcRunNumber == 1 & numberOfGeneratedParticles == Parameters::numberOfAcceptedParticlesRequiredForTheFirstRun) {
//        std::ofstream ost(Parameters::ABCfolderPath + "simulatedSummaryStatistics.csv", std::fstream::out);
//        if(!ost){std::cerr << "[ERROR] can't open output file : simulatedSummaryStatistics.csv" << std::endl;}
//        
//        for (auto it = simulatedsummaryStatistic.begin(); it != simulatedsummaryStatistic.end(); ++it) {
//            ost << it->first << "\t";
//            for (int i = 0; i < it->second.size()-1; i++) {
//                ost << it->second[i] << "\t";
//            }
//            ost << it->second[it->second.size()-1];
//            ost << "\n";
//        }
//    }
//}





