//
//  HerdExit.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 28/06/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_HerdExit_h
#define ParatubBetweenDairyHerd_HerdExit_h

#include <gsl/gsl_randist.h>
#include <vector>

#include "Parameters.h"
#include "ParametersSpecificToEachHerd.h"
#include "HealthStateCompartmentsContainer.hpp"
#include "BirthsContainer.hpp"

class HerdExit {
    // Variables ===========================================================
    
public:
    // Static variables ====================================================
//    static std::vector<int> salesOverTimeCalfSR;
//    static std::vector<int> salesOverTimeCalfTLIsIc;
//    static std::vector<int> salesOverTimeHeiferSR;
//    static std::vector<int> salesOverTimeHeiferTLIsIc;
    // Constructor =========================================================
    HerdExit();
    // Member functions ====================================================    
    int Update(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& t, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer);
    
    void PopulationDynamicFromAgeOneToAgeFirstCalvingMinus0ne(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& t, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer);

    int Update_bis(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& t, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer);
    
    int PopulationDynamicConcerningPositiveTestedAnimals(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& t, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer);
    
    void UpdateSR(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& t, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer);
    
    void UpdateSR_part1(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& t, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer);
    
    void UpdateSR_part2(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& t, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer);
    
    void UpdateSR_positiveTestedAnimals(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& t, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer);
    
};

#endif
