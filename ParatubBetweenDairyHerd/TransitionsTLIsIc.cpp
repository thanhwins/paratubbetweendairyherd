//
//  TransitionsTLIsIc.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 05/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#include "TransitionsTLIsIc.h"

// Constructor =========================================================
TransitionsTLIsIc::TransitionsTLIsIc(){
    newIc = 0;
    newIs = 0;
    newL = 0;
    
    newIc_positiveTested = 0;
    newIs_positiveTested = 0;
    newL_positiveTested = 0;
}


// Member functions ====================================================
void TransitionsTLIsIc::update(gsl_rng * randomGenerator, const int& t, const int& age, const HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer){
    if (age < Parameters::ageSign-1) {
        newIc = 0; newIs = 0; newL = 0;
        newIc_positiveTested = 0; newIs_positiveTested = 0; newL_positiveTested = 0;
        // T > L
        if (iHealthStateCompartmentsContainer.iCompartmentT.present[age]>0) {
            newL = gsl_ran_binomial(randomGenerator, (1. - std::exp( - (1./Parameters::durShedding))), iHealthStateCompartmentsContainer.iCompartmentT.present[age]);
        }
        // positiveTested
        if (Parameters::managementOfPositiveTestedTakeIntoAccount) {
            if (iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age]>0) {
                newL_positiveTested = gsl_ran_binomial(randomGenerator, (1. - std::exp( - (1./Parameters::durShedding))), iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age]);
            }
        }
    } else {
        newIc = 0; newIs = 0; newL = 0;
        newIc_positiveTested = 0; newIs_positiveTested = 0; newL_positiveTested = 0;
        // Is > Ic
        if (iHealthStateCompartmentsContainer.iCompartmentIs.present[age]>0) {
            newIc = gsl_ran_binomial(randomGenerator, (1. - std::exp( - (1./Parameters::durIs))), iHealthStateCompartmentsContainer.iCompartmentIs.present[age]);
        }
        // L > Is
        if (iHealthStateCompartmentsContainer.iCompartmentL.present[age]>0) {
            newIs = gsl_ran_binomial(randomGenerator, (1. - std::exp( - (1./Parameters::durL))), iHealthStateCompartmentsContainer.iCompartmentL.present[age]);
        }
        // T > L
        if (iHealthStateCompartmentsContainer.iCompartmentT.present[age]>0) {
            newL = gsl_ran_binomial(randomGenerator, (1. - std::exp( - (1./Parameters::durShedding))), iHealthStateCompartmentsContainer.iCompartmentT.present[age]);
        }
        
        //positiveTested
        if (Parameters::managementOfPositiveTestedTakeIntoAccount) {
            // Is > Ic
            if (iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age]>0) {
                newIc_positiveTested = gsl_ran_binomial(randomGenerator, (1. - std::exp( - (1./Parameters::durIs))), iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age]);
            }
            // L > Is
            if (iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age]>0) {
                newIs_positiveTested = gsl_ran_binomial(randomGenerator, (1. - std::exp( - (1./Parameters::durL))), iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age]);
            }
            // T > L
            if (iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age]>0) {
                newL_positiveTested = gsl_ran_binomial(randomGenerator, (1. - std::exp( - (1./Parameters::durShedding))), iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age]);
            }
        }
    }
}


void TransitionsTLIsIc::transfer(const int& t, const int& age, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer, SummaryInfection& DynInf){
    if (age < Parameters::ageSign) {
        iHealthStateCompartmentsContainer.iCompartmentL.present[age] += newL;
        iHealthStateCompartmentsContainer.iCompartmentT.present[age] -= newL;
        
        // positiveTested
        if (Parameters::managementOfPositiveTestedTakeIntoAccount) {
            iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age] += newL_positiveTested;
            iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age] -= newL_positiveTested;
        }
    } else {
        iHealthStateCompartmentsContainer.iCompartmentIc.present[age] += newIc;
        iHealthStateCompartmentsContainer.iCompartmentIs.present[age] = iHealthStateCompartmentsContainer.iCompartmentIs.present[age] - newIc + newIs;
        
        iHealthStateCompartmentsContainer.iCompartmentL.present[age] = iHealthStateCompartmentsContainer.iCompartmentL.present[age] + newL - newIs;
        iHealthStateCompartmentsContainer.iCompartmentT.present[age] -= newL;
        
        DynInf.IncidenceIs[t] += newIs;
        DynInf.IncidenceIc[t] += newIc;
        
        // positiveTested
        if (Parameters::managementOfPositiveTestedTakeIntoAccount) {
            iHealthStateCompartmentsContainer.iCompartmentIc_positiveTested.present[age] += newIc_positiveTested;
            iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age] = iHealthStateCompartmentsContainer.iCompartmentIs_positiveTested.present[age] - newIc_positiveTested + newIs_positiveTested;
            
            iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age] = iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.present[age] + newL_positiveTested - newIs_positiveTested;
            iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.present[age] -= newL_positiveTested;
            
            DynInf.IncidenceIs[t] += newIs_positiveTested;
            DynInf.IncidenceIc[t] += newIc_positiveTested;
        }
    }
}

