//
//  Chronometer.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 07/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#include "Chronometer.h"

// Constructor =========================================================
Chronometer::Chronometer(){
    
}


// Member functions ====================================================
void Chronometer::start(){
    tbegin = std::chrono::steady_clock::now();
}

void Chronometer::stop(){
    tend = std::chrono::steady_clock::now();
    texec += std::chrono::duration_cast<std::chrono::nanoseconds>(tend - tbegin).count();
}

