//
//  Births.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 24/06/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_Births_h
#define ParatubBetweenDairyHerd_Births_h

#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <numeric>
#include <vector>

#include "Parameters.h"
#include "SummaryInfection.h"
#include "SummaryDynamic.h"
#include "AgeCompartments.h"
#include "ParametersSpecificToEachHerd.h"

class Births {
    // Variables ===========================================================
//    std::vector<int> Lact;
//    int bsum;
    int bS;
//    int bT;
//    int t;
        
public:
    // Variables ====================================================
//    std::vector<int> birthsOverTimeS;
//    std::vector<int> birthsOverTimeT;
    // Constructor =========================================================
    Births();
    // Member functions ====================================================
    
    // Updating and computation of the in-utero infections
//    void update(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& t, const AgeCompartments& iCompartment, const double& paramlacta, const SummaryDynamic& iSummaryDynamic);
//    void inutero(gsl_rng * randomGenerator, const double& paraminfinutero);
    // Transfers
//    void transfer(AgeCompartments& iCompartmentSR, AgeCompartments& iCompartmentT, SummaryInfection& DynInf);
    // Access to variables
//    int lact(int x) {return Lact[x];}
//    int birthsSum() {return bsum;}
    int birthsS() {return bS;}
//    int birthsT() {return bT;}
    
//    void setLact(const int& position, const int& quantity);
    void setBirthsS(const int& quantity);
};

#endif
