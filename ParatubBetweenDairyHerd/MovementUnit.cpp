//
//  MovementUnit.cpp
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunee on 26/04/2015.
//  Copyright (c) 2015 Gaël Beaunée. All rights reserved.
//

#include "MovementUnit.h"

// Constructor =========================================================
MovementUnit::MovementUnit(){}

MovementUnit::MovementUnit(const std::string& theDestination, const int& theAge){
    destination = theDestination;
    age = theAge;
}


std::string MovementUnit::getDestination(){
    return destination;
}


int MovementUnit::getAge(){
    return age;
}