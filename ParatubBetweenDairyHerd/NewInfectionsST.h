//
//  NewInfectionsST.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 05/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_NewInfectionsST_h
#define ParatubBetweenDairyHerd_NewInfectionsST_h

#include <iostream>
#include <gsl/gsl_randist.h>
#include <vector>
#include <cmath>

#include "Parameters.h"
#include "HealthStateCompartmentsContainer.hpp"
#include "SummaryInfection.h"
#include "MapInEnvironments.h"
#include "UtilitiesFunctions.h"

class NewInfectionsST {
    // Variables ===========================================================
    double hcInBuilding; // headcount of animals in buildings
    double hcUnweaned; // headcount of calves unweaned
    double hcWeanedIn; // headcount of calves weaned in
    double hcWeanedOut; // headcount of calves weaned out
    double hcYheifer; // headcount of young heifers
    double hcHeifer; // headcount of heifers
    
    int newInfColo; // new infected animals due to colostrum route
    int newInfMilk; // new infected animals due to milk route
    int newInfLocal; // new infected animals due to local environment route
    int newInfGlobal; // new infected animals due to global environment route
    
public:
    // Variables ===========================================================
//    std::vector<int> InfOut6monthCC; // infections outside the building : calf-to-calf
//    std::vector<int> InfIn6monthCC; // infections inside the building : calf-to-calf
//    std::vector<int> InfIn6monthAC; // infections inside the building : adult-to-calf
    
    // Constructor =========================================================
    NewInfectionsST();
    
    // Member functions ====================================================
    void updateVariables(const int& t, const int& weeknumber, const HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer);
    
    void update(gsl_rng * randomGenerator, const int& t, const int& age, const int& weeknumber, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const MapInEnvironments& Environments, const HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer);
    
    void transfer(const int& t, const int& age, const int& weeknumber, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer, SummaryInfection& DynInf);
    
    int getNewInfColo(){
        return newInfColo;
    }
    
};

#endif
