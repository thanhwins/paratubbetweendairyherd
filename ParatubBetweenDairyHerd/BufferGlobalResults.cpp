//
//  BufferGlobalResults.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 08/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#include "BufferGlobalResults.h"

BufferGlobalResults::BufferGlobalResults(){
    // Variables ===========================================================
    vMetapopPersistence.resize(Parameters::simutime, 0); if (Parameters::nbHerdsInfected > 0) { vMetapopPersistence[0] = 1; }
    
}

void BufferGlobalResults::clear(){
    vMetapopPersistence.clear(); vMetapopPersistence.resize(Parameters::simutime, 0); if (Parameters::nbHerdsInfected > 0) { vMetapopPersistence[0] = 1; }
    
}


// Member functions ====================================================
//void BufferGlobalResults::updateBuffer(){
//    
//}



