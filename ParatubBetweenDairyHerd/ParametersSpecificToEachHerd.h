//
//  ParametersSpecificToEachHerd.h
//  ParatubBetweenDairyHerd
//
//  Created by Gaël Beaunée on 05/01/2014.
//  Copyright (c) 2014 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_ParametersSpecificToEachHerd_h
#define ParatubBetweenDairyHerd_ParametersSpecificToEachHerd_h

#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <random>
#include <gsl/gsl_randist.h>

#include "UtilitiesFunctions.h"
#include "Parameters.h"
#include "ParametersSpecificToEachRepetition.h"
#include "ObservedSampleInformations.hpp"

class ParametersSpecificToEachHerd {
public:
    // Constructor =========================================================
    ParametersSpecificToEachHerd();
    ParametersSpecificToEachHerd(const ParametersSpecificToEachRepetition& ParamRep, const std::string& theId);
    
    // Variables ===========================================================
    std::string farmId;
    int farmIndex;
    
    // Parameters ==========================================================
    // General information about simulations -------------------------------
    //    int weeknumber;
    
    int beginDate;
    int lastSamplingDate;
    
    std::map<int, ObservedSampleInformations> samplingDates;

    int herdSize;
    
//    std::vector<double> vAnnualSexRatio;
//    std::vector<double> vAnnualBirthsRates;
//    std::vector<double> vAnnualBirthsNumber;
    std::vector<short int> vAnnualBirthEvents;
    
    std::vector< std::vector<double>> vvAnnualDeathAndCullingRates;
    std::vector< std::vector<double>> vvAnnualCullingRates;
    double deathRateBirth;
//    double sexRatio;
//    double birthRate;
//    double birthNumber;
    std::vector<double> vDeathRate;
    std::vector<double> vCullingRate;
    std::vector<double> vDeathAndCullingRate;
    
    double numberOfIncomingMovements;
    bool numberOfIncomingMovementsAboveTheMinimum;
    double numberOfOutgoingMovements;
    bool numberOfOutgoingMovementsAboveTheMinimum;
    
    double polarity;
    
    double outgoingDegree;
    double incomingDegree;
    
    
    bool testAtPurchaseImplementedInThisFarm = false;
    
    bool cullingImprovementImplementedInThisFarm;
    double cullingRateIcInThisFarm;
    
    bool testAndCullImplementedInThisFarm;
    int testAndCullDate;
    bool aNewIcAppearSinceTheLastTestAndCull;
    int dateOfOnsetOfTheFirstIcSinceTheLastTestAndCull;
    
    bool hygieneImprovementImplementedInThisFarm;
    double deathRateBactInsideInThisFarm;
    
    bool calfManagementImprovementImplementedInThisFarm;
    double infGlobalEnvInThisFarm;
    
    
    bool cullingImprovementAndTestAndCullAlreadySet;
    
    
    // Member functions ====================================================
    void Clear();
    
    // Updating variables which change over time
    //    void init(); // weeknumber
    
    void Update(const ParametersSpecificToEachRepetition& ParamRep, const int& t, const int& cumulativeNumberOfIc); // hcCow , hcHeifer , cullingRate, weeknumber
    
    void setTheImplementationOfControlMeasures(const ParametersSpecificToEachRepetition& ParamRep);
    
    void UpdateTestAndCullDate(const ParametersSpecificToEachRepetition& ParamRep);
    
    void initializeParametersOfControlMeasures(const int& incidenceOfIc);
    
    void updateParametersOfControlMeasures(const int& timeStep, const int& incidenceOfIc);
    
    void resetParametersOfControlMeasures();
    
};

#endif
