//
//  BufferPersistentResults.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 09/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

//=================================================================
// Accumulator for runs which persist at the end of the simulation
//=================================================================

#ifndef ParatubBetweenDairyHerd_BufferPersistentResults_h
#define ParatubBetweenDairyHerd_BufferPersistentResults_h

#include <vector>
#include <map>
#include <string>
#include <boost/accumulators/numeric/functional/vector.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/sum.hpp>
#include <boost/accumulators/statistics/count.hpp>

#include "Parameters.h"
//#include "DairyHerd.h"
#include "AgeCompartments.h"
#include "SummaryInfection.h"
#include "SummaryDynamic.h"
#include "Births.h"
#include "MapInEnvironments.h"
#include "UtilitiesFunctions.h"
#include "VectorPlus.h"
#include "VectorMinus.h"

class BufferPersistentResults {
public:
    // Constructor =========================================================
    BufferPersistentResults();
    
    void clear();
    
    // Vectors =============================================================
    
    std::vector<int> vBufferOutDegreeOfSelectedNode;
    std::vector<int> vBufferInDegreeOfSelectedNode;
    
    std::vector<double> vBufferNbOutMovsOfSelectedNode;
    std::vector<double> vBufferNbInMovsOfSelectedNode;
    
    std::vector<int> vBufferSizeOfSelectedNode;
    
    std::vector<double> vBufferPrevalenceInSelectedNodes;
    
    std::vector<double> vBufferPrevalenceInInfectedHerdsAtTheEnd;
    
    int bNbNewInfectedHerdsSinceSimulationStart;
    int bNbNewInfectedHerdsSinceTestsStart;
    
    // accumulators ========================================================
    std::vector<int> bInfectedHerds;
    std::vector<int> bAffectedHerds;
    std::vector<int> bNeverInfectedHerds;
    std::vector<int> bAlreadyInfectedHerds;
    
    std::vector<double> bInfectedProportionMeta;
    std::vector<double> bInfectiousProportionMeta;
    std::vector<double> bAffectedProportionMeta;
    
    std::vector<double> bInfectedProportionFirst;
    std::vector<double> bInfectiousProportionFirst;
    std::vector<double> bAffectedProportionFirst;
    
//    std::vector<double> bInfectedProportionFirstInit;
//    std::vector<double> bInfectiousProportionFirstInit;
//    std::vector<double> bAffectedProportionFirstInit;
    
    std::vector<double> bHeadcountMeta;
    std::vector<double> bHeadcountFirst;
//    std::vector<double> bHeadcountFirstInit;
    std::vector<double> bHeadcountSecond;
    
    
    int bInfectedByFirst;
    int bInfectedBySecond;
    int bInfectedByExt;
        
    int bInfectedByTestedMov;
    
    std::vector<int> bNbOutgoingMov;
    std::vector<int> bNbOutgoingMovTested;
    std::vector<int> bNbOutgoingInfectedMov;
    std::vector<int> bNbOutgoingInfectedMovTested;
    
    std::vector<int> bNbIncomingMov;
    std::vector<int> bNbIncomingMovTested;
    std::vector<int> bNbIncomingInfectedMov;
    std::vector<int> bNbIncomingInfectedMovTested;
    
    int bIntroducionOfInfectiousAnimalsIfNoTest;
    int bIntroducionOfInfectiousAnimalsAvoided;
    
    
    
    std::vector<std::vector<std::string>> vBufferInfectionDurationWhenExtinction;
    std::vector<std::vector<std::string>> vBufferInfectionDurationIfNoExtinction;
    
    std::vector<std::vector<std::string>> vBufferExtinctionBefore1Years;
    std::vector<std::vector<std::string>> vBufferExtinctionBefore2Years;
    std::vector<std::vector<std::string>> vBufferExtinctionBefore3Years;
    
    std::vector<std::vector<std::string>> vBufferExtinctionBefore5Years;
    std::vector<std::vector<std::string>> vBufferNoExtinctionBefore5Years;
        
    
    std::vector<std::vector<std::string>> vvBufferInfosHerdPrevalenceAfter5Years;
//    std::vector<std::vector<double>> vvBufferDynamicHerdPrevalenceInfectedAfter5Years;
//    std::vector<std::vector<double>> vvBufferDynamicHerdPrevalenceInfectiousAfter5Years;
//    std::vector<std::vector<double>> vvBufferDynamicHerdPrevalenceAffectedAfter5Years;
    
    
//    std::vector<std::vector<double>> vvBufferPersistenceDynamicOver5Years;
    std::vector<std::vector<std::string>> vvBufferPersistenceOver5Years;
    
    
    std::vector<std::vector<std::string>> vBufferSourceAndDestinationOfInfectedMovements_SourceIsFirst; //
    std::vector<std::vector<std::string>> vBufferSourceAndDestinationOfInfectedMovements_SourceIsSecond; //
    std::vector<std::vector<std::string>> vBufferSourceAndDestinationOfInfectedMovements_SourceIsExt; //
    
    std::vector<std::vector<std::string>> vBufferNbInfectedMovementsPerHerd; //
    
    
    
    int bNumberOfTSaleByPrimary;
    int bNumberOfLSaleByPrimary;
    int bNumberOfIsSaleByPrimary;
    
    int bNumberOfNewInfectionFromPrimary;
    
    
    
    std::vector<int> vBufferMatrixOfInfectedHerdsAfterHalfTheSimulationTime;
    std::vector<int> vBufferMatrixOfInfectedHerdsAtTheEndOfTheSimulationTime;
    std::vector<int> vBufferMatrixOfAlreadyInfectedHerdsAfterHalfTheSimulationTime;
    std::vector<int> vBufferMatrixOfAlreadyInfectedHerdsAtTheEndOfTheSimulationTime;
    
    std::vector<int> vBufferMatrixOfNumberOfInfection;
    std::vector<int> vBufferMatrixOfNumberOfInfectedAnimalsPurchased;
    std::vector<int> vBufferMatrixOfNumberOfWeeksWithInfection;
    
    std::vector<int> vBufferPrimaryCaseMatrix;
        
    
//    std::vector<int> vBufferMatrixOfNumberOfAnimalsTestedMovIn;
//    std::vector<int> vBufferMatrixOfNumberOfAnimalsTestedMovOut;
    std::vector<int> vBufferMatrixOfNumberOfIcAnimalsCulled;
    std::vector<int> vBufferMatrixOfNumberOfAnimalsCulledDuringTestAndCull;
    std::vector<int> vBufferMatrixOfNumberOfAnimalsTestedDuringTestAndCull;



    std::map<std::string, std::vector<double>> buffer_mFarms_vectPrevalence;
    
    
    
    
    // Member functions ====================================================
    void updateBuffer(const int& timeStep, const int& typeInfectedHerd, const SummaryDynamic& iSummaryDynamic);
    
    void updateBufferInit(const std::string& theFarmId, const int& theFarmIndex, const int& typeInfectedHerd, SummaryDynamic& iSummaryDynamic);
//    void updateBufferInit(const DairyHerd& theFarm);

};

#endif
