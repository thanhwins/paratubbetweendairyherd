//
//  HerdCharacteristics.cpp
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunee on 26/04/2015.
//  Copyright (c) 2015 Gaël Beaunée. All rights reserved.
//

#include "HerdCharacteristics.h"

HerdCharacteristics::HerdCharacteristics(){}

HerdCharacteristics::HerdCharacteristics(const int& theHerdSize, const double& theInDegree, const double& theOutDegree, const double& theInStrengthAll, const double& theOutStrengthAll){
    herdSize = theHerdSize;
    inDegree = theInDegree;
    outDegree = theOutDegree;
    inStrengthAll = theInStrengthAll;
    outStrengthAll = theOutStrengthAll;
}

