//
//  ParametersSpecificToEachHerd.cpp
//  ParatubBetweenDairyHerd
//
//  Created by Gaël Beaunée on 05/01/2014.
//  Copyright (c) 2014 Gaël Beaunée. All rights reserved.
//

#include "ParametersSpecificToEachHerd.h"

// Static variables ====================================================

// Constructor =========================================================
ParametersSpecificToEachHerd::ParametersSpecificToEachHerd(){}

ParametersSpecificToEachHerd::ParametersSpecificToEachHerd(const ParametersSpecificToEachRepetition& ParamRep, const std::string& theId){
    farmId = theId;
    farmIndex = static_cast<int>( std::find(Parameters::vFarmID.begin(), Parameters::vFarmID.end(), farmId) - Parameters::vFarmID.begin() );
    
    beginDate = Parameters::mFarmsCharacteristics[farmId].beginDate;
    
    lastSamplingDate = Parameters::mFarmsCharacteristics[farmId].lastSampleDate;
    
    samplingDates = Parameters::mFarmsDateSampleCount[farmId];
//    std::cout << farmId << std::endl;
//    for (auto date: samplingDates) {
//        std::cout << date.first << " - " << date.second << std::endl;
//    }

    herdSize = Parameters::mFarmsCharacteristics[farmId].herdSize;
    
    if (Parameters::proportionOfFarmsWithTestAtPurchase > 0.0) {
        if (Parameters::proportionOfFarmsWithTestAtPurchase < 1.0) {
//            std::cout << testAtPurchaseImplementedInThisFarm << " - ";
//            std::cout << Parameters::proportionOfFarmsWithTestAtPurchase;
            bool withTestAtPurchase = gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::proportionOfFarmsWithTestAtPurchase);
//            std::cout << " - " << withTestAtPurchase;
            if (withTestAtPurchase) {
                testAtPurchaseImplementedInThisFarm = true;
            } else {
                testAtPurchaseImplementedInThisFarm = false;
            }
//            std::cout << testAtPurchaseImplementedInThisFarm << std::endl;
        } else {
            testAtPurchaseImplementedInThisFarm = true;
        }
    } else {
        testAtPurchaseImplementedInThisFarm = false;
    }

    cullingImprovementImplementedInThisFarm = false;
    cullingRateIcInThisFarm = Parameters::cullingRateIc;
    
    testAndCullImplementedInThisFarm = false;
    aNewIcAppearSinceTheLastTestAndCull = false;
    testAndCullDate = 0;
    
    hygieneImprovementImplementedInThisFarm = false;
    deathRateBactInsideInThisFarm = Parameters::deathRateBactInside;
    
    calfManagementImprovementImplementedInThisFarm = false;
    
    if (Parameters::ABCrejection | Parameters::ABCsmc | Parameters::ABCcompleteTrajectories) {
        infGlobalEnvInThisFarm = ParamRep.abcInfGlobalEnv;
    } else {
        infGlobalEnvInThisFarm = Parameters::infGlobalEnv;
    }
    
    
    cullingImprovementAndTestAndCullAlreadySet = false;
    
    if (Parameters::proportionOfFarmsWithHygieneImprovement > 0.0) {
        if (Parameters::proportionOfFarmsWithHygieneImprovement < 1.0) {
            if (gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::proportionOfFarmsWithHygieneImprovement)) {
                deathRateBactInsideInThisFarm = Parameters::deathRateBactInsideIfHygieneImprovement;
                hygieneImprovementImplementedInThisFarm = true;
            }
        } else {
            deathRateBactInsideInThisFarm = Parameters::deathRateBactInsideIfHygieneImprovement;
            hygieneImprovementImplementedInThisFarm = true;
        }
    }
    
    if (Parameters::proportionOfFarmsWithCalfManagementImprovement > 0.0) {
        if (Parameters::proportionOfFarmsWithCalfManagementImprovement < 1.0) {
            if (gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::proportionOfFarmsWithCalfManagementImprovement)) {
                if (Parameters::ABCrejection | Parameters::ABCsmc | Parameters::ABCcompleteTrajectories) {
                    infGlobalEnvInThisFarm = ParamRep.abcInfGlobalEnvIfCalfManagementImprovement;
                } else {
                    infGlobalEnvInThisFarm = Parameters::infGlobalEnvIfCalfManagementImprovement;
                }
                calfManagementImprovementImplementedInThisFarm = true;
            }
        } else {
            if (Parameters::ABCrejection | Parameters::ABCsmc | Parameters::ABCcompleteTrajectories) {
                infGlobalEnvInThisFarm = ParamRep.abcInfGlobalEnvIfCalfManagementImprovement;
            } else {
                infGlobalEnvInThisFarm = Parameters::infGlobalEnvIfCalfManagementImprovement;
            }
            calfManagementImprovementImplementedInThisFarm = true;
        }
    }

    
    
    
    numberOfIncomingMovements = Parameters::mFarmsCharacteristics[farmId].inStrengthAll;
    if (numberOfIncomingMovements >= Parameters::minimumOfIncomingMovements) {
        numberOfIncomingMovementsAboveTheMinimum = true;
    } else {
        numberOfIncomingMovementsAboveTheMinimum = false;
    }
    numberOfOutgoingMovements = Parameters::mFarmsCharacteristics[farmId].outStrengthAll;
    if (numberOfOutgoingMovements >= Parameters::minimumOfOutgoingMovements) {
        numberOfOutgoingMovementsAboveTheMinimum = true;
    } else {
        numberOfOutgoingMovementsAboveTheMinimum = false;
    }
    
    polarity = (numberOfOutgoingMovements - numberOfIncomingMovements) / (numberOfOutgoingMovements + numberOfIncomingMovements);
    
    incomingDegree = Parameters::mFarmsCharacteristics[farmId].inDegree;
    outgoingDegree = Parameters::mFarmsCharacteristics[farmId].outDegree;
    
    // Death and Culling Rates
    for (int year = 0; year<Parameters::mHerdCullingRates[farmId].size()/Parameters::nbYears; year++) {
        std::vector<double> vRatesOfTheYear;
        std::vector<double> vCullingRatesOfTheYear;
        
        vRatesOfTheYear.insert(vRatesOfTheYear.end(), Parameters::mHerdCullingRates[farmId].begin()+year*Parameters::mHerdCullingRates[farmId].size()/Parameters::nbYears, Parameters::mHerdCullingRates[farmId].begin()+year*Parameters::mHerdCullingRates[farmId].size()/Parameters::nbYears+Parameters::mHerdCullingRates[farmId].size()/Parameters::nbYears);
        assert(vRatesOfTheYear.size() == 9);
        
        vCullingRatesOfTheYear.insert(vCullingRatesOfTheYear.end(), Parameters::mHerdCullingRates[farmId].begin()+year*Parameters::mHerdCullingRates[farmId].size()/Parameters::nbYears+4, Parameters::mHerdCullingRates[farmId].begin()+year*Parameters::mHerdCullingRates[farmId].size()/Parameters::nbYears+Parameters::mHerdCullingRates[farmId].size()/Parameters::nbYears);
        assert(vCullingRatesOfTheYear.size() == 5);
        
        vRatesOfTheYear[1] = 1-std::pow((1-(vRatesOfTheYear[1]) ), (365./52.));
        vRatesOfTheYear[1] = 1 - std::exp( - vRatesOfTheYear[1] );
        
        vRatesOfTheYear[2] = 1-std::pow((1-(vRatesOfTheYear[2]) ), (365./52.));
        vRatesOfTheYear[2] = 1 - std::exp( - vRatesOfTheYear[2] );
        
        vRatesOfTheYear[3] = 1-std::pow((1-(vRatesOfTheYear[3])), (365./52.));
        vRatesOfTheYear[3] = 1 - std::exp( - vRatesOfTheYear[3] );
        
        vRatesOfTheYear[4] = 1-std::pow((1-(vRatesOfTheYear[4])), (365./52.));
        vRatesOfTheYear[4] = 1 - std::exp( - vRatesOfTheYear[4] );
        
        vRatesOfTheYear[5] = 1-std::pow((1-(vRatesOfTheYear[5])), (365./52.));
        vRatesOfTheYear[5] = 1 - std::exp( - vRatesOfTheYear[5] );
        
        vRatesOfTheYear[6] = 1-std::pow((1-(vRatesOfTheYear[6])), (365./52.));
        vRatesOfTheYear[6] = 1 - std::exp( - vRatesOfTheYear[6] );
        
        vRatesOfTheYear[7] = 1-std::pow((1-(vRatesOfTheYear[7])), (365./52.));
        vRatesOfTheYear[7] = 1 - std::exp( - vRatesOfTheYear[7] );
        
        vRatesOfTheYear[8] = 1-std::pow((1-(vRatesOfTheYear[8])), (365./52.));
        vRatesOfTheYear[8] = 1 - std::exp( - vRatesOfTheYear[8] );
        
        vvAnnualDeathAndCullingRates.emplace_back(vRatesOfTheYear);
        
        
        
        vCullingRatesOfTheYear[0] = 1-std::pow((1-(vCullingRatesOfTheYear[0]) ), (365.));
        vCullingRatesOfTheYear[1] = 1-std::pow((1-(vCullingRatesOfTheYear[1]) ), (365.));
        vCullingRatesOfTheYear[2] = 1-std::pow((1-(vCullingRatesOfTheYear[2]) ), (365.));
        vCullingRatesOfTheYear[3] = 1-std::pow((1-(vCullingRatesOfTheYear[3]) ), (365.));
        vCullingRatesOfTheYear[4] = 1-std::pow((1-(vCullingRatesOfTheYear[4]) ), (365.));
        
        vvAnnualCullingRates.emplace_back(vCullingRatesOfTheYear);
    }
    assert(vvAnnualDeathAndCullingRates.size() == Parameters::mHerdCullingRates[farmId].size()/Parameters::nbYears);
    assert(vvAnnualCullingRates.size() == Parameters::mHerdCullingRates[farmId].size()/Parameters::nbYears);
    
//    std::cout << num << std::endl;
//    for (int i = 0; i<vvAnnualDeathAndCullingRates.size(); i++) {
//        for (int j = 0; j<vvAnnualDeathAndCullingRates[i].size(); j++) {
//            std::cout << vvAnnualDeathAndCullingRates[i][j] << " ";
//        }
//        std::cout << std::endl;
//    }
//    std::cout << std::endl;
    
//    std::cout << farmId << std::endl;
    deathRateBirth = vvAnnualDeathAndCullingRates[0][0];
    
    vDeathRate.clear();
    std::vector<double> deathRate_part1; deathRate_part1.resize(2, vvAnnualDeathAndCullingRates[0][1]);
    std::vector<double> deathRate_part2; deathRate_part2.resize(7, vvAnnualDeathAndCullingRates[0][2]);
    std::vector<double> deathRate_part3; deathRate_part3.resize(121, vvAnnualDeathAndCullingRates[0][3]);
    vDeathRate.insert(vDeathRate.end(), deathRate_part1.begin(), deathRate_part1.end());
    vDeathRate.insert(vDeathRate.end(), deathRate_part2.begin(), deathRate_part2.end());
    vDeathRate.insert(vDeathRate.end(), deathRate_part3.begin(), deathRate_part3.end());
    
    vCullingRate.clear();
    vCullingRate = {vvAnnualDeathAndCullingRates[0][4], vvAnnualDeathAndCullingRates[0][5], vvAnnualDeathAndCullingRates[0][6], vvAnnualDeathAndCullingRates[0][7], vvAnnualDeathAndCullingRates[0][8]};

    vDeathAndCullingRate.clear();
    vDeathAndCullingRate.insert(vDeathAndCullingRate.end(), vDeathRate.begin(), vDeathRate.end());
    vDeathAndCullingRate.insert(vDeathAndCullingRate.end(), vCullingRate.begin(), vCullingRate.end());
    
    
//    vAnnualSexRatio.insert(vAnnualSexRatio.end(), Parameters::mHerdBirthsRatio[farmId].begin(), Parameters::mHerdBirthsRatio[farmId].end());
//    assert(vAnnualSexRatio.size() == Parameters::nbYears);
//    sexRatio = vAnnualSexRatio[0];
    
//    vAnnualBirthsRates.insert(vAnnualBirthsRates.end(), Parameters::vvHerdBirthsRates[num].begin(), Parameters::vvHerdBirthsRates[num].end());
//    assert(vAnnualBirthsRates.size() == Parameters::nbYears);
    
//    for (int i = 0; i<vAnnualBirthsRates.size(); i++) {
//        double birthRateBuffer = std::max(0., vAnnualBirthsRates[i]);
//        double maxValue = 1-std::pow((1-(1.0/56.3)), (52.));
//        birthRateBuffer = std::min(maxValue, birthRateBuffer);
//        birthRateBuffer = 1-std::pow((1-(birthRateBuffer)), (1./52.));
////        birthRateBuffer = 1 - std::exp( - birthRateBuffer);
//        vAnnualBirthsRates[i] = birthRateBuffer;
//    }
    
//    birthRate = vAnnualBirthsRates[0];
////    birthRate = 1-std::pow((1-(birthRate)), (1./52.));
//    birthRate = 1 - std::exp( - birthRate);
    
    
//    vAnnualBirthsNumber.insert(vAnnualBirthsNumber.end(), Parameters::vvHerdBirthsNumber[num].begin(), Parameters::vvHerdBirthsNumber[num].end());
//    assert(vAnnualBirthsNumber.size() == Parameters::nbYears);
//    birthNumber = vAnnualBirthsNumber[0];
    
    
    vAnnualBirthEvents.insert(vAnnualBirthEvents.end(), Parameters::mHerdBirthEvents[farmId].begin(), Parameters::mHerdBirthEvents[farmId].end());
    assert(vAnnualBirthEvents.size() == Parameters::simutime);
    
}


void ParametersSpecificToEachHerd::Clear(){
    deathRateBirth = 0.0;
//    sexRatio = 0.0;
    vDeathRate.clear();
    vCullingRate.clear();
    vDeathAndCullingRate.clear();
}

// Member functions ====================================================
//void ParametersSpecificToEachHerd::init(){
//    weeknumber = 0;
//}


void ParametersSpecificToEachHerd::Update(const ParametersSpecificToEachRepetition& ParamRep, const int& t, const int& cumulativeNumberOfIc){
    // culling rate
    if ((t-1)%Parameters::timestepInOneYear == 0) {
        int year = ((t-1)/Parameters::timestepInOneYear)%vvAnnualDeathAndCullingRates.size();
        
        deathRateBirth = vvAnnualDeathAndCullingRates[year][0];
        
        vDeathRate.clear();
        std::vector<double> deathRate_part1; deathRate_part1.resize(2, vvAnnualDeathAndCullingRates[year][1]);
        std::vector<double> deathRate_part2; deathRate_part2.resize(7, vvAnnualDeathAndCullingRates[year][2]);
        std::vector<double> deathRate_part3; deathRate_part3.resize(121, vvAnnualDeathAndCullingRates[year][3]);
        vDeathRate.insert(vDeathRate.end(), deathRate_part1.begin(), deathRate_part1.end());
        vDeathRate.insert(vDeathRate.end(), deathRate_part2.begin(), deathRate_part2.end());
        vDeathRate.insert(vDeathRate.end(), deathRate_part3.begin(), deathRate_part3.end());
        
        vCullingRate.clear();
        vCullingRate = {vvAnnualDeathAndCullingRates[year][4], vvAnnualDeathAndCullingRates[year][5], vvAnnualDeathAndCullingRates[year][6], vvAnnualDeathAndCullingRates[year][7], vvAnnualDeathAndCullingRates[year][8]};

        vDeathAndCullingRate.clear();
        vDeathAndCullingRate.insert(vDeathAndCullingRate.end(), vDeathRate.begin(), vDeathRate.end());
        vDeathAndCullingRate.insert(vDeathAndCullingRate.end(), vCullingRate.begin(), vCullingRate.end());
        
//        sexRatio = vAnnualSexRatio[year];

//        birthRate = vAnnualBirthsRates[year];
////        birthRate = 1-std::pow((1-(birthRate)), (1./52.));
//        birthRate = 1 - std::exp( - birthRate);
        
//        birthNumber = vAnnualBirthsNumber[year];
        
    }
    
    if ((cumulativeNumberOfIc > 0) & (cullingImprovementAndTestAndCullAlreadySet == false)) {
        setTheImplementationOfControlMeasures(ParamRep);
    }
    
    UpdateTestAndCullDate(ParamRep);
}



void ParametersSpecificToEachHerd::setTheImplementationOfControlMeasures(const ParametersSpecificToEachRepetition& ParamRep){
    if (Parameters::withinHerdBiosecurity & (cullingImprovementAndTestAndCullAlreadySet == false)) {
        
        cullingImprovementAndTestAndCullAlreadySet = true;
        
        if (Parameters::proportionOfFarmsWithCullingImprovement > 0.0) {
            if (Parameters::proportionOfFarmsWithCullingImprovement < 1.0) {
                if (gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::proportionOfFarmsWithCullingImprovement)) {
                    cullingRateIcInThisFarm = Parameters::cullingRateIcIfCullingImprovement;
                    cullingImprovementImplementedInThisFarm = true;
                }
            } else {
                cullingRateIcInThisFarm = Parameters::cullingRateIcIfCullingImprovement;
                cullingImprovementImplementedInThisFarm = true;
            }
        }
        
        if (Parameters::proportionOfFarmsWithTestAndCull > 0.0) {
            if (Parameters::proportionOfFarmsWithTestAndCull < 1.0) {
                if (gsl_ran_bernoulli(ParamRep.randomGenerator, Parameters::proportionOfFarmsWithTestAndCull)) {
                    testAndCullImplementedInThisFarm = true;
                }
            } else {
                testAndCullImplementedInThisFarm = true;
            }
        }
        
    }
}



void ParametersSpecificToEachHerd::UpdateTestAndCullDate(const ParametersSpecificToEachRepetition& ParamRep){
    if (Parameters::typeOfTestAndCullStrategy == Parameters::testAndCullStrategyIfOnsetOfNewIc) {
        if (testAndCullDate == 0) {
            if ((aNewIcAppearSinceTheLastTestAndCull == true) & (testAndCullImplementedInThisFarm == true)) {
                testAndCullDate = dateOfOnsetOfTheFirstIcSinceTheLastTestAndCull + Parameters::delayBetweenTheOnsetOfAnIcAndTheImplementationOfTheTestAndCull;
            }
        }
    }
    if (Parameters::typeOfTestAndCullStrategy == Parameters::testAndCullStrategyEachYearAfterOnsetOfIc) {
        if (testAndCullDate == 0) {
            if (testAndCullImplementedInThisFarm == true) {
                testAndCullDate = dateOfOnsetOfTheFirstIcSinceTheLastTestAndCull + Parameters::delayBetweenTheOnsetOfAnIcAndTheImplementationOfTheTestAndCull;
            }
        }
    }
}



void ParametersSpecificToEachHerd::initializeParametersOfControlMeasures(const int& numberOfIc){
    if (numberOfIc > 0) {
        aNewIcAppearSinceTheLastTestAndCull = true;
        dateOfOnsetOfTheFirstIcSinceTheLastTestAndCull = 1;
    }
}



void ParametersSpecificToEachHerd::updateParametersOfControlMeasures(const int& timeStep, const int& incidenceOfIc){
    aNewIcAppearSinceTheLastTestAndCull += incidenceOfIc;
    if (aNewIcAppearSinceTheLastTestAndCull & (dateOfOnsetOfTheFirstIcSinceTheLastTestAndCull == 0)) {
        dateOfOnsetOfTheFirstIcSinceTheLastTestAndCull = timeStep;
    }
}



void ParametersSpecificToEachHerd::resetParametersOfControlMeasures(){
    aNewIcAppearSinceTheLastTestAndCull = false;
    if (Parameters::typeOfTestAndCullStrategy == Parameters::testAndCullStrategyEachYearAfterOnsetOfIc) {
        testAndCullDate = testAndCullDate + Parameters::timestepInOneYear;
    } else {
        testAndCullDate = 0;
    }
}




