//
//  MetapopSummary.cpp
//  ParatubBetweenDairyHerd
//
//  Created by Gaël Beaunée on 13/07/2014.
//  Copyright (c) 2014 Gaël Beaunée. All rights reserved.
//

#include "MetapopSummary.h"

MetapopSummary::MetapopSummary(){
    presentSR.resize(Parameters::nbStage, 0);
    presentT.resize(Parameters::nbStage, 0);
    presentL.resize(Parameters::nbStage, 0);
    presentIs.resize(Parameters::nbStage, 0);
    presentIc.resize(Parameters::nbStage, 0);
    
    pastSR.resize(Parameters::nbStage, 0);
    pastT.resize(Parameters::nbStage, 0);
    pastL.resize(Parameters::nbStage, 0);
    pastIs.resize(Parameters::nbStage, 0);
    pastIc.resize(Parameters::nbStage, 0);
    
    presentNbHerdsWithIc = 0;
    presentNbHerdsWithMoreThanTwoIc = 0;
    
    pastNbHerdsWithIc = 0;
    pastNbHerdsWithMoreThanTwoIc = 0;
    
    vNbHerdsWithIc.resize(Parameters::simutime, 0);
    vNbHerdsWithMoreThanTwoIc.resize(Parameters::simutime, 0);
    
    NbInitiallyInfectedHerds = 0;
    NbOtherInfectedHerds = 0;
    
    vNbInitiallyInfectedHerds.resize(Parameters::simutime, 0);
    vNbOtherInfectedHerds.resize(Parameters::simutime, 0);
    
    pastInfected = 0;
    pastInfectious = 0;
    pastAffected = 0;
    pastHeadcount = 0;
    
    
    vNbHerdsWithTm.resize(Parameters::simutime, 0);
    vNbHerdsWithCu.resize(Parameters::simutime, 0);
    vNbHerdsWithH.resize(Parameters::simutime, 0);
    vNbHerdsWithCm.resize(Parameters::simutime, 0);
    vNbHerdsWithTc.resize(Parameters::simutime, 0);



    vPrevalenceIntraHerd.resize(Parameters::simutime, 0.0);
    vNbHerdInfected.resize(Parameters::simutime, 0.0);
}

void MetapopSummary::Clear(ParametersSpecificToEachRepetition& ParamRep){
    presentSR.clear(); presentSR.resize(Parameters::nbStage, 0);
    presentT.clear(); presentT.resize(Parameters::nbStage, 0);
    presentL.clear(); presentL.resize(Parameters::nbStage, 0);
    presentIs.clear(); presentIs.resize(Parameters::nbStage, 0);
    presentIc.clear(); presentIs.resize(Parameters::nbStage, 0);
    
    pastSR.clear(); pastSR.resize(Parameters::nbStage, 0);
    pastT.clear(); pastT.resize(Parameters::nbStage, 0);
    pastL.clear(); pastL.resize(Parameters::nbStage, 0);
    pastIs.clear(); pastIs.resize(Parameters::nbStage, 0);
    pastIc.clear(); pastIs.resize(Parameters::nbStage, 0);
    
    presentNbHerdsWithIc = 0;
    presentNbHerdsWithMoreThanTwoIc = 0;
    
    pastNbHerdsWithIc = 0;
    pastNbHerdsWithMoreThanTwoIc = 0;
    
    vNbHerdsWithIc.clear(); vNbHerdsWithIc.resize(Parameters::simutime, 0);
    vNbHerdsWithMoreThanTwoIc.clear(); vNbHerdsWithMoreThanTwoIc.resize(Parameters::simutime, 0);
    
    NbInitiallyInfectedHerds = 0;
    NbOtherInfectedHerds = 0;
    
    vNbInitiallyInfectedHerds.clear(); vNbInitiallyInfectedHerds.resize(Parameters::simutime, 0);
    vNbOtherInfectedHerds.clear(); vNbOtherInfectedHerds.resize(Parameters::simutime, 0);
    
    pastInfected = 0;
    pastInfectious = 0;
    pastAffected = 0;
    pastHeadcount = 0;
    
    
    vNbHerdsWithTm.clear(); vNbHerdsWithTm.resize(Parameters::simutime, 0);
    vNbHerdsWithCu.clear(); vNbHerdsWithCu.resize(Parameters::simutime, 0);
    vNbHerdsWithH.clear(); vNbHerdsWithH.resize(Parameters::simutime, 0);
    vNbHerdsWithCm.clear(); vNbHerdsWithCm.resize(Parameters::simutime, 0);
    vNbHerdsWithTc.clear(); vNbHerdsWithTc.resize(Parameters::simutime, 0);



    vPrevalenceIntraHerd.clear(); vPrevalenceIntraHerd.resize(Parameters::simutime, 0.0);
    vNbHerdInfected.clear(); vNbHerdInfected.resize(Parameters::simutime, 0.0);
}

//void MetapopSummary::Update(const DairyHerd& vHerd){
//    for (int ageAnimal=0; ageAnimal<Parameters::nbStage; ageAnimal++) {
//        presentSR[ageAnimal] += static_cast<double>(vHerd.iCompartmentSR.past[ageAnimal]);
//        presentT[ageAnimal] += static_cast<double>(vHerd.iCompartmentT.past[ageAnimal]);
//        presentL[ageAnimal] += static_cast<double>(vHerd.iCompartmentL.past[ageAnimal]);
//        presentIs[ageAnimal] += static_cast<double>(vHerd.iCompartmentIs.past[ageAnimal]);
//        presentIc[ageAnimal] += static_cast<double>(vHerd.iCompartmentIc.past[ageAnimal]);
//    }
//    if (vHerd.presenceOfIcAtThisTime) {
//        presentNbHerdsWithIc ++;
//    }
//    if (vHerd.cumulativeNumberOfIc >= 2) {
//        presentNbHerdsWithMoreThanTwoIc ++;
//    }
//    
//    if (vHerd.persistenceIndicator) {
//        if (vHerd.typeInfectedHerd == 1) {
//            NbInitiallyInfectedHerds ++;
//        } else {
//            NbOtherInfectedHerds ++;
//        }
//    }
//}




void MetapopSummary::Resume(const int& timeStep){
    
    vNbHerdsWithIc[timeStep] += presentNbHerdsWithIc;
    vNbHerdsWithMoreThanTwoIc[timeStep] += presentNbHerdsWithMoreThanTwoIc;
    
    vNbInitiallyInfectedHerds[timeStep] += NbInitiallyInfectedHerds;
    vNbOtherInfectedHerds[timeStep] += NbOtherInfectedHerds;
    
    pastSR.swap(presentSR);
    pastT.swap(presentT);
    pastL.swap(presentL);
    pastIs.swap(presentIs);
    pastIc.swap(presentIc);
        
    presentSR.clear(); presentSR.resize(Parameters::nbStage, 0);
    presentT.clear(); presentT.resize(Parameters::nbStage, 0);
    presentL.clear(); presentL.resize(Parameters::nbStage, 0);
    presentIs.clear(); presentIs.resize(Parameters::nbStage, 0);
    presentIc.clear(); presentIc.resize(Parameters::nbStage, 0);
    
    
    pastAffected = static_cast<double>(std::accumulate(pastIc.begin(), pastIc.end(), 0));
    pastInfectious = pastAffected + static_cast<double>(std::accumulate(pastIs.begin(), pastIs.end(), 0) + std::accumulate(pastT.begin(), pastT.end(), 0));
    pastInfected = pastInfectious + static_cast<double>(std::accumulate(pastL.begin(), pastL.end(), 0));
    pastHeadcount = pastInfected + static_cast<double>(std::accumulate(pastSR.begin(), pastSR.end(), 0));

    pastNbHerdsWithIc = presentNbHerdsWithIc;
    pastNbHerdsWithMoreThanTwoIc = presentNbHerdsWithMoreThanTwoIc;
    
    presentNbHerdsWithIc = 0;
    presentNbHerdsWithMoreThanTwoIc = 0;
    
    NbInitiallyInfectedHerds = 0;
    NbOtherInfectedHerds = 0;
    
}


void MetapopSummary::initialCopy(const int& timeStep){

    //    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false & Parameters::typeOfTheRiskToPurchaseInfectedAnimalOutsideTheMetapopulation == 0) {
    pastSR = presentSR;
    pastT = presentT;
    pastL = presentL;
    pastIs = presentIs;
    pastIc = presentIc;

    pastAffected = static_cast<double>(std::accumulate(pastIc.begin(), pastIc.end(), 0));
    pastInfectious = pastAffected + static_cast<double>(std::accumulate(pastIs.begin(), pastIs.end(), 0) + std::accumulate(pastT.begin(), pastT.end(), 0));
    pastInfected = pastInfectious + static_cast<double>(std::accumulate(pastL.begin(), pastL.end(), 0));
    pastHeadcount = pastInfected + static_cast<double>(std::accumulate(pastSR.begin(), pastSR.end(), 0));
    //    }

    pastNbHerdsWithIc = presentNbHerdsWithIc;
    pastNbHerdsWithMoreThanTwoIc = presentNbHerdsWithMoreThanTwoIc;

}



void MetapopSummary::updateControlStrategyCensus(const int& timeStep, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd){
    if (iParametersSpecificToEachHerd.testAtPurchaseImplementedInThisFarm) {
        vNbHerdsWithTm[timeStep] ++;
    } else if (iParametersSpecificToEachHerd.cullingImprovementImplementedInThisFarm) {
        vNbHerdsWithCu[timeStep] ++;
    } else if (iParametersSpecificToEachHerd.hygieneImprovementImplementedInThisFarm) {
        vNbHerdsWithH[timeStep] ++;
    } else if (iParametersSpecificToEachHerd.calfManagementImprovementImplementedInThisFarm) {
        vNbHerdsWithCm[timeStep] ++;
    } else if (iParametersSpecificToEachHerd.testAndCullImplementedInThisFarm) {
        vNbHerdsWithTc[timeStep] ++;
    }
}






