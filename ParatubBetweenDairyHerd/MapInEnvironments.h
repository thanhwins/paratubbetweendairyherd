//
//  MapInEnvironments.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 05/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_MapInEnvironments_h
#define ParatubBetweenDairyHerd_MapInEnvironments_h

#include <iostream>
#include <vector>
#include <gsl/gsl_randist.h>

#include "Parameters.h"
#include "HealthStateCompartmentsContainer.hpp"
#include "BirthsContainer.hpp"
#include "MapShedding.h"

class MapInEnvironments {
    // Variables ===========================================================
    int cleaningindicator1 = 0; // non weaned claf
    int cleaningindicator2 = 0; // weaned calf
    
public:
    // Variables ===========================================================
    std::vector<double> int_1;
    std::vector<double> int_2;
    std::vector<double> int_3;
    std::vector<double> int_4;
    std::vector<double> int_5;
    std::vector<double> global;
    
    std::vector<double> ext_1;
    std::vector<double> ext_2;
    std::vector<double> ext_3;
    std::vector<double> ext;
    
    std::vector<double> milk;
    std::vector<double> colostrum;
    
    // Constructor =========================================================
    MapInEnvironments();
    
    // Member functions ====================================================
    void update(gsl_rng * randomGenerator, const int& t, const int& weeknumber, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const MapShedding& QtyBacteria, const HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer, BirthsContainer& iBirthsContainer);
};

#endif
