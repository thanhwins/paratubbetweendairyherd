//
//  SummaryDynamic.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 01/08/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_SummaryDynamic_h
#define ParatubBetweenDairyHerd_SummaryDynamic_h

#include "Parameters.h"
#include "UtilitiesFunctions.h"
//#include "AgeCompartments.h"
#include "HealthStateCompartmentsContainer.hpp"

struct SummaryDynamic {
    // Constructor =========================================================
    SummaryDynamic();
    
    // Variables ===========================================================
    std::vector<int> headcount;
    std::vector<int> infected;
    std::vector<int> infectious;
    std::vector<int> affected;
    
    std::vector<double> headcountLact;
    
    bool extinct;
    
    // Member functions ====================================================    
    void update(const int& t, const HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer);
    
    void PastUpdate(const int& t, const HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer);
    
};

#endif
