//
//  ABCsmcParticleAttributes.h
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunée on 27/05/2015.
//  Copyright (c) 2015 Gaël Beaunée. All rights reserved.
//

#ifndef __ParatubBetweenDairyHerd__ABCsmcParticleAttributes__
#define __ParatubBetweenDairyHerd__ABCsmcParticleAttributes__

#include <stdio.h>

class ABCsmcParticleAttributes {
    
public:
    // Constructor =========================================================
    ABCsmcParticleAttributes();
    
    // Variables ===========================================================
    double PreviousValue;
    double PreviousWeight;
    double PreviousNormalizedWeight;
    
    double PerturbedValue;
    double PerturbedWeight;
    double PerturbedNormalizedWeight;
    
    // Member functions ====================================================
};

#endif /* defined(__ParatubBetweenDairyHerd__ABCsmcParticleAttributes__) */
