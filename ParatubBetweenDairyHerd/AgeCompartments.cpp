//
//  AgeCompartments.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 09/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#include "AgeCompartments.h"

// Constructor =========================================================
AgeCompartments::AgeCompartments(){
    // Variables ===========================================================
    past.resize(Parameters::nbStage,0);
    present.resize(Parameters::nbStage,0);
    
    calvesUnweaned.resize(Parameters::simutime,0); // SRTL
    calvesWeanedIn.resize(Parameters::simutime,0); // SRTL
    calvesWeanedOut.resize(Parameters::simutime,0); // SRTL
//    calves06.resize(Parameters::simutime,0); // SRTL
//    calves612.resize(Parameters::simutime,0); // SRTL
    yHeifer.resize(Parameters::simutime,0); // SRTL
    heifer.resize(Parameters::simutime,0); // SRTLIsIc
    lact1.resize(Parameters::simutime,0); // SRLIsIc
    lact2.resize(Parameters::simutime,0); // SRLIsIc
    lact3.resize(Parameters::simutime,0); // SRLIsIc
    lact4.resize(Parameters::simutime,0); // SRLIsIc
    lact5.resize(Parameters::simutime,0); // SRLIsIc
    lactAll.resize(Parameters::simutime,0); // SRLIsIc
    total.resize(Parameters::simutime,0); // SRTLIsIc
//    ageSusceptible.resize(Parameters::simutime,0); // SR
}

// Member functions ====================================================
void AgeCompartments::update(const int& t){
    calvesUnweaned[t] = U_functions::SumOfAnimalsByAge(present, 1, Parameters::ageWeaning);
    calvesWeanedIn[t] = U_functions::SumOfAnimalsByAge(present, Parameters::ageWeaning+1, Parameters::ageGrazing);
    calvesWeanedOut[t] = U_functions::SumOfAnimalsByAge(present, Parameters::ageGrazing+1, Parameters::ageCalf);
//    calves06[t] = U_functions::SumOfAnimalsByAge(present, 1, 26);
//    calves612[t] = U_functions::SumOfAnimalsByAge(present, 27, Parameters::ageCalf);
    yHeifer[t] = U_functions::SumOfAnimalsByAge(present, Parameters::ageCalf+1, Parameters::ageYheifer);
    heifer[t] = U_functions::SumOfAnimalsByAge(present, Parameters::ageYheifer+1, Parameters::ageFirstCalving);
    lact1[t] = present[Parameters::ageFirstCalving+0];
    lact2[t] = present[Parameters::ageFirstCalving+1];
    lact3[t] = present[Parameters::ageFirstCalving+2];
    lact4[t] = present[Parameters::ageFirstCalving+3];
    lact5[t] = present[Parameters::ageFirstCalving+4];
    lactAll[t] = lact1[t] + lact2[t] + lact3[t] + lact4[t] + lact5[t];
    total[t] = U_functions::SumOfAnimalsByAge(present, 1, Parameters::nbStage);
//    ageSusceptible[t] = U_functions::SumOfAnimalsByAge(present, 1, Parameters::durSusceptible);
    
    past.swap(present);
    present.clear();
    present.resize(Parameters::nbStage,0);
}



void AgeCompartments::PastUpdate(const int& t){
    calvesUnweaned[t-1] = U_functions::SumOfAnimalsByAge(past, 1, Parameters::ageWeaning);
    calvesWeanedIn[t-1] = U_functions::SumOfAnimalsByAge(past, Parameters::ageWeaning+1, Parameters::ageGrazing);
    calvesWeanedOut[t-1] = U_functions::SumOfAnimalsByAge(past, Parameters::ageGrazing+1, Parameters::ageCalf);
//    calves06[t-1] = U_functions::SumOfAnimalsByAge(past, 1, 26);
//    calves612[t-1] = U_functions::SumOfAnimalsByAge(past, 27, Parameters::ageCalf);
    yHeifer[t-1] = U_functions::SumOfAnimalsByAge(past, Parameters::ageCalf+1, Parameters::ageYheifer);
    heifer[t-1] = U_functions::SumOfAnimalsByAge(past, Parameters::ageYheifer+1, Parameters::ageFirstCalving);
    lact1[t-1] = past[Parameters::ageFirstCalving+0];
    lact2[t-1] = past[Parameters::ageFirstCalving+1];
    lact3[t-1] = past[Parameters::ageFirstCalving+2];
    lact4[t-1] = past[Parameters::ageFirstCalving+3];
    lact5[t-1] = past[Parameters::ageFirstCalving+4];
    lactAll[t-1] = lact1[t-1] + lact2[t-1] + lact3[t-1] + lact4[t-1] + lact5[t-1];
    total[t-1] = U_functions::SumOfAnimalsByAge(past, 1, Parameters::nbStage);
//    ageSusceptible[t-1] = U_functions::SumOfAnimalsByAge(past, 1, Parameters::durSusceptible);
}



void AgeCompartments::UpdateAfterAddingInfectiousAnimals(const int& t){
    calvesUnweaned[t] = U_functions::SumOfAnimalsByAge(past, 1, Parameters::ageWeaning);
    calvesWeanedIn[t] = U_functions::SumOfAnimalsByAge(past, Parameters::ageWeaning+1, Parameters::ageGrazing);
    calvesWeanedOut[t] = U_functions::SumOfAnimalsByAge(past, Parameters::ageGrazing+1, Parameters::ageCalf);
//    calves06[t] = U_functions::SumOfAnimalsByAge(past, 1, 26);
//    calves612[t] = U_functions::SumOfAnimalsByAge(past, 27, Parameters::ageCalf);
    yHeifer[t] = U_functions::SumOfAnimalsByAge(past, Parameters::ageCalf+1, Parameters::ageYheifer);
    heifer[t] = U_functions::SumOfAnimalsByAge(past, Parameters::ageYheifer+1, Parameters::ageFirstCalving);
    lact1[t] = past[Parameters::ageFirstCalving+0];
    lact2[t] = past[Parameters::ageFirstCalving+1];
    lact3[t] = past[Parameters::ageFirstCalving+2];
    lact4[t] = past[Parameters::ageFirstCalving+3];
    lact5[t] = past[Parameters::ageFirstCalving+4];
    lactAll[t] = lact1[t] + lact2[t] + lact3[t] + lact4[t] + lact5[t];
    total[t] = U_functions::SumOfAnimalsByAge(past, 1, Parameters::nbStage);
//    ageSusceptible[t] = U_functions::SumOfAnimalsByAge(past, 1, Parameters::durSusceptible);
}



