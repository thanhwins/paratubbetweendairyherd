//
//  AgeCompartments.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 09/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_AgeCompartments_h
#define ParatubBetweenDairyHerd_AgeCompartments_h

#include <vector>

#include "Parameters.h"
#include "UtilitiesFunctions.h"
//#include "HerdDynamic.h"

struct AgeCompartments {
    // Constructor =========================================================
    AgeCompartments();
    
    // Variables ===========================================================
    std::vector<unsigned short int> past;
    std::vector<unsigned short int> present;
    
    std::vector<unsigned short int> calvesUnweaned; // Calves unweaned
    std::vector<unsigned short int> calvesWeanedIn; // Calves weaned in
    std::vector<unsigned short int> calvesWeanedOut; // Calves weaned out
    //    std::vector<int> calves06; // Calves 0-6 months
    //    std::vector<int> calves612; // Calves 6-12 months
    std::vector<unsigned short int> yHeifer; // Young heifers
    std::vector<unsigned short int> heifer; // Heifer
    std::vector<unsigned short int> lact1; // L1
    std::vector<unsigned short int> lact2; // L2
    std::vector<unsigned short int> lact3; // L3
    std::vector<unsigned short int> lact4; // L4
    std::vector<unsigned short int> lact5; // L5+
    std::vector<unsigned short int> lactAll; // L1+
    std::vector<unsigned short int> total; // Total
    //    std::vector<int> ageSusceptible;
    
    // Member functions ====================================================    
    void update(const int& t);
    
    void PastUpdate(const int& t);
    
    void UpdateAfterAddingInfectiousAnimals(const int& t);
    
};

#endif
