//
//  Chronometer.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 07/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef Chronometer_h
#define Chronometer_h

#include <chrono>
#include <cmath>

class Chronometer {
    // Variables ===========================================================
    std::chrono::steady_clock::time_point  tbegin,tend;
    double texec=0.;
    
public:
    // Constructor =========================================================
    Chronometer();
    // Member functions ====================================================
    void start();
    void stop();
    double display(){return texec/pow(10, 9);}
    
};

#endif
