//
//  ABCsmcSetOfPreviousParticles.h
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunée on 28/05/2015.
//  Copyright (c) 2015 Gaël Beaunée. All rights reserved.
//

#ifndef __ParatubBetweenDairyHerd__ABCsmcSetOfPreviousParticles__
#define __ParatubBetweenDairyHerd__ABCsmcSetOfPreviousParticles__

#include <stdio.h>
#include <vector>

class ABCsmcSetOfPreviousParticles {
    
public:
    // Constructor =========================================================
    ABCsmcSetOfPreviousParticles();
    
    // Variables ===========================================================
    std::vector<double> previousParticleValues;
    std::vector<double> previousParticleWeights;
    std::vector<double> previousParticleNormalizedWeights;
    double sumWeights;
    double empiricalStandardDeviation;
    double ess;
    
    // Member functions ====================================================
    
};

#endif /* defined(__ParatubBetweenDairyHerd__ABCsmcSetOfPreviousParticles__) */
