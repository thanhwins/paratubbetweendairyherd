//
//  HealthStateCompartmentsContainer.hpp
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunée on 08/06/2016.
//  Copyright © 2016 Gaël Beaunée. All rights reserved.
//

#ifndef HealthStateCompartmentsContainer_hpp
#define HealthStateCompartmentsContainer_hpp

#include <stdio.h>

#include "Parameters.h"
#include "AgeCompartments.h"
#include "UtilitiesFunctions.h"

struct HealthStateCompartmentsContainer {
    // Constructor =========================================================
    HealthStateCompartmentsContainer();
    
    // Variables ===========================================================
    AgeCompartments iCompartmentSR, iCompartmentT, iCompartmentL, iCompartmentIs, iCompartmentIc;
    AgeCompartments iCompartmentSR_positiveTested, iCompartmentT_positiveTested, iCompartmentL_positiveTested, iCompartmentIs_positiveTested, iCompartmentIc_positiveTested;
    std::vector<unsigned short int> extraDeath;
    
    // Member functions ====================================================
};

#endif /* HealthStateCompartmentsContainer_hpp */
